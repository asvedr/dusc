from dataclasses import dataclass
from uuid import UUID

from pydantic import BaseModel

from .http_client import HttpClient


class DecFile(BaseModel):
    name: str
    size: int
    user_id: UUID | None
    file_id: UUID


class Message(BaseModel):
    id: int
    uuid: UUID
    user: UUID | None
    text: str
    file: DecFile | None
    read: bool
    received: bool


class Messages(BaseModel):
    data: list[Message]


@dataclass
class ClientMsgs:
    http: HttpClient

    def get_list(
        self,
        chat: str,
	    last_msg_uid: str = None,
    ) -> list[Message]:
        url = f"/api/chats/msgs?chat={chat}"
        if last_msg_uid:
            url += f"&last_msg_uid={last_msg_uid}"
        resp = self.http.get(url)
        return Messages(**resp.json()).data

    def send_text(
        self, 
        chat: str,
	    text: str,
    ) -> None:
        self.http.post(
            "/api/msgs/text/",
            json={"chat": chat, "text": text},
        )
