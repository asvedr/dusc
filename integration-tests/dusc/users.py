from dataclasses import dataclass
from uuid import UUID

from pydantic import BaseModel

from dusc.http_client import HttpClient


class UserShort(BaseModel):
    uuid: UUID
    name: str
    pic: str
    key_code: str


class UserFull(BaseModel):
    uuid: UUID
    name: str
    description: str
    pic: str
    key: str
    key_code: str
    status: str
    addrs: list[str]


class UserList(BaseModel):
    data: list[UserShort]


@dataclass
class ClientUsers:
    http: HttpClient

    def add_addr(self, user: UUID, addr: str) -> None:
        self.http.post(
            "/api/users/add-addr/",
            json={"uuid": str(user), "addr": addr},
        )

    def connect(self, name: str, contact: str) -> None:
        self.http.post(
            "/api/users/connect/",
            json={"name": name, "contact": contact},
        )

    def get_list(self) -> list[UserShort]:
        resp = self.http.get("/api/users/list/")
        return UserList(**resp.json()).data
    
    def get_details(self, user_id: UUID) -> UserFull:
        resp = self.http.get(f"/api/users/detail?id={user_id}")
        return UserFull(**resp.json())

    actions = ("ban", "del", "trust")

    def moderate(self, user_id: UUID, action: str) -> None:
        assert action in self.actions
        self.http.post(
            f"/api/users/moderate?id={user_id}&action={action}"
        )
