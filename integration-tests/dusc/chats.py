from dataclasses import dataclass
from uuid import UUID

from pydantic import BaseModel

from dusc.http_client import HttpClient


class CreateOrUpdateResponse(BaseModel):
    id: str


class Chat(BaseModel):
    id: str
    name: str
    last_msg: str
    new: bool


class ChatListResponse(BaseModel):
    data: list[Chat]


class ChatUser(BaseModel):
    uuid: UUID
    name: str
    pic: str
    key_code: str


class ChatDetails(BaseModel):
    id: str
    name: str
    participants: list[ChatUser]


@dataclass
class ClientChats:
    http: HttpClient

    def create_or_update(
        self,
        chat_id: UUID | None = None,
        users: list[UUID] = None,
        name: str | None = None,
    ) -> str:
        js = {}
        if chat_id:
            js["chat_id"] = str(chat_id)
        if users:
            js["users"] = [str(u) for u in users]
        if name:
            js["name"] = name
        resp = self.http.post("/api/chats/", json=js)
        return CreateOrUpdateResponse(**resp.json()).id

    def get_list(self) -> list[Chat]:
        resp = self.http.get("/api/chats/")
        return ChatListResponse(**resp.json()).data

    def get_details(self, chat_id: str) -> ChatDetails:
        resp = self.http.get(f"/api/chats/info?id={chat_id}")
        return ChatDetails(**resp.json())

    actions = ("ban", "del")

    def moderate(self, chat_id: str, action: str) -> None:
        assert action in self.actions
        self.http.post(f"/api/chats/moderate?id={chat_id}&action={action}")
