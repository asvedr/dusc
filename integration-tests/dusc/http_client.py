from dataclasses import dataclass
import requests


class HttpError(Exception):
    ...


@dataclass
class UnexpectedError(HttpError):
    status: int
    content: bytes

    def __str__(self) -> str:
        s = self.status
        c = self.content
        return f"UnexpectedError(status={s}, contnet={c})"


@dataclass
class DuscError(Exception):
    status: int
    code: str
    details: str

    def __str__(self) -> str:
        s = self.status
        c = self.code
        d = repr(self.details)
        return f"DuscError(status={s}, code={c}, details={d})"


class DuscNotFound(DuscError):
    ...


def create_json_error(**kwargs):
    match kwargs["status"]:
        case 404:
            return DuscNotFound(**kwargs)
        case _:
            return DuscError(**kwargs)


class HttpClient:
    def __init__(self, port: int):
        self.port = port

    def get(self, url: str, **kwargs) -> requests.Response:
        return self.request("get", url, **kwargs)

    def post(self, url: str, **kwargs) -> requests.Response:
        return self.request("post", url, **kwargs)

    def patch(self, url: str, **kwargs) -> requests.Response:
        return self.request("patch", url, **kwargs)

    def delete(self, url: str, **kwargs) -> requests.Response:
        return self.request("delete", url, **kwargs)

    def request(self, method: str, url: str, **kwargs) -> requests.Response:
        if not url.startswith("/"):
            url = "/" + url
        resp = requests.request(
            method, 
            f"http://localhost:{self.port}{url}",
            **kwargs
        )
        if resp.status_code == 200:
            return resp
        try:
            data = resp.json()
        except Exception:
            raise UnexpectedError(status=resp.status_code, content=resp.content)
        else:
            raise create_json_error(
                status=resp.status_code,
                code=data["code"],
                details=data["details"],
            )
