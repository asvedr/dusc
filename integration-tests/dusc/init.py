from uuid import UUID
from dataclasses import dataclass

from pydantic import BaseModel
from .http_client import HttpClient


class CheckInitResponse(BaseModel):
    status: str


class LoginResponse(BaseModel):
    token: str


@dataclass
class ClientInit:
    http: HttpClient

    def check(self) -> bool:
        resp = self.http.get("/api/check-init/")
        js = CheckInitResponse(**resp.json())
        res_map = {"INITED": True, "NOT_INITED": False}
        if js.status not in res_map:
            raise Exception("invalid init status: " + js.status)
        return res_map[js.status]                  

    def init(
        self,
        name: str,
        description: str,
        password: str = "",
        uuid: UUID | None = None,
        priv_key: str | None = None,
    ) -> None:
        js = {
            "name": name,
            "description": description,
            "password": password,
        }
        if uuid:
            js["uuid"] = str(uuid)
        if priv_key:
            js["priv_key"] = priv_key
        self.http.post("/api/init/", json=js)

    def login(self, pwd: str) -> str:
        resp = self.http.post("/api/login/", json={"password": pwd})
        return LoginResponse(**resp.json()).token
