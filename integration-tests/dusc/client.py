import subprocess as sp
import os

from .http_client import HttpClient
from .settings import ClientSettings
from .init import ClientInit
from .users import ClientUsers
from .msgs import ClientMsgs
from .chats import ClientChats
from .servers import ClientServers
from .common import wait_until

class Client:
    def __init__(
        self,
        exec_path: str,
        db_path: str,
        api_port: int,
        udp_port: int,
        force_my_addr: str = None,
        log_prefix: str = None,
        wrap_output: bool = False,
    ):
        http = HttpClient(api_port)
        self.exec_path = exec_path
        self.db_path = db_path
        self.api_port = api_port
        self.upd_port = udp_port
        self.force_my_addr = force_my_addr
        self.log_prefix = log_prefix
        self.wrap_output = wrap_output
        self.settings = ClientSettings(http=http)
        self.init = ClientInit(http=http)
        self.users = ClientUsers(http=http)
        self.msgs = ClientMsgs(http=http)
        self.chats = ClientChats(http=http)
        self.servers = ClientServers(http=http)
        self.proc = None
    
    def wait_until_addr_is_ready(self):
        wait_until(lambda: self.settings.get_my_addr() != "")

    def __enter__(self):
        os.environ["API_PORT"] = str(self.api_port)
        os.environ["UDP_PORT"] = str(self.upd_port)
        os.environ["STORAGE"] = self.db_path
        os.environ["FORCE_MY_ADDR"] = self.force_my_addr or ""
        os.environ["LOG_UDP"] = "1"
        os.environ["LOG_PREFIX"] = self.log_prefix or ""
        cmd = [self.exec_path, "run"]
        stdout = None
        stderr = None
        if self.wrap_output:
            stdout = sp.PIPE
            stderr = sp.PIPE
        self.proc = sp.Popen(cmd, stdout=stdout, stderr=stderr)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.proc.kill()
        self.proc.wait(2)
