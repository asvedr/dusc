import time
from typing import Callable


DEFAULT_TIMEOUT = 3


def wait_until(fun: Callable[[], bool], timeout: int = DEFAULT_TIMEOUT) -> None:
    started = time.time()
    while not fun():
        if time.time() - started > timeout:
            raise TimeoutError("wait_until: expired")
        time.sleep(0.1)
