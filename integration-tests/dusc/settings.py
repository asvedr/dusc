from uuid import UUID
from dataclasses import dataclass

from pydantic import BaseModel

from .http_client import HttpClient


class Me(BaseModel):
    uuid: UUID
    priv_key: str
    pub_key: str
    key_code: str
    name: str
    description: str
    pic: str


class MyContact(BaseModel):
    data: str


class MyAddr(BaseModel):
    addr: str


@dataclass
class ClientSettings:
    http: HttpClient

    def get_me(self) -> Me:
        resp = self.http.get("/api/settings/me/")
        return Me(**resp.json())
    
    def get_my_contact(self) -> str:
        resp = self.http.get("/api/settings/contact/")
        return MyContact(**resp.json()).data
    
    def patch_me(
        self,
        uuid: UUID | None = None,
        priv_key: str | None = None,
        name: str | None = None,
        description: str | None = None,
        pic_path: str | None = None,
    ) -> None:
        js = {
            "uuid": uuid and str(uuid),
            "priv_key": priv_key,
            "name": name,
            "description": description,
            "pic_path": pic_path,
        }
        self.http.patch(
            "/api/settings/me/",
            json={k: v for k, v in js.items() if v is not None},
        )

    def regenerate(
        self,
        uuid: bool = False,
        priv_key: bool = False,
    ) -> None:
        query = "&".join(
            f"{k}=1"
            for k, v in [("uuid", uuid), ("priv_key", priv_key)]
            if v
        )
        url = "/api/settings/regenerate"
        if query:
            url += "?" + query
        return self.http.post(url)

    def get_my_addr(self) -> str:
        resp = self.http.get("/api/settings/my-addr/")
        return MyAddr(**resp.json()).addr
