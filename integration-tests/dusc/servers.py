from dataclasses import dataclass

from pydantic import BaseModel

from dusc.http_client import HttpClient


class Server(BaseModel):
    addr: str
    status: str


class ServersResponse(BaseModel):
    data: list[Server]


@dataclass
class ClientServers:
    http: HttpClient

    def create(self, tp: str, value: str) -> None:
        self.http.post(
            "/api/servers/",
            json={"type":tp, "value":value},
        )
    
    def get(self, tp: str) -> list[int]:
        resp = self.http.get(f"/api/servers?type={tp}")
        return ServersResponse(**resp.json())
    
    def delete(self, addr: str) -> None:
        self.http.delete(f"/api/servers?addr={addr}")
