from typing import Any
import pytest

from dusc.client import Client
from dusc.consts import ALICE_PUB_KEY, BOB_PRIV_KEY, ALICE_PRIV_KEY, ALICE_UID, BOB_UID

class TestSettings:
    @pytest.fixture(autouse=True)
    def setup(self, alice: Client):
        self.client = alice

    def test_get_me(self) -> None:
        me = self.client.settings.get_me()
        assert me.priv_key == ALICE_PRIV_KEY
        assert me.pub_key == ALICE_PUB_KEY
        assert me.uuid == ALICE_UID
        assert me.name == "Alice"
        assert me.key_code
        assert me.description == "..."
        assert me.pic == ""

    def test_get_my_contact(self) -> None:
        contact = self.client.settings.get_my_contact()
        assert contact


class TestMofifyMe:
    @pytest.fixture(autouse=True)
    def setup(self, alice: Client):
        self.client = alice

    @pytest.mark.parametrize(
        "key,value,changed",
        [
            ("uuid", BOB_UID, []),
            ("priv_key", BOB_PRIV_KEY, ["pub_key", "key_code"]),
            ("name", "bob", []),
            ("description", "123", []),
        ],
    )
    def test_patch_me(
        self,
        key: str, 
        value: Any, 
        changed: list[str],
    ) -> None:
        all_params = [
            "uuid", "priv_key", "pub_key",
            "key_code", "name", "description",
        ]
        before = self.client.settings.get_me()
        self.client.settings.patch_me(**{key: value})
        after = self.client.settings.get_me()
        for param in all_params:
            if param == key:
                assert getattr(before, param) != value
                assert getattr(after, param) == value
            elif param in changed:
                assert getattr(before, param) != getattr(after, param)
            else:
                assert getattr(before, param) == getattr(after, param)

    @pytest.mark.parametrize("uuid", [True, False])
    @pytest.mark.parametrize("priv_key", [True, False])
    def test_regenerate(self, uuid: bool, priv_key: bool) -> None:
        before = self.client.settings.get_me()
        self.client.settings.regenerate(
            uuid=uuid,
            priv_key=priv_key,
        )
        after = self.client.settings.get_me()
        if uuid:
            assert before.uuid != after.uuid
        else:
            assert before.uuid == after.uuid
        if priv_key:
            assert before.priv_key != after.priv_key
            assert before.pub_key != after.pub_key
            assert before.key_code != after.key_code
        else:
            assert before.priv_key == after.priv_key
            assert before.pub_key == after.pub_key
            assert before.key_code == after.key_code
