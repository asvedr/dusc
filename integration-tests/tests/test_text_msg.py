import pytest

from dusc.client import Client
from dusc.common import wait_until


class TestPrivateMsg:
    @pytest.fixture(autouse=True)
    def setup(
        self,
        alice: Client,
        bob: Client,
        alice_and_bob_contacted,
    ) -> None:
        self.alice = alice
        self.bob = bob
        self.alice_id = alice.settings.get_me().uuid
        self.bob_id = bob.settings.get_me().uuid

    def test_send_text(self) -> None:
        chat = f"U{self.bob_id}"

        self.alice.msgs.send_text(chat, "Hello, Bob!")

        msgs = self.alice.msgs.get_list(chat)

        assert len(msgs) == 1
        msg = msgs[0]
        assert not msg.user
        assert msg.text == "Hello, Bob!"
        assert not msg.file
        assert msg.read
        assert not msg.received

        wait_until(
            lambda: self.alice.msgs.get_list(chat)[0].received,
            timeout=3,
        )

        msgs = self.bob.msgs.get_list(f"U{self.alice_id}")

        assert len(msgs) == 1
        msg = msgs[0]
        assert msg.user
        assert msg.text == "Hello, Bob!"
        assert not msg.file
        assert not msg.read
        assert msg.received
