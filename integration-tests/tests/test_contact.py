import pytest

from dusc.client import Client
from dusc.common import wait_until
from dusc.http_client import DuscNotFound


class TestContactUsers:
    @pytest.fixture(autouse=True)
    def setup(self, alice: Client, bob: Client) -> None:
        alice.wait_until_addr_is_ready()
        bob.wait_until_addr_is_ready()
        self.alice = alice
        self.bob = bob
    
    def test_contact(self):
        bob_contact = self.bob.settings.get_my_contact()
        alice_id = self.alice.settings.get_me().uuid
        bob_id = self.bob.settings.get_me().uuid
        alice_contact = self.alice.settings.get_my_contact()
        bob_addr = self.bob.settings.get_my_addr()
        assert alice_contact != bob_contact
        self.alice.users.connect("Bob", bob_contact)
        self.bob.users.connect("Alice", alice_contact)

        user = self.alice.users.get_details(bob_id)
        assert user.name == "Bob"
        assert user.addrs == []

        self.alice.users.add_addr(bob_id, bob_addr)

        def predicate() -> bool:
            user = self.alice.users.get_details(bob_id)
            return len(user.addrs) > 0

        wait_until(predicate)
        user = self.bob.users.get_details(alice_id)
        assert len(user.addrs) > 0


class TestUserMethods:
    @pytest.fixture(autouse=True)
    def setup(
        self, 
        alice: Client, 
        bob: Client,
        alice_and_bob_contacted: None,
    ) -> None:
        alice.wait_until_addr_is_ready()
        bob.wait_until_addr_is_ready()
        self.alice = alice
        self.bob = bob
        self.bob_id = bob.settings.get_me().uuid
    
    def test_get_user_list(self) -> None:
        users = self.alice.users.get_list()
        assert len(users) == 1
        bob = users[0]
        assert bob.uuid == self.bob_id
        assert bob.name == "Bob"
        assert bob.pic == ""
        assert bob.key_code
    
    def test_get_user_details(self) -> None:
        user = self.alice.users.get_details(self.bob_id)
        assert user.name == "Bob"
        assert user.description == ""
        assert user.pic == ""
        assert user.key
        assert user.key_code
        assert user.status == "new"
        assert user.addrs == ["127.0.0.1:8088"]

    def test_trust_user(self) -> None:
        self.alice.users.moderate(self.bob_id, "trust")
        user = self.alice.users.get_details(self.bob_id)
        assert user.status == "trusted"
    
    def test_del_user(self) -> None:
        self.alice.users.moderate(self.bob_id, "del")
        with pytest.raises(DuscNotFound):
            self.alice.users.get_details(self.bob_id)
        users = self.alice.users.get_list()
        assert users == []
