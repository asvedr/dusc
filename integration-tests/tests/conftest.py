import os
import time
from typing import Iterator

import pytest

from dusc.client import Client
from dusc.consts import ALICE_PRIV_KEY, ALICE_UID, BOB_PRIV_KEY, BOB_UID
from dusc.common import wait_until

EXEC_PATH = os.environ.get("EXEC_PATH", "./dusc.exe")
API_PORT = int(os.environ.get("API_PORT", 8080))
UDP_PORT = int(os.environ.get("UDP_PORT", API_PORT + 1))
ALICE_API_PORT = int(os.environ.get("ALICE_API_PORT", 8085))
ALICE_UDP_PORT = int(os.environ.get("ALICE_UDP_PORT", ALICE_API_PORT + 1))
BOB_API_PORT = int(os.environ.get("BOB_API_PORT", 8087))
BOB_UDP_PORT = int(os.environ.get("BOB_UDP_PORT", BOB_API_PORT + 1))


@pytest.fixture(scope="class")
def clean_client() -> Iterator[Client]:
    with Client(
        exec_path=EXEC_PATH,
        db_path=":memory:",
        api_port=API_PORT,
        udp_port=UDP_PORT,
    ) as client:
        time.sleep(1)
        yield client


@pytest.fixture(scope="class")
def alice() -> Iterator[Client]:
    with Client(
        exec_path=EXEC_PATH,
        db_path=":memory:",
        api_port=ALICE_API_PORT,
        udp_port=ALICE_UDP_PORT,
        force_my_addr=f"127.0.0.1:{ALICE_UDP_PORT}",
        log_prefix="Alice ",
    ) as client:
        time.sleep(1)
        client.init.init(
            name="Alice",
            description="...",
            password="",
            uuid=ALICE_UID,
            priv_key=ALICE_PRIV_KEY,
        )
        yield client

@pytest.fixture(scope="class")
def bob() -> Iterator[Client]:
    with Client(
        exec_path=EXEC_PATH,
        db_path=":memory:",
        api_port=BOB_API_PORT,
        udp_port=BOB_UDP_PORT,
        force_my_addr=f"127.0.0.1:{BOB_UDP_PORT}",
        log_prefix="Bob   ",
    ) as client:
        time.sleep(1)
        client.init.init(
            name="Bob",
            description="...",
            password="",
            uuid=BOB_UID,
            priv_key=BOB_PRIV_KEY,
        )
        yield client

@pytest.fixture(scope="class")
def alice_and_bob_contacted(alice: Client, bob: Client) -> None:
    a_get_addr = alice.settings.get_my_addr
    b_get_addr = bob.settings.get_my_addr
    wait_until(lambda: a_get_addr() != "" and b_get_addr != "")
    alice.users.connect("Bob", bob.settings.get_my_contact())
    bob.users.connect("Alice", alice.settings.get_my_contact())

    bob_id = bob.settings.get_me().uuid
    # alice_id = alice.settings.get_me().uuid
    bob_addr = bob.settings.get_my_addr()

    alice.users.add_addr(bob_id, bob_addr)

    alice.users.add_addr(bob_id, bob_addr)

    def predicate() -> bool:
        user = alice.users.get_details(bob_id)
        return len(user.addrs) > 0

    wait_until(predicate)
