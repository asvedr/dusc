import pytest

from dusc.client import Client
from dusc.common import wait_until


class TestChatForTwo:
    @pytest.fixture(autouse=True)
    def setup(
        self,
        alice: Client,
        bob: Client,
        alice_and_bob_contacted,
    ) -> None:
        self.alice = alice
        self.bob = bob
        self.alice_id = alice.settings.get_me().uuid
        self.bob_id = bob.settings.get_me().uuid
    
    def test_create_chat(self) -> None:
        chats = self.alice.chats.get_list()
        assert len(chats) == 1
        assert chats[0].id == f"U{self.bob_id}"
        chats = self.bob.chats.get_list()
        assert len(chats) == 1
        assert chats[0].id == f"U{self.alice_id}"

        chat_id = self.alice.chats.create_or_update(
            users=[self.bob_id],
            name="You can stay under my umbrella",
        )
        assert chat_id[0] == "G"

        chats = self.alice.chats.get_list()
        assert len(chats) == 2
        assert chat_id in [chats[1].id, chats[0].id]

        wait_until(
            lambda: len(self.bob.chats.get_list()) == 2,
        )

        chats = self.bob.chats.get_list()
        assert chat_id in [chats[1].id, chats[0].id]
