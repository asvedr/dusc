import uuid

import pytest
from dusc.client import Client

class TestInit:
    @pytest.fixture(autouse=True)
    def setup(self, clean_client: Client):
        self.client = clean_client

    def test_check_init(self) -> None:
        uid = uuid.uuid4()
        assert not self.client.init.check()
        self.client.init.init(
            name="alex",
            description="...",
            password="",
            uuid=uid,
        )
        assert self.client.init.check()
        me = self.client.settings.get_me()
        assert me.name == "alex"
        assert me.description == "..."
        assert me.uuid == uid
