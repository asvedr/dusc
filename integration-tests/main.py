import sys
from typing import Callable

from dusc.client import Client
import scripts


def prepare_list(*args) -> dict[str, Callable[[list[str]], None]]:
    return {
        arg.__name__.split(".")[1]: arg.main 
        for arg in args
    }


script_list = prepare_list(
    scripts.send_hello,
    scripts.receive_hello,
)


def main():
    scripts.send_hello
    if len(sys.argv) == 1:
        print("scripts: " + ", ".join(script_list.keys()))
        return
    try:
        cmd = script_list[sys.argv[1]]
    except IndexError:
        print("invalid command")
        return
    cmd(sys.argv[2:])


if __name__ == '__main__':
    main()
