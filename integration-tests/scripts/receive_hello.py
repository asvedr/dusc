import os
import time
from uuid import UUID

from dusc.client import Client
from dusc.common import wait_until
from dusc.consts import BOB_UID, BOB_PRIV_KEY, ALICE_CONTACT


EXEC_PATH = os.environ.get("EXEC_PATH", "./dusc.exe")
STUN_SERVER = os.environ.get("STUN")
PORT = int(os.environ.get("PORT"))
TIMEOUT = int(os.environ.get("TIMEOUT", 5))


def action(client: Client):
    if not STUN_SERVER:
        print("stun server not set")
        return
    time.sleep(1.0)
    client.init.init(
        name="Alice",
        description="",
        uuid=BOB_UID,
        priv_key=BOB_PRIV_KEY,
    )
    
    client.servers.create("stun", STUN_SERVER)

    print("WAITING FOR GETTING ADDR...")

    wait_until(
        lambda: client.settings.get_my_addr() != "",
        timeout=TIMEOUT,
    )

    addr = client.settings.get_my_addr()
    split = addr.split(':')
    split[-1] = str(client.upd_port)
    print(f"MY ADDR: {addr} OR {':'.join(split)}")
    print(f"MY ADDR: {client.settings.get_my_addr()}")
    # print(f"MY CONTACT: {client.settings.get_my_contact()}")

    # cnt = input("input receiver contact: ")
    cnt = ALICE_CONTACT
    client.users.connect("Receiver", cnt.strip())
    chats = client.chats.get_list()
    chat = chats[0].id
    user = UUID(chat[1:])

    input("press enter if ping is sent")

    wait_until(
        lambda: bool(client.users.get_details(user).addrs),
        timeout=TIMEOUT,
    )

    print("PING RECEIVED")
    print("WAITING FOR MSG")
    
    wait_until(
        lambda: bool(client.msgs.get_list(chat)),
        timeout=TIMEOUT,
    )

    msg = client.msgs.get_list(chat)[0]
    print("GOT MSG: " + msg.text)
    print("SUCCESS")


def main(_: list[str]) -> None:
    api_port = PORT
    udp_port = api_port + 1
    client = Client(
        exec_path=EXEC_PATH,
        db_path=":memory:",
        api_port=api_port,
        udp_port=udp_port,
        log_prefix="sender ",
        wrap_output=True,
    )
    try:
        with client:
            action(client)
    except:
        stdout = client.proc.stdout.read()
        print("--- STDOUT ---")
        print(stdout.decode())
        stderr = client.proc.stderr.read()
        print("--- STDERR ---")
        print(stderr.decode())
        raise
