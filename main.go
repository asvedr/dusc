package main

import (
	"dusc/src/entrypoints/check_stun"
	"dusc/src/entrypoints/config"
	"dusc/src/entrypoints/run"

	"gitlab.com/asvedr/cldi/cl"
)

func main() {
	cl.NewRunnerMulty(
		"",
		"Decentalized Udp Secure Chat",
		config.EP{},
		run.EP{},
		check_stun.EP{},
	).Run()
}
