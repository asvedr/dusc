//go:build !gitlab
// +build !gitlab

package udp_test

import (
	"dusc/src/app/serializers"
	"dusc/src/entities/busmsg"
	"dusc/src/entities/config"
	"dusc/src/factories"
	"dusc/src/impls/net/datagram"
	"dusc/src/impls/net/udp"
	"dusc/src/proto"
	"errors"
	"strings"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

type Setup struct {
	stop     error
	sender   proto.IBusSender
	receiver proto.IBusReceiver
}

func new_setup() Setup {
	config := &config.Config{
		Udp_Port:             8012,
		Udp_Max_Msg_Lifetime: 10,
		Udp_Datagram_Len:     30,
		Log_Udp:              true,
	}
	return Setup{
		stop: errors.New("stop"),
		sender: udp.NewSender(
			config,
			serializers.Serde.Get(),
			serializers.MsgPackedSerializer.Get(),
			datagram.NewSeparator(config),
		),
		receiver: udp.NewReceiver(
			config,
			serializers.Serde.Get(),
			serializers.MsgPackedSerializer.Get(),
			datagram.NewCompiler(config, &factories.Time{}),
		),
	}
}

func TestSendRecvShortMsg(t *testing.T) {
	setup := new_setup()
	var wg sync.WaitGroup
	wg.Add(1)
	var received busmsg.Packed
	setup.receiver.SetHandler(func(msg busmsg.Packed) error {
		received = msg
		return setup.stop
	})
	go func() {
		defer wg.Done()
		setup.receiver.Listen()
	}()
	msg := busmsg.Packed{
		Encrypted: []byte{1, 2, 3, 4, 5, 6, 7, 8},
		HashSum:   []byte{5, 4, 3, 2, 1},
	}
	cnt, err := setup.sender.Send("127.0.0.1:8012", msg)
	assert.Nil(t, err)
	assert.Equal(t, cnt, 1)
	wg.Wait()
	assert.Equal(t, msg, received)
}

func TestSendRecvLongMsg(t *testing.T) {
	setup := new_setup()
	var wg sync.WaitGroup
	wg.Add(1)
	var received busmsg.Packed
	setup.receiver.SetHandler(func(msg busmsg.Packed) error {
		received = msg
		return setup.stop
	})
	go func() {
		defer wg.Done()
		setup.receiver.Listen()
	}()
	msg := busmsg.Packed{
		Encrypted: []byte(strings.Repeat("msg", 5)),
		HashSum:   []byte{5, 4, 3, 2, 1},
	}
	cnt, err := setup.sender.Send("127.0.0.1:8012", msg)
	assert.Nil(t, err)
	assert.Equal(t, 2, cnt)
	wg.Wait()
	assert.Equal(t, msg, received)
}
