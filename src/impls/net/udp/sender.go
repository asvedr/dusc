package udp

import (
	"dusc/src/entities/busmsg"
	"dusc/src/entities/config"
	"dusc/src/proto"
	"dusc/src/utils"
	"log"
	"net"
)

type sender struct {
	serde      proto.ISerdeMaker
	serializer proto.ISerializer[busmsg.Packed]
	separator  proto.IDatagramSeparator

	log_bytes bool
}

func NewSender(
	config *config.Config,
	serde proto.ISerdeMaker,
	serializer proto.ISerializer[busmsg.Packed],
	separator proto.IDatagramSeparator,
) proto.IBusSender {
	return &sender{
		serde:      serde,
		serializer: serializer,
		separator:  separator,
		log_bytes:  config.Log_Udp,
	}
}

func (self *sender) Send(addr string, msg busmsg.Packed) (int, error) {
	udpAddr, err := net.ResolveUDPAddr("udp", addr)
	if err != nil {
		return 0, err
	}
	conn, err := net.DialUDP("udp", nil, udpAddr)
	if err != nil {
		return 0, err
	}
	defer conn.Close()
	bts := utils.DumpBts(self.serde, self.serializer, msg)
	chunks := self.separator.Split(bts)
	chunks_len := len(chunks)
	for _, chunk := range chunks {
		written, err := conn.Write(chunk)
		if self.log_bytes {
			log.Printf(
				"udp_sender: msg_len: %d, sent: %d",
				chunks_len,
				written,
			)
		}
		if err != nil {
			return 0, err
		}
	}
	return chunks_len, nil
}
