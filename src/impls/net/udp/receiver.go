package udp

import (
	"dusc/src/entities/busmsg"
	"dusc/src/entities/config"
	"dusc/src/proto"
	"fmt"
	"log"
	"net"
)

type receiver struct {
	serde      proto.ISerdeMaker
	serializer proto.ISerializer[busmsg.Packed]
	compiler   proto.IDatagramCompiler

	port      int
	log_bytes bool
	buf_size  int

	handler func(busmsg.Packed) error
}

func NewReceiver(
	config *config.Config,
	serde proto.ISerdeMaker,
	serializer proto.ISerializer[busmsg.Packed],
	compiler proto.IDatagramCompiler,
) proto.IBusReceiver {
	return &receiver{
		serde:      serde,
		serializer: serializer,
		port:       config.Udp_Port,
		log_bytes:  config.Log_Udp,
		compiler:   compiler,
		buf_size:   config.Udp_Datagram_Len,
	}
}

func (self *receiver) SetHandler(f func(busmsg.Packed) error) {
	self.handler = f
}

func (self *receiver) Listen() error {
	if self.handler == nil {
		panic("UPD handler not set")
	}
	handler := self.handler
	my_addr := fmt.Sprintf("0.0.0.0:%d", self.port)
	udpAddr, err := net.ResolveUDPAddr("udp", my_addr)
	if err != nil {
		return err
	}
	conn, err := net.ListenUDP("udp", udpAddr)
	if err != nil {
		return err
	}
	defer conn.Close()
	buf := make([]byte, self.buf_size)
	for {
		count, addr, err := conn.ReadFromUDP(buf[:])
		if err != nil {
			log.Printf("udp receiver: failed. Addr=%v. Err=%v", addr, err)
			continue
		}
		if count == 0 {
			log.Printf("udp receiver: empty msg. Addr=%v", addr)
		}
		if self.log_bytes {
			log.Printf("udp receiver: received msg len: %d", count)
		}
		msg_bts, err := self.compiler.Add(buf[:count])
		if err != nil {
			log.Printf("udp receiver: can not collect msg")
			continue
		}
		if msg_bts == nil {
			continue
		}
		if self.log_bytes {
			log.Printf("udp receiver: combined msg len: %d", len(msg_bts))
		}
		msg, err := self.serializer.Read(self.serde.NewReader(msg_bts))
		if err != nil {
			log.Printf("udp receiver: can not deserialize: %v", err)
			continue
		}
		err = handler(msg)
		if err != nil {
			return err
		}
	}
}
