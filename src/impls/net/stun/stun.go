package stun

import (
	"dusc/src/entities/errs"
	"dusc/src/proto"
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/ccding/go-stun/stun"
)

type factory struct{}
type client struct {
	host string
	port int
}

func New() proto.IFactory[proto.ISTUNClient] {
	return factory{}
}

func (factory) Make(addr string) (proto.ISTUNClient, error) {
	split := strings.Split(addr, ":")
	if len(split) != 2 {
		return nil, errs.InvalidServer{Key: "STUN", Addr: addr}
	}
	port, err := strconv.ParseInt(split[1], 10, 16)
	client := client{
		host: split[0],
		port: int(port),
	}
	return client, err
}

func (self client) GetMe() (string, error) {
	native := stun.NewClient()
	native.SetServerHost(self.host, self.port)
	native.SetVerbose(true)
	nat, host, err := native.Discover()
	if err != nil {
		return "", err
	}
	switch nat {
	case stun.NATError:
	case stun.NATUnknown:
	case stun.NATBlocked:
	case stun.NATPortRestricted:
		return "", errors.New("Invalid NAT status: " + nat.String())
	}
	log.Printf("NAT status: %v", nat)
	return finalize_host(host), nil
}

func finalize_host(host *stun.Host) string {
	if host.Family() == 1 {
		// v4
		return fmt.Sprintf("%s:%d", host.IP(), host.Port())
	} else {
		// v6
		return fmt.Sprintf("[%s]:%d", host.IP(), host.Port())
	}
}
