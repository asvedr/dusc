package datagram

import (
	"dusc/src/entities/config"
	"dusc/src/proto"
	"dusc/src/utils"
	"math/rand"
)

type separator struct {
	max_datagram_len int
	max_data_len     int
}

func NewSeparator(
	config *config.Config,
) proto.IDatagramSeparator {
	hdr_len := len(Datagram{}.Header())
	return &separator{
		max_datagram_len: config.Udp_Datagram_Len,
		max_data_len:     config.Udp_Datagram_Len - hdr_len,
	}
}

func (self separator) Split(msg []byte) [][]byte {
	var result [][]byte
	msg_id := rand.Uint32()
	chunks := utils.Chunkify(msg, self.max_data_len)
	max_chunks := len(chunks)
	for i, body := range chunks {
		dtg := Datagram{
			MsgId:     msg_id,
			Index:     uint8(i),
			MaxChunks: uint8(max_chunks),
			Body:      body,
		}
		result = append(result, dtg.ToBytes())
	}
	return result
}
