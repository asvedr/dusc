package datagram

import (
	"dusc/src/entities/config"
	"dusc/src/proto"
	"sync"
)

type compiler struct {
	time         proto.ITime
	raw_msgs     map[uint32]*msg
	mtx          sync.Mutex
	max_lifetime uint64
}

type msg struct {
	required  map[int]bool
	datagrams [][]byte
	updated   uint64
}

func NewCompiler(
	config *config.Config,
	time proto.ITime,
) proto.IDatagramCompiler {
	return &compiler{
		time:         time,
		raw_msgs:     map[uint32]*msg{},
		max_lifetime: uint64(config.Udp_Max_Msg_Lifetime),
	}
}

func (self *compiler) Add(bts []byte) ([]byte, error) {
	var dtg Datagram
	err := dtg.Parse(bts)
	if err != nil {
		return nil, err
	}
	if dtg.MaxChunks == 1 {
		return dtg.Body, nil
	}
	now := self.time.Now()
	self.mtx.Lock()
	defer self.mtx.Unlock()
	msg := self.register_dtg(dtg, now)
	var result []byte
	if len(msg.required) == 0 {
		result = msg.concat()
	}
	self.flush(now)
	return result, nil
}

func (self *compiler) register_dtg(dtg Datagram, now uint64) *msg {
	msg := self.raw_msgs[dtg.MsgId]
	if msg == nil {
		msg = init_msg(int(dtg.MaxChunks))
		self.raw_msgs[dtg.MsgId] = msg
	}
	msg.datagrams[dtg.Index] = dtg.Body
	msg.updated = now
	delete(msg.required, int(dtg.Index))
	return msg
}

func (self *compiler) flush(now uint64) {
	to_del := []uint32{}
	for key, msg := range self.raw_msgs {
		if msg.updated+self.max_lifetime < now {
			to_del = append(to_del, key)
		}
	}
	for _, key := range to_del {
		delete(self.raw_msgs, key)
	}
}

func init_msg(max_chunks int) *msg {
	required := map[int]bool{}
	for i := range max_chunks {
		required[i] = true
	}
	return &msg{
		required:  required,
		datagrams: make([][]byte, max_chunks),
	}
}

func (self *msg) concat() []byte {
	var result []byte
	for _, bts := range self.datagrams {
		result = append(result, bts...)
	}
	return result
}
