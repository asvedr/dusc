package datagram

import (
	"encoding/binary"
	"errors"
)

type Datagram struct {
	MsgId     uint32
	Index     uint8
	MaxChunks uint8
	Body      []byte
}

func (self Datagram) Header() []byte {
	bts := binary.LittleEndian.AppendUint32(nil, self.MsgId)
	bts = append(bts, self.Index, self.MaxChunks)
	return bts
}

func (self Datagram) ToBytes() []byte {
	hdr := self.Header()
	return append(hdr, self.Body...)
}

func (self *Datagram) Parse(src []byte) error {
	if len(src) < len(self.Header()) {
		return errors.New("too short")
	}
	self.MsgId = binary.LittleEndian.Uint32(src)
	self.Index = src[4]
	self.MaxChunks = src[5]
	self.Body = append([]byte{}, src[6:]...)
	return nil
}
