package datagram_test

import (
	"dusc/src/entities/config"
	"dusc/src/factories"
	"dusc/src/impls/net/datagram"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDatagramShortMsg(t *testing.T) {
	conf := &config.Config{Udp_Datagram_Len: 10}
	separator := datagram.NewSeparator(conf)
	compiler := datagram.NewCompiler(conf, &factories.Time{})

	datagrams := separator.Split([]byte("abc"))
	assert.Equal(t, 1, len(datagrams))

	msg, err := compiler.Add(datagrams[0])
	assert.Nil(t, err)
	assert.Equal(t, []byte("abc"), msg)
}

func TestDatagramLongMsg(t *testing.T) {
	conf := &config.Config{Udp_Datagram_Len: 10}
	separator := datagram.NewSeparator(conf)
	compiler := datagram.NewCompiler(conf, &factories.Time{})

	orig_msg := []byte("123456789 10")

	datagrams := separator.Split(orig_msg)
	assert.Equal(t, 3, len(datagrams))

	msg, err := compiler.Add(datagrams[0])
	assert.Nil(t, err)
	assert.Nil(t, msg)

	msg, err = compiler.Add(datagrams[1])
	assert.Nil(t, err)
	assert.Nil(t, msg)

	msg, err = compiler.Add(datagrams[2])
	assert.Nil(t, err)
	assert.Equal(t, orig_msg, msg)
}
