package redis

import (
	"context"
	"dusc/src/entities/config"
	"dusc/src/entities/errs"
	"dusc/src/proto"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/google/uuid"
)

type storage struct {
	db     *redis.Client
	expire time.Duration
}

type parsed_addr struct {
	addr string
	db   int
	user string
	pwd  string
}

var ctx = context.Background()

func New(
	config *config.Config,
	addr string,
) (proto.IExtStorage, error) {
	parsed := parse_addr(addr)
	if parsed == nil {
		return nil, errs.InvalidServer{
			Key:  "redis",
			Addr: addr,
		}
	}
	db := redis.NewClient(&redis.Options{
		Addr:     parsed.addr,
		Password: parsed.pwd,
		Username: parsed.user,
		DB:       parsed.db,
	})
	res := storage{
		db:     db,
		expire: time.Second * time.Duration(config.Redis_Lifetime_Sec),
	}
	return res, nil
}

func (self storage) PublushMe(uid uuid.UUID, addr string) error {
	res := self.db.Set(
		ctx,
		uid.String(),
		addr,
		self.expire,
	)
	return res.Err()
}

func (self storage) FetchUsers(uids []uuid.UUID) (map[uuid.UUID][]string, error) {
	res := map[uuid.UUID][]string{}
	for _, uid := range uids {
		addr, err := self.db.Get(ctx, uid.String()).Result()
		if err != nil {
			log.Printf("Can not get %s: %v", uid.String(), err)
			continue
		}
		res[uid] = []string{addr}
	}
	return res, nil
}

// user:pwd@host:port/db
func parse_addr(src string) *parsed_addr {
	split := strings.Split(src, "@")
	if len(split) != 2 {
		return nil
	}
	userpwd := strings.Split(split[0], ":")
	if len(userpwd) != 2 {
		return nil
	}
	user := userpwd[0]
	pwd := userpwd[1]
	addrdb := strings.Split(split[1], "/")
	if len(addrdb) != 2 {
		return nil
	}
	addr := addrdb[0]
	db, err := strconv.ParseInt(addrdb[1], 10, 32)
	if err != nil {
		return nil
	}
	parsed := parsed_addr{
		addr: addr,
		db:   int(db),
		pwd:  pwd,
		user: user,
	}
	return &parsed
}
