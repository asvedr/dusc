package ext_storages

import (
	"dusc/src/entities/config"
	"dusc/src/entities/errs"
	"dusc/src/impls/ext_storages/redis"
	"dusc/src/proto"
	"strings"
)

type factory struct {
	config *config.Config
}

var prefix_map = map[string]func(*config.Config, string) (proto.IExtStorage, error){
	"r": redis.New,
}

func Factory(config *config.Config) proto.IFactory[proto.IExtStorage] {
	return factory{config: config}
}

func (self factory) Make(addr string) (proto.IExtStorage, error) {
	for prefix, fun := range prefix_map {
		if strings.HasPrefix(addr, prefix) {
			addr = strings.TrimPrefix(addr, prefix)
			return fun(self.config, addr)
		}
	}
	return nil, errs.InvalidServer{Key: "unknown", Addr: addr}
}
