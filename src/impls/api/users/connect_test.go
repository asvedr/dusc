package users_test

import (
	"dusc/src/app/serializers"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestConnectBadRequest(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Db.Settings.SetMe(factories.Me.Get())
	resp := srv.Call(
		factories.ApiRequest{
			Method: "POST",
			Url:    "/api/users/connect/",
			Json:   map[string]any{"contact": "abcdef"},
		},
	)
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, `{"code":"INVALID_BODY","details":"invalid contact"}`, string(resp.Body))
}

func TestConnectOk(t *testing.T) {
	ser := serializers.ContactStrSerializer.Get()
	uid, _ := uuid.NewRandom()
	cnt := local.ClientContact{
		PubKey: &factories.PrivKey2.Get().PublicKey,
		Uid:    uid,
	}
	cnt_str := ser.Write(cnt)

	srv := factories.NewApiServer()
	srv.Db.Settings.SetMe(factories.Me.Get())
	resp := srv.Call(
		factories.ApiRequest{
			Method: "POST",
			Url:    "/api/users/connect/",
			Json:   map[string]any{"contact": cnt_str},
		},
	)
	assert.Equal(t, 200, resp.Status)
	assert.Equal(t, `{"status": "ok"}`, string(resp.Body))
	usrs, _ := srv.Db.User.GetByUid(uid)
	assert.Equal(t, 1, len(usrs))
	assert.Equal(t, cnt.PubKey, usrs[0].PubKey)
}
