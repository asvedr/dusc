package users_test

import (
	"dusc/src/factories"
	"dusc/src/impls/api/users"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetListEmpty(t *testing.T) {
	srv := factories.NewApiServer()
	resp := srv.Call(factories.ApiRequest{
		Url: "/api/users/list/",
	})
	assert.Equal(t, 200, resp.Status)
	exp := users.GetUserListResponse{Data: []users.UserShort{}}
	got := users.GetUserListResponse{}
	resp.ParseJson(&got)
	assert.Equal(t, exp, got)
}

func TestGetListOne(t *testing.T) {
	srv := factories.NewApiServer()
	me := factories.Me.Get()
	srv.Db.User.Contact("alex", me.Uid, &me.PrivKey.PublicKey)
	srv.Crypto.GetKeyCodeResp = []string{"kc"}

	resp := srv.Call(factories.ApiRequest{
		Url: "/api/users/list/",
	})
	assert.Equal(t, 200, resp.Status)
	exp := users.GetUserListResponse{Data: []users.UserShort{
		{Uuid: me.Uid, Name: "alex", KeyCode: "kc"},
	}}
	got := users.GetUserListResponse{}
	resp.ParseJson(&got)
	assert.Equal(t, exp, got)
}
