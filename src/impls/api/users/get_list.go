package users

import (
	"dusc/src/entities/api"
	"dusc/src/entities/local"
	"dusc/src/proto"

	"github.com/google/uuid"
)

type GetUserListHandler struct {
	locker proto.IDbLocker
	users  proto.IUserRepo
	crypto proto.ICrypto
}

type UserShort struct {
	Uuid    uuid.UUID `json:"uuid"`
	Name    string    `json:"name"`
	Pic     string    `json:"pic"`
	KeyCode string    `json:"key_code"`
}

type GetUserListResponse struct {
	Data []UserShort `json:"data"`
}

func NewGetUserList(
	locker proto.IDbLocker,
	users proto.IUserRepo,
	crypto proto.ICrypto,
) proto.IApiHandler {
	return &GetUserListHandler{
		locker: locker,
		users:  users,
		crypto: crypto,
	}
}

func (self *GetUserListHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/users/list/",
		AuthTokenRequired: true,
	}
}

func (self *GetUserListHandler) Handle(a_query any, body any) (any, error) {
	var err error
	var uids []uuid.UUID
	var users []local.User
	self.locker.Lock(func() {
		uids, err = self.users.GetUids()
		if err != nil {
			return
		}
		users, err = self.users.GetByUid(uids...)
	})
	if err != nil {
		return nil, err
	}
	result := []UserShort{}
	for _, user := range users {
		short := UserShort{
			Uuid:    user.Id,
			Name:    user.Info.Name,
			Pic:     user.Info.Photo,
			KeyCode: self.crypto.GetKeyCode(user.PubKey),
		}
		result = append(result, short)
	}
	return GetUserListResponse{Data: result}, nil
}
