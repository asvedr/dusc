package users

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/errs"
	"dusc/src/proto"

	"github.com/google/uuid"
)

type AddAddrHandler struct {
	locker   proto.IDbLocker
	addr_man proto.IAddrManager
}

type AddAddrBody struct {
	Uuid uuid.UUID `json:"uuid"`
	Addr string    `json:"addr"`
}

func NewAddAddr(
	locker proto.IDbLocker,
	addr_man proto.IAddrManager,
) proto.IApiHandler {
	return &AddAddrHandler{locker: locker, addr_man: addr_man}
}

func (self *AddAddrHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/users/add-addr/",
		Method:            "POST",
		AuthTokenRequired: true,
		BodyParserFactory: &AddAddrBody{},
	}
}

func (self *AddAddrHandler) Handle(_ any, a_body any) (any, error) {
	body := a_body.(*AddAddrBody)
	var err error
	self.locker.Lock(func() {
		err = self.addr_man.PingUser(body.Uuid, body.Addr)
	})
	if err != nil {
		err = dispatch_err(err)
	}
	return nil, err
}

func dispatch_err(err error) error {
	switch err.(type) {
	case errs.MyAddrIsUnknown:
		return api_err.MyAddrIsUnknown
	case errs.NotFound:
		return api_err.NotFound("user not found")
	default:
		return err
	}
}
