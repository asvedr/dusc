package users_test

import (
	"dusc/src/factories"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestAddAddrOk(t *testing.T) {
	srv := factories.NewApiServer()
	uid, _ := uuid.NewRandom()
	srv.AddrManager.PingUserResp = []error{nil}

	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/users/add-addr/",
		Json: map[string]any{
			"uuid": uid,
			"addr": "127.0.0.1:123",
		},
	})
	assert.Equal(t, 200, resp.Status)
	assert.Equal(
		t,
		[]factories.PingUserCall{{
			Uid:  uid,
			Addr: "127.0.0.1:123",
		}},
		srv.AddrManager.PingUserCalls,
	)
}
