package users

import (
	"crypto/rsa"
	"dusc/src/entities/local"
	"dusc/src/proto"
)

func MakeHandlers(
	locker proto.IDbLocker,
	users proto.IUserRepo,
	chats proto.IChatRepo,
	crypto proto.ICrypto,
	cnt_ser proto.IStrSerializer[local.ClientContact],
	key_ser proto.IStrSerializer[*rsa.PublicKey],
	addr_man proto.IAddrManager,
) []proto.IApiHandler {
	return []proto.IApiHandler{
		NewConnectUser(locker, users, chats, cnt_ser),
		NewGetUserDetails(locker, users, key_ser, crypto),
		NewGetUserList(locker, users, crypto),
		NewModerateUser(locker, users, chats),
		NewAddAddr(locker, addr_man),
	}
}
