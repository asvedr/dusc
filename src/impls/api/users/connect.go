package users

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"

	"github.com/google/uuid"
)

type ConnectUserHandler struct {
	locker proto.IDbLocker
	users  proto.IUserRepo
	chats  proto.IChatRepo
	ser    proto.IStrSerializer[local.ClientContact]
}

type ConnectUserBody struct {
	Name    *string `json:"name"`
	Contact string  `json:"contact"`
}

func NewConnectUser(
	locker proto.IDbLocker,
	users proto.IUserRepo,
	chats proto.IChatRepo,
	ser proto.IStrSerializer[local.ClientContact],
) proto.IApiHandler {
	return &ConnectUserHandler{
		locker: locker,
		users:  users,
		chats:  chats,
		ser:    ser,
	}
}

func (self *ConnectUserHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/users/connect/",
		Method:            "POST",
		BodyParserFactory: &ConnectUserBody{},
		AuthTokenRequired: true,
	}
}

var err_inv_contact = errors.New("invalid contact")

func (self *ConnectUserHandler) Handle(query any, a_body any) (any, error) {
	body := a_body.(*ConnectUserBody)
	contact, err := self.ser.Read(body.Contact)
	if err != nil {
		return nil, api_err.InvalidBody(err_inv_contact)
	}
	var name string
	if body.Name == nil {
		name = contact.Uid.String()
	} else {
		name = *body.Name
	}
	self.locker.Lock(func() {
		err = self.users.Contact(
			name,
			contact.Uid,
			contact.PubKey,
		)
		if err != nil {
			return
		}
		err = self.chats.Create(local.Chat{
			Id:    local.ChatId{Id: contact.Uid},
			Users: []uuid.UUID{contact.Uid},
		})
		if err != nil {
			return
		}
	})
	return nil, err
}
