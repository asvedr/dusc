package users

import (
	"crypto/rsa"
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/local"
	"dusc/src/impls/api/common"
	"dusc/src/proto"

	"github.com/google/uuid"
	"gitlab.com/asvedr/cldi/cl"
)

type GetUserDetailsHandler struct {
	locker  proto.IDbLocker
	users   proto.IUserRepo
	key_ser proto.IStrSerializer[*rsa.PublicKey]
	crypto  proto.ICrypto
}

type GetUserDetailsQuery struct {
	Id uuid.UUID `name:"id"`
}

type GetUserDetailsResponse struct {
	Uuid        uuid.UUID `json:"uuid"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Pic         string    `json:"pic"`
	Key         string    `json:"key"`
	KeyCode     string    `json:"key_code"`
	Status      string    `json:"status"`
	Addrs       []string  `json:"addrs"`
}

func NewGetUserDetails(
	locker proto.IDbLocker,
	users proto.IUserRepo,
	key_ser proto.IStrSerializer[*rsa.PublicKey],
	crypto proto.ICrypto,
) proto.IApiHandler {
	return &GetUserDetailsHandler{
		locker:  locker,
		users:   users,
		key_ser: key_ser,
		crypto:  crypto,
	}
}

func (self *GetUserDetailsHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path: "/api/users/detail/",
		QueryParser: cl.MakeQueryParser[GetUserDetailsQuery](
			common.QParserCtx,
		),
		AuthTokenRequired: true,
	}
}

func (self *GetUserDetailsHandler) Handle(a_query any, body any) (any, error) {
	query := a_query.(GetUserDetailsQuery)
	var user *local.User
	var addrs []string
	var err error
	self.locker.Lock(func() {
		user, addrs, err = self.fetch_data(query.Id)
	})
	if err != nil {
		return nil, err
	}
	resp := GetUserDetailsResponse{
		Uuid:        user.Id,
		Name:        user.Info.Name,
		Description: user.Info.Description,
		Pic:         user.Info.Photo,
		Key:         self.key_ser.Write(user.PubKey),
		KeyCode:     self.crypto.GetKeyCode(user.PubKey),
		Status:      user.Status.String(),
		Addrs:       addrs,
	}
	return resp, nil
}

func (self *GetUserDetailsHandler) fetch_data(id uuid.UUID) (*local.User, []string, error) {
	users, err := self.users.GetByUid(id)
	if err != nil {
		return nil, nil, err
	}
	if len(users) == 0 {
		return nil, nil, api_err.NotFound("user not found")
	}
	user := &users[0]
	addrs, err := self.users.GetAddrs(user.Id)
	if addrs == nil {
		addrs = []string{}
	}
	return user, addrs, err
}
