package users

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/local"
	"dusc/src/impls/api/common"
	"dusc/src/proto"
	"errors"

	"github.com/google/uuid"
	"gitlab.com/asvedr/cldi/cl"
)

type ModerateUserHandler struct {
	locker proto.IDbLocker
	users  proto.IUserRepo
	chats  proto.IChatRepo
}

type ModerateUserQuery struct {
	Id     uuid.UUID `name:"id"`
	Action string    `name:"action"`
}

func NewModerateUser(
	locker proto.IDbLocker,
	users proto.IUserRepo,
	chats proto.IChatRepo,
) proto.IApiHandler {
	return &ModerateUserHandler{
		locker: locker,
		users:  users,
		chats:  chats,
	}
}

func (self *ModerateUserHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:   "/api/users/moderate/",
		Method: "POST",
		QueryParser: cl.MakeQueryParser[ModerateUserQuery](
			common.QParserCtx,
		),
		AuthTokenRequired: true,
	}
}

var err_inv_action = errors.New("invalid action")

func (self *ModerateUserHandler) Handle(a_query any, body any) (any, error) {
	query := a_query.(ModerateUserQuery)
	var action func()
	var err error
	switch query.Action {
	case "ban":
		action = func() {
			err = self.users.SetStatus(query.Id, local.UserStatusBanned)
		}
	case "del":
		action = func() {
			err = self.users.Delete(query.Id)
		}
	case "trust":
		action = func() {
			err = errors.Join(
				self.chats.Delete(local.ChatId{Id: query.Id}),
				self.users.SetStatus(query.Id, local.UserStatusTrusted),
			)
		}
	default:
		return nil, api_err.InvalidBody(err_inv_action)
	}
	self.locker.Lock(func() {
		var users []local.User
		users, err = self.users.GetByUid(query.Id)
		if err != nil {
			return
		}
		if len(users) == 0 {
			err = api_err.NotFound("user not found")
			return
		}
		action()
	})
	return nil, err
}
