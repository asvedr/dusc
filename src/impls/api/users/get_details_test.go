package users_test

import (
	"dusc/src/app/serializers"
	"dusc/src/factories"
	"dusc/src/impls/api/users"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestGetUserNotFound(t *testing.T) {
	uid, _ := uuid.NewRandom()
	srv := factories.NewApiServer()

	resp := srv.Call(factories.ApiRequest{
		Url: "/api/users/detail?id=" + uid.String(),
	})
	assert.Equal(t, 404, resp.Status)
	assert.Equal(
		t,
		`{"code":"NOT_FOUND","details":"user not found"}`,
		string(resp.Body),
	)
}

func TestGetUserOk(t *testing.T) {
	uid, _ := uuid.NewRandom()
	key := &factories.PrivKey1.Get().PublicKey
	ser := serializers.PubKeyStrSerializer.Get()
	srv := factories.NewApiServer()
	err := srv.Db.User.Contact("Name", uid, key)
	assert.Nil(t, err)
	err = srv.Db.User.SetLastSeen(uid, 1, "user-addr")
	assert.Nil(t, err)
	srv.Crypto.GetKeyCodeResp = []string{"code"}

	resp := srv.Call(factories.ApiRequest{
		Url: "/api/users/detail?id=" + uid.String(),
	})
	assert.Equal(t, 200, resp.Status)
	exp := users.GetUserDetailsResponse{
		Uuid:    uid,
		Name:    "Name",
		Key:     ser.Write(key),
		KeyCode: "code",
		Status:  "new",
		Addrs:   []string{"user-addr"},
	}
	var got users.GetUserDetailsResponse
	resp.ParseJson(&got)
	assert.Equal(t, exp, got)
}
