package users_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestTrustUser(t *testing.T) {
	me := factories.Me.Get()
	srv := factories.NewApiServer()
	srv.Db.User.Contact("alex", me.Uid, &me.PrivKey.PublicKey)
	resp := srv.Call(factories.ApiRequest{
		Url: fmt.Sprintf(
			"/api/users/moderate?id=%s&action=trust",
			me.Uid.String(),
		),
		Method: "POST",
	})
	assert.Equal(t, 200, resp.Status)
	user, err := srv.Db.User.GetByUid(me.Uid)
	assert.Nil(t, err)
	assert.Equal(t, local.UserStatusTrusted, user[0].Status)
}

func TestBanUser(t *testing.T) {
	me := factories.Me.Get()
	srv := factories.NewApiServer()
	srv.Db.User.Contact("alex", me.Uid, &me.PrivKey.PublicKey)
	resp := srv.Call(factories.ApiRequest{
		Url: fmt.Sprintf(
			"/api/users/moderate?id=%s&action=ban",
			me.Uid.String(),
		),
		Method: "POST",
	})
	assert.Equal(t, 200, resp.Status)
	user, err := srv.Db.User.GetByUid(me.Uid)
	assert.Nil(t, err)
	assert.Equal(t, local.UserStatusBanned, user[0].Status)
}

func TestDeleteUser(t *testing.T) {
	me := factories.Me.Get()
	srv := factories.NewApiServer()
	srv.Db.User.Contact("alex", me.Uid, &me.PrivKey.PublicKey)
	resp := srv.Call(factories.ApiRequest{
		Url: fmt.Sprintf(
			"/api/users/moderate?id=%s&action=del",
			me.Uid.String(),
		),
		Method: "POST",
	})
	assert.Equal(t, 200, resp.Status)
	uids, err := srv.Db.User.GetUids()
	assert.Nil(t, err)
	assert.Equal(t, 0, len(uids))
}

func TestModerateInvalidAction(t *testing.T) {
	uid, _ := uuid.NewRandom()
	srv := factories.NewApiServer()
	resp := srv.Call(factories.ApiRequest{
		Url: fmt.Sprintf(
			"/api/users/moderate?id=%s&action=xxx",
			uid.String(),
		),
		Method: "POST",
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, `{"code":"INVALID_BODY","details":"invalid action"}`, string(resp.Body))
}

func TestModerateNotFound(t *testing.T) {
	uid, _ := uuid.NewRandom()
	srv := factories.NewApiServer()
	resp := srv.Call(factories.ApiRequest{
		Url: fmt.Sprintf(
			"/api/users/moderate?id=%s&action=ban",
			uid.String(),
		),
		Method: "POST",
	})
	assert.Equal(t, 404, resp.Status)
	assert.Equal(t, `{"code":"NOT_FOUND","details":"user not found"}`, string(resp.Body))
}
