package api_init

import (
	"crypto/rsa"
	"dusc/src/proto"
)

func MakeHandlers(
	locker proto.IDbLocker,
	settings proto.ISettingsRepo,
	initializer proto.IInitializer,
	token_manager proto.IAuthTokenManager,
	hasher proto.IHasher,
	key_ser proto.IStrSerializer[*rsa.PrivateKey],
) []proto.IApiHandler {
	return []proto.IApiHandler{
		NewInit(locker, initializer, key_ser),
		NewCheckInit(locker, initializer),
		NewLogin(
			locker,
			settings,
			token_manager,
			hasher,
		),
	}
}
