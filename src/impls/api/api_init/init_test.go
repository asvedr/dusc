package api_init_test

import (
	"dusc/src/app/serializers"
	"dusc/src/factories"
	"errors"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestInitInvalidBody(t *testing.T) {
	srv := factories.NewApiServer()
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/init/",
		Json:   map[string]any{"password": "abc"},
	})
	assert.Equal(t, resp.Status, 400)
	assert.Equal(t, "name not set", resp.ParseErr().Details)
}

func TestInitNoBody(t *testing.T) {
	srv := factories.NewApiServer()
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/init/",
	})
	assert.Equal(t, resp.Status, 400)
	assert.Equal(t, "EOF", resp.ParseErr().Details)
}

func TestInitDefault(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Initializer.InitResp = []error{nil}
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/init/",
		Json:   map[string]any{"name": "Alex"},
	})
	assert.Equal(t, resp.Status, 200)
	assert.Equal(t, 1, len(srv.Initializer.InitCalls))
	call := srv.Initializer.InitCalls[0]
	exp := factories.InitCall{Name: "Alex"}
	assert.Equal(t, exp, call)
}

func TestInitCustom(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Initializer.InitResp = []error{nil}
	uid, _ := uuid.NewRandom()
	ser := serializers.PrivKeyStrSerializer.Get()
	key := factories.PrivKey2.Get()
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/init/",
		Json: map[string]any{
			"uuid":        uid.String(),
			"priv_key":    ser.Write(key),
			"name":        "Alex",
			"description": "Descr",
			"password":    "abc",
		},
	})
	assert.Equal(t, resp.Status, 200)
	assert.Equal(t, 1, len(srv.Initializer.InitCalls))
	call := srv.Initializer.InitCalls[0]
	pwd := "abc"
	exp := factories.InitCall{
		Uuid:        &uid,
		PrivKey:     key,
		Name:        "Alex",
		Description: "Descr",
		Password:    &pwd,
	}
	assert.Equal(t, exp, call)
}

func TestInitError(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Initializer.InitResp = []error{
		errors.New("init error"),
	}
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/init/",
		Json:   map[string]any{"name": "Alex"},
	})
	assert.Equal(t, "init error", resp.ParseErr().Details)
	assert.Equal(t, resp.Status, 500)
}
