package api_init

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/proto"
)

type LoginHandler struct {
	locker        proto.IDbLocker
	settings      proto.ISettingsRepo
	token_manager proto.IAuthTokenManager
	hasher        proto.IHasher
}

type LoginBody struct {
	Password string `json:"password"`
}

type LoginResponse struct {
	Token string `json:"token"`
}

func NewLogin(
	locker proto.IDbLocker,
	settings proto.ISettingsRepo,
	token_manager proto.IAuthTokenManager,
	hasher proto.IHasher,
) proto.IApiHandler {
	return &LoginHandler{
		locker:        locker,
		settings:      settings,
		token_manager: token_manager,
		hasher:        hasher,
	}
}

func (self *LoginHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/login/",
		Method:            "POST",
		AuthTokenRequired: false,
		BodyParserFactory: &LoginBody{},
	}
}

func (self *LoginHandler) Handle(query any, a_body any) (any, error) {
	body := a_body.(*LoginBody)
	hashed := self.hasher.GetStrSum([]byte(body.Password))
	var expected string
	var err error
	self.locker.Lock(func() {
		expected, err = self.settings.GetPwdHash()
	})
	if err != nil {
		return nil, err
	}
	if expected != hashed {
		return nil, api_err.InvalidPassword
	}
	token, err := self.token_manager.GenToken()
	if err != nil {
		return nil, err
	}
	return LoginResponse{Token: token}, nil
}
