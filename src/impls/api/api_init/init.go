package api_init

import (
	"crypto/rsa"
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/proto"
	"errors"

	"github.com/google/uuid"
)

type InitHandler struct {
	locker      proto.IDbLocker
	initializer proto.IInitializer
	key_ser     proto.IStrSerializer[*rsa.PrivateKey]
}

type InitBody struct {
	Uuid        *uuid.UUID `json:"uuid"`
	PrivKey     *string    `json:"priv_key"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	Password    string     `json:"password"`
}

func NewInit(
	locker proto.IDbLocker,
	initializer proto.IInitializer,
	key_ser proto.IStrSerializer[*rsa.PrivateKey],
) proto.IApiHandler {
	return &InitHandler{
		locker:      locker,
		initializer: initializer,
		key_ser:     key_ser,
	}
}

func (self *InitHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/init/",
		Method:            "POST",
		AuthTokenRequired: true,
		BodyParserFactory: &InitBody{},
	}
}

func (self *InitHandler) Handle(query any, a_body any) (any, error) {
	body := a_body.(*InitBody)
	if len(body.Name) == 0 {
		return nil, api_err.InvalidBody(errors.New("name not set"))
	}
	priv_key, err := self.get_priv_key(body.PrivKey)
	if err != nil {
		return nil, err
	}
	var password *string
	if len(body.Password) > 0 {
		password = &body.Password
	}
	self.locker.Lock(func() {
		err = self.initializer.Init(
			body.Uuid,
			priv_key,
			body.Name,
			body.Description,
			password,
		)
	})
	return nil, err
}

func (self *InitHandler) get_priv_key(src *string) (*rsa.PrivateKey, error) {
	if src == nil {
		return nil, nil
	}
	return self.key_ser.Read(*src)
}
