package api_init_test

import (
	"dusc/src/factories"
	"dusc/src/impls/api/api_init"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCheckInitTrue(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Initializer.Inited = true
	resp := srv.Call(factories.ApiRequest{Url: "/api/check-init/"})
	assert.Equal(t, 200, resp.Status)
	val := api_init.CheckInitResponse{}
	resp.ParseJson(&val)
	assert.Equal(t, "INITED", val.Status)
}

func TestCheckInitFalse(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Initializer.Inited = false
	resp := srv.Call(factories.ApiRequest{Url: "/api/check-init/"})
	assert.Equal(t, 200, resp.Status)
	val := api_init.CheckInitResponse{}
	resp.ParseJson(&val)
	assert.Equal(t, "NOT_INITED", val.Status)
}
