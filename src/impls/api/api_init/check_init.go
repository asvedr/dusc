package api_init

import (
	"dusc/src/entities/api"
	"dusc/src/entities/errs"
	"dusc/src/proto"
	"errors"
)

type CheckInitHandler struct {
	locker      proto.IDbLocker
	initializer proto.IInitializer
}

const INITED = "INITED"
const NOT_INITED = "NOT_INITED"

type CheckInitResponse struct {
	Status string `json:"status"`
}

func NewCheckInit(
	locker proto.IDbLocker,
	initializer proto.IInitializer,
) proto.IApiHandler {
	return &CheckInitHandler{
		locker:      locker,
		initializer: initializer,
	}
}

func (self *CheckInitHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/check-init/",
		Method:            "GET",
		AuthTokenRequired: false,
	}
}

func (self *CheckInitHandler) Handle(query any, body any) (any, error) {
	var err error
	self.locker.Lock(func() {
		err = self.initializer.CheckInit()
	})
	if errors.As(err, &errs.NotInitialized{}) {
		return CheckInitResponse{Status: NOT_INITED}, nil
	}
	if err != nil {
		return nil, err
	}
	return CheckInitResponse{Status: INITED}, nil
}
