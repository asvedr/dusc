package api_init_test

import (
	"dusc/src/app/components"
	"dusc/src/factories"
	"dusc/src/impls/api/api_init"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoginInvalidBody(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Db.Settings.SetPwdHash("1")
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/login/",
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, "INVALID_BODY", resp.ParseErr().Code)
}

func TestLoginInvalidPassword(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Db.Settings.SetPwdHash("1")
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/login/",
		Json:   map[string]any{"password": "abc"},
	})
	assert.Equal(t, 403, resp.Status)
	assert.Equal(t, "INVALID_PASSWORD", resp.ParseErr().Code)
}

func TestLoginInvalidOk(t *testing.T) {
	srv := factories.NewApiServer()
	srv.TokenManager.Token = "<token>"
	hasher := components.Hasher.Get()
	srv.Db.Settings.SetPwdHash(hasher.GetStrSum([]byte("abc")))
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/login/",
		Json:   map[string]any{"password": "abc"},
	})
	assert.Equal(t, 200, resp.Status)
	data := api_init.LoginResponse{}
	resp.ParseJson(&data)
	exp := api_init.LoginResponse{Token: "<token>"}
	assert.Equal(t, exp, data)
}
