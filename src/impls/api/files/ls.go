package files

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/proto"
	"errors"
)

type LsHandler struct {
	fs proto.IFS
}

type LsBody struct {
	Path *string `json:"path"`
}

type LsItem struct {
	Name string `json:"name"`
	Path string `json:"path"`
}

type LsResponse struct {
	Dirs    []LsItem `json:"dirs"`
	Files   []LsItem `json:"files"`
	Parent  *string  `json:"parent"`
	Current string   `json:"current"`
}

var err_can_not_get_dir_content = errors.New("can not get dir content")
var err_can_not_get_default_path = errors.New("can not get default path")

func NewLs(fs proto.IFS) proto.IApiHandler {
	return &LsHandler{fs: fs}
}

func (self *LsHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Method:            "POST",
		Path:              "/api/files/ls/",
		BodyParserFactory: &LsBody{},
		AuthTokenRequired: true,
	}
}

func (self *LsHandler) Handle(query any, a_body any) (any, error) {
	body := a_body.(*LsBody)
	var path string
	var err error
	if body.Path == nil {
		path, err = self.default_path()
	} else {
		path = *body.Path
	}
	if err != nil {
		return nil, err
	}
	parent, err := self.get_parent_dir(path)
	if err != nil {
		return nil, err
	}
	dirs, files, err := self.get_content(path)
	if err != nil {
		return nil, err
	}
	resp := LsResponse{
		Dirs:    dirs,
		Files:   files,
		Parent:  parent,
		Current: path,
	}
	return resp, nil
}

func (self *LsHandler) get_content(path string) ([]LsItem, []LsItem, error) {
	var dirs, files []LsItem
	content, err := self.fs.GetDirContent(path)
	if err != nil {
		return nil, nil, api_err.InvalidBody(err_can_not_get_dir_content)
	}
	for _, item := range content {
		ls_item := LsItem{Name: item.Name, Path: item.Path}
		if item.IsDir {
			dirs = append(dirs, ls_item)
		} else {
			files = append(files, ls_item)
		}
	}
	return dirs, files, nil
}

func (self *LsHandler) get_parent_dir(current string) (*string, error) {
	path, err := self.fs.GetParentDir(current)
	if err != nil {
		return nil, err
	}
	return &path, nil
}

func (self *LsHandler) default_path() (string, error) {
	path, err := self.fs.GetAbsPath(".")
	if err != nil {
		return "", api_err.InvalidBody(err_can_not_get_default_path)
	}
	return path, nil
}
