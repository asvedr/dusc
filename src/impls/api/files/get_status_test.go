package files_test

import (
	"dusc/src/factories"
	"dusc/src/impls/api/files"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestGetStatusNotFound(t *testing.T) {
	srv := factories.NewApiServer()
	user_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()
	srv.Downloader.GetLoadingStatusData = map[uuid.UUID]map[uuid.UUID]int{
		user_id: {},
	}
	resp := srv.Call(factories.ApiRequest{
		Url: fmt.Sprintf(
			"/api/files/get-status?user_id=%s&file_id=%s",
			user_id,
			file_id,
		),
	})
	assert.Equal(t, 404, resp.Status)
	assert.Equal(t, "task not found", resp.ParseErr().Details)
}

func TestGetStatusProgress(t *testing.T) {
	srv := factories.NewApiServer()
	user_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()
	srv.Downloader.GetLoadingStatusData = map[uuid.UUID]map[uuid.UUID]int{
		user_id: {file_id: 42},
	}
	resp := srv.Call(factories.ApiRequest{
		Url: fmt.Sprintf(
			"/api/files/get-status?user_id=%s&file_id=%s",
			user_id,
			file_id,
		),
	})
	assert.Equal(t, 200, resp.Status)
	data := files.GetFileStatusResponse{}
	resp.ParseJson(&data)
	exp := files.GetFileStatusResponse{
		Progress:  42,
		Completed: false,
	}
	assert.Equal(t, exp, data)
}

func TestGetStatusCompleted(t *testing.T) {
	srv := factories.NewApiServer()
	user_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()
	srv.Downloader.GetLoadingStatusData = map[uuid.UUID]map[uuid.UUID]int{
		user_id: {file_id: 100},
	}
	resp := srv.Call(factories.ApiRequest{
		Url: fmt.Sprintf(
			"/api/files/get-status?user_id=%s&file_id=%s",
			user_id,
			file_id,
		),
	})
	assert.Equal(t, 200, resp.Status)
	data := files.GetFileStatusResponse{}
	resp.ParseJson(&data)
	exp := files.GetFileStatusResponse{
		Progress:  100,
		Completed: true,
	}
	assert.Equal(t, exp, data)
}
