package files

import "dusc/src/proto"

func MakeHandlers(
	locker proto.IDbLocker,
	msgs proto.IMsgRepo,
	downloader proto.IFileDownloader,
	fs proto.IFS,
) []proto.IApiHandler {
	return []proto.IApiHandler{
		NewDownloadFile(locker, msgs, downloader),
		NewGetFileStatus(downloader),
		NewLs(fs),
	}
}
