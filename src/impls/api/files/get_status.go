package files

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/errs"
	"dusc/src/impls/api/common"
	"dusc/src/proto"
	"errors"

	"github.com/google/uuid"
	"gitlab.com/asvedr/cldi/cl"
)

type GetFileStatusHandler struct {
	downloader proto.IFileDownloader
}

type GetFileStatusResponse struct {
	Progress  int  `json:"progress"`
	Completed bool `json:"completed"`
	// obsolete
	Failed bool `json:"failed"`
}

type GetFileStatusQuery struct {
	UserId uuid.UUID `name:"user_id"`
	FileId uuid.UUID `name:"file_id"`
}

func NewGetFileStatus(downloader proto.IFileDownloader) proto.IApiHandler {
	return &GetFileStatusHandler{downloader: downloader}
}

func (self *GetFileStatusHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Method: "GET",
		Path:   "/api/files/get-status/",
		QueryParser: cl.MakeQueryParser[GetFileStatusQuery](
			common.QParserCtx,
		),
		AuthTokenRequired: true,
	}
}

func (self *GetFileStatusHandler) Handle(a_query any, body any) (any, error) {
	query := a_query.(GetFileStatusQuery)
	status, err := self.downloader.GetLoadingStatus(query.UserId, query.FileId)
	if errors.Is(err, errs.NotFound{}) {
		return nil, api_err.NotFound("task not found")
	}
	if err != nil {
		return nil, err
	}
	resp := GetFileStatusResponse{
		Progress:  status,
		Completed: status == 100,
	}
	return resp, nil
}
