package files

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"

	"github.com/google/uuid"
)

type DownloadFileHandler struct {
	locker     proto.IDbLocker
	msgs       proto.IMsgRepo
	downloader proto.IFileDownloader
}

type DownloadFileBody struct {
	MsgId uuid.UUID `json:"msg_id"`
	Path  string    `json:"path"`
}

var err_msg_has_no_file = errors.New("msg has no file")
var err_msg_was_not_received = errors.New("msg was not received")
var err_already_started = api_err.ApiError{
	Status:  409,
	Code:    "ALREADY_STARTED",
	Details: "Downloading is already started",
}

func NewDownloadFile(
	locker proto.IDbLocker,
	msgs proto.IMsgRepo,
	downloader proto.IFileDownloader,
) proto.IApiHandler {
	return &DownloadFileHandler{
		locker:     locker,
		msgs:       msgs,
		downloader: downloader,
	}
}

func (self *DownloadFileHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Method:            "POST",
		Path:              "/api/files/download/",
		BodyParserFactory: &DownloadFileBody{},
		AuthTokenRequired: true,
	}
}

func (self *DownloadFileHandler) Handle(query any, a_body any) (any, error) {
	body := a_body.(*DownloadFileBody)
	var msg *local.Msg
	var err error
	self.locker.Lock(func() {
		msg, err = self.msgs.GetByUid(body.MsgId)
	})
	if errors.Is(err, errs.NotFound{}) {
		return nil, api_err.NotFound("msg not found")
	}
	if msg.AttachFileMeta == nil {
		return nil, api_err.InvalidBody(err_msg_has_no_file)
	}
	if msg.Sender == nil {
		return nil, api_err.InvalidBody(err_msg_was_not_received)
	}
	err = self.downloader.StartLoading(
		*msg.Sender,
		msg.AttachFileMeta.FileId,
		int(msg.AttachFileMeta.Parts),
		body.Path,
	)
	if errors.Is(err, errs.LoadingAlreadyStarted{}) {
		return nil, err_already_started
	}
	return nil, err
}
