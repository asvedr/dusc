package files_test

import (
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestDownloadAlreadyStarted(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Downloader.StartLoadingResp = errs.LoadingAlreadyStarted{}
	sender, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()
	srv.Db.Msg.Create(local.NewMsg{
		Sender:  &sender,
		Visible: true,
		Uid:     msg_id,
		AttachFileMeta: &local.AttachFileMeta{
			FileId: file_id,
			Parts:  10,
			Size:   100,
		},
	})
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/files/download/",
		Json: map[string]any{
			"msg_id": msg_id.String(),
			"path":   "path_to_save",
		},
	})
	assert.Equal(t, 409, resp.Status)
	assert.Equal(t, "ALREADY_STARTED", resp.ParseErr().Code)
}

func TestDownloadOk(t *testing.T) {
	srv := factories.NewApiServer()
	sender, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()
	srv.Db.Msg.Create(local.NewMsg{
		Sender:  &sender,
		Visible: true,
		Uid:     msg_id,
		AttachFileMeta: &local.AttachFileMeta{
			FileId: file_id,
			Parts:  10,
			Size:   100,
		},
	})
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/files/download/",
		Json: map[string]any{
			"msg_id": msg_id.String(),
			"path":   "path_to_save",
		},
	})
	assert.Equal(t, 200, resp.Status)
}

func TestDownloadMsgNotFound(t *testing.T) {
	srv := factories.NewApiServer()
	msg_id, _ := uuid.NewRandom()
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/files/download/",
		Json: map[string]any{
			"msg_id": msg_id.String(),
			"path":   "path_to_save",
		},
	})
	assert.Equal(t, 404, resp.Status)
	assert.Equal(t, "msg not found", resp.ParseErr().Details)
}

func TestDownloadMsgNotReceived(t *testing.T) {
	srv := factories.NewApiServer()
	receiver, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()
	srv.Db.Msg.Create(local.NewMsg{
		Receiver: &receiver,
		Visible:  true,
		Uid:      msg_id,
		AttachFileMeta: &local.AttachFileMeta{
			FileId: file_id,
			Parts:  10,
			Size:   100,
		},
	})
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/files/download/",
		Json: map[string]any{
			"msg_id": msg_id.String(),
			"path":   "path_to_save",
		},
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, "msg was not received", resp.ParseErr().Details)
}

func TestDownloadMsgInvalidAttach2(t *testing.T) {
	srv := factories.NewApiServer()
	sender, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()
	srv.Db.Msg.Create(local.NewMsg{
		Sender:  &sender,
		Visible: true,
		Uid:     msg_id,
	})
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/files/download/",
		Json: map[string]any{
			"msg_id": msg_id.String(),
			"path":   "path_to_save",
		},
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, "msg has no file", resp.ParseErr().Details)
}
