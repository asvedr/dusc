package files_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/api/files"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLsOk(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Fs.GetDirContentData = map[string][]local.DirItem{
		"/home": {
			{Name: "dir_a", Path: "/home/dir_a", IsDir: true},
			{Name: "dir_b", Path: "/home/dir_b", IsDir: true},
			{Name: "file_a", Path: "/home/file_a"},
			{Name: "file_b", Path: "/home/file_b"},
		},
	}
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/files/ls/",
		Json:   map[string]string{"path": "/home"},
	})
	assert.Equal(t, 200, resp.Status)
	data := files.LsResponse{}
	resp.ParseJson(&data)
	parent := "/"
	exp := files.LsResponse{
		Dirs: []files.LsItem{
			{Name: "dir_a", Path: "/home/dir_a"},
			{Name: "dir_b", Path: "/home/dir_b"},
		},
		Files: []files.LsItem{
			{Name: "file_a", Path: "/home/file_a"},
			{Name: "file_b", Path: "/home/file_b"},
		},
		Parent:  &parent,
		Current: "/home",
	}
	assert.Equal(t, exp, data)
}
