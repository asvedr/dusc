package tasks

import (
	"dusc/src/entities/api"
	"dusc/src/proto"

	"gitlab.com/asvedr/cldi/cl"
)

type TriggerPeriodicHandler struct {
	// task_scheduler: TS,
}

type TriggerPeriodicQuery struct {
	Name string `name:"name"`
}

func NewTriggerPeriodic() proto.IApiHandler {
	return &TriggerPeriodicHandler{}
}

func (self *TriggerPeriodicHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Method:            "POST",
		Path:              "/api/tasks/trigger-periodic/",
		QueryParser:       cl.MakeQueryParser[TriggerPeriodicQuery](),
		AuthTokenRequired: true,
	}
}

func (self *TriggerPeriodicHandler) Handle(a_query any, _ any) (any, error) {
	query := a_query.(TriggerPeriodicQuery)
	println("trigger called for " + query.Name)
	return nil, nil
}
