package tasks

import "dusc/src/proto"

func MakeHandlers() []proto.IApiHandler {
	return []proto.IApiHandler{
		NewTriggerPeriodic(),
	}
}
