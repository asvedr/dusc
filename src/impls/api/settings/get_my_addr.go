package settings

import (
	"dusc/src/entities/api"
	"dusc/src/proto"
)

type GetMyAddrHandler struct {
	addr_man proto.IAddrManager
}

type GetMyAddrResponse struct {
	Addr string `json:"addr"`
}

func NewGetMyAddr(addr_man proto.IAddrManager) proto.IApiHandler {
	return &GetMyAddrHandler{addr_man: addr_man}
}

func (self *GetMyAddrHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/settings/my-addr/",
		AuthTokenRequired: true,
	}
}

func (self *GetMyAddrHandler) Handle(_ any, _ any) (any, error) {
	addr := self.addr_man.GetMyAddr()
	return GetMyAddrResponse{Addr: addr}, nil
}
