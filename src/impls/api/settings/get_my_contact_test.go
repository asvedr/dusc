package settings_test

import (
	"dusc/src/entities/api_err"
	"dusc/src/factories"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetContactNotInitied(t *testing.T) {
	srv := factories.NewApiServer()
	resp := srv.Call(
		factories.ApiRequest{Url: "/api/settings/contact/"},
	)
	assert.Equal(t, 400, resp.Status)
	exp := api_err.NotInitialized().Error()
	assert.Equal(t, exp, string(resp.Body))
}

func TestGetContactOk(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Db.Settings.SetMe(factories.Me.Get())
	resp := srv.Call(
		factories.ApiRequest{Url: "/api/settings/contact/"},
	)
	assert.Equal(t, 200, resp.Status)
	exp, _ := json.Marshal(
		map[string]string{
			"data": "VTTLcfNMQv+rBrtDwtkM4wEOMIIBCgKCAQEAuQkJJwJDz5xMU/Qi8Q06ojnymox287vpKaOamDu8LweMQ8xL7PdZj5jZBNlokbdGbe5k68y5NZp1ynjYQawQDHQGD5VBvlya88f5kTC2P8ctJ9j8vtMOj88nwRfe/iazENvwP5REedtOhHVVgfN6deSFnO5/WVgzH3jrloFV0iTBPxV+6byEqs0uozK7GUnP0skvyTRESBqyIcJK2LdbDERAGckTMZq96QHbfkqdiUCoKh7euSsjHhaQg1ejOiGBH9Gcmf6hsGbzXT1sDaHfb86Mup43+1X/0FDodxxA4creeJVueYSrka3z+bOktR9wkd5OuiBvNeZStOzQlOJB8wIDAQAB",
		},
	)
	assert.Equal(t, string(exp), string(resp.Body))
}
