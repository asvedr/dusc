package settings_test

import (
	"dusc/src/app/serializers"
	"dusc/src/entities/api_err"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/api/settings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetMeNotInited(t *testing.T) {
	server := factories.NewApiServer()

	resp := server.Call(factories.ApiRequest{Url: "/api/settings/me/"})
	assert.Equal(t, 400, resp.Status)
	exp := api_err.NotInitialized().Error()
	assert.Equal(t, exp, string(resp.Body))
}

func TestGetMeOk(t *testing.T) {
	priv_ser := serializers.PrivKeyStrSerializer.Get()
	pub_ser := serializers.PubKeyStrSerializer.Get()
	server := factories.NewApiServer()

	server.Db.Settings.SetMe(local.Me{
		PrivKey: factories.PrivKey1.Get(),
		Uid:     factories.Uuid.Get(),
	})
	server.Db.Settings.SetName("Name")
	server.Db.Settings.SetDescription("Descr")
	server.Crypto.GetKeyCodeResp = []string{"abc"}

	resp := server.Call(factories.ApiRequest{Url: "/api/settings/me/"})
	assert.Equal(t, 200, resp.Status)
	exp := settings.GetMeResponse{
		Uuid:        factories.Uuid.Get(),
		PrivKey:     priv_ser.Write(factories.PrivKey1.Get()),
		PubKey:      pub_ser.Write(&factories.PrivKey1.Get().PublicKey),
		KeyCode:     "abc",
		Name:        "Name",
		Description: "Descr",
	}
	var got settings.GetMeResponse
	resp.ParseJson(&got)
	assert.Equal(t, exp, got)
}
