package settings_test

import (
	"crypto/rsa"
	"dusc/src/factories"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRegenerateOnInit(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Crypto.GenPrivKeyResp = []*rsa.PrivateKey{
		factories.PrivKey2.Get(),
	}

	resp := srv.Call(factories.ApiRequest{
		Url:    "/api/settings/regenerate/",
		Method: "POST",
	})
	assert.Equal(t, 200, resp.Status)
	me, err := srv.Db.Settings.GetMe()
	assert.Nil(t, err)
	assert.Equal(t, me.PrivKey, factories.PrivKey2.Get())
}

func TestRegenerateNone(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Db.Settings.SetMe(factories.Me.Get())

	resp := srv.Call(factories.ApiRequest{
		Url:    "/api/settings/regenerate/",
		Method: "POST",
	})
	assert.Equal(t, 200, resp.Status)
	me, err := srv.Db.Settings.GetMe()
	assert.Nil(t, err)
	assert.Equal(t, *me, factories.Me.Get())
}

func TestRegenerateAll(t *testing.T) {
	srv := factories.NewApiServer()
	original := factories.Me.Get()
	srv.Db.Settings.SetMe(original)
	srv.Crypto.GenPrivKeyResp = []*rsa.PrivateKey{
		factories.PrivKey2.Get(),
	}

	resp := srv.Call(factories.ApiRequest{
		Url:    "/api/settings/regenerate?uuid=1&priv_key=1",
		Method: "POST",
	})
	assert.Equal(t, 200, resp.Status)
	me, err := srv.Db.Settings.GetMe()
	assert.Nil(t, err)
	assert.NotEqual(t, me.PrivKey, original.PrivKey)
	assert.Equal(t, me.PrivKey, factories.PrivKey2.Get())
	assert.NotEqual(t, me.Uid, original.Uid)
}
