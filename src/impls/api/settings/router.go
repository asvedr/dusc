package settings

import (
	"crypto/rsa"
	"dusc/src/entities/local"
	"dusc/src/proto"
)

func MakeHandlers(
	locker proto.IDbLocker,
	settings proto.ISettingsRepo,
	priv_key_ser proto.IStrSerializer[*rsa.PrivateKey],
	pub_key_ser proto.IStrSerializer[*rsa.PublicKey],
	cnt_ser proto.IStrSerializer[local.ClientContact],
	crypto proto.ICrypto,
	addr_man proto.IAddrManager,
) []proto.IApiHandler {
	return []proto.IApiHandler{
		NewGetMe(
			locker,
			settings,
			priv_key_ser,
			pub_key_ser,
			crypto,
		),
		NewGetMyContact(locker, settings, cnt_ser),
		NewPatchMe(locker, settings, priv_key_ser),
		NewRegenerate(locker, settings, crypto),
		NewGetMyAddr(addr_man),
	}
}
