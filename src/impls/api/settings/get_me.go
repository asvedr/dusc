package settings

import (
	"crypto/rsa"
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"

	"github.com/google/uuid"
)

type GetMeHandler struct {
	locker        proto.IDbLocker
	settings_repo proto.ISettingsRepo
	priv_key_ser  proto.IStrSerializer[*rsa.PrivateKey]
	pub_key_ser   proto.IStrSerializer[*rsa.PublicKey]
	crypto        proto.ICrypto
}

type GetMeResponse struct {
	Uuid        uuid.UUID `json:"uuid"`
	PrivKey     string    `json:"priv_key"`
	PubKey      string    `json:"pub_key"`
	KeyCode     string    `json:"key_code"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Pic         string    `json:"pic"`
}

func NewGetMe(
	locker proto.IDbLocker,
	settings_repo proto.ISettingsRepo,
	priv_key_ser proto.IStrSerializer[*rsa.PrivateKey],
	pub_key_ser proto.IStrSerializer[*rsa.PublicKey],
	crypto proto.ICrypto,
) proto.IApiHandler {
	return &GetMeHandler{
		locker:        locker,
		settings_repo: settings_repo,
		priv_key_ser:  priv_key_ser,
		pub_key_ser:   pub_key_ser,
		crypto:        crypto,
	}
}

func (self *GetMeHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/settings/me/",
		AuthTokenRequired: true,
	}
}

func (self *GetMeHandler) Handle(query any, body any) (any, error) {
	var me *local.Me
	var meta local.ApiUserMeta
	var err error
	self.locker.Lock(func() {
		me, err = self.settings_repo.GetMe()
		if errors.As(err, &errs.NotInitialized{}) {
			err = api_err.NotInitialized()
		}
		if err != nil {
			return
		}
		meta, err = self.settings_repo.GetMeta()
	})
	if err != nil {
		return nil, err
	}
	pub_key := &me.PrivKey.PublicKey
	resp := &GetMeResponse{
		Uuid:        me.Uid,
		PrivKey:     self.priv_key_ser.Write(me.PrivKey),
		PubKey:      self.pub_key_ser.Write(pub_key),
		KeyCode:     self.crypto.GetKeyCode(pub_key),
		Name:        meta.Name,
		Description: meta.Description,
		Pic:         meta.Photo,
	}
	return resp, nil
}
