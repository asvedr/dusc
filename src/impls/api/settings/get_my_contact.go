package settings

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"
)

type GetMyContactHandler struct {
	locker   proto.IDbLocker
	settings proto.ISettingsRepo
	ser      proto.IStrSerializer[local.ClientContact]
}

type GetMyContactResponse struct {
	Data string `json:"data"`
}

func NewGetMyContact(
	locker proto.IDbLocker,
	settings proto.ISettingsRepo,
	ser proto.IStrSerializer[local.ClientContact],
) proto.IApiHandler {
	return &GetMyContactHandler{
		locker:   locker,
		settings: settings,
		ser:      ser,
	}
}

func (self *GetMyContactHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/settings/contact/",
		AuthTokenRequired: true,
	}
}

func (self *GetMyContactHandler) Handle(query any, body any) (any, error) {
	var me *local.Me
	var contact local.ClientContact
	var err error
	self.locker.Lock(func() {
		me, err = self.settings.GetMe()
		if err != nil {
			return
		}
		contact = local.ClientContact{
			Uid:    me.Uid,
			PubKey: &me.PrivKey.PublicKey,
		}
	})
	if errors.As(err, &errs.NotInitialized{}) {
		err = api_err.NotInitialized()
	}
	if err != nil {
		return nil, err
	}
	resp := GetMyContactResponse{
		Data: self.ser.Write(contact),
	}
	return resp, nil
}
