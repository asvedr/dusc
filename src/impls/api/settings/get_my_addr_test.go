package settings_test

import (
	"dusc/src/factories"
	"dusc/src/impls/api/settings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetMyAddr(t *testing.T) {
	srv := factories.NewApiServer()

	resp := srv.Call(factories.ApiRequest{Url: "/api/settings/my-addr/"})
	assert.Equal(t, 200, resp.Status)
	data := settings.GetMyAddrResponse{}
	resp.ParseJson(&data)
	assert.Equal(t, "", data.Addr)

	srv.AddrManager.MyAddr = "123"

	resp = srv.Call(factories.ApiRequest{Url: "/api/settings/my-addr/"})
	assert.Equal(t, 200, resp.Status)
	data = settings.GetMyAddrResponse{}
	resp.ParseJson(&data)
	assert.Equal(t, "123", data.Addr)
}
