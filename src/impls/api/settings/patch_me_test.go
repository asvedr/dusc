package settings_test

import (
	"dusc/src/app/serializers"
	"dusc/src/factories"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestPatchMeEmpty(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Db.Settings.SetMe(factories.Me.Get())

	resp := srv.Call(
		factories.ApiRequest{
			Url:    "/api/settings/me/",
			Method: "PATCH",
			Json:   map[string]any{},
		},
	)
	assert.Equal(t, 200, resp.Status)
	me, err := srv.Db.Settings.GetMe()
	assert.Nil(t, err)
	assert.Equal(t, *me, factories.Me.Get())
	meta, err := srv.Db.Settings.GetMeta()
	assert.Nil(t, err)
	assert.Equal(t, "", meta.Name)
	assert.Equal(t, "", meta.Description)
}

func TestPatchMeValues(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Db.Settings.SetMe(factories.Me.Get())

	uid, _ := uuid.NewRandom()
	skey := serializers.PrivKeyStrSerializer.Get().Write(
		factories.PrivKey2.Get(),
	)
	resp := srv.Call(
		factories.ApiRequest{
			Url:    "/api/settings/me/",
			Method: "PATCH",
			Json: map[string]any{
				"uuid":        uid.String(),
				"priv_key":    skey,
				"name":        "alex",
				"description": "descr",
			},
		},
	)
	assert.Equal(t, 200, resp.Status)
	me, err := srv.Db.Settings.GetMe()
	assert.Nil(t, err)
	assert.NotEqual(t, *me, factories.Me.Get())
	assert.Equal(t, me.Uid, uid)
	assert.Equal(t, me.PrivKey, factories.PrivKey2.Get())
	meta, err := srv.Db.Settings.GetMeta()
	assert.Nil(t, err)
	assert.Equal(t, "alex", meta.Name)
	assert.Equal(t, "descr", meta.Description)
}
