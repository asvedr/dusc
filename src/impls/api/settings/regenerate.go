package settings

import (
	"dusc/src/entities/api"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"

	"github.com/google/uuid"
	"gitlab.com/asvedr/cldi/cl"
)

type RegenerateHandler struct {
	locker   proto.IDbLocker
	settings proto.ISettingsRepo
	crypto   proto.ICrypto
}

type RegenerateQuery struct {
	Uuid    bool `name:"uuid" opt:"t" default:"0"`
	PrivKey bool `name:"priv_key" opt:"t" default:"0"`
}

func NewRegenerate(
	locker proto.IDbLocker,
	settings proto.ISettingsRepo,
	crypto proto.ICrypto,
) proto.IApiHandler {
	return &RegenerateHandler{
		locker:   locker,
		settings: settings,
		crypto:   crypto,
	}
}

func (self *RegenerateHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/settings/regenerate/",
		Method:            "POST",
		QueryParser:       cl.MakeQueryParser[RegenerateQuery](),
		AuthTokenRequired: true,
	}
}

func (self *RegenerateHandler) Handle(a_query any, body any) (any, error) {
	query := a_query.(RegenerateQuery)
	var me *local.Me
	var err error
	self.locker.Lock(func() {
		me, err = self.settings.GetMe()
	})
	if errors.As(err, &errs.NotInitialized{}) {
		query.PrivKey = true
		query.Uuid = true
		me = &local.Me{}
		err = nil
	}
	if err != nil {
		return nil, err
	}

	if query.Uuid {
		me.Uid, err = uuid.NewRandom()
	}
	if err != nil {
		return nil, err
	}
	if query.PrivKey {
		me.PrivKey, err = self.crypto.GenPrivKey()
	}
	if err != nil {
		return nil, err
	}
	self.locker.Lock(func() {
		err = self.settings.SetMe(*me)
	})
	return nil, err
}
