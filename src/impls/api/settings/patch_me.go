package settings

import (
	"crypto/rsa"
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"

	"github.com/google/uuid"
)

type PatchMeHandler struct {
	locker   proto.IDbLocker
	settings proto.ISettingsRepo
	key_ser  proto.IStrSerializer[*rsa.PrivateKey]
	// pic_loader proto.IPicLoader
}

type PatchMeBody struct {
	Uuid        *uuid.UUID `json:"uuid"`
	PrivKey     *string    `json:"priv_key"`
	Name        *string    `json:"name"`
	Description *string    `json:"description"`
	PicPath     *string    `json:"pic_path"`
}

var err_invalid_priv_key = errors.New("invalid private key")

func NewPatchMe(
	locker proto.IDbLocker,
	settings proto.ISettingsRepo,
	key_ser proto.IStrSerializer[*rsa.PrivateKey],
) proto.IApiHandler {
	return &PatchMeHandler{
		locker:   locker,
		settings: settings,
		key_ser:  key_ser,
	}
}

func (self *PatchMeHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/settings/me/",
		Method:            "PATCH",
		BodyParserFactory: &PatchMeBody{},
		AuthTokenRequired: true,
	}
}

func (self *PatchMeHandler) Handle(query any, a_body any) (any, error) {
	body := a_body.(*PatchMeBody)
	var err error
	var me local.Me
	self.locker.Lock(func() {
		if body.Name != nil {
			err = self.settings.SetName(*body.Name)
		}
		if err != nil {
			return
		}
		if body.Description != nil {
			err = self.settings.SetDescription(*body.Description)
		}
		if err != nil {
			return
		}
		if body.HasUpdMe() {
			me, err = self.prepare_me(body)
			if err != nil {
				return
			}
			err = self.settings.SetMe(me)
		}
	})
	return nil, err
}

func (self *PatchMeHandler) prepare_me(body *PatchMeBody) (me local.Me, err error) {
	var dflt *local.Me
	dflt, err = self.settings.GetMe()
	me = *dflt
	if err != nil {
		return
	}
	if body.Uuid != nil {
		me.Uid = *body.Uuid
	}
	if body.PrivKey != nil {
		me.PrivKey, err = self.key_ser.Read(*body.PrivKey)
	}
	if err != nil {
		err = api_err.InvalidBody(err_invalid_priv_key)
	}
	return
}

func (self PatchMeBody) HasUpdMe() bool {
	return self.Uuid != nil || self.PrivKey != nil
}
