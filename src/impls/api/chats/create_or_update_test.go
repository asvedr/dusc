package chats_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/api/chats"
	"dusc/src/utils"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestCreate(t *testing.T) {
	srv := factories.NewApiServer()
	srv.TaskExecutor.ExecuteNowResp = []error{nil}
	user, _ := uuid.NewRandom()
	srv.Db.User.Contact(
		"alex", user, &factories.PrivKey2.Get().PublicKey,
	)
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/chats/",
		Json: chats.CreateOrUpdateChatBody{
			Users: []uuid.UUID{user},
			Name:  "name",
		},
	})
	assert.Equal(t, 200, resp.Status)
	chats, err := srv.Db.Chats.GetFullList()
	assert.Nil(t, err)
	assert.Equal(t, 1, len(chats))
	chat_id := chats[0].Id
	assert.True(t, chat_id.IsGroup)
	participants, err := srv.Db.Chats.GetParticipants(chat_id)
	assert.Nil(t, err)
	assert.Equal(t, participants, []uuid.UUID{user})
	msgs, err := srv.Db.Msg.GetMessagesToSend()
	assert.Nil(t, err)
	assert.Equal(t, &user, msgs[0].Receiver)
	assert.Equal(t, chat_id, msgs[0].Chat)
	assert.Equal(t, "name", msgs[0].AttachInvite.Name)
	assert.Equal(t, 0, len(msgs[0].AttachInvite.Users))
}

func TestUpdate(t *testing.T) {
	srv := factories.NewApiServer()
	srv.TaskExecutor.ExecuteNowResp = []error{nil, nil}
	user_a, _ := uuid.NewRandom()
	user_b, _ := uuid.NewRandom()
	chat_uid, _ := uuid.NewRandom()
	srv.Db.User.Contact(
		"alex", user_a, &factories.PrivKey2.Get().PublicKey,
	)
	srv.Db.User.Contact(
		"bob", user_b, &factories.PrivKey1.Get().PublicKey,
	)
	chat_id := local.ChatId{IsGroup: true, Id: chat_uid}
	srv.Db.Chats.Create(local.Chat{
		Id:    chat_id,
		Name:  "chat",
		Users: []uuid.UUID{user_a},
	})
	s_chat_id := chat_id.String()
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/chats/",
		Json: chats.CreateOrUpdateChatBody{
			ChatId: &s_chat_id,
			Users:  []uuid.UUID{user_b},
			Name:   "chat",
		},
	})
	assert.Equal(t, 200, resp.Status)
	participants, err := srv.Db.Chats.GetParticipants(chat_id)
	assert.Nil(t, err)
	assert.Equal(
		t,
		utils.SortUids(participants),
		utils.SortUids([]uuid.UUID{user_a, user_b}),
	)
	msgs, err := srv.Db.Msg.GetMessagesToSend()
	assert.Nil(t, err)
	assert.Equal(t, 2, len(msgs))
}

func TestUpdateNotFound(t *testing.T) {
	srv := factories.NewApiServer()
	user_a, _ := uuid.NewRandom()
	user_b, _ := uuid.NewRandom()
	chat_uid, _ := uuid.NewRandom()
	srv.Db.User.Contact(
		"alex", user_a, &factories.PrivKey2.Get().PublicKey,
	)
	srv.Db.User.Contact(
		"bob", user_b, &factories.PrivKey1.Get().PublicKey,
	)
	chat_id := local.ChatId{IsGroup: true, Id: chat_uid}
	s_chat_id := chat_id.String()
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/chats/",
		Json: chats.CreateOrUpdateChatBody{
			ChatId: &s_chat_id,
			Users:  []uuid.UUID{user_b},
			Name:   "chat",
		},
	})
	assert.Equal(t, 404, resp.Status)
	msgs, err := srv.Db.Msg.GetMessagesToSend()
	assert.Nil(t, err)
	assert.Equal(t, 0, len(msgs))
}
