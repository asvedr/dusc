package chats

import "dusc/src/proto"

func MakeHandlers(
	locker proto.IDbLocker,
	users proto.IUserRepo,
	chats proto.IChatRepo,
	crypto proto.ICrypto,
	msg proto.IMsgMaker,
) []proto.IApiHandler {
	return []proto.IApiHandler{
		NewCreateOrUpdateChat(
			locker,
			users,
			chats,
			crypto,
			msg,
		),
		NewModerateChat(locker, chats),
		NewGetChatList(locker, chats),
		NewGetChatDetails(
			locker,
			chats,
			users,
			crypto,
		),
	}
}
