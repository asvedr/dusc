package chats_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/api/chats"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestGetListEmpty(t *testing.T) {
	srv := factories.NewApiServer()
	resp := srv.Call(factories.ApiRequest{
		Url: "/api/chats/",
	})
	assert.Equal(t, 200, resp.Status)
	body := chats.GetChatListResponse{}
	resp.ParseJson(&body)
	assert.Equal(
		t,
		chats.GetChatListResponse{},
		body,
	)
}

func TestGetListOne(t *testing.T) {
	srv := factories.NewApiServer()
	chat_id := local.ChatId{IsGroup: true}
	chat_id.Id, _ = uuid.NewRandom()
	user_id, _ := uuid.NewRandom()
	srv.Db.User.Contact("alex", user_id, &factories.PrivKey2.Get().PublicKey)
	srv.Db.Msg.Create(local.NewMsg{
		Chat:    chat_id,
		Visible: false,
		Text:    "msg data",
		Read:    false,
	})
	srv.Db.Chats.Create(
		local.Chat{
			Id:    chat_id,
			Name:  "chat",
			Users: []uuid.UUID{user_id},
		},
	)
	resp := srv.Call(factories.ApiRequest{
		Url: "/api/chats/",
	})
	assert.Equal(t, 200, resp.Status)
	body := chats.GetChatListResponse{
		Data: []chats.ApiChat{{
			Id:      chat_id.String(),
			Name:    "chat",
			LastMsg: "",
			New:     false,
		}},
	}
	resp.ParseJson(&body)
}

func TestGetListOneWithMsg(t *testing.T) {
	srv := factories.NewApiServer()
	chat_id := local.ChatId{IsGroup: true}
	chat_id.Id, _ = uuid.NewRandom()
	user_id, _ := uuid.NewRandom()
	srv.Db.User.Contact("alex", user_id, &factories.PrivKey2.Get().PublicKey)
	srv.Db.Msg.Create(local.NewMsg{
		Chat:    chat_id,
		Visible: true,
		Text:    "msg data",
		Read:    false,
	})
	srv.Db.Chats.Create(
		local.Chat{
			Id:    chat_id,
			Name:  "chat",
			Users: []uuid.UUID{user_id},
		},
	)
	resp := srv.Call(factories.ApiRequest{
		Url: "/api/chats/",
	})
	assert.Equal(t, 200, resp.Status)
	body := chats.GetChatListResponse{
		Data: []chats.ApiChat{{
			Id:      chat_id.String(),
			Name:    "chat",
			LastMsg: "msg data",
			New:     true,
		}},
	}
	resp.ParseJson(&body)
}
