package chats

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"

	"github.com/google/uuid"
	"gitlab.com/asvedr/cldi/cl"
)

type GetChatDetailsHandler struct {
	locker proto.IDbLocker
	chats  proto.IChatRepo
	users  proto.IUserRepo
	crypto proto.ICrypto
}

type GetChatDetailsQuery struct {
	Id string `name:"id"`
}

type ApiUser struct {
	Uuid    uuid.UUID `json:"uuid"`
	Name    string    `json:"name"`
	Pic     string    `json:"pic"`
	KeyCode string    `json:"key_code"`
}

type GetChatDetailsResponse struct {
	Id           string    `json:"id"`
	Name         string    `json:"name"`
	Participants []ApiUser `json:"participants"`
}

func NewGetChatDetails(
	locker proto.IDbLocker,
	chats proto.IChatRepo,
	users proto.IUserRepo,
	crypto proto.ICrypto,
) proto.IApiHandler {
	return &GetChatDetailsHandler{
		locker: locker,
		chats:  chats,
		users:  users,
		crypto: crypto,
	}
}

func (self *GetChatDetailsHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/chats/info/",
		QueryParser:       cl.MakeQueryParser[GetChatDetailsQuery](),
		AuthTokenRequired: true,
	}
}

func (self *GetChatDetailsHandler) Handle(a_query any, body any) (any, error) {
	query := a_query.(GetChatDetailsQuery)
	id, err := local.StringToChatId(query.Id)
	if err != nil {
		return nil, err
	}
	var chat local.Chat
	var uids []uuid.UUID
	var users []local.User
	self.locker.Lock(func() {
		chat, err = self.chats.GetById(id)
		if errors.As(err, &errs.NotFound{}) {
			err = api_err.NotFound("chat not found")
			return
		}
		if err != nil {
			return
		}
		uids, err = self.chats.GetParticipants(id)
		if err != nil {
			return
		}
		users, err = self.users.GetByUid(uids...)
	})
	if err != nil {
		return nil, err
	}
	var participants []ApiUser
	for _, usr := range users {
		pic := ""
		api_usr := ApiUser{
			Uuid:    usr.Id,
			Name:    usr.Info.Name,
			Pic:     pic,
			KeyCode: self.crypto.GetKeyCode(usr.PubKey),
		}
		participants = append(participants, api_usr)
	}
	resp := GetChatDetailsResponse{
		Id:           chat.Id.String(),
		Name:         chat.Name,
		Participants: participants,
	}
	return resp, nil
}
