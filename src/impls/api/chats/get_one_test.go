package chats_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/api/chats"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestGetChatNotFound(t *testing.T) {
	srv := factories.NewApiServer()
	chat_id := local.ChatId{IsGroup: true}
	chat_id.Id, _ = uuid.NewRandom()
	resp := srv.Call(factories.ApiRequest{
		Url: "/api/chats/info?id=" + chat_id.String(),
	})
	err := resp.ParseErr()
	assert.Equal(t, 404, err.Status)
	assert.Equal(t, "chat not found", err.Details)
}

func TestGetChatFound(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Crypto.GetKeyCodeResp = []string{"kcode"}
	chat_id := local.ChatId{IsGroup: true}
	chat_id.Id, _ = uuid.NewRandom()
	user_id, _ := uuid.NewRandom()
	srv.Db.User.Contact(
		"user",
		user_id,
		&factories.PrivKey2.Get().PublicKey,
	)
	srv.Db.Chats.Create(local.Chat{
		Id:    chat_id,
		Name:  "name",
		Users: []uuid.UUID{user_id},
	})
	resp := srv.Call(factories.ApiRequest{
		Url: "/api/chats/info?id=" + chat_id.String(),
	})
	assert.Equal(t, 200, resp.Status)
	chat := chats.GetChatDetailsResponse{}
	resp.ParseJson(&chat)
	assert.Equal(
		t,
		chats.GetChatDetailsResponse{
			Id:   chat_id.String(),
			Name: "name",
			Participants: []chats.ApiUser{{
				Uuid:    user_id,
				Name:    "user",
				Pic:     "",
				KeyCode: "kcode",
			}},
		},
		chat,
	)
}
