package chats

import (
	"dusc/src/entities/api"
	"dusc/src/entities/local"
	"dusc/src/proto"
)

type GetChatListHandler struct {
	locker proto.IDbLocker
	chats  proto.IChatRepo
}

type ApiChat struct {
	Id      string `json:"id"`
	Name    string `json:"name"`
	LastMsg string `json:"last_msg"`
	New     bool   `json:"new"`
}

type GetChatListResponse struct {
	Data []ApiChat `json:"data"`
}

func NewGetChatList(locker proto.IDbLocker, chats proto.IChatRepo) proto.IApiHandler {
	return &GetChatListHandler{
		locker: locker,
		chats:  chats,
	}
}

func (self *GetChatListHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/chats/",
		AuthTokenRequired: true,
	}
}

func (self *GetChatListHandler) Handle(query any, body any) (any, error) {
	var info []local.ChatShort
	var err error
	self.locker.Lock(func() {
		info, err = self.chats.GetShortInfoForAll()
	})
	if err != nil {
		return nil, err
	}
	resp := GetChatListResponse{}
	for _, chat := range info {
		resp.Data = append(
			resp.Data,
			ApiChat{
				Id:      chat.Id.String(),
				Name:    chat.Name,
				LastMsg: chat.LastMsg,
				New:     !chat.Read,
			},
		)
	}
	return resp, nil
}
