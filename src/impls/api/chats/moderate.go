package chats

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"

	"gitlab.com/asvedr/cldi/cl"
)

type ModerateChatHandler struct {
	locker proto.IDbLocker
	chats  proto.IChatRepo
}

type ModerateChatQuery struct {
	Id     string `name:"id"`
	Action string `name:"action"`
}

func NewModerateChat(
	locker proto.IDbLocker,
	chats proto.IChatRepo,
) proto.IApiHandler {
	return &ModerateChatHandler{locker: locker, chats: chats}
}

func (self *ModerateChatHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/chats/moderate/",
		Method:            "POST",
		AuthTokenRequired: true,
		QueryParser:       cl.MakeQueryParser[ModerateChatQuery](),
	}
}

func (self *ModerateChatHandler) Handle(a_query any, body any) (any, error) {
	query := a_query.(ModerateChatQuery)
	chat_id, err := local.StringToChatId(query.Id)
	if err != nil {
		return nil, api_err.InvalidQuery(err)
	}
	var action func()
	switch query.Action {
	case "ban":
		action = func() {
			err = self.chats.BanChat(chat_id)
		}
	case "del":
		action = func() {
			err = self.chats.Delete(chat_id)
		}
	default:
		return nil, api_err.InvalidQuery(errors.New("invalid action"))
	}
	self.locker.Lock(func() {
		_, err = self.chats.GetById(chat_id)
		if errors.As(err, &errs.NotFound{}) {
			err = api_err.NotFound("chat not found")
			return
		}
		action()
	})
	return nil, err
}
