package chats_test

import (
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func setup() (factories.ApiServer, local.ChatId) {
	srv := factories.NewApiServer()
	chat_id := local.ChatId{IsGroup: true}
	chat_id.Id, _ = uuid.NewRandom()
	srv.Db.Chats.Create(local.Chat{
		Id:    chat_id,
		Name:  "chat",
		Users: []uuid.UUID{},
	})
	return srv, chat_id
}

func TestModerateBan(t *testing.T) {
	srv, chat_id := setup()
	resp := srv.Call(factories.ApiRequest{
		Url:    "/api/chats/moderate?action=ban&id=" + chat_id.String(),
		Method: "POST",
	})
	assert.Equal(t, 200, resp.Status)
	_, err := srv.Db.Chats.GetById(chat_id)
	assert.Nil(t, err)
	// assert.True(t, chat.)
}

func TestModerateDel(t *testing.T) {
	srv, chat_id := setup()
	resp := srv.Call(factories.ApiRequest{
		Url:    "/api/chats/moderate?action=del&id=" + chat_id.String(),
		Method: "POST",
	})
	assert.Equal(t, 200, resp.Status)
	_, err := srv.Db.Chats.GetById(chat_id)
	assert.Equal(t, errs.NotFound{}, err)
}

func TestModerateInvAction(t *testing.T) {
	srv, chat_id := setup()
	resp := srv.Call(factories.ApiRequest{
		Url:    "/api/chats/moderate?action=xxx&id=" + chat_id.String(),
		Method: "POST",
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, "invalid action", resp.ParseErr().Details)
}

func TestModerateNotFound(t *testing.T) {
	srv, chat_id := setup()
	chat_id.IsGroup = false
	resp := srv.Call(factories.ApiRequest{
		Url:    "/api/chats/moderate?action=ban&id=" + chat_id.String(),
		Method: "POST",
	})
	assert.Equal(t, 404, resp.Status)
	assert.Equal(t, "chat not found", resp.ParseErr().Details)
}
