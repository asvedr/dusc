package chats

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"dusc/src/utils"
	"errors"

	"github.com/google/uuid"
)

type CreateOrUpdateChatHandler struct {
	locker proto.IDbLocker
	users  proto.IUserRepo
	chats  proto.IChatRepo
	crypto proto.ICrypto
	msg    proto.IMsgMaker
}

type CreateOrUpdateChatBody struct {
	ChatId *string     `json:"chat_id"`
	Users  []uuid.UUID `json:"users"`
	Name   string      `json:"name"`
}

type CreateOrUpdateChatResponse struct {
	Id string `json:"id"`
}

func NewCreateOrUpdateChat(
	locker proto.IDbLocker,
	users proto.IUserRepo,
	chats proto.IChatRepo,
	crypto proto.ICrypto,
	msg proto.IMsgMaker,
) proto.IApiHandler {
	return &CreateOrUpdateChatHandler{
		locker: locker,
		users:  users,
		chats:  chats,
		crypto: crypto,
		msg:    msg,
	}
}

func (self *CreateOrUpdateChatHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/chats/",
		Method:            "POST",
		BodyParserFactory: &CreateOrUpdateChatBody{},
		AuthTokenRequired: true,
	}
}

func (self *CreateOrUpdateChatHandler) Handle(query any, a_body any) (any, error) {
	body := a_body.(*CreateOrUpdateChatBody)
	if body.ChatId == nil {
		return self.create_chat(body.Name, body.Users)
	}
	id, err := local.StringToChatId(*body.ChatId)
	if err != nil {
		return nil, err
	}
	return self.update_chat(id, body.Name, body.Users)
}

func (self *CreateOrUpdateChatHandler) update_chat(
	chat_id local.ChatId,
	name string,
	new_users []uuid.UUID,
) (res CreateOrUpdateChatResponse, err error) {
	var old_users []uuid.UUID
	self.locker.Lock(func() {
		old_users, err = self.chats.GetParticipants(chat_id)
		if err != nil {
			return
		}
		_, err = self.chats.GetById(chat_id)
		if errors.As(err, &errs.NotFound{}) {
			err = api_err.NotFound("chat not found")
			return
		}
		if err != nil {
			return
		}
		err = self.chats.SetName(chat_id, name)
		if err != nil {
			return
		}
		err = self.chats.AddParticipants(chat_id, new_users)
	})
	if err != nil {
		return
	}
	all_users := utils.ConcatUniq(new_users, old_users)
	self.locker.Lock(func() {
		err = self.msg.MakeInvite(
			chat_id.Id,
			name,
			all_users,
			new_users...,
		)
		if err != nil {
			return
		}
		err = self.msg.MakeInvite(
			chat_id.Id,
			name,
			new_users,
			old_users...,
		)
	})
	res.Id = chat_id.String()
	return
}

func (self *CreateOrUpdateChatHandler) create_chat(
	name string,
	users []uuid.UUID,
) (res CreateOrUpdateChatResponse, err error) {
	var uid uuid.UUID
	uid, err = uuid.NewRandom()
	if err != nil {
		return
	}
	chat_id := local.ChatId{Id: uid, IsGroup: true}
	uid_set := utils.ConcatUniq(users)
	self.locker.Lock(func() {
		err = self.chats.Create(local.Chat{
			Id:    chat_id,
			Name:  name,
			Users: users,
		})
		if err != nil {
			return
		}
		err = self.msg.MakeInvite(
			chat_id.Id,
			name,
			uid_set,
			uid_set...,
		)
	})
	if err != nil {
		return
	}
	res.Id = chat_id.String()
	return
}
