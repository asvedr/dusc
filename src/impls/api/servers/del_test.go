package servers_test

import (
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDelOk(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Db.Servers.Add(local.Server{
		Type: local.ServerTypeSTUN,
		Addr: "abcd",
	})
	resp := srv.Call(factories.ApiRequest{
		Method: "DELETE",
		Url:    "/api/servers?addr=abcd",
	})
	assert.Equal(t, 200, resp.Status)
	println(string(resp.Body))
	_, err := srv.Db.Servers.GetByAddr("abcd")
	assert.Equal(t, errs.NotFound{}, err)
}

func TestDelNotFound(t *testing.T) {
	srv := factories.NewApiServer()
	resp := srv.Call(factories.ApiRequest{
		Method: "DELETE",
		Url:    "/api/servers?addr=abcd",
	})
	assert.Equal(t, 404, resp.Status)
}
