package servers

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"
)

type CreateServerHandler struct {
	locker       proto.IDbLocker
	srv_repo     proto.IServerRepo
	stun_factory proto.IFactory[proto.ISTUNClient]
	stor_factory proto.IFactory[proto.IExtStorage]
}

type CreateServerBody struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

var err_invalid_addr = errors.New("invalid server addr")

func NewCreateServer(
	locker proto.IDbLocker,
	srv_repo proto.IServerRepo,
	stun_factory proto.IFactory[proto.ISTUNClient],
	stor_factory proto.IFactory[proto.IExtStorage],
) proto.IApiHandler {
	return &CreateServerHandler{
		locker:       locker,
		srv_repo:     srv_repo,
		stun_factory: stun_factory,
		stor_factory: stor_factory,
	}
}

func (self *CreateServerHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Method:            "POST",
		Path:              "/api/servers/",
		BodyParserFactory: &CreateServerBody{},
		AuthTokenRequired: true,
	}
}

func (self *CreateServerHandler) Handle(query any, a_body any) (any, error) {
	body := a_body.(*CreateServerBody)
	sv_type, err := local.StringToServerType(body.Type)
	if err != nil {
		return nil, api_err.InvalidBody(err)
	}
	addr := body.Value
	switch sv_type {
	case local.ServerTypeSTUN:
		_, err = self.stun_factory.Make(addr)
	case local.ServerTypeExtStor:
		_, err = self.stor_factory.Make(addr)
	default:
		panic("sv not implemented: " + sv_type.String())
	}
	if err != nil {
		return nil, api_err.InvalidBody(err_invalid_addr)
	}
	self.locker.Lock(func() {
		_, err = self.srv_repo.GetByAddr(addr)
		if err == nil {
			err = api_err.AlreadyExists
			return
		}
		if !errors.Is(err, errs.NotFound{}) {
			return
		}
		err = self.srv_repo.Add(local.Server{
			Addr: addr,
			Type: sv_type,
		})
	})
	return nil, err
}
