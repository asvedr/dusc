package servers

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/errs"
	"dusc/src/proto"
	"errors"

	"gitlab.com/asvedr/cldi/cl"
)

type DelServerHandler struct {
	locker   proto.IDbLocker
	srv_repo proto.IServerRepo
}

type DelServerQuery struct {
	Addr string `name:"addr"`
}

func NewDelServer(
	locker proto.IDbLocker,
	srv_repo proto.IServerRepo,
) proto.IApiHandler {
	return &DelServerHandler{
		locker:   locker,
		srv_repo: srv_repo,
	}
}

func (self *DelServerHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Method:            "DELETE",
		Path:              "/api/servers/",
		QueryParser:       cl.MakeQueryParser[DelServerQuery](),
		AuthTokenRequired: true,
	}
}

func (self *DelServerHandler) Handle(a_query any, _ any) (any, error) {
	query := a_query.(DelServerQuery)
	var err error
	self.locker.Lock(func() {
		_, err = self.srv_repo.GetByAddr(query.Addr)
		if err != nil {
			return
		}
		err = self.srv_repo.Delete(query.Addr)
	})
	if errors.Is(err, errs.NotFound{}) {
		err = api_err.NotFound("server not found")
	}
	return nil, err
}
