package servers_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateInvalidSrvType(t *testing.T) {
	srv := factories.NewApiServer()
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/servers/",
		Json:   map[string]any{"type": "123", "value": "abcd"},
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, "INVALID_BODY", resp.ParseErr().Code)
	assert.Equal(t, "invalid server type", resp.ParseErr().Details)
	assert.Nil(t, srv.StunFactory.MakeCalls)
	assert.Nil(t, srv.ExtStorageFactory.MakeCalls)
}

func TestCreateInvalidStun(t *testing.T) {
	srv := factories.NewApiServer()
	srv.StunFactory.MakeResp = []any{
		errors.New("bad stun"),
	}
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/servers/",
		Json: map[string]any{
			"type":  "stun",
			"value": "abcd",
		},
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, "INVALID_BODY", resp.ParseErr().Code)
	assert.Equal(t, "invalid server addr", resp.ParseErr().Details)
	assert.Equal(t, []string{"abcd"}, srv.StunFactory.MakeCalls)
	got, err := srv.Db.Servers.GetByType(local.ServerTypeSTUN)
	assert.Nil(t, err)
	assert.Equal(t, 0, len(got))
}

func TestCreateInvalidExtStor(t *testing.T) {
	srv := factories.NewApiServer()
	srv.ExtStorageFactory.MakeResp = []any{
		errors.New("bad stor"),
	}
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/servers/",
		Json: map[string]any{
			"type":  "stor",
			"value": "abcd",
		},
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, "INVALID_BODY", resp.ParseErr().Code)
	assert.Equal(t, "invalid server addr", resp.ParseErr().Details)
	assert.Equal(t, []string{"abcd"}, srv.ExtStorageFactory.MakeCalls)
	got, err := srv.Db.Servers.GetByType(local.ServerTypeExtStor)
	assert.Nil(t, err)
	assert.Equal(t, 0, len(got))
}

func TestCreateStunOk(t *testing.T) {
	srv := factories.NewApiServer()
	srv.StunFactory.MakeResp = []any{&factories.Stun{}}
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/servers/",
		Json: map[string]any{
			"type":  "stun",
			"value": "abcd",
		},
	})
	assert.Equal(t, 200, resp.Status)
	got, err := srv.Db.Servers.GetByType(local.ServerTypeSTUN)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(got))
	assert.Equal(t, "abcd", got[0].Addr)
}

func TestCreateExtStorOk(t *testing.T) {
	srv := factories.NewApiServer()
	srv.ExtStorageFactory.MakeResp = []any{&factories.ExtStorage{}}
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/servers/",
		Json: map[string]any{
			"type":  "stor",
			"value": "abcd",
		},
	})
	assert.Equal(t, 200, resp.Status)
	got, err := srv.Db.Servers.GetByType(local.ServerTypeExtStor)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(got))
	assert.Equal(t, "abcd", got[0].Addr)
}

func TestCreateDuplicate(t *testing.T) {
	srv := factories.NewApiServer()
	srv.StunFactory.MakeResp = []any{
		&factories.Stun{},
		&factories.Stun{},
	}
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/servers/",
		Json: map[string]any{
			"type":  "stun",
			"value": "abcd",
		},
	})
	assert.Equal(t, 200, resp.Status)
	resp = srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/servers/",
		Json: map[string]any{
			"type":  "stun",
			"value": "abcd",
		},
	})
	assert.Equal(t, 409, resp.Status)
	assert.Equal(t, "ALREADY_EXISTS", resp.ParseErr().Code)
}
