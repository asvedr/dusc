package servers

import "dusc/src/proto"

func MakeHandlers(
	locker proto.IDbLocker,
	srv_repo proto.IServerRepo,
	stun_factory proto.IFactory[proto.ISTUNClient],
	stor_factory proto.IFactory[proto.IExtStorage],
	server_status proto.IServerStatusStorage,
) []proto.IApiHandler {
	return []proto.IApiHandler{
		NewCreateServer(
			locker,
			srv_repo,
			stun_factory,
			stor_factory,
		),
		NewGetServers(locker, srv_repo, server_status),
		NewDelServer(locker, srv_repo),
	}
}
