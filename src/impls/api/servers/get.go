package servers

import (
	"dusc/src/entities/api"
	"dusc/src/entities/local"
	"dusc/src/proto"

	"gitlab.com/asvedr/cldi/cl"
)

type GetServersHandler struct {
	locker        proto.IDbLocker
	servers       proto.IServerRepo
	server_status proto.IServerStatusStorage
}

type GetServersQuery struct {
	Type string `name:"type"`
}

type ApiServer struct {
	Addr   string `json:"addr"`
	Status string `json:"status"`
}

type GetServersResponse struct {
	Data []ApiServer `json:"data"`
}

func NewGetServers(
	locker proto.IDbLocker,
	servers proto.IServerRepo,
	server_status proto.IServerStatusStorage,
) proto.IApiHandler {
	return &GetServersHandler{
		locker:        locker,
		servers:       servers,
		server_status: server_status,
	}
}

func (self *GetServersHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Method:            "GET",
		Path:              "/api/servers/",
		QueryParser:       cl.MakeQueryParser[GetServersQuery](),
		AuthTokenRequired: true,
	}
}

func (self *GetServersHandler) Handle(a_query any, _ any) (any, error) {
	query := a_query.(GetServersQuery)
	tp, err := local.StringToServerType(query.Type)
	if err != nil {
		return nil, err
	}
	var servers []local.Server
	self.locker.Lock(func() {
		servers, err = self.servers.GetByType(tp)
	})
	statuses := self.server_status.GetStatuses()
	res := GetServersResponse{Data: []ApiServer{}}
	for _, srv := range servers {
		status := statuses[srv.Addr]
		api_srv := ApiServer{
			Addr:   srv.Addr,
			Status: status.String(),
		}
		res.Data = append(res.Data, api_srv)
	}
	return res, nil
}
