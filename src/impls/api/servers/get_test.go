package servers_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/api/servers"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetServersEmpty(t *testing.T) {
	srv := factories.NewApiServer()
	res := srv.Call(factories.ApiRequest{
		Method: "GET",
		Url:    "/api/servers?type=stun",
	})
	assert.Equal(t, 200, res.Status)
	data := servers.GetServersResponse{}
	res.ParseJson(&data)
	assert.Equal(t, 0, len(data.Data))

	res = srv.Call(factories.ApiRequest{
		Method: "GET",
		Url:    "/api/servers?type=stor",
	})
	assert.Equal(t, 200, res.Status)
	data = servers.GetServersResponse{}
	res.ParseJson(&data)
	assert.Equal(t, 0, len(data.Data))
}

func TestGetServersStatusUnknown(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Db.Servers.Add(local.Server{
		Type: local.ServerTypeSTUN,
		Addr: "abcd",
	})

	res := srv.Call(factories.ApiRequest{
		Method: "GET",
		Url:    "/api/servers?type=stun",
	})
	assert.Equal(t, 200, res.Status)
	data := servers.GetServersResponse{}
	res.ParseJson(&data)
	assert.Equal(
		t,
		[]servers.ApiServer{{Addr: "abcd", Status: "unknown"}},
		data.Data,
	)

	res = srv.Call(factories.ApiRequest{
		Method: "GET",
		Url:    "/api/servers?type=stor",
	})
	assert.Equal(t, 200, res.Status)
	data = servers.GetServersResponse{}
	res.ParseJson(&data)
	assert.Equal(t, []servers.ApiServer{}, data.Data)
}

func TestGetServersStatusKnown(t *testing.T) {
	srv := factories.NewApiServer()
	srv.Db.Servers.Add(local.Server{
		Type: local.ServerTypeSTUN,
		Addr: "abcd",
	})
	srv.ServerStatusStorage.SetStatuses(
		map[string]local.ServerStatus{
			"abcd": local.ServerStatusOk,
		},
	)

	res := srv.Call(factories.ApiRequest{
		Method: "GET",
		Url:    "/api/servers?type=stun",
	})
	assert.Equal(t, 200, res.Status)
	data := servers.GetServersResponse{}
	res.ParseJson(&data)
	assert.Equal(
		t,
		[]servers.ApiServer{{Addr: "abcd", Status: "ok"}},
		data.Data,
	)

}
