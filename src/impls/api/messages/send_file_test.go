package messages_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"encoding/base64"
	"errors"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestSendFileBadBody(t *testing.T) {
	srv := factories.NewApiServer()

	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/msg/file/",
		Json:   map[string]any{},
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, "invalid chat id", resp.ParseErr().Details)

	var chat_id local.ChatId
	chat_id.Id, _ = uuid.NewRandom()
	srv.Db.Chats.Create(local.Chat{Id: chat_id})
	resp = srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/msg/file/",
		Json: map[string]any{
			"chat": chat_id.String(),
			"name": "name",
		},
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, "file not set", resp.ParseErr().Details)
	files, err := srv.Db.Files.GetRegisteredFiles()
	assert.Nil(t, err)
	assert.Equal(t, 0, len(files))
}

func TestSendFileByPathOk(t *testing.T) {
	srv := factories.NewApiServer()
	var chat_id local.ChatId
	chat_id.Id, _ = uuid.NewRandom()
	user, _ := uuid.NewRandom()
	srv.Db.User.Contact("user", user, &factories.PrivKey1.Get().PublicKey)
	srv.Db.Chats.Create(local.Chat{Id: chat_id, Users: []uuid.UUID{user}})
	srv.TaskExecutor.ExecuteNowResp = []error{nil}
	srv.Fs.GetMetaResp = []any{
		local.FSFileMeta{Blocks: 10, Size: 100},
	}

	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/msg/file/",
		Json: map[string]any{
			"chat": chat_id.String(),
			"path": "/path/to/file",
			"name": "file",
		},
	})
	assert.Equal(t, 200, resp.Status)

	files, err := srv.Db.Files.GetRegisteredFiles()
	assert.Nil(t, err)
	assert.Equal(t, 1, len(files))
	file := files[0]
	assert.Equal(t, 10, file.Blocks)
	assert.Equal(t, int64(100), file.Size)
	assert.Equal(t, "/path/to/file", file.Path)
	assert.Equal(t, "file", file.Name)
	assert.Equal(t, []uuid.UUID{user}, file.Users)
}

func TestSendFileByPathNotFound(t *testing.T) {
	srv := factories.NewApiServer()
	var chat_id local.ChatId
	chat_id.Id, _ = uuid.NewRandom()
	user, _ := uuid.NewRandom()
	srv.Db.User.Contact("user", user, &factories.PrivKey1.Get().PublicKey)
	srv.Db.Chats.Create(local.Chat{Id: chat_id, Users: []uuid.UUID{user}})
	srv.Fs.GetMetaResp = []any{errors.New("<file-not-found>")}

	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/msg/file/",
		Json: map[string]any{
			"chat": chat_id.String(),
			"path": "/path/to/file",
			"name": "file",
		},
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, "CAN_NOT_LOAD_FILE", resp.ParseErr().Code)
	assert.Equal(t, "<file-not-found>", resp.ParseErr().Details)
}

func TestSendFileByData(t *testing.T) {
	srv := factories.NewApiServer()
	srv.TaskExecutor.ExecuteNowResp = []error{nil}
	var chat_id local.ChatId
	chat_id.Id, _ = uuid.NewRandom()
	user, _ := uuid.NewRandom()
	srv.Db.User.Contact("user", user, &factories.PrivKey1.Get().PublicKey)
	srv.Db.Chats.Create(local.Chat{Id: chat_id, Users: []uuid.UUID{user}})
	srv.Fs.GetMetaResp = []any{
		local.FSFileMeta{Blocks: 10, Size: 100},
	}
	srv.Fs.WriteFileResp = []error{nil}
	bts := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/msg/file/",
		Json: map[string]any{
			"chat": chat_id.String(),
			"data": base64.StdEncoding.EncodeToString(bts),
			"name": "file",
		},
	})
	assert.Equal(t, 200, resp.Status)

	files, err := srv.Db.Files.GetRegisteredFiles()
	assert.Nil(t, err)
	assert.Equal(t, 1, len(files))
	file := files[0]
	assert.Equal(t, 10, file.Blocks)
	assert.Equal(t, int64(100), file.Size)
	assert.Equal(t, "/upload/"+file.Id.String(), file.Path)
	assert.Equal(t, "file", file.Name)
	assert.Equal(t, []uuid.UUID{user}, file.Users)
	data := srv.Fs.WriteFileData["/upload/"+file.Id.String()]
	assert.Equal(t, bts, data)

	msgs, err := srv.Db.Msg.GetChatMsgs(chat_id, nil, 10, true)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(msgs))
	msg := msgs[0]
	assert.Nil(t, msg.Sender)
	assert.Equal(t, &user, msg.Receiver)
	assert.Equal(t, chat_id, msg.Chat)
	assert.True(t, msg.Visible)
	assert.Equal(t, "file", msg.Text)
	assert.False(t, msg.Received)
	assert.True(t, msg.Read)
	assert.Equal(t, srv.Time.Now(), msg.CreatedAt)
	assert.Equal(t, srv.Time.Now(), msg.ReceivedAt)
	assert.Nil(t, msg.AttachInvite)
	assert.Equal(
		t,
		&local.AttachFileMeta{
			FileId:    file.Id,
			LocalPath: "/upload/" + file.Id.String(),
			Parts:     10,
			Size:      100,
		},
		msg.AttachFileMeta,
	)
}

func TestSendFileCanNotSave(t *testing.T) {
	srv := factories.NewApiServer()
	var chat_id local.ChatId
	chat_id.Id, _ = uuid.NewRandom()
	user, _ := uuid.NewRandom()
	srv.Db.User.Contact("user", user, &factories.PrivKey1.Get().PublicKey)
	srv.Db.Chats.Create(local.Chat{Id: chat_id, Users: []uuid.UUID{user}})
	srv.Fs.GetMetaResp = []any{
		local.FSFileMeta{Blocks: 10, Size: 100},
	}
	srv.Fs.WriteFileResp = []error{errors.New("can not save")}
	bts := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/msg/file/",
		Json: map[string]any{
			"chat": chat_id.String(),
			"data": base64.StdEncoding.EncodeToString(bts),
			"name": "file",
		},
	})
	assert.Equal(t, 400, resp.Status)
	assert.Equal(t, "CAN_NOT_SAVE_FILE", resp.ParseErr().Code)
	assert.Equal(t, "can not save", resp.ParseErr().Details)
}
