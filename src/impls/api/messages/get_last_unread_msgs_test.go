package messages_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/api/messages"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestGetLastUnreadNone(t *testing.T) {
	srv := factories.NewApiServer()
	resp := srv.Call(factories.ApiRequest{
		Method: "GET",
		Url:    "/api/msgs/last-unread/",
	})
	assert.Equal(t, 200, resp.Status)
	var data, exp messages.GetLastUnreadMsgsResponse
	resp.ParseJson(&data)
	assert.Equal(t, exp, data)
}

func TestGetLastUnreadSome(t *testing.T) {
	srv := factories.NewApiServer()
	sender, _ := uuid.NewRandom()
	msg1, _ := uuid.NewRandom()
	msg2, _ := uuid.NewRandom()
	srv.Db.Msg.Create(local.NewMsg{
		Sender:     &sender,
		Chat:       local.ChatId{Id: sender},
		Visible:    true,
		Uid:        msg1,
		Text:       "abc",
		Received:   true,
		CreatedAt:  1,
		ReceivedAt: 1,
	})
	srv.Db.Msg.Create(local.NewMsg{
		Sender:     &sender,
		Chat:       local.ChatId{Id: sender},
		Visible:    true,
		Uid:        msg2,
		Text:       "def",
		Read:       true,
		Received:   true,
		CreatedAt:  2,
		ReceivedAt: 2,
	})
	resp := srv.Call(factories.ApiRequest{
		Method: "GET",
		Url:    "/api/msgs/last-unread/",
	})
	assert.Equal(t, 200, resp.Status)
	var data messages.GetLastUnreadMsgsResponse
	resp.ParseJson(&data)
	exp := messages.GetLastUnreadMsgsResponse{
		Data: []messages.ApiUnreadMsg{{
			Id:   1,
			Uuid: msg1,
			User: sender,
			Text: "abc",
			Chat: local.ChatId{Id: sender}.String(),
		}},
	}
	assert.Equal(t, exp, data)
}
