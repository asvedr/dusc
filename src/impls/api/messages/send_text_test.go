package messages_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestSendTextOk(t *testing.T) {
	srv := factories.NewApiServer()
	srv.TaskExecutor.ExecuteNowResp = []error{nil}
	user, _ := uuid.NewRandom()
	chat_id := local.ChatId{Id: user}
	srv.Db.User.Contact("user", user, &factories.PrivKey1.Get().PublicKey)
	srv.Db.Chats.Create(local.Chat{Id: chat_id, Users: []uuid.UUID{user}})
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/msgs/text/",
		Json: map[string]any{
			"chat": chat_id.String(),
			"text": "Hello world",
		},
	})
	assert.Equal(t, 200, resp.Status)
	msgs, err := srv.Db.Msg.GetChatMsgs(chat_id, nil, 10, true)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(msgs))
	msg := msgs[0]

	assert.Nil(t, msg.Sender)
	assert.Equal(t, &user, msg.Receiver)
	assert.Equal(t, chat_id, msg.Chat)
	assert.True(t, msg.Visible)
	assert.Equal(t, "Hello world", msg.Text)
	assert.False(t, msg.Received)
	assert.True(t, msg.Read)
	assert.Equal(t, srv.Time.Now(), msg.CreatedAt)
	assert.Equal(t, srv.Time.Now(), msg.ReceivedAt)
	assert.Nil(t, msg.AttachInvite)
	assert.Nil(t, msg.AttachFileMeta)
}
