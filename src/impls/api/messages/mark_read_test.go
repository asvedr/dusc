package messages_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestMarkReadOk(t *testing.T) {
	srv := factories.NewApiServer()
	sender, _ := uuid.NewRandom()
	msg, _ := uuid.NewRandom()
	srv.Db.Msg.Create(local.NewMsg{
		Sender:   &sender,
		Uid:      msg,
		Text:     "text",
		Received: true,
		Read:     false,
	})
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/msgs/mark-read/",
		Json:   map[string]any{"msgs": []any{msg}},
	})
	assert.Equal(t, 200, resp.Status)
	got, err := srv.Db.Msg.GetByUid(msg)
	assert.Nil(t, err)
	assert.True(t, got.Read)
}

func TestMarkReadNotFound(t *testing.T) {
	srv := factories.NewApiServer()
	msg, _ := uuid.NewRandom()
	resp := srv.Call(factories.ApiRequest{
		Method: "POST",
		Url:    "/api/msgs/mark-read/",
		Json:   map[string]any{"msgs": []any{msg}},
	})
	assert.Equal(t, 200, resp.Status)
}
