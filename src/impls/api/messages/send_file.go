package messages

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"encoding/base64"
	"errors"
	"path"
	"strings"

	"github.com/google/uuid"
)

type SendFileMsgHandler struct {
	locker     proto.IDbLocker
	chats      proto.IChatRepo
	files      proto.ISharedFileRepo
	msg_maker  proto.IMsgMaker
	fs         proto.IFS
	upload_dir string
}

type SendFileMsgBody struct {
	Chat string  `json:"chat"`
	Path *string `json:"path"`
	Data *string `json:"data"`
	Name string  `json:"name"`
}

func NewSendFileMsg(
	locker proto.IDbLocker,
	chats proto.IChatRepo,
	files proto.ISharedFileRepo,
	msg_maker proto.IMsgMaker,
	fs proto.IFS,
	upload_dir string,
) proto.IApiHandler {
	return &SendFileMsgHandler{
		locker:     locker,
		chats:      chats,
		files:      files,
		msg_maker:  msg_maker,
		fs:         fs,
		upload_dir: upload_dir,
	}
}

func (self *SendFileMsgHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Method:            "POST",
		Path:              "/api/msg/file/",
		BodyParserFactory: &SendFileMsgBody{},
		AuthTokenRequired: true,
	}
}

var err_invalid_chat_id = errors.New("invalid chat id")
var err_file_not_set = errors.New("file not set")
var err_invalid_base_64 = errors.New("invalid base 64")

func (self *SendFileMsgHandler) Handle(query any, a_body any) (any, error) {
	body := a_body.(*SendFileMsgBody)
	chat_id, err := local.StringToChatId(body.Chat)
	if err != nil {
		return nil, api_err.InvalidBody(err_invalid_chat_id)
	}
	file_id, err := uuid.NewRandom()
	if err != nil {
		return nil, err
	}
	var meta *local.FileMeta
	if body.Path != nil {
		meta, err = self.register_file_via_path(*body.Path, body.Name, file_id)
	} else if body.Data != nil {
		meta, err = self.register_file_via_data(*body.Data, body.Name, file_id)
	} else {
		return nil, api_err.InvalidBody(err_file_not_set)
	}
	if err != nil {
		return nil, err
	}
	var users []uuid.UUID
	self.locker.Lock(func() {
		users, err = self.chats.GetParticipants(chat_id)
		if err != nil {
			return
		}
		if len(users) == 0 {
			err = api_err.Internal("chat has no users")
			return
		}
		err = self.files.RegisterFile(*meta, users)
		if err != nil {
			return
		}
		err = self.msg_maker.MakeFileMeta(
			chat_id,
			meta.Id,
			meta.Name,
			meta.Path,
			int16(meta.Blocks),
			uint64(meta.Size),
			users...,
		)
	})
	return nil, err
}

func (self *SendFileMsgHandler) register_file_via_path(
	path string,
	name string,
	file_id uuid.UUID,
) (*local.FileMeta, error) {
	fs_meta, err := self.fs.GetMeta(path)
	if err != nil {
		return nil, api_err.CanNotLoadFile(err)
	}
	meta := &local.FileMeta{
		FSFileMeta: fs_meta,
		Id:         file_id,
		Path:       path,
		Name:       name,
	}
	return meta, nil
}

func (self *SendFileMsgHandler) register_file_via_data(
	data string,
	name string,
	file_id uuid.UUID,
) (*local.FileMeta, error) {
	normalized := strings.ReplaceAll(data, "\n", "")
	bts, err := base64.StdEncoding.DecodeString(normalized)
	if err != nil {
		return nil, api_err.InvalidBody(err_invalid_base_64)
	}
	file_path := path.Join(self.upload_dir, file_id.String())
	err = self.fs.WriteFile(file_path, bts)
	if err != nil {
		return nil, api_err.CanNotSaveFile(err)
	}
	return self.register_file_via_path(file_path, name, file_id)
}
