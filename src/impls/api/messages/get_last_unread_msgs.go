package messages

import (
	"dusc/src/entities/api"
	"dusc/src/entities/local"
	"dusc/src/proto"

	"github.com/google/uuid"
)

const get_last_unread_msgs_limit = 5

type GetLastUnreadMsgsHandler struct {
	locker proto.IDbLocker
	msgs   proto.IMsgRepo
}

type ApiUnreadMsg struct {
	Id   int       `json:"id"`
	Uuid uuid.UUID `json:"uuid"`
	User uuid.UUID `json:"user"`
	Text string    `json:"text"`
	Chat string    `json:"chat"`
}

type GetLastUnreadMsgsResponse struct {
	Data []ApiUnreadMsg `json:"data"`
}

func NewGetLastUnreadMsgs(locker proto.IDbLocker, msgs proto.IMsgRepo) proto.IApiHandler {
	return &GetLastUnreadMsgsHandler{locker: locker, msgs: msgs}
}

func (self *GetLastUnreadMsgsHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path:              "/api/msgs/last-unread/",
		AuthTokenRequired: true,
	}
}

func (self *GetLastUnreadMsgsHandler) Handle(query any, body any) (any, error) {
	var msgs []*local.Msg
	var err error
	self.locker.Lock(func() {
		msgs, err = self.msgs.GetLastUnreadMessages(get_last_unread_msgs_limit)
	})
	if err != nil {
		return nil, err
	}
	resp := GetLastUnreadMsgsResponse{}
	for _, msg := range msgs {
		if msg.Sender == nil {
			continue
		}
		api_msg := ApiUnreadMsg{
			Id:   msg.Id,
			Uuid: msg.Uid,
			User: *msg.Sender,
			Text: msg.Text,
			Chat: msg.Chat.String(),
		}
		resp.Data = append(resp.Data, api_msg)
	}
	return resp, nil
}
