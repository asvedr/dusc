package messages

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/impls/api/common"
	"dusc/src/proto"
	"errors"

	"github.com/google/uuid"
	"gitlab.com/asvedr/cldi/cl"
)

const get_messages_limit = 20

type GetMessagesHandler struct {
	locker proto.IDbLocker
	chats  proto.IChatRepo
	msgs   proto.IMsgRepo
}

// Set "last_msg" OR "last_msg_uid" to paginate result
type GetMessagesQuery struct {
	Chat       string     `name:"chat"`
	LastMsg    *int       `name:"last_msg" opt:"t"`
	LastMsgUid *uuid.UUID `name:"last_msg_uid" opt:"t"`
}

type GetMessagesDecFile struct {
	Name   string     `json:"name"`
	Size   uint64     `json:"size"`
	UserId *uuid.UUID `json:"user_id"`
	FileId uuid.UUID  `json:"file_id"`
}

type GetMessagesApiMsg struct {
	Id   int                 `json:"id"`
	Uuid uuid.UUID           `json:"uuid"`
	User *uuid.UUID          `json:"user"`
	Text string              `json:"text"`
	File *GetMessagesDecFile `json:"file"`
	// obsolete
	Sent     bool `json:"sent"`
	Read     bool `json:"read"`
	Received bool `json:"received"`
}

type GetMessagesResponse struct {
	Data []GetMessagesApiMsg `json:"data"`
}

func NewGetMessages(
	locker proto.IDbLocker,
	chats proto.IChatRepo,
	msgs proto.IMsgRepo,
) proto.IApiHandler {
	return &GetMessagesHandler{
		locker: locker,
		chats:  chats,
		msgs:   msgs,
	}
}

func (self *GetMessagesHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Path: "/api/chats/msgs/",
		QueryParser: cl.MakeQueryParser[GetMessagesQuery](
			common.QParserCtx,
		),
		AuthTokenRequired: true,
	}
}

func (self *GetMessagesHandler) Handle(a_query any, body any) (any, error) {
	query := a_query.(GetMessagesQuery)
	chat_id, err := local.StringToChatId(query.Chat)
	if err != nil {
		return nil, api_err.InvalidQuery(err)
	}
	var msgs []*local.Msg
	self.locker.Lock(func() {
		_, err = self.chats.GetById(chat_id)
		if errors.Is(err, errs.NotFound{}) {
			err = api_err.NotFound("chat not found")
			return
		}
		msgs, err = self.get_chat_msgs(chat_id, query)
	})
	if err != nil {
		return nil, err
	}
	resp := GetMessagesResponse{}
	for _, msg := range msgs {
		var dec_file *GetMessagesDecFile
		if msg.AttachFileMeta != nil {
			dec_file = &GetMessagesDecFile{
				Name:   msg.Text,
				Size:   msg.AttachFileMeta.Size,
				UserId: msg.Sender,
				FileId: msg.AttachFileMeta.FileId,
			}
		}
		text := msg.Text
		if dec_file != nil {
			text = ""
		}
		api_msg := GetMessagesApiMsg{
			Id:       msg.Id,
			Uuid:     msg.Uid,
			User:     msg.Sender,
			Text:     text,
			File:     dec_file,
			Read:     msg.Read,
			Received: msg.Received,
		}
		resp.Data = append(resp.Data, api_msg)
	}
	return resp, nil
}

func (self *GetMessagesHandler) get_chat_msgs(
	chat_id local.ChatId,
	query GetMessagesQuery,
) ([]*local.Msg, error) {
	if query.LastMsg != nil {
		return self.msgs.GetChatMsgsByIntId(
			chat_id,
			query.LastMsg,
			get_messages_limit,
			true,
		)
	}
	return self.msgs.GetChatMsgs(
		chat_id,
		query.LastMsgUid,
		get_messages_limit,
		true,
	)
}
