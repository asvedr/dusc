package messages

import "dusc/src/proto"

func MakeHandlers(
	locker proto.IDbLocker,
	msgs proto.IMsgRepo,
	chats proto.IChatRepo,
	files proto.ISharedFileRepo,
	msg_maker proto.IMsgMaker,
	fs proto.IFS,
	upload_dir string,
) []proto.IApiHandler {
	return []proto.IApiHandler{
		NewGetLastUnreadMsgs(locker, msgs),
		NewGetMessages(locker, chats, msgs),
		NewMarkReadMsg(locker, msgs),
		NewSendFileMsg(locker, chats, files, msg_maker, fs, upload_dir),
		NewSendTextMsg(locker, msg_maker, chats),
	}
}
