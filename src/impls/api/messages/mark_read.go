package messages

import (
	"dusc/src/entities/api"
	"dusc/src/proto"

	"github.com/google/uuid"
)

type MarkReadMsgHandler struct {
	locker proto.IDbLocker
	msgs   proto.IMsgRepo
}

type MarkReadMsgBody struct {
	Msgs []uuid.UUID `json:"msgs"`
}

func NewMarkReadMsg(locker proto.IDbLocker, msgs proto.IMsgRepo) proto.IApiHandler {
	return &MarkReadMsgHandler{locker: locker, msgs: msgs}
}

func (self *MarkReadMsgHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Method:            "POST",
		Path:              "/api/msgs/mark-read/",
		BodyParserFactory: &MarkReadMsgBody{},
		AuthTokenRequired: true,
	}
}

func (self *MarkReadMsgHandler) Handle(query any, a_body any) (any, error) {
	body := a_body.(*MarkReadMsgBody)
	var err error
	self.locker.Lock(func() {
		err = self.msgs.MarkRead(body.Msgs...)
	})
	return nil, err
}
