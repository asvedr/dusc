package messages

import (
	"dusc/src/entities/api"
	"dusc/src/entities/local"
	"dusc/src/proto"

	"github.com/google/uuid"
)

type SendTextMsgHandler struct {
	locker    proto.IDbLocker
	msg_maker proto.IMsgMaker
	chats     proto.IChatRepo
}

type SendTextMsgBody struct {
	Chat string `json:"chat"`
	Text string `json:"text"`
}

func NewSendTextMsg(
	locker proto.IDbLocker,
	msg_maker proto.IMsgMaker,
	chats proto.IChatRepo,
) proto.IApiHandler {
	return &SendTextMsgHandler{
		locker:    locker,
		msg_maker: msg_maker,
		chats:     chats,
	}
}

func (self *SendTextMsgHandler) Schema() api.HandlerSchema {
	return api.HandlerSchema{
		Method:            "POST",
		Path:              "/api/msgs/text/",
		BodyParserFactory: &SendTextMsgBody{},
		AuthTokenRequired: true,
	}
}

func (self *SendTextMsgHandler) Handle(query any, a_body any) (any, error) {
	body := a_body.(*SendTextMsgBody)
	chat_id, err := local.StringToChatId(body.Chat)
	if err != nil {
		return nil, err
	}
	var users []uuid.UUID
	self.locker.Lock(func() {
		users, err = self.chats.GetParticipants(chat_id)
		if err != nil {
			return
		}
		err = self.msg_maker.MakeText(chat_id, body.Text, users...)
	})
	return nil, err
}
