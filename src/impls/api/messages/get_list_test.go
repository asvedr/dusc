package messages_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/api/messages"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestGetMsgsNotFound(t *testing.T) {
	srv := factories.NewApiServer()
	var chat_id local.ChatId
	chat_id.Id, _ = uuid.NewRandom()
	resp := srv.Call(factories.ApiRequest{
		Url: "/api/chats/msgs?chat=" + chat_id.String(),
	})
	assert.Equal(t, 404, resp.Status)
	assert.Equal(t, "chat not found", resp.ParseErr().Details)
}

func TestGetMsgsEmpty(t *testing.T) {
	srv := factories.NewApiServer()
	var chat_id local.ChatId
	chat_id.Id, _ = uuid.NewRandom()
	srv.Db.Chats.Create(local.Chat{Id: chat_id})
	resp := srv.Call(factories.ApiRequest{
		Url: "/api/chats/msgs?chat=" + chat_id.String(),
	})
	assert.Equal(t, 200, resp.Status)
	data := messages.GetMessagesResponse{}
	resp.ParseJson(&data)
	assert.Equal(
		t,
		messages.GetMessagesResponse{},
		data,
	)
}

func TestGetMsgsData(t *testing.T) {
	srv := factories.NewApiServer()
	var chat_id local.ChatId
	chat_id.Id, _ = uuid.NewRandom()
	user_id, _ := uuid.NewRandom()
	msg1, _ := uuid.NewRandom()
	msg2, _ := uuid.NewRandom()
	srv.Db.Chats.Create(local.Chat{Id: chat_id})
	srv.Db.Msg.Create(local.NewMsg{
		Sender:     &user_id,
		Chat:       chat_id,
		Visible:    true,
		Uid:        msg1,
		Text:       "Hello",
		Received:   true,
		Read:       true,
		CreatedAt:  1,
		ReceivedAt: 1,
	})
	srv.Db.Msg.Create(local.NewMsg{
		Receiver:   &user_id,
		Chat:       chat_id,
		Visible:    true,
		Uid:        msg2,
		Text:       "World",
		Received:   false,
		Read:       false,
		CreatedAt:  2,
		ReceivedAt: 2,
	})
	resp := srv.Call(factories.ApiRequest{
		Url: "/api/chats/msgs?chat=" + chat_id.String(),
	})
	assert.Equal(t, 200, resp.Status)
	data := messages.GetMessagesResponse{}
	resp.ParseJson(&data)
	exp := messages.GetMessagesResponse{
		Data: []messages.GetMessagesApiMsg{
			{
				Id:       1,
				Uuid:     msg1,
				User:     &user_id,
				Text:     "Hello",
				Read:     true,
				Received: true,
			},
			{
				Id:       2,
				Uuid:     msg2,
				Text:     "World",
				Read:     false,
				Received: false,
			},
		},
	}
	assert.Equal(t, exp, data)
}

func TestGetMsgsFile(t *testing.T) {
	srv := factories.NewApiServer()
	var chat_id local.ChatId
	chat_id.Id, _ = uuid.NewRandom()
	user_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()
	msg, _ := uuid.NewRandom()
	srv.Db.Chats.Create(local.Chat{Id: chat_id})
	attach := &local.AttachFileMeta{FileId: file_id, Parts: 10, Size: 100}
	srv.Db.Msg.Create(local.NewMsg{
		Sender:         &user_id,
		Chat:           chat_id,
		Visible:        true,
		Uid:            msg,
		Text:           "Hello",
		CreatedAt:      1,
		ReceivedAt:     1,
		AttachFileMeta: attach,
	})
	resp := srv.Call(factories.ApiRequest{
		Url: "/api/chats/msgs?chat=" + chat_id.String(),
	})
	assert.Equal(t, 200, resp.Status)
	data := messages.GetMessagesResponse{}
	resp.ParseJson(&data)
	exp := messages.GetMessagesResponse{
		Data: []messages.GetMessagesApiMsg{{
			Id:   1,
			Uuid: msg,
			User: &user_id,
			File: &messages.GetMessagesDecFile{
				Name:   "Hello",
				Size:   100,
				UserId: &user_id,
				FileId: file_id,
			},
		}},
	}
	assert.Equal(t, exp, data)
}

func TestGetMsgsFilePaginationById(t *testing.T) {
	srv := factories.NewApiServer()
	var chat_id local.ChatId
	chat_id.Id, _ = uuid.NewRandom()
	srv.Db.Chats.Create(local.Chat{Id: chat_id})
	var msg_ids []uuid.UUID
	for i := range 50 {
		msg_id, _ := uuid.NewRandom()
		srv.Db.Msg.Create(local.NewMsg{
			Chat:       chat_id,
			Visible:    true,
			Uid:        msg_id,
			CreatedAt:  uint64(i),
			ReceivedAt: uint64(i),
		})
		msg_ids = append(msg_ids, msg_id)
	}
	req := factories.ApiRequest{
		Url: fmt.Sprintf(
			"/api/chats/msgs?chat=%s&last_msg=40",
			chat_id.String(),
		),
	}
	resp := srv.Call(req)
	assert.Equal(t, 200, resp.Status)
	data := messages.GetMessagesResponse{}
	resp.ParseJson(&data)
	exp := messages.GetMessagesResponse{}
	for i := range 20 {
		id := 20 + i
		uid := msg_ids[19+i]
		msg := messages.GetMessagesApiMsg{Id: id, Uuid: uid}
		exp.Data = append(exp.Data, msg)
	}
	assert.Equal(t, exp.Data, data.Data)
}

func TestGetMsgsFilePaginationByUid(t *testing.T) {
	srv := factories.NewApiServer()
	var chat_id local.ChatId
	chat_id.Id, _ = uuid.NewRandom()
	srv.Db.Chats.Create(local.Chat{Id: chat_id})
	var msg_ids []uuid.UUID
	for i := range 50 {
		msg_id, _ := uuid.NewRandom()
		srv.Db.Msg.Create(local.NewMsg{
			Chat:       chat_id,
			Visible:    true,
			Uid:        msg_id,
			CreatedAt:  uint64(i),
			ReceivedAt: uint64(i),
		})
		msg_ids = append(msg_ids, msg_id)
	}
	req := factories.ApiRequest{
		Url: fmt.Sprintf(
			"/api/chats/msgs?chat=%s&last_msg_uid=%s",
			chat_id.String(),
			msg_ids[40].String(),
		),
	}
	resp := srv.Call(req)
	assert.Equal(t, 200, resp.Status)
	data := messages.GetMessagesResponse{}
	resp.ParseJson(&data)
	exp := messages.GetMessagesResponse{}
	for i := range 20 {
		id := 20 + i + 1
		uid := msg_ids[20+i]
		msg := messages.GetMessagesApiMsg{Id: id, Uuid: uid}
		exp.Data = append(exp.Data, msg)
	}
	assert.Equal(t, exp.Data, data.Data)
}
