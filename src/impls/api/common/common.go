package common

import (
	"github.com/google/uuid"
	"gitlab.com/asvedr/cldi/cl"
)

func q_parser_ctx_maker() cl.ParserContext {
	ctx := cl.NewCtx()
	cl.AddType(ctx, uuid.Parse)
	return ctx
}

var QParserCtx cl.ParserContext = q_parser_ctx_maker()
