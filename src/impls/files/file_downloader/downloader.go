package file_downloader

import (
	"dusc/src/entities/busmsg"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"log"
	"sync"

	"github.com/google/uuid"
)

const drop_after_complete = 10
const retry_timeout = 10

type Downloader struct {
	time   proto.ITime
	sender proto.IMsgSendWorkflow
	fs     proto.IFS

	mtx   sync.Mutex
	files map[string]*meta
}

type meta struct {
	user_id          uuid.UUID
	file_id          uuid.UUID
	path             string
	max_parts        int
	expected_part    int
	retry_request_at uint64
	drop_at          uint64
}

func New(
	time proto.ITime,
	sender proto.IMsgSendWorkflow,
	fs proto.IFS,
) *Downloader {
	return &Downloader{
		time:   time,
		sender: sender,
		fs:     fs,

		files: map[string]*meta{},
	}
}

//lint:ignore U1000 reason: compile time verification
func verify_interface() {
	var _ proto.ITask = &Downloader{}
	var _ proto.IFileDownloader = &Downloader{}
}

func (self *Downloader) Schema() local.TaskSchema {
	return local.TaskSchema{
		Timeout: 5,
		Name:    local.TaskNameDownloadFile,
	}
}

func (self *Downloader) StartLoading(
	user_id uuid.UUID,
	file_id uuid.UUID,
	max_parts int,
	path_to_save string,
) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	key := map_key(user_id, file_id)
	_, found := self.files[key]
	if found {
		return errs.LoadingAlreadyStarted{}
	}
	self.files[key] = &meta{
		user_id:          user_id,
		file_id:          file_id,
		path:             path_to_save,
		max_parts:        max_parts,
		expected_part:    0,
		retry_request_at: 0,
	}
	return nil
}

func (self *Downloader) GetLoadingStatus(user_id uuid.UUID, file_id uuid.UUID) (int, error) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	meta := self.files[map_key(user_id, file_id)]
	if meta == nil {
		return 0, errs.NotFound{}
	}
	if meta.expected_part == -1 {
		return 100, nil
	}
	percent := float64(meta.expected_part) / float64(meta.max_parts)
	return int(percent * 100.0), nil
}

func (self *Downloader) AddBlock(
	user_id uuid.UUID,
	file_id uuid.UUID,
	part int,
	data []byte,
) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	meta := self.files[map_key(user_id, file_id)]
	if meta.expected_part != part {
		log.Printf(
			"File %s. Expected part %d, got part %d. Ignore",
			file_id.String(),
			meta.expected_part,
			part,
		)
		return nil
	}
	err := self.fs.AppendFile(meta.path, data)
	if err != nil {
		return err
	}
	meta.expected_part += 1
	meta.retry_request_at = self.time.Now()
	if meta.expected_part >= meta.max_parts {
		meta.expected_part = -1
	}
	return nil
}

func (self *Downloader) Execute() error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	drop_wf := map[string]*meta{}
	now := self.time.Now()
	for key, file := range self.files {
		if file.expected_part == -1 {
			drop_wf[key] = file
			continue
		}
		if file.retry_request_at <= now {
			self.retry_block_request(now, file)
		}
	}
	for key, file := range drop_wf {
		if file.drop_at == 0 {
			file.drop_at = now + drop_after_complete
			continue
		}
		if file.drop_at < now {
			delete(self.files, key)
		}
	}
	return nil
}

func (self *Downloader) retry_block_request(now uint64, meta *meta) {
	msg := &busmsg.FileSegmentRequest{
		FileId: meta.file_id,
		Part:   uint16(meta.expected_part),
	}
	err := self.sender.AddToQueueMinial(meta.user_id, msg)
	meta.retry_request_at = now + retry_timeout
	if err != nil {
		log.Printf("can not retry file %v: %v", meta.file_id, err)
	}
}

func map_key(user_id uuid.UUID, file_id uuid.UUID) string {
	return user_id.String() + "|" + file_id.String()
}
