package file_downloader_test

import (
	"dusc/src/entities/busmsg"
	"dusc/src/entities/errs"
	"dusc/src/factories"
	"dusc/src/impls/files/file_downloader"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

// 0. init
//   - check status
//
// 1. Start loading
//   - check status
//
// 2. Request block
//   - check status
//
// 3. Got block
//   - check status
//
// 4. Request another
//   - check status
//
// 5. Got final block
//   - check status
//
// 6. Stop loading
//   - check status
func TestDownloadWF(t *testing.T) {
	time := &factories.Time{}
	send_wf := &factories.MsgSendWF{}
	fs := &factories.FS{}
	loader := file_downloader.New(time, send_wf, fs)

	user_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()

	// STEP 0
	loader.Execute()
	_, err := loader.GetLoadingStatus(user_id, file_id)
	assert.Equal(t, errs.NotFound{}, err)

	// STEP 1,2
	err = loader.StartLoading(user_id, file_id, 2, "/file.txt")
	assert.Nil(t, err)
	status, err := loader.GetLoadingStatus(user_id, file_id)
	assert.Nil(t, err)
	assert.Equal(t, 0, status)
	loader.Execute()
	// vvvv STEP 2 vvvvv
	assert.Equal(t, 1, len(send_wf.Calls))
	msg := &busmsg.FileSegmentRequest{
		FileId: file_id,
		Part:   0,
	}
	assert.Equal(
		t,
		factories.MsgSendWFMinial{Receiver: user_id, Msg: msg},
		send_wf.Calls[0],
	)
	status, err = loader.GetLoadingStatus(user_id, file_id)
	assert.Nil(t, err)
	assert.Equal(t, 0, status)
	// ^^^^^ STEP 2 ^^^^^

	// STEP 3, 4
	err = loader.AddBlock(user_id, file_id, 0, []byte{1, 2, 3})
	assert.Nil(t, err)
	assert.Equal(t, fs.AppendFileData["/file.txt"], []byte{1, 2, 3})
	loader.Execute()
	// vvvv STEP 4 vvvvv
	assert.Equal(t, 2, len(send_wf.Calls))
	msg = &busmsg.FileSegmentRequest{
		FileId: file_id,
		Part:   1,
	}
	assert.Equal(
		t,
		factories.MsgSendWFMinial{Receiver: user_id, Msg: msg},
		send_wf.Calls[1],
	)
	status, err = loader.GetLoadingStatus(user_id, file_id)
	assert.Nil(t, err)
	assert.Equal(t, 50, status)
	// ^^^^^ STEP 4 ^^^^^

	// STEP 5
	err = loader.AddBlock(user_id, file_id, 1, []byte{4, 5, 6})
	assert.Nil(t, err)
	assert.Equal(t, fs.AppendFileData["/file.txt"], []byte{4, 5, 6})
	loader.Execute()
	assert.Equal(t, 2, len(send_wf.Calls))
	status, err = loader.GetLoadingStatus(user_id, file_id)
	assert.Nil(t, err)
	assert.Equal(t, 100, status)

	// STEP 6
	time.NowData += 20
	loader.Execute()
	_, err = loader.GetLoadingStatus(user_id, file_id)
	assert.Equal(t, errs.NotFound{}, err)
}

func TestRetry(t *testing.T) {
	time := &factories.Time{}
	send_wf := &factories.MsgSendWF{}
	fs := &factories.FS{}
	loader := file_downloader.New(time, send_wf, fs)

	user_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()

	err := loader.StartLoading(user_id, file_id, 2, "/file.txt")
	assert.Nil(t, err)
	loader.Execute()

	msg := &busmsg.FileSegmentRequest{
		FileId: file_id,
		Part:   0,
	}
	call := factories.MsgSendWFMinial{Receiver: user_id, Msg: msg}

	assert.Equal(t, 1, len(send_wf.Calls))
	assert.Equal(t, call, send_wf.Calls[0])

	loader.Execute()
	assert.Equal(t, 1, len(send_wf.Calls))

	time.NowData += 30

	loader.Execute()
	assert.Equal(t, 2, len(send_wf.Calls))
	assert.Equal(t, call, send_wf.Calls[1])
}

func TestReceiveUnexpectedPart(t *testing.T) {
	time := &factories.Time{}
	send_wf := &factories.MsgSendWF{}
	fs := &factories.FS{}
	loader := file_downloader.New(time, send_wf, fs)

	user_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()

	err := loader.StartLoading(user_id, file_id, 2, "/file.txt")
	assert.Nil(t, err)
	loader.Execute()

	loader.AddBlock(user_id, file_id, 1, []byte{1, 2, 3})
	loader.AddBlock(user_id, file_id, -1, []byte{1, 2, 3})
	assert.Equal(t, 0, len(fs.WriteFileData))
}
