package file_uploader_test

import (
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/files/file_uploader"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

var file_id, _ = uuid.NewRandom()

func file_meta() local.FileMeta {
	return local.FileMeta{
		FSFileMeta: local.FSFileMeta{
			Blocks: 4,
			Size:   100,
		},
		Id:   file_id,
		Path: "/abc/d",
		Name: "d",
	}
}

func TestGetFilePartNotFound(t *testing.T) {
	repo := factories.NewDb().Files

	user_id, _ := uuid.NewRandom()

	uploader := file_uploader.New(&factories.FS{}, repo)
	_, err := uploader.GetFilePart(file_id, user_id, 5)
	assert.Equal(t, errs.NotFound{}, err)
}

func TestGetFilePartAccessDenied(t *testing.T) {
	repo := factories.NewDb().Files

	user_1, _ := uuid.NewRandom()
	user_2, _ := uuid.NewRandom()

	err := repo.RegisterFile(file_meta(), []uuid.UUID{user_1})
	assert.Nil(t, err)

	uploader := file_uploader.New(&factories.FS{}, repo)
	_, err = uploader.GetFilePart(file_id, user_2, 5)
	assert.Equal(t, errs.NotFound{}, err)
}

func TestGetFilePartOk(t *testing.T) {
	fs := &factories.FS{}
	repo := factories.NewDb().Files

	user, _ := uuid.NewRandom()

	err := repo.RegisterFile(file_meta(), []uuid.UUID{user})
	assert.Nil(t, err)
	fs.GetBlockData = map[string][][]byte{
		file_meta().Path: {
			{0, 0, 0},
			{1, 2, 3},
			{4, 5, 6},
		},
	}

	uploader := file_uploader.New(fs, repo)
	bts, err := uploader.GetFilePart(file_id, user, 1)
	assert.Nil(t, err)
	assert.Equal(t, []byte{1, 2, 3}, bts)
}

func TestGetFilePartOutOfRange(t *testing.T) {
	repo := factories.NewDb().Files

	user, _ := uuid.NewRandom()

	err := repo.RegisterFile(file_meta(), []uuid.UUID{user})
	assert.Nil(t, err)

	uploader := file_uploader.New(&factories.FS{}, repo)
	_, err = uploader.GetFilePart(file_id, user, 10)
	assert.ErrorAs(t, err, &file_uploader.FilePartOutOfRange{})
}
