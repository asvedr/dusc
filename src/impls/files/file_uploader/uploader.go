package file_uploader

import (
	"dusc/src/proto"
	"fmt"

	"github.com/google/uuid"
)

type uploader struct {
	fs   proto.IFS
	repo proto.ISharedFileRepo
}

type FilePartOutOfRange struct {
	max  int
	part int
}

func (self FilePartOutOfRange) Error() string {
	return fmt.Sprintf(
		"FilePartOutOfRange(%d of %d)",
		self.part,
		self.max,
	)
}

func New(
	fs proto.IFS,
	repo proto.ISharedFileRepo,
) proto.IFileUploader {
	return uploader{fs: fs, repo: repo}
}

func (self uploader) GetFilePart(
	file_id uuid.UUID,
	user_id uuid.UUID,
	part int,
) ([]byte, error) {
	meta, err := self.repo.GetFileForUser(file_id, user_id)
	if err != nil {
		return nil, err
	}
	if part < 0 || part >= meta.Blocks {
		return nil, FilePartOutOfRange{
			part: part,
			max:  meta.Blocks,
		}
	}
	return self.fs.GetBlock(meta.Path, part)
}
