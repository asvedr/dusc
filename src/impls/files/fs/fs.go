package fs

import (
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"
	"io"
	"os"
	"path/filepath"
	"sort"
)

type fs struct{}

const file_mode = 0o664
const block_size int64 = 1000

func New() proto.IFS {
	return fs{}
}

func (fs) ExecDir() (string, error) {
	path, err := os.Executable()
	return filepath.Dir(path), err
}

func (fs) CreateDir(path string) error {
	return os.MkdirAll(path, os.ModePerm)
}

func (fs) ReadFile(path string) ([]byte, error) {
	return os.ReadFile(path)
}

func (fs) WriteFile(path string, data []byte) error {
	return os.WriteFile(path, data, file_mode)
}

func (fs) AppendFile(path string, data []byte) error {
	if !check_file_exists(path) {
		return fs{}.WriteFile(path, data)
	}
	file, err := os.OpenFile(path, os.O_APPEND, file_mode)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = file.Seek(0, io.SeekEnd)
	if err != nil {
		return err
	}
	_, err = file.Write(data)
	return err
}

func (fs) GetBlock(path string, block_id int) ([]byte, error) {
	file, err := os.OpenFile(path, os.O_RDONLY, file_mode)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	shift := block_size * int64(block_id)
	_, err = file.Seek(shift, io.SeekStart)
	buf := make([]byte, block_size)
	count, err := file.Read(buf)
	if err != nil {
		return nil, err
	}
	if count < int(block_size) {
		return buf[:count], nil
	} else {
		return buf, nil
	}
}

func (fs) GetMeta(path string) (local.FSFileMeta, error) {
	info, err := os.Stat(path)
	if err != nil {
		return local.FSFileMeta{}, err
	}
	size := info.Size()
	meta := local.FSFileMeta{
		Blocks: int(size / block_size),
		Size:   size,
	}
	return meta, nil
}

func (fs) GetParentDir(path string) (string, error) {
	path, err := fs{}.GetAbsPath(path)
	return filepath.Dir(path), err
}

func (fs) GetAbsPath(path string) (string, error) {
	return filepath.Abs(path)
}

func (fs) GetDirContent(path string) ([]local.DirItem, error) {
	ents, err := os.ReadDir(path)
	if err != nil {
		return nil, err
	}
	res := make([]local.DirItem, len(ents))
	for i, ent := range ents {
		res[i] = local.DirItem{
			Name:  ent.Name(),
			Path:  filepath.Join(path, ent.Name()),
			IsDir: ent.IsDir(),
		}
	}
	sort.Slice(res, func(i, j int) bool {
		return res[i].Name < res[j].Name
	})
	return res, err
}

func check_file_exists(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}
