package db_locker

import (
	"gitlab.com/asvedr/safe_sqlite"
)

type Locker struct {
	db        safe_sqlite.Db
	locked    safe_sqlite.IDb
	protected bool
}

func New(path string, protected bool) (*Locker, error) {
	db, err := safe_sqlite.New(path)
	if err != nil {
		return nil, err
	}
	return &Locker{db: db, protected: protected}, nil
}

func (self *Locker) Lock(f func()) {
	self.db.WithLocked(func(db safe_sqlite.IDb) error {
		self.locked = db
		f()
		self.locked = nil
		return nil
	})
}

func (self *Locker) Db() safe_sqlite.IDb {
	locked := self.locked
	if locked != nil {
		return locked
	}
	if self.protected {
		panic("db usage out of lock is prohibited")
	}
	return self.db
}
