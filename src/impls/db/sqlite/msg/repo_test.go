package msg_test

import (
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func from_me() local.NewMsg {
	uid := factories.Uuid.Get()
	msg_id, _ := uuid.Parse("5534cb71-f34c-42ff-ab06-bb43c2d90ce3")
	return local.NewMsg{
		Sender:     &uid,
		Chat:       local.ChatId{IsGroup: true, Id: uid},
		Uid:        msg_id,
		Text:       "abc",
		Visible:    true,
		CreatedAt:  2,
		ReceivedAt: 1,
	}
}

func to_me() local.NewMsg {
	uid := factories.Uuid.Get()
	msg_id, _ := uuid.Parse("5a34cb71-f34c-42ff-ab06-bb43c2d90ce3")
	return local.NewMsg{
		Receiver:   &uid,
		Chat:       local.ChatId{IsGroup: true, Id: uid},
		Uid:        msg_id,
		Text:       "abc",
		Visible:    true,
		Received:   true,
		CreatedAt:  3,
		ReceivedAt: 4,
	}
}

func TestRetry(t *testing.T) {
	repo := factories.NewDb().Msg

	err := repo.Create(from_me())
	assert.Nil(t, err)

	err = repo.Create(from_me())
	assert.Equal(t, errs.UidAlreadyUsed{}, err)
}

func TestGetChatMsgsByIntId(t *testing.T) {
	repo := factories.NewDb().Msg
	repo.Create(from_me())
	repo.Create(to_me())
	last_msg, err := repo.GetByUid(to_me().Uid)
	assert.Nil(t, err)

	msgs, err := repo.GetChatMsgs(from_me().Chat, nil, 2, true)
	assert.Nil(t, err)
	assert.Equal(t, 2, len(msgs))
	assert.Equal(t, msgs[0].NewMsg, from_me())
	assert.Equal(t, msgs[1].NewMsg, to_me())

	last_id := last_msg.Id
	msgs, err = repo.GetChatMsgsByIntId(from_me().Chat, &last_id, 2, true)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(msgs))
	assert.Equal(t, msgs[0].NewMsg, from_me())
}

func TestGetChatMsgs(t *testing.T) {
	repo := factories.NewDb().Msg
	repo.Create(from_me())
	repo.Create(to_me())
	last_msg, err := repo.GetByUid(to_me().Uid)
	assert.Nil(t, err)

	msgs, err := repo.GetChatMsgs(from_me().Chat, nil, 2, true)
	assert.Nil(t, err)
	assert.Equal(t, 2, len(msgs))
	assert.Equal(t, msgs[0].NewMsg, from_me())
	assert.Equal(t, msgs[1].NewMsg, to_me())

	last_id := last_msg.Uid
	msgs, err = repo.GetChatMsgs(from_me().Chat, &last_id, 2, true)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(msgs))
	assert.Equal(t, msgs[0].NewMsg, from_me())
}

func TestGetByUid(t *testing.T) {
	repo := factories.NewDb().Msg
	repo.Create(from_me())
	repo.Create(to_me())

	new_id, _ := uuid.Parse("5534cb71-fffc-42ff-ab06-bb43c2d90ce3")

	_, err := repo.GetByUid(new_id)
	assert.Equal(t, errs.NotFound{}, err)

	msg, err := repo.GetByUid(from_me().Uid)
	assert.Nil(t, err)
	assert.Equal(t, from_me(), msg.NewMsg)
}

func TestGetMessagesToSend(t *testing.T) {
	repo := factories.NewDb().Msg
	repo.Create(from_me())
	repo.Create(to_me())

	msgs, err := repo.GetMessagesToSend()
	assert.Nil(t, err)
	assert.Equal(t, 1, len(msgs))
	assert.Equal(t, msgs[0].NewMsg, from_me())

	assert.Nil(t, repo.MarkReceived(from_me().Uid))
	msgs, err = repo.GetMessagesToSend()
	assert.Nil(t, err)
	assert.Equal(t, 0, len(msgs))
}

func TestMarkRead(t *testing.T) {
	repo := factories.NewDb().Msg
	repo.Create(from_me())
	repo.Create(to_me())

	assert.Nil(t, repo.MarkRead(from_me().Uid))

	msg, err := repo.GetByUid(from_me().Uid)
	assert.Nil(t, err)
	assert.True(t, msg.Read)

	msg, err = repo.GetByUid(to_me().Uid)
	assert.Nil(t, err)
	assert.False(t, msg.Read)
}

func TestDelete(t *testing.T) {
	repo := factories.NewDb().Msg
	repo.Create(from_me())
	repo.Create(to_me())

	assert.Nil(t, repo.Delete(from_me().Uid))

	_, err := repo.GetByUid(from_me().Uid)
	assert.Equal(t, errs.NotFound{}, err)

	_, err = repo.GetByUid(to_me().Uid)
	assert.Nil(t, err)
}
