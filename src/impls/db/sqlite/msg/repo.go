package msg

import (
	"database/sql"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/impls/db/sqlite/common"
	"dusc/src/impls/db/sqlite/db_locker"
	"dusc/src/proto"
	"encoding/json"
	"errors"
	"fmt"
	"slices"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/asvedr/safe_sqlite"
)

const q_create_tbl = `
CREATE TABLE IF NOT EXISTS dusc_msg (
	Id INTEGER PRIMARY KEY NOT NULL,
	Sender TEXT,
	Receiver TEXT,
	Chat TEXT,
	Uid TEXT NOT NULL UNIQUE,
	Text TEXT,
	Attached TEXT,
	Visible BOOLEAN,
	Received BOOLEAN,
	Read BOOLEAN,
	CreatedAt INTEGER,
	ReceivedAt INTEGER
)`

const fields = ` Id,Sender,Receiver,Chat,Uid,
Text,Attached,Visible,Received,
Read,CreatedAt,ReceivedAt `

type repo struct {
	db *db_locker.Locker
}

func New(
	db *db_locker.Locker,
) (proto.IMsgRepo, error) {
	err := db.Db().Exec(q_create_tbl)
	return repo{db: db}, err
}

func (self repo) Create(msg local.NewMsg) error {
	query := `
		INSERT INTO dusc_msg 
		(
			Sender,Receiver,Chat,Uid,
			Text,Attached,Visible,Received,
			Read,CreatedAt,ReceivedAt
		)
		VALUES (?,?,?,?,?,?,?,?,?,?,?)
	`
	attached := ser_attach(&msg)
	err := self.db.Db().Exec(
		query,
		ser_opt_uid(msg.Sender),
		ser_opt_uid(msg.Receiver),
		msg.Chat.String(),
		msg.Uid.String(),
		msg.Text,
		attached,
		msg.Visible,
		msg.Received,
		msg.Read,
		msg.CreatedAt,
		msg.ReceivedAt,
	)
	if common.IsUniqError(err) {
		err = errs.UidAlreadyUsed{}
	}
	return err
}

func (self repo) GetLastUnreadMessages(limit int) ([]*local.Msg, error) {
	query := fmt.Sprintf(
		`SELECT %s 
		FROM dusc_msg
		WHERE Read = False
		ORDER BY Id DESC
		LIMIT %d`,
		fields,
		limit,
	)
	return safe_sqlite.FetchSlice(self.db.Db(), deser_msg, query)
}

func (self repo) GetChatMsgsByIntId(
	chat_id local.ChatId,
	last_id *int,
	count int,
	filter_visible bool,
) ([]*local.Msg, error) {
	where := self.get_chat_msgs_by_id_where(last_id)
	return self.get_chat_msgs(chat_id, where, count, filter_visible)
}

func (self repo) GetChatMsgs(
	chat_id local.ChatId,
	last_id *uuid.UUID,
	count int,
	filter_visible bool,
) ([]*local.Msg, error) {
	where, err := self.get_chat_msgs_where(last_id)
	if err != nil {
		return nil, err
	}
	return self.get_chat_msgs(chat_id, where, count, filter_visible)
}

func (self repo) get_chat_msgs(
	chat_id local.ChatId,
	where string,
	count int,
	filter_visible bool,
) ([]*local.Msg, error) {
	if filter_visible {
		where += " AND Visible = TRUE"
	}
	query := fmt.Sprintf(
		`SELECT %s FROM dusc_msg
		WHERE %s
		ORDER BY Id DESC LIMIT ?`,
		fields,
		where,
	)
	msgs, err := safe_sqlite.FetchSlice[*local.Msg](
		self.db.Db(),
		deser_msg,
		query,
		chat_id.String(),
		count,
	)
	slices.Reverse(msgs)
	return msgs, err
}

func (self repo) GetByUid(id uuid.UUID) (*local.Msg, error) {
	msg, err := safe_sqlite.FetchOne[*local.Msg](
		self.db.Db(),
		deser_msg,
		"SELECT "+fields+" FROM dusc_msg WHERE Uid = ?",
		id.String(),
	)
	if err != nil {
		return nil, err
	}
	if msg == nil {
		return nil, errs.NotFound{}
	}
	return *msg, nil
}

func (self repo) GetMessagesToSend() ([]*local.Msg, error) {
	return safe_sqlite.FetchSlice[*local.Msg](
		self.db.Db(),
		deser_msg,
		"SELECT "+fields+" FROM dusc_msg WHERE not Received ORDER BY Id",
	)
}

func (self repo) MarkRead(msg_id ...uuid.UUID) error {
	strs := []any{}
	for _, id := range msg_id {
		strs = append(strs, id.String())
	}
	return self.db.Db().Exec(
		"UPDATE dusc_msg SET Read = true WHERE Uid = ?",
		strs...,
	)
}

func (self repo) MarkReceived(msg_id uuid.UUID) error {
	return self.db.Db().Exec(
		"UPDATE dusc_msg SET Received = true WHERE Uid = ?",
		msg_id.String(),
	)
}

func (self repo) Delete(msg_ids ...uuid.UUID) error {
	if len(msg_ids) == 0 {
		return nil
	}
	tmpl := strings.Repeat(",?", len(msg_ids))
	strs := []any{}
	for _, id := range msg_ids {
		strs = append(strs, id.String())
	}
	return self.db.Db().Exec(
		`DELETE FROM dusc_msg WHERE Uid IN (`+tmpl[1:]+`)`,
		strs...,
	)
}

func (self repo) get_chat_msgs_where(last_id *uuid.UUID) (string, error) {
	if last_id == nil {
		return "Chat = ?", nil
	}
	id, err := safe_sqlite.FetchOne[int](
		self.db.Db(),
		func(r *sql.Rows) (int, error) {
			var res int
			err := r.Scan(&res)
			return res, err
		},
		"SELECT Id FROM dusc_msg WHERE Uid = ?",
		last_id.String(),
	)
	if err != nil {
		return "", err
	}
	if id == nil {
		return "", errs.NotFound{}
	}
	return self.get_chat_msgs_by_id_where(id), nil
}

func (self repo) get_chat_msgs_by_id_where(last_id *int) string {
	if last_id == nil {
		return "Chat = ?"
	}
	return fmt.Sprintf("Chat = ? AND Id < %d", *last_id)
}

func deser_msg(row *sql.Rows) (*local.Msg, error) {
	var msg local.Msg
	var chat string
	var attached string
	err := row.Scan(
		&msg.Id,
		&msg.Sender,
		&msg.Receiver,
		&chat,
		&msg.Uid,
		&msg.Text,
		&attached,
		&msg.Visible,
		&msg.Received,
		&msg.Read,
		&msg.CreatedAt,
		&msg.ReceivedAt,
	)
	if err != nil {
		return nil, err
	}
	msg.Chat, err = local.StringToChatId(chat)
	if err != nil {
		return nil, err
	}
	err = deser_attach(&msg, attached)
	return &msg, err
}

func ser_opt_uid(id *uuid.UUID) *string {
	if id == nil {
		return nil
	}
	txt := id.String()
	return &txt
}

func ser_attach(msg *local.NewMsg) string {
	if msg.AttachFileMeta != nil {
		bts, _ := json.Marshal(msg.AttachFileMeta)
		return "f" + string(bts)
	}
	if msg.AttachInvite != nil {
		bts, _ := json.Marshal(msg.AttachInvite)
		return "i" + string(bts)
	}
	return ""
}

func deser_attach(msg *local.Msg, src string) error {
	bts := []byte(src)
	if len(bts) == 0 {
		return nil
	}
	var err error
	switch bts[0] {
	case 'f':
		err = json.Unmarshal(bts[1:], &msg.AttachFileMeta)
	case 'i':
		err = json.Unmarshal(bts[1:], &msg.AttachInvite)
	default:
		err = errors.New("can not deser attach")
	}
	return err
}
