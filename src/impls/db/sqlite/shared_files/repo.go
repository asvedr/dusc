package shared_files

import (
	"database/sql"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/impls/db/sqlite/db_locker"
	"dusc/src/proto"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/asvedr/safe_sqlite"
)

const q_cr_fl_table = `
CREATE TABLE IF NOT EXISTS dusc_file(
	Id TEXT PRIMARY KEY NOT NULL,
	Blocks INTEGER NOT NULL,
	Size INTEGER NOT NULL,
	Path TEXT,
	Name TEXT
)
`
const q_cr_acc_table = `
CREATE TABLE IF NOT EXISTS dusc_file_access(
	Id INTEGER PRIMARY KEY NOT NULL,
	FileId TEXT NOT NULL,
	UserId TEXT NOT NULL
)
`

type repo struct {
	db *db_locker.Locker
}

func New(db *db_locker.Locker) (proto.ISharedFileRepo, error) {
	err := db.Db().Exec(q_cr_fl_table)
	if err != nil {
		return nil, err
	}
	err = db.Db().Exec(q_cr_acc_table)
	return repo{db: db}, err
}

func (self repo) RegisterFile(meta local.FileMeta, users []uuid.UUID) error {
	if len(users) == 0 {
		return nil
	}
	meta_id := meta.Id.String()
	err := self.db.Db().Exec(
		`INSERT INTO dusc_file
		(Id, Blocks, Size, Path, Name)
		VALUES (?, ?, ?, ?, ?)`,
		meta_id,
		meta.Blocks,
		meta.Size,
		meta.Path,
		meta.Name,
	)
	if err != nil {
		return err
	}
	if len(users) == 0 {
		return nil
	}
	tmpl := strings.Repeat(",(?,?)", len(users))
	vals := []any{}
	for _, user := range users {
		vals = append(vals, meta_id, user.String())
	}
	return self.db.Db().Exec(
		`INSERT INTO dusc_file_access
		 (FileId, UserId) VALUES `+tmpl[1:],
		vals...,
	)
}

func (self repo) GetFileForUser(file_id uuid.UUID, user_id uuid.UUID) (local.FileMeta, error) {
	meta, err := safe_sqlite.FetchOne[local.FileMeta](
		self.db.Db(),
		deser_meta,
		`SELECT f.Id, f.Blocks, f.Size, f.Path, f.Name
		 FROM dusc_file as f JOIN dusc_file_access AS a ON a.FileId = f.Id
		 WHERE f.Id = ? AND a.UserId = ?`,
		file_id.String(),
		user_id.String(),
	)
	if err != nil {
		return local.FileMeta{}, err
	}
	if meta == nil {
		return local.FileMeta{}, errs.NotFound{}
	}
	return *meta, err
}

func (self repo) GetRegisteredFiles() ([]*local.FileMetaUserAccess, error) {
	files, err := safe_sqlite.FetchSlice(
		self.db.Db(),
		deser_meta,
		`SELECT Id, Blocks, Size, Path, Name
		 FROM dusc_file ORDER BY Id`,
	)
	if err != nil {
		return nil, err
	}
	id_to_file := map[string]*local.FileMetaUserAccess{}
	result := []*local.FileMetaUserAccess{}
	for _, file := range files {
		fmua := &local.FileMetaUserAccess{FileMeta: file}
		id_to_file[file.Id.String()] = fmua
		result = append(result, fmua)
	}
	iter := func(r *sql.Rows) error {
		var s_file_id, s_user_id string
		err := r.Scan(&s_file_id, &s_user_id)
		if err != nil {
			return err
		}
		user_id, err := uuid.Parse(s_user_id)
		if err != nil {
			return err
		}
		file, found := id_to_file[s_file_id]
		if !found {
			return nil
		}
		file.Users = append(file.Users, user_id)
		return nil
	}
	query := `SELECT FileId, UserId
	          FROM dusc_file_access ORDER BY Id`
	err = self.db.Db().IterMany(iter, query)
	return result, err
}

func (self repo) DeleteFileById(file_id uuid.UUID) error {
	err := self.db.Db().Exec(
		"DELETE FROM dusc_file_access WHERE FileId = ?",
		file_id.String(),
	)
	if err != nil {
		return err
	}
	return self.db.Db().Exec(
		"DELETE FROM dusc_file WHERE Id = ?",
		file_id.String(),
	)
}

func deser_meta(r *sql.Rows) (local.FileMeta, error) {
	var meta local.FileMeta
	var id string
	err := r.Scan(
		&id,
		&meta.Blocks,
		&meta.Size,
		&meta.Path,
		&meta.Name,
	)
	if err != nil {
		return meta, err
	}
	meta.Id, err = uuid.Parse(id)
	return meta, err
}
