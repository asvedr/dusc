package shared_files_test

import (
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func new_meta() local.FileMeta {
	file_id, _ := uuid.Parse("41d7ed54-5b59-427a-9a6b-a453dd22512b")
	return local.FileMeta{
		FSFileMeta: local.FSFileMeta{
			Blocks: 12,
			Size:   123,
		},
		Id:   file_id,
		Path: "/abc.txt",
		Name: "abc.txt",
	}
}

func TestGetFileForUser(t *testing.T) {
	uid1, _ := uuid.Parse("031c6feb-10d2-49f9-ab63-217e5889131b")
	uid2, _ := uuid.Parse("aa4e365f-b43e-44ee-ab1d-54a5af4c5a3e")
	repo := factories.NewDb().Files
	meta := new_meta()

	err := repo.RegisterFile(meta, []uuid.UUID{uid1})
	assert.Nil(t, err)

	got, err := repo.GetFileForUser(meta.Id, uid1)
	assert.Nil(t, err)
	assert.Equal(t, meta, got)

	_, err = repo.GetFileForUser(meta.Id, uid2)
	assert.Equal(t, errs.NotFound{}, err)
}

func TestDeleteFileById(t *testing.T) {
	uid1, _ := uuid.Parse("031c6feb-10d2-49f9-ab63-217e5889131b")
	repo := factories.NewDb().Files
	meta := new_meta()

	err := repo.RegisterFile(meta, []uuid.UUID{uid1})
	assert.Nil(t, err)

	got, err := repo.GetFileForUser(meta.Id, uid1)
	assert.Nil(t, err)
	assert.Equal(t, meta, got)

	assert.Nil(t, repo.DeleteFileById(meta.Id))

	_, err = repo.GetFileForUser(meta.Id, uid1)
	assert.Equal(t, errs.NotFound{}, err)
}
