package chats

import (
	"database/sql"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/impls/db/sqlite/db_locker"
	"dusc/src/proto"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/asvedr/safe_sqlite"
)

type repo struct {
	db *db_locker.Locker
}

const cr_chat_table = `
CREATE TABLE IF NOT EXISTS dusc_chat (
	Id TEXT PRIMARY KEY,
	Name TEXT,
	Banned BOOLEAN
)`

const cr_m2m_table = `
CREATE TABLE IF NOT EXISTS dusc_chat_user (
	Key TEXT PRIMARY KEY,
	ChatId TEXT NOT NULL,
	UserId TEXT NOT NULL
)`

func New(
	db *db_locker.Locker,
	user_repo proto.IUserRepo, // dependency
	msg_repo proto.IMsgRepo, // dependency
) (proto.IChatRepo, error) {
	err := db.Db().Exec(cr_chat_table)
	if err != nil {
		return nil, err
	}
	err = db.Db().Exec(cr_m2m_table)
	if err != nil {
		return nil, err
	}
	return repo{db: db}, err
}

func (self repo) Create(chat local.Chat) error {
	err := self.db.Db().Exec(
		`INSERT OR IGNORE INTO dusc_chat (Id, Name, Banned) VALUES (?, ?, false)`,
		chat.Id.String(),
		chat.Name,
	)
	if err != nil {
		return err
	}
	return self.AddParticipants(chat.Id, chat.Users)
}

func (self repo) GetById(id local.ChatId) (local.Chat, error) {
	sid := id.String()
	opt_chat, err := safe_sqlite.FetchOne(
		self.db.Db(),
		deser_plain,
		`SELECT Id, Name FROM dusc_chat WHERE Id = ?`,
		sid,
	)
	var chat local.Chat
	if err != nil {
		return chat, err
	}
	if opt_chat == nil {
		return chat, errs.NotFound{}
	}
	chat = *opt_chat
	chat.Users, err = self.GetParticipants(chat.Id)
	return chat, err
}

func (self repo) SetName(id local.ChatId, name string) error {
	return self.db.Db().Exec(
		"UPDATE dusc_chat SET Name = ? WHERE Id = ?",
		name,
		id.String(),
	)
}

func (self repo) AddParticipants(chat local.ChatId, users []uuid.UUID) error {
	if len(users) == 0 {
		return nil
	}
	tmpl := strings.Repeat(",(?,?,?)", len(users))
	params := []any{}
	for _, uid := range users {
		params = append(
			params,
			m2m_key(chat, uid),
			chat.String(),
			uid.String(),
		)
	}
	return self.db.Db().Exec(
		`INSERT OR REPLACE INTO dusc_chat_user
		(Key, ChatId, UserId)
		VALUES `+tmpl[1:],
		params...,
	)
}

func (self repo) GetParticipants(id local.ChatId) ([]uuid.UUID, error) {
	query := `
	SELECT UserId FROM dusc_chat_user
	WHERE ChatId = ? ORDER BY UserId
	`
	return safe_sqlite.FetchSlice(
		self.db.Db(),
		deser_uid,
		query,
		id.String(),
	)
}

func (self repo) GetShortInfoForAll() ([]local.ChatShort, error) {
	query := `
	WITH msgs AS (
		SELECT * FROM dusc_msg WHERE Visible = TRUE
	), chat_to_last_msg AS (
		SELECT c.Id, MAX(m.Id) as MsgId
		FROM dusc_chat AS c LEFT JOIN dusc_msg AS m
			ON c.Id = m.Chat
		GROUP BY c.Id
	) SELECT
		c.Id,
		c.Name,
		COALESCE(m.Text, ''),
		COALESCE(m.Read, TRUE)
	FROM chat_to_last_msg AS ctm JOIN dusc_chat AS c
		ON ctm.Id = c.Id
	LEFT JOIN dusc_msg AS m
		ON ctm.MsgId = m.Id
	ORDER BY m.Received DESC, c.Id DESC
	`
	return safe_sqlite.FetchSlice[local.ChatShort](
		self.db.Db(),
		func(r *sql.Rows) (local.ChatShort, error) {
			var res local.ChatShort
			var sid string
			err := r.Scan(
				&sid,
				&res.Name,
				&res.LastMsg,
				&res.Read,
			)
			if err != nil {
				return res, err
			}
			res.Id, err = local.StringToChatId(sid)
			return res, err
		},
		query,
	)
}

func (self repo) GetFullList() ([]local.ChatListItem, error) {
	return safe_sqlite.FetchSlice(
		self.db.Db(),
		func(r *sql.Rows) (local.ChatListItem, error) {
			var item local.ChatListItem
			var sid string
			err := r.Scan(&sid, &item.Name, &item.Banned)
			if err != nil {
				return item, err
			}
			item.Id, err = local.StringToChatId(sid)
			return item, err
		},
		"SELECT Id, Name, Banned FROM dusc_chat ORDER BY Id",
	)
}

func (self repo) BanChat(id local.ChatId) error {
	err := self.db.Db().Exec(
		"UPDATE dusc_chat SET banned = TRUE WHERE id = ?",
		id.String(),
	)
	if err != nil {
		return err
	}
	return self.db.Db().Exec(
		"DELETE FROM dusc_msg WHERE Chat = ?",
		id.String(),
	)
}

func (self repo) Delete(id local.ChatId) error {
	err := self.db.Db().Exec(
		"DELETE FROM dusc_chat_user WHERE ChatId = ?",
		id.String(),
	)
	if err != nil {
		return err
	}
	return self.db.Db().Exec(
		"DELETE FROM dusc_chat WHERE Id = ?",
		id.String(),
	)
}

func deser_plain(r *sql.Rows) (local.Chat, error) {
	var sid, name string
	err := r.Scan(&sid, &name)
	if err != nil {
		return local.Chat{}, err
	}
	id, err := local.StringToChatId(sid)
	return local.Chat{Id: id, Name: name}, err
}

func deser_uid(r *sql.Rows) (uuid.UUID, error) {
	var sid string
	err := r.Scan(&sid)
	if err != nil {
		return uuid.UUID{}, err
	}
	return uuid.Parse(sid)
}

func m2m_key(chat local.ChatId, uid uuid.UUID) string {
	return chat.String() + "|" + uid.String()
}
