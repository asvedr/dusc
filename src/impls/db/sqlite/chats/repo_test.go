package chats_test

import (
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

var ch_id, _ = uuid.Parse("30cfe7b5-de20-4e1c-ba10-12351ee9ba69")
var uid1, _ = uuid.Parse("4253269d-c0b3-4459-a3a1-5233014b247f")
var uid2, _ = uuid.Parse("b1746369-feea-4c03-b1f8-092723838736")
var chat_id = local.ChatId{IsGroup: true, Id: ch_id}

func TestGetById(t *testing.T) {
	repo := factories.NewDb().Chats
	chat := local.Chat{
		Id:    chat_id,
		Name:  "name",
		Users: []uuid.UUID{uid1, uid2},
	}
	err := repo.Create(chat)
	assert.Nil(t, err)
	got, err := repo.GetById(chat_id)
	assert.Nil(t, err)
	assert.Equal(t, chat.Id.String(), got.Id.String())
	assert.Equal(t, chat.Name, got.Name)
	assert.Equal(t, chat.Users, got.Users)
}

func TestSetName(t *testing.T) {
	repo := factories.NewDb().Chats
	chat := local.Chat{
		Id:    chat_id,
		Name:  "name",
		Users: []uuid.UUID{uid1, uid2},
	}
	err := repo.Create(chat)
	assert.Nil(t, err)
	assert.Nil(t, repo.SetName(chat_id, "new_name"))
	got, err := repo.GetById(chat_id)
	assert.Nil(t, err)
	assert.Equal(t, "new_name", got.Name)
}

func TestGetFullList(t *testing.T) {
	repo := factories.NewDb().Chats
	id_a := local.ChatId{Id: uid1}
	id_b := local.ChatId{Id: uid2}
	chat_a := local.Chat{Id: id_a, Name: "A"}
	chat_b := local.Chat{Id: id_b, Name: "B"}
	assert.Nil(t, repo.Create(chat_a))
	assert.Nil(t, repo.Create(chat_b))
	list, err := repo.GetFullList()
	assert.Nil(t, err)

	expected := []local.ChatListItem{
		{Id: id_a, Name: "A", Banned: false},
		{Id: id_b, Name: "B", Banned: false},
	}
	assert.Equal(t, expected, list)

	assert.Nil(t, repo.BanChat(id_a))

	list, err = repo.GetFullList()
	assert.Nil(t, err)

	expected = []local.ChatListItem{
		{Id: id_a, Name: "A", Banned: true},
		{Id: id_b, Name: "B", Banned: false},
	}
	assert.Equal(t, expected, list)
}

func TestGetShortInfoForAll(t *testing.T) {
	db := factories.NewDb()
	id_a := local.ChatId{Id: uid1}
	id_b := local.ChatId{Id: uid2}
	chat_a := local.Chat{Id: id_a, Name: "A"}
	chat_b := local.Chat{Id: id_b, Name: "B"}
	assert.Nil(t, db.Chats.Create(chat_a))
	assert.Nil(t, db.Chats.Create(chat_b))

	err := db.Msg.Create(local.NewMsg{
		Chat:       id_a,
		Uid:        uid1,
		Text:       "msg1",
		CreatedAt:  1,
		ReceivedAt: 1,
	})
	assert.Nil(t, err)
	err = db.Msg.Create(local.NewMsg{
		Chat:       id_a,
		Uid:        uid2,
		Text:       "msg2",
		CreatedAt:  2,
		ReceivedAt: 2,
	})
	assert.Nil(t, err)

	list, err := db.Chats.GetShortInfoForAll()
	assert.Nil(t, err)
	expected := []local.ChatShort{
		{Id: id_a, Name: "A", LastMsg: "msg2"},
		{Id: id_b, Name: "B", Read: true},
	}
	assert.Equal(t, expected, list)
}

func TestDelete(t *testing.T) {
	repo := factories.NewDb().Chats
	id_a := local.ChatId{Id: uid1}
	id_b := local.ChatId{Id: uid2}
	chat_a := local.Chat{Id: id_a, Name: "A"}
	chat_b := local.Chat{Id: id_b, Name: "B"}
	assert.Nil(t, repo.Create(chat_a))
	assert.Nil(t, repo.Create(chat_b))

	_, err := repo.GetById(id_a)
	assert.Nil(t, err)

	assert.Nil(t, repo.Delete(id_a))

	_, err = repo.GetById(id_a)
	assert.Equal(t, errs.NotFound{}, err)
}
