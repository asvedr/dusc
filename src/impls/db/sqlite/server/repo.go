package server

import (
	"database/sql"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/impls/db/sqlite/db_locker"
	"dusc/src/proto"
	"strings"

	"gitlab.com/asvedr/safe_sqlite"
)

const cr_table = `CREATE TABLE IF NOT EXISTS dusc_server(
	addr TEXT PRIMARY KEY,
	type INTEGER
)`

type repo struct {
	db *db_locker.Locker
}

func New(db *db_locker.Locker) (proto.IServerRepo, error) {
	err := db.Db().Exec(cr_table)
	return repo{db: db}, err
}

func (self repo) Add(servers ...local.Server) error {
	if len(servers) == 0 {
		return nil
	}
	query := `
		INSERT INTO dusc_server
		(addr, type) VALUES
	` + strings.Repeat(",(?,?)", len(servers))[1:]
	params := []any{}
	for _, srv := range servers {
		params = append(
			params,
			srv.Addr,
			int(srv.Type),
		)
	}
	return self.db.Db().Exec(query, params...)
}

func (self repo) GetByType(tp local.ServerType) ([]local.Server, error) {
	query := `SELECT addr, type FROM dusc_server WHERE type = ?`
	return safe_sqlite.FetchSlice(
		self.db.Db(),
		deser_srv,
		query,
		int(tp),
	)
}

func (self repo) GetByAddr(addr string) (local.Server, error) {
	srv, err := safe_sqlite.FetchOne(
		self.db.Db(),
		deser_srv,
		`SELECT addr, type FROM dusc_server WHERE addr = ?`,
		addr,
	)
	if err != nil {
		return local.Server{}, err
	}
	if srv == nil {
		return local.Server{}, errs.NotFound{}
	}
	return *srv, err
}

func (self repo) Delete(addrs ...string) error {
	if len(addrs) == 0 {
		return nil
	}
	params := []any{}
	for _, addr := range addrs {
		params = append(params, addr)
	}
	tmpl := strings.Repeat(",?", len(addrs))
	return self.db.Db().Exec(
		"DELETE FROM dusc_server WHERE addr IN ("+tmpl[1:]+")",
		params...,
	)
}

func deser_srv(r *sql.Rows) (local.Server, error) {
	var srv local.Server
	err := r.Scan(&srv.Addr, &srv.Type)
	return srv, err
}
