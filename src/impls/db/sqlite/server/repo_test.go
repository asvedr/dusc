package server_test

import (
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/stretchr/testify/assert"
)

func servers() []local.Server {
	return []local.Server{
		{Addr: "abc.com", Type: local.ServerTypeExtStor},
		{Addr: "123.com", Type: local.ServerTypeSTUN},
	}
}

func TestGetByType(t *testing.T) {
	repo := factories.NewDb().Servers

	got, err := repo.GetByType(local.ServerTypeExtStor)
	assert.Nil(t, err)
	assert.Equal(t, 0, len(got))

	assert.Nil(t, repo.Add(servers()...))

	got, err = repo.GetByType(local.ServerTypeExtStor)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(got))
	assert.Equal(t, servers()[0], got[0])

	got, err = repo.GetByType(local.ServerTypeSTUN)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(got))
	assert.Equal(t, servers()[1], got[0])
}

func TestGetByAddr(t *testing.T) {
	repo := factories.NewDb().Servers

	_, err := repo.GetByAddr("abc.com")
	assert.Equal(t, errs.NotFound{}, err)

	assert.Nil(t, repo.Add(servers()...))

	got, err := repo.GetByAddr("abc.com")
	assert.Nil(t, err)
	assert.Equal(t, servers()[0], got)
}

func TestDelete(t *testing.T) {
	repo := factories.NewDb().Servers

	assert.Nil(t, repo.Add(servers()...))

	got, err := repo.GetByAddr("abc.com")
	assert.Nil(t, err)
	assert.Equal(t, servers()[0], got)

	assert.Nil(t, repo.Delete("abc.com"))

	_, err = repo.GetByAddr("abc.com")
	assert.Equal(t, errs.NotFound{}, err)
}
