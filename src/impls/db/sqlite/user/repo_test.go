package user_test

import (
	"crypto/rsa"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func uid1() uuid.UUID {
	val, err := uuid.Parse("cde7561a-f9c9-4c28-aefe-b13d9caaae68")
	if err != nil {
		panic(err.Error())
	}
	return val
}

func uid2() uuid.UUID {
	val, err := uuid.Parse("f9e4771c-8ccd-4c5f-b25b-f346f913da2c")
	if err != nil {
		panic(err.Error())
	}
	return val
}

func pub_key() *rsa.PublicKey {
	return &factories.PrivKey1.Get().PublicKey
}

func TestContact(t *testing.T) {
	repo := factories.NewDb().User
	err := repo.Contact("abc", uid1(), pub_key())
	assert.Nil(t, err)
	err = repo.Contact("abc", uid1(), pub_key())
	assert.Equal(t, errs.UidAlreadyUsed{}, err)
}

func TestGetPubKey(t *testing.T) {
	repo := factories.NewDb().User
	err := repo.Contact("abc", uid1(), pub_key())
	assert.Nil(t, err)

	key, err := repo.GetPubKey(uid1())
	assert.Nil(t, err)
	assert.Equal(t, pub_key(), key)

	key, err = repo.GetPubKey(uid1())
	assert.Nil(t, err)
	assert.Equal(t, pub_key(), key)

	_, err = repo.GetPubKey(uid2())
	assert.Equal(t, errs.NotFound{}, err)
}

func TestSetLastSeen(t *testing.T) {
	repo := factories.NewDb().User

	err := repo.Contact("abc", uid1(), pub_key())
	assert.Nil(t, err)
	assert.Nil(t, repo.SetLastSeen(uid1(), 123, "127.0.0.1"))
	assert.Nil(t, repo.SetLastSeen(uid1(), 12, "127.0.0.2"))

	ls, err := repo.GetLastSeen(uid1())
	assert.Nil(t, err)
	assert.Equal(t, uint64(123), ls)
}

func TestGetAddrs(t *testing.T) {
	repo := factories.NewDb().User

	err := repo.Contact("abc", uid1(), pub_key())
	assert.Nil(t, err)
	assert.Nil(t, repo.SetLastSeen(uid1(), 123, "a1"))
	assert.Nil(t, repo.SetLastSeen(uid1(), 1234, "a2"))

	addrs, err := repo.GetAddrs(uid1())
	assert.Nil(t, err)
	assert.Equal(t, addrs, []string{"a2", "a1"})
}

func TestGetAddrMap(t *testing.T) {
	repo := factories.NewDb().User

	err := repo.Contact("A", uid1(), pub_key())
	assert.Nil(t, err)
	err = repo.Contact("B", uid2(), &factories.PrivKey2.Get().PublicKey)
	assert.Nil(t, err)

	addr_map, err := repo.GetAddrMap()
	assert.Nil(t, err)
	assert.Equal(t, 0, len(addr_map))

	repo.SetLastSeen(uid1(), 1, "a.com")
	repo.SetLastSeen(uid1(), 2, "b.com")
	repo.SetLastSeen(uid2(), 3, "c.com")
	repo.SetLastSeen(uid2(), 4, "c.com")

	addr_map, err = repo.GetAddrMap()
	assert.Nil(t, err)
	assert.Equal(
		t,
		map[uuid.UUID][]string{
			uid1(): {"b.com", "a.com"},
			uid2(): {"c.com"},
		},
		addr_map,
	)
}

func TestGetPubKeyAndAddrs(t *testing.T) {
	repo := factories.NewDb().User

	err := repo.Contact("A", uid1(), pub_key())
	assert.Nil(t, err)

	key, addrs, err := repo.GetPubKeyAndAddrs(uid1())
	assert.Nil(t, err)
	assert.Equal(t, key, pub_key())
	assert.Nil(t, addrs)

	repo.SetLastSeen(uid1(), 1, "a.com")
	repo.SetLastSeen(uid1(), 2, "b.com")

	key, addrs, err = repo.GetPubKeyAndAddrs(uid1())
	assert.Nil(t, err)
	assert.Equal(t, key, pub_key())
	assert.Equal(t, addrs, []string{"b.com", "a.com"})

	_, _, err = repo.GetPubKeyAndAddrs(uid2())
	assert.Equal(t, errs.NotFound{}, err)
}

func TestGetUids(t *testing.T) {
	repo := factories.NewDb().User

	err := repo.Contact("A", uid1(), pub_key())
	assert.Nil(t, err)

	uids, err := repo.GetUids()
	assert.Nil(t, err)
	assert.Equal(t, []uuid.UUID{uid1()}, uids)
}

func TestDeleteExpiredAddrs(t *testing.T) {
	repo := factories.NewDb().User

	err := repo.Contact("A", uid1(), pub_key())
	assert.Nil(t, err)

	key, addrs, err := repo.GetPubKeyAndAddrs(uid1())
	assert.Nil(t, err)
	assert.Equal(t, key, pub_key())
	assert.Nil(t, addrs)

	repo.SetLastSeen(uid1(), 1, "a.com")
	repo.SetLastSeen(uid1(), 3, "b.com")

	assert.Nil(t, repo.DeleteExpiredAddrs(2))

	addrs, err = repo.GetAddrs(uid1())
	assert.Nil(t, err)
	assert.Equal(t, []string{"b.com"}, addrs)
}

func TestGetByUid(t *testing.T) {
	repo := factories.NewDb().User

	uid, _ := uuid.NewRandom()
	err := repo.Contact("A", uid, pub_key())
	assert.Nil(t, err)

	users, err := repo.GetByUid(uid)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(users))
	assert.Equal(
		t,
		local.User{
			Id:     uid,
			PubKey: pub_key(),
			Info:   &local.ApiUserMeta{Name: "A"},
			Status: local.UserStatusNew,
		},
		users[0],
	)

	err = repo.SetStatus(uid, local.UserStatusBanned)
	assert.Nil(t, err)

	users, err = repo.GetByUid(uid)
	assert.Nil(t, err)
	assert.Equal(
		t,
		local.User{
			Id:     uid,
			PubKey: pub_key(),
			Info:   &local.ApiUserMeta{Name: "A"},
			Status: local.UserStatusBanned,
		},
		users[0],
	)

	assert.Nil(t, repo.Delete(uid))

	users, err = repo.GetByUid(uid)
	assert.Nil(t, err)
	assert.Equal(t, 0, len(users))
}
