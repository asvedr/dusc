package user

import (
	"crypto/rsa"
	"database/sql"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/impls/db/sqlite/common"
	"dusc/src/impls/db/sqlite/db_locker"
	"dusc/src/proto"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/asvedr/safe_sqlite"
)

const q_cr_usr_table = `
	CREATE TABLE IF NOT EXISTS dusc_user (
		id TEXT PRIMARY KEY,
		name TEXT NOT NULL,
		pub_key TEXT NOT NULL,
		status INTEGER NOT NULL
	)
`
const q_cr_addr_table = `
	CREATE TABLE IF NOT EXISTS dusc_user_addr (
		addr TEXT PRIMARY KEY,
		user_id TEXT NOT NULL,
		last_seen INTEGER NOT NULL
	)
`

type repo struct {
	db      *db_locker.Locker
	key_ser proto.IStrSerializer[*rsa.PublicKey]

	key_cache map[uuid.UUID]*rsa.PublicKey
}

func New(
	db *db_locker.Locker,
	key_ser proto.IStrSerializer[*rsa.PublicKey],
) (proto.IUserRepo, error) {
	err := create_tables(db.Db())
	return repo{
		db:        db,
		key_ser:   key_ser,
		key_cache: map[uuid.UUID]*rsa.PublicKey{},
	}, err
}

func (self repo) Contact(name string, id uuid.UUID, key *rsa.PublicKey) error {
	err := self.db.Db().Exec(
		"INSERT INTO dusc_user (id, name, pub_key, status) VALUES (?, ?, ?, ?)",
		id.String(),
		name,
		self.key_ser.Write(key),
		local.UserStatusNew,
	)
	if common.IsUniqError(err) {
		err = errs.UidAlreadyUsed{}
	}
	if err != nil {
		return err
	}
	self.key_cache[id] = key
	return nil
}

func (self repo) GetPubKey(id uuid.UUID) (*rsa.PublicKey, error) {
	key := self.key_cache[id]
	if key != nil {
		return key, nil
	}
	s_key, err := safe_sqlite.FetchOne[string](
		self.db.Db(),
		func(r *sql.Rows) (string, error) {
			var buf string
			err := r.Scan(&buf)
			return buf, err
		},
		"SELECT pub_key FROM dusc_user WHERE id = ?",
		id,
	)
	if err != nil {
		return nil, err
	}
	if s_key == nil {
		return nil, errs.NotFound{}
	}
	key, err = self.key_ser.Read(*s_key)
	if err != nil {
		return nil, err
	}
	self.key_cache[id] = key
	return key, nil
}

func (self repo) SetLastSeen(id uuid.UUID, time uint64, addr string) error {
	query := `
		INSERT OR REPLACE INTO dusc_user_addr
		(addr, user_id, last_seen)
		VALUES (?, ?, ?)
	`
	return self.db.Db().Exec(
		query,
		addr,
		id.String(),
		time,
	)
}

func (self repo) GetLastSeen(id uuid.UUID) (uint64, error) {
	query := `
		SELECT last_seen FROM dusc_user_addr
		WHERE user_id = ?
		ORDER BY last_seen DESC
		LIMIT 1
	`
	ts, err := safe_sqlite.FetchOne(
		self.db.Db(),
		func(r *sql.Rows) (uint64, error) {
			var buf uint64
			err := r.Scan(&buf)
			return buf, err
		},
		query,
		id.String(),
	)
	if err != nil {
		return 0, err
	}
	if ts == nil {
		return 0, nil
	} else {
		return *ts, nil
	}
}

func (self repo) GetAddrs(id uuid.UUID) ([]string, error) {
	query := `
		SELECT addr FROM dusc_user_addr
		WHERE user_id = ?
		ORDER BY last_seen DESC
	`
	return safe_sqlite.FetchSlice(
		self.db.Db(),
		func(r *sql.Rows) (string, error) {
			var buf string
			err := r.Scan(&buf)
			return buf, err
		},
		query,
		id.String(),
	)
}

func (self repo) GetAddrMap() (map[uuid.UUID][]string, error) {
	type pair struct {
		user uuid.UUID
		addr string
	}
	pairs, err := safe_sqlite.FetchSlice(
		self.db.Db(),
		func(r *sql.Rows) (pair, error) {
			var res pair
			err := r.Scan(&res.user, &res.addr)
			return res, err
		},
		`SELECT user_id, addr
		 FROM dusc_user_addr
		 ORDER BY last_seen DESC`,
	)
	if err != nil {
		return nil, err
	}
	res := map[uuid.UUID][]string{}
	for _, pair := range pairs {
		addrs := res[pair.user]
		addrs = append(addrs, pair.addr)
		res[pair.user] = addrs
	}
	return res, nil
}

func (self repo) GetPubKeyAndAddrs(id uuid.UUID) (*rsa.PublicKey, []string, error) {
	key, err := self.GetPubKey(id)
	if err != nil {
		return nil, nil, err
	}
	addrs, err := self.GetAddrs(id)
	return key, addrs, err
}

func (self repo) GetUids() ([]uuid.UUID, error) {
	return safe_sqlite.FetchSlice(
		self.db.Db(),
		func(r *sql.Rows) (uuid.UUID, error) {
			var buf string
			err := r.Scan(&buf)
			if err != nil {
				return uuid.UUID{}, err
			}
			return uuid.Parse(buf)
		},
		"SELECT id FROM dusc_user ORDER BY id",
	)
}

func (self repo) DeleteExpiredAddrs(threshold_ts uint64) error {
	return self.db.Db().Exec(
		"DELETE FROM dusc_user_addr WHERE last_seen < ?",
		threshold_ts,
	)
}

func (self repo) GetByUid(ids ...uuid.UUID) ([]local.User, error) {
	if len(ids) == 0 {
		return []local.User{}, nil
	}
	tmpl := strings.Repeat(",?", len(ids))
	params := []any{}
	for _, id := range ids {
		params = append(params, id.String())
	}
	users, err := safe_sqlite.FetchSlice(
		self.db.Db(),
		deser_user,
		"SELECT id, name, status FROM dusc_user WHERE id IN ("+tmpl[1:]+")",
		params...,
	)
	if err != nil {
		return nil, err
	}
	for i, user := range users {
		err = self.enrich_user(&user)
		if err != nil {
			return nil, err
		}
		users[i] = user
	}
	return users, nil
}

func (self repo) SetStatus(id uuid.UUID, status local.UserStatus) error {
	return self.db.Db().Exec(
		"UPDATE dusc_user SET status = ? WHERE id = ?",
		int(status),
		id.String(),
	)
}

func (self repo) Delete(id uuid.UUID) error {
	return self.db.Db().Exec(
		"DELETE FROM dusc_user WHERE id = ?",
		id.String(),
	)
}

func (self repo) enrich_user(user *local.User) error {
	key, err := self.GetPubKey(user.Id)
	user.PubKey = key
	return err
}

func create_tables(db safe_sqlite.IDb) error {
	err := db.Exec(q_cr_usr_table)
	if err != nil {
		return err
	}
	return db.Exec(q_cr_addr_table)
}

func deser_user(rows *sql.Rows) (res local.User, err error) {
	var id string
	var meta local.ApiUserMeta
	err = rows.Scan(
		&id,
		&meta.Name,
		&res.Status,
	)
	if err != nil {
		return
	}
	res.Info = &meta
	res.Id, err = uuid.Parse(id)
	return
}
