package common

import "strings"

func IsUniqError(err error) bool {
	if err == nil {
		return false
	}
	return strings.Contains(err.Error(), "constraint failed: UNIQUE")
}
