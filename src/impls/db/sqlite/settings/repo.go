package settings

import (
	"crypto/rsa"
	"database/sql"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/impls/db/sqlite/db_locker"
	"dusc/src/proto"

	"github.com/google/uuid"
	"gitlab.com/asvedr/safe_sqlite"
)

const q_cr_table string = `
CREATE TABLE IF NOT EXISTS dusc_settings (
    Key string primary key,
    Val string not null
)
`

const (
	KeyKey     = "key"
	KeyUid     = "uid"
	KeyName    = "name"
	KeyDescr   = "description"
	KeyMeta    = "meta"
	KeyPwdHash = "pwd"
)

type repo struct {
	locker  *db_locker.Locker
	key_ser proto.IStrSerializer[*rsa.PrivateKey]
}

func New(
	locker *db_locker.Locker,
	key_ser proto.IStrSerializer[*rsa.PrivateKey],
) (proto.ISettingsRepo, error) {
	repo := repo{locker: locker, key_ser: key_ser}
	err := locker.Db().Exec(q_cr_table)
	return repo, err
}

func (self repo) GetMe() (*local.Me, error) {
	data, err := safe_sqlite.FetchMap[string, string](
		self.locker.Db(),
		func(r *sql.Rows) (string, string, error) {
			var key, val string
			err := r.Scan(&key, &val)
			return key, val, err
		},
		"SELECT Key, Val FROM dusc_settings WHERE Key IN (?, ?)",
		KeyKey,
		KeyUid,
	)
	if err != nil {
		return nil, err
	}
	if data[KeyKey] == "" || data[KeyUid] == "" {
		return nil, errs.NotInitialized{}
	}
	key, err := self.key_ser.Read(data[KeyKey])
	if err != nil {
		return nil, err
	}
	uid, err := uuid.Parse(data[KeyUid])
	return &local.Me{PrivKey: key, Uid: uid}, err
}

func (self repo) SetMe(me local.Me) error {
	return self.locker.Db().Exec(
		"INSERT OR REPLACE INTO dusc_settings (Key, Val) VALUES (?, ?), (?, ?)",
		KeyKey,
		self.key_ser.Write(me.PrivKey),
		KeyUid,
		me.Uid.String(),
	)
}

func (self repo) SetName(name string) error {
	return self.locker.Db().Exec(
		"INSERT OR REPLACE INTO dusc_settings (Key, Val) VALUES (?, ?)",
		KeyName,
		name,
	)
}

func (self repo) SetDescription(descr string) error {
	return self.locker.Db().Exec(
		"INSERT OR REPLACE INTO dusc_settings (Key, Val) VALUES (?, ?)",
		KeyDescr,
		descr,
	)
}

func (self repo) GetMeta() (res local.ApiUserMeta, err error) {
	res.Name, err = self.get_str_field(KeyName)
	if err != nil {
		return
	}
	res.Description, err = self.get_str_field(KeyDescr)
	return
}

func (self repo) CheckInited() (bool, error) {
	cnt, err := safe_sqlite.FetchOne[int](
		self.locker.Db(),
		func(r *sql.Rows) (int, error) {
			var cnt int
			err := r.Scan(&cnt)
			return cnt, err
		},
		"SELECT count(*) FROM dusc_settings WHERE Key IN (?, ?)",
		KeyKey,
		KeyUid,
	)
	return *cnt == 2, err
}

// opt[string]
func (self repo) SetPwdHash(hash string) error {
	return self.locker.Db().Exec(
		"INSERT OR REPLACE INTO dusc_settings (Key, Val) VALUES (?, ?)",
		KeyPwdHash,
		hash,
	)
}

// opt[string]
func (self repo) GetPwdHash() (string, error) {
	return self.get_str_field(KeyPwdHash)
}

func (self repo) get_str_field(key string) (string, error) {
	data, err := safe_sqlite.FetchOne[string](
		self.locker.Db(),
		func(r *sql.Rows) (string, error) {
			var val string
			err := r.Scan(&val)
			return val, err
		},
		"SELECT Val FROM dusc_settings WHERE Key = ?",
		key,
	)
	if err != nil {
		return "", err
	}
	if data == nil {
		return "", nil
	}
	return *data, nil
}
