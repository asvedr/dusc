package settings_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestName(t *testing.T) {
	repo := factories.NewDb().Settings

	meta, err := repo.GetMeta()
	assert.Nil(t, err)
	assert.Equal(t, meta.Name, "")

	assert.Nil(t, repo.SetName("abc"))
	meta, err = repo.GetMeta()
	assert.Nil(t, err)
	assert.Equal(t, meta.Name, "abc")
}

func TestDescription(t *testing.T) {
	repo := factories.NewDb().Settings

	meta, err := repo.GetMeta()
	assert.Nil(t, err)
	assert.Equal(t, meta.Description, "")

	assert.Nil(t, repo.SetDescription("abc"))
	meta, err = repo.GetMeta()
	assert.Nil(t, err)
	assert.Equal(t, meta.Description, "abc")
}

func TestPwd(t *testing.T) {
	repo := factories.NewDb().Settings

	hash, err := repo.GetPwdHash()
	assert.Nil(t, err)
	assert.Equal(t, "", hash)

	assert.Nil(t, repo.SetPwdHash("abc"))

	hash, err = repo.GetPwdHash()
	assert.Nil(t, err)
	assert.Equal(t, "abc", hash)
}

func TestInited(t *testing.T) {
	repo := factories.NewDb().Settings
	flag, err := repo.CheckInited()
	assert.Nil(t, err)
	assert.False(t, flag)
	err = repo.SetMe(local.Me{
		PrivKey: factories.PrivKey1.Get(),
		Uid:     factories.Uuid.Get(),
	})
	assert.Nil(t, err)
	flag, err = repo.CheckInited()
	assert.Nil(t, err)
	assert.True(t, flag)
}
