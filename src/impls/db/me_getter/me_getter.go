package me_getter

import (
	"dusc/src/entities/config"
	"dusc/src/entities/local"
	"dusc/src/proto"
)

type getter struct {
	source       proto.ISettingsRepo
	time         proto.ITime
	me           *local.Me
	update_me_at uint64
	me_lifetime  uint64
}

func New(
	source proto.ISettingsRepo,
	time proto.ITime,
	config *config.Config,
) proto.IMeGetter {
	return &getter{
		time:        time,
		source:      source,
		me_lifetime: uint64(config.Me_Lifetime),
	}
}

func (self *getter) GetMe() (*local.Me, error) {
	now := self.time.Now()
	if self.me == nil || self.update_me_at < now {
		me, err := self.source.GetMe()
		if err != nil {
			return nil, err
		}
		self.me = me
		self.update_me_at = now + self.me_lifetime
	}
	return self.me, nil
}
