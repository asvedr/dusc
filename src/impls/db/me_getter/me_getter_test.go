package me_getter_test

import (
	"dusc/src/entities/config"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/db/me_getter"
	"dusc/src/impls/serializers/key_serializer"
	"testing"

	"github.com/stretchr/testify/assert"
)

func serialize(me local.Me) string {
	uid := me.Uid.String()
	key := key_serializer.NewPrivStr().Write(me.PrivKey)
	return uid + "\n" + key
}

func TestGetter(t *testing.T) {
	repo := factories.NewDb().Settings
	time := &factories.Time{}
	getter := me_getter.New(
		repo,
		time,
		&config.Config{Me_Lifetime: 3},
	)

	me1 := local.Me{
		PrivKey: factories.PrivKey1.Get(),
		Uid:     factories.Uuid.Get(),
	}
	me2 := local.Me{
		PrivKey: factories.PrivKey2.Get(),
		Uid:     factories.Uuid.Get(),
	}

	assert.Nil(t, repo.SetMe(me1))
	me, err := getter.GetMe()
	assert.Nil(t, err)
	assert.Equal(t, serialize(me1), serialize(*me))

	repo.SetMe(me2)
	me, err = getter.GetMe()
	assert.Nil(t, err)
	assert.Equal(t, serialize(me1), serialize(*me))

	time.NowData += 5
	me, err = getter.GetMe()
	assert.Nil(t, err)
	assert.Equal(t, serialize(me2), serialize(*me))
}
