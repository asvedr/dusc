package hasher_test

import (
	"dusc/src/impls/common/hasher"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHashMsg(t *testing.T) {
	h := hasher.New()

	hash1 := h.GetSum([]byte{1, 2, 3})
	hash2 := h.GetSum([]byte{3, 2, 1})
	hash3 := h.GetSum([]byte{1, 2, 3})

	assert.Equal(t, hash1, hash3)
	assert.NotEqual(t, hash1, hash2)
}
