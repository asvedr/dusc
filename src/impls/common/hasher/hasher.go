package hasher

import (
	"crypto/sha1"
	"dusc/src/proto"
	"encoding/base64"
)

type hasher struct{}

func New() proto.IHasher {
	return hasher{}
}

func (self hasher) GetSum(bts []byte) []byte {
	hasher := sha1.New()
	hasher.Write(bts)
	return hasher.Sum(nil)
}

func (self hasher) GetStrSum(bts []byte) string {
	hashed := self.GetSum(bts)
	return base64.StdEncoding.EncodeToString(hashed)
}
