package crypto_test

import (
	"dusc/src/entities/busmsg"
	"dusc/src/factories"
	"dusc/src/impls/common/crypto"
	"dusc/src/impls/common/hasher"
	"dusc/src/impls/serializers/key_serializer"
	"dusc/src/impls/serializers/msg_serializer"
	"dusc/src/impls/serializers/serde"
	"dusc/src/proto"
	"encoding/base64"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func new_crypto() proto.ICrypto {
	key_ser := key_serializer.NewPub()
	msg_ser := msg_serializer.NewSigned(key_ser)
	return crypto.New(
		factories.Rand{},
		serde.New(),
		msg_ser,
		hasher.New(),
	)
}

func msg() busmsg.Signed {
	msg_id, _ := uuid.Parse("1420b3fe-af30-436e-bf56-29ac34f61811")
	sender, _ := uuid.Parse("26133130-d5ab-4985-b53a-9dc2ad5d6980")
	msg := &busmsg.Text{
		Base: busmsg.Base{
			CreatedAt: 123456,
			SenderId:  sender,
		},
		MsgId: msg_id,
		Text:  "abcd",
	}
	return busmsg.Signed{
		Msg:       msg,
		Signature: []byte{1, 2, 3, 4, 5},
	}
}

func TestValid(t *testing.T) {
	key := factories.PrivKey1.Get()
	cr := new_crypto()
	code, err := cr.Encrypt(msg(), &key.PublicKey)
	assert.Nil(t, err)
	dec, err := cr.Decrypt(code, key)
	assert.Nil(t, err)
	assert.Equal(t, msg(), dec)
}

func TestInvalidKey(t *testing.T) {
	cr := new_crypto()
	code, err := cr.Encrypt(msg(), &factories.PrivKey1.Get().PublicKey)
	assert.Nil(t, err)
	_, err = cr.Decrypt(code, factories.PrivKey2.Get())
	assert.NotNil(t, err)
}

func TestInvalidData(t *testing.T) {
	key := factories.PrivKey1.Get()
	cr := new_crypto()
	code, err := cr.Encrypt(msg(), &key.PublicKey)
	assert.Nil(t, err)
	_, err = cr.Decrypt(code[2:], key)
	assert.NotNil(t, err)
}

func TestLongMessage(t *testing.T) {
	sender, _ := uuid.Parse("26133130-d5ab-4985-b53a-9dc2ad5d6980")
	sig, err := base64.StdEncoding.DecodeString(
		"Tgw26ZdGDSgRi+cFE8qu1+gJ3rSgW9YCREBQ3NAO3MJtb3Oo4dawODUTPA/0f9DlEl0rd9Hqu5TMKLdY9uO5Y4Xp5vnEEJdhdtQPH5e8JIAd8ZI3ai4kOUlHyxyNq+5y3Xi6h/1U8wroViXsWz14eYpz3Yny5QhA/TYWhqaow7iTR1UNGMkIaFNg+TeygHU9n8Gv+e4yPjlROGkM0bDXqfnDUUqaUWB7AuK4zN6vvqPKTIdB40jX40Am0qVebX4OBiNGm4d/zq4oBwhBh7WDiauggcl6NGZwsNLTrw7Y2gjtirEmx+rXUDVaXWFtdHlD0+odcJpSrshdwfJHUdkyvVFnVkMWEurEyrRKgfH+oE8wKLOxboj2LfOEAEAdQz+RNXXv+jUwEX4iX6y5zhCUHT+ChmA9tlNc0tEjPtFSkueptdMbjO7FzB0OG49O2GTQKY2GKStui1sXxhkL/5t6Oirj0Jde3JHf/6NleEVaiLbI3j6ji+oOzlcRdTmmdgyCbCzdZq/C05U0jHavrUIdnY94PUeUdcZ6Xz2JBdpceKuwMC2ikGdDsAwB9JJh2cegCVIGsIBiLxVgSNU/hIOJofL34wW60yMXXH4B7P2QAI8gjUunft8IAGZztjOOJLl71FINhmqs6eTaju3KkrisO5yKqPFnpE3h+yS/1n4Yo8k=",
	)
	assert.Nil(t, err)
	msg := busmsg.Signed{
		Msg: &busmsg.Ping{
			Base: busmsg.Base{
				CreatedAt: 123456,
				SenderId:  sender,
			},
			SentFromAddr: "127.0.0.1:8086",
			SendToAddr:   "127.0.0.1:8088",
			PublicKey:    &factories.PrivKey1.Get().PublicKey,
		},
		Signature: sig,
	}
	cr := new_crypto()
	key := factories.PrivKey2.Get()

	/*code,*/
	_, err = cr.Encrypt(msg, &key.PublicKey)
	assert.Nil(t, err)
	// dec, err := cr.Decrypt(code, key)
	// assert.Nil(t, err)
	// assert.Equal(t, msg, dec)
}
