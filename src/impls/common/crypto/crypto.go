package crypto

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/proto"
	"dusc/src/utils"
	"encoding/base64"
	"fmt"
	"io"
)

const enc_chunk_len = 200 // 245

type crypto struct {
	rand       io.Reader
	serde      proto.ISerdeMaker
	serializer proto.ISerializer[busmsg.Signed]
	hasher     proto.IHasher
}

func New(
	rand io.Reader,
	serde proto.ISerdeMaker,
	serializer proto.ISerializer[busmsg.Signed],
	hasher proto.IHasher,
) proto.ICrypto {
	return &crypto{
		rand:       rand,
		serde:      serde,
		serializer: serializer,
		hasher:     hasher,
	}
}

func (self *crypto) Encrypt(msg busmsg.Signed, key *rsa.PublicKey) ([]byte, error) {
	f := func(chunk []byte) ([]byte, error) {
		return rsa.EncryptPKCS1v15(self.rand, key, chunk)
	}
	bts := utils.DumpBts(self.serde, self.serializer, msg)
	res, err := process_bytes(f, bts, enc_chunk_len)
	return res, err
}

func (self *crypto) Decrypt(bts []byte, key *rsa.PrivateKey) (busmsg.Signed, error) {
	f := func(chunk []byte) ([]byte, error) {
		return rsa.DecryptPKCS1v15(self.rand, key, chunk)
	}
	decrypted, err := process_bytes(f, bts, key.Size())
	if err != nil {
		return busmsg.Signed{}, err
	}
	buf := self.serde.NewReader(decrypted)
	return self.serializer.Read(buf)
}

func (self *crypto) GetKeyCode(key *rsa.PublicKey) string {
	bts := key.N.Bytes()
	bts = append(bts, []byte(fmt.Sprint(key.E))...)
	res := self.hasher.GetSum(bts)
	return base64.StdEncoding.EncodeToString(res)
}

func (self *crypto) GenPrivKey() (*rsa.PrivateKey, error) {
	return rsa.GenerateKey(self.rand, 4096)
}

func process_bytes(
	f func([]byte) ([]byte, error),
	src []byte,
	step int,
) ([]byte, error) {
	src_len := len(src)
	result := []byte{}
	for start := 0; start < src_len; start += step {
		end := min(start+step, src_len)
		processed, err := f(src[start:end])
		if err != nil {
			return nil, err
		}
		result = append(result, processed...)
	}
	return result, nil
}
