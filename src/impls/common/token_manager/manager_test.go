package token_manager_test

import (
	"dusc/src/impls/common/token_manager"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestModeOff(t *testing.T) {
	man := token_manager.New()
	assert.False(t, man.IsAuthTurnedOn())
	tkn, err := man.GenToken()
	assert.Nil(t, err)
	assert.Equal(t, "", tkn)
	assert.True(t, man.ValidateToken("abc"))
}

func TestModeOn(t *testing.T) {
	man := token_manager.New()
	man.SetActive(true)
	assert.True(t, man.IsAuthTurnedOn())
	tkn, err := man.GenToken()
	assert.Nil(t, err)
	assert.NotEqual(t, "", tkn)
	assert.False(t, man.ValidateToken("abc"))
	assert.True(t, man.ValidateToken(tkn))
}
