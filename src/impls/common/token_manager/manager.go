package token_manager

import (
	"dusc/src/proto"
	"strings"
	"sync"

	"github.com/google/uuid"
)

type manager struct {
	mtx   sync.Mutex
	is_on bool
	token string
}

func New() proto.IAuthTokenManager {
	return &manager{}
}

func (self *manager) SetActive(value bool) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	self.is_on = value
}

func (self *manager) IsAuthTurnedOn() bool {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	return self.is_on
}

func (self *manager) GenToken() (string, error) {
	id, err := uuid.NewRandom()
	if err != nil {
		return "", err
	}
	token := strings.ReplaceAll(id.String(), "-", "")

	self.mtx.Lock()
	defer self.mtx.Unlock()
	if !self.is_on {
		return "", nil
	}

	self.token = token
	return token, nil
}

func (self *manager) ValidateToken(token string) bool {
	self.mtx.Lock()
	defer self.mtx.Unlock()

	return !self.is_on || self.token == token
}
