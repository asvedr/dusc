package time

import (
	"dusc/src/proto"
	"time"
)

type timer struct{}

func New() proto.ITime {
	return timer{}
}

func (timer) Now() uint64 {
	tm := time.Now()
	return uint64(tm.Unix())
}
