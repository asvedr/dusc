package signer

import (
	"crypto"
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/proto"
	"dusc/src/utils"
	"io"
)

type signer struct {
	serde      proto.ISerdeMaker
	serializer proto.ISerializer[busmsg.Signed]
	rand       io.Reader
	pss_opts   *rsa.PSSOptions
	hash       crypto.Hash
}

func New(
	rand io.Reader,
	serde proto.ISerdeMaker,
	serializer proto.ISerializer[busmsg.Signed],
) proto.IMsgSigner {
	hash := crypto.SHA256
	return &signer{
		serde:      serde,
		serializer: serializer,
		rand:       rand,
		hash:       hash,
		pss_opts:   &rsa.PSSOptions{SaltLength: 4, Hash: hash},
	}
}

func (self *signer) Sign(msg busmsg.DynMsg, key *rsa.PrivateKey) (busmsg.Signed, error) {
	sign, err := rsa.SignPSS(
		self.rand,
		key,
		self.hash,
		self.dump(msg),
		self.pss_opts,
	)
	return busmsg.Signed{Msg: msg, Signature: sign}, err
}

func (self *signer) Validate(msg busmsg.Signed, key *rsa.PublicKey) bool {
	err := rsa.VerifyPSS(
		key,
		self.hash,
		self.dump(msg.Msg),
		msg.Signature,
		self.pss_opts,
	)
	return err == nil
}

func (self *signer) dump(msg busmsg.DynMsg) []byte {
	bts := utils.DumpBts(
		self.serde,
		self.serializer,
		busmsg.Signed{Msg: msg},
	)
	h := self.hash.New()
	h.Write(bts)
	return h.Sum(nil)
}
