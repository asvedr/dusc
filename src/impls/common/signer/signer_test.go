package signer_test

import (
	"dusc/src/entities/busmsg"
	"dusc/src/factories"
	"dusc/src/impls/common/signer"
	"dusc/src/impls/serializers/key_serializer"
	"dusc/src/impls/serializers/msg_serializer"
	"dusc/src/impls/serializers/serde"
	"dusc/src/proto"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func new_signer() proto.IMsgSigner {
	return signer.New(
		factories.Rand{},
		serde.New(),
		msg_serializer.NewSigned(
			key_serializer.NewPub(),
		),
	)
}

func TestSameMsg(t *testing.T) {
	key := factories.PrivKey1.Get()
	msg_id, _ := uuid.Parse("1420b3fe-af30-436e-bf56-29ac34f61811")
	sender, _ := uuid.Parse("26133130-d5ab-4985-b53a-9dc2ad5d6980")

	msg := &busmsg.Text{
		Base:  busmsg.Base{SenderId: sender},
		MsgId: msg_id,
		Text:  "abcdef",
	}

	signed, err := new_signer().Sign(msg, key)

	assert.Nil(t, err)

	to_check := busmsg.Signed{
		Msg: &busmsg.Text{
			Base:  busmsg.Base{SenderId: sender},
			MsgId: msg_id,
			Text:  "abcdef",
		},
		Signature: signed.Signature,
	}

	status := new_signer().Validate(to_check, &key.PublicKey)
	assert.True(t, status)
}

func TestDiffMsg(t *testing.T) {
	key := factories.PrivKey1.Get()
	msg_id, _ := uuid.Parse("1420b3fe-af30-436e-bf56-29ac34f61811")
	sender, _ := uuid.Parse("26133130-d5ab-4985-b53a-9dc2ad5d6980")

	msg := &busmsg.Text{
		Base:  busmsg.Base{SenderId: sender},
		MsgId: msg_id,
		Text:  "abcdef",
	}

	signed, err := new_signer().Sign(msg, key)

	assert.Nil(t, err)

	to_check := busmsg.Signed{
		Msg: &busmsg.Text{
			Base:  busmsg.Base{SenderId: sender},
			MsgId: msg_id,
			Text:  "1234",
		},
		Signature: signed.Signature,
	}

	status := new_signer().Validate(to_check, &key.PublicKey)
	assert.False(t, status)
}

func TestInvalidSig(t *testing.T) {
	msg_id, _ := uuid.Parse("1420b3fe-af30-436e-bf56-29ac34f61811")
	sender, _ := uuid.Parse("26133130-d5ab-4985-b53a-9dc2ad5d6980")

	to_check := busmsg.Signed{
		Msg: &busmsg.Text{
			Base:  busmsg.Base{SenderId: sender},
			MsgId: msg_id,
			Text:  "abcdef",
		},
		Signature: []byte{1, 2, 3},
	}

	key := factories.PrivKey1.Get()

	status := new_signer().Validate(to_check, &key.PublicKey)
	assert.False(t, status)

	to_check.Signature = nil
	status = new_signer().Validate(to_check, &key.PublicKey)
	assert.False(t, status)
}
