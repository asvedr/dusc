package msg_dispatcher

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"
	"log"
	"reflect"

	"github.com/google/uuid"
)

type dispatcher struct {
	msg_repo    proto.IMsgRepo
	user_repo   proto.IUserRepo
	chat_repo   proto.IChatRepo
	time        proto.ITime
	downloader  proto.IFileDownloader
	uploader    proto.IFileUploader
	msg_send_wf proto.IMsgSendWorkflow
}

func New(
	msg_repo proto.IMsgRepo,
	user_repo proto.IUserRepo,
	chat_repo proto.IChatRepo,
	time proto.ITime,
	downloader proto.IFileDownloader,
	uploader proto.IFileUploader,
	msg_send_wf proto.IMsgSendWorkflow,
) proto.IMsgDispatcher {
	return &dispatcher{
		msg_repo:    msg_repo,
		user_repo:   user_repo,
		chat_repo:   chat_repo,
		time:        time,
		downloader:  downloader,
		uploader:    uploader,
		msg_send_wf: msg_send_wf,
	}
}

func (self *dispatcher) Dispatch(msg busmsg.DynMsg) error {
	log.Printf("DISPATCH MSG: %v", msg)
	switch body := msg.(type) {
	case *busmsg.Text:
		return self.dispatch_text(body)
	case *busmsg.FileMeta:
		return self.dispatch_file_meta(body)
	case *busmsg.FileSegment:
		return self.dispatch_file_seg(body)
	case *busmsg.FileSegmentRequest:
		return self.dispatch_file_seg_req(body)
	case *busmsg.SignalConfirmReceived:
		return self.dispatch_sig_conf(body)
	case *busmsg.Ping:
		return self.dispatch_ping(body)
	case *busmsg.Pong:
		return self.dispatch_pong(body)
	case *busmsg.Invite:
		return self.dispatch_invite(body)
	default:
		panic("unsupported msg type: " + reflect.TypeOf(msg).String())
	}
}

func (self *dispatcher) dispatch_text(msg *busmsg.Text) error {
	chat := make_chat_id(msg.SenderId, msg.ChatId)
	err := self.create_chat_if_required(chat, msg.SenderId)
	if err != nil {
		return err
	}
	new_msg := local.NewMsg{
		Sender:     &msg.SenderId,
		Chat:       chat,
		Uid:        msg.MsgId,
		Text:       msg.Text,
		Visible:    true,
		Received:   true,
		Read:       false,
		CreatedAt:  msg.CreatedAt,
		ReceivedAt: self.time.Now(),
	}
	err = self.msg_repo.Create(new_msg)
	return self.send_cofirmation(msg.SenderId, msg.MsgId, err)
}

func (self *dispatcher) dispatch_file_meta(msg *busmsg.FileMeta) error {
	chat := make_chat_id(msg.SenderId, msg.ChatId)
	err := self.create_chat_if_required(chat, msg.SenderId)
	if err != nil {
		return err
	}
	new_msg := local.NewMsg{
		Sender:  &msg.SenderId,
		Chat:    chat,
		Uid:     msg.MsgId,
		Text:    msg.FileName,
		Visible: true,
		AttachFileMeta: &local.AttachFileMeta{
			FileId: msg.FileId,
			Parts:  int16(msg.Parts),
			Size:   msg.FileSize,
		},
		Received:   true,
		Read:       false,
		CreatedAt:  msg.CreatedAt,
		ReceivedAt: self.time.Now(),
	}
	err = self.msg_repo.Create(new_msg)
	return self.send_cofirmation(msg.SenderId, msg.MsgId, err)
}

func (self *dispatcher) dispatch_file_seg(msg *busmsg.FileSegment) error {
	return self.downloader.AddBlock(
		msg.SenderId,
		msg.FileId,
		int(msg.Part),
		msg.Data,
	)
}

func (self *dispatcher) dispatch_file_seg_req(msg *busmsg.FileSegmentRequest) error {
	upl := &busmsg.FileSegment{
		FileId: msg.FileId,
		Part:   msg.Part,
	}
	enc_key, addrs, err := self.user_repo.GetPubKeyAndAddrs(msg.SenderId)
	if err != nil {
		return err
	}
	routine := func() {
		data, err := self.uploader.GetFilePart(
			msg.FileId,
			msg.SenderId,
			int(msg.Part),
		)
		if err != nil {
			log.Printf("Uploading file error: %v", err)
			return
		}
		upl.Data = data
		self.msg_send_wf.AddToQueue(msg.SenderId, addrs, upl, enc_key)
	}
	go routine()
	return nil
}

func (self *dispatcher) dispatch_sig_conf(msg *busmsg.SignalConfirmReceived) error {
	return self.msg_repo.MarkReceived(msg.MsgId)
}

func (self *dispatcher) dispatch_ping(ping *busmsg.Ping) error {
	pong := &busmsg.Pong{SendToAddr: ping.SendToAddr}
	self.msg_send_wf.AddToQueue(
		ping.SenderId,
		[]string{ping.SentFromAddr},
		pong,
		ping.PublicKey,
	)
	key, err := self.user_repo.GetPubKey(ping.SenderId)
	if err != nil {
		return err
	}
	log.Printf(
		"GOT PING FOR %s. PUB KEY MATCHED? %v",
		ping.SenderId,
		key.Equal(ping.PublicKey),
	)
	if key.Equal(ping.PublicKey) {
		self.user_repo.SetLastSeen(
			ping.SenderId,
			self.time.Now(),
			ping.SentFromAddr,
		)
	}
	return nil
}

func (self *dispatcher) dispatch_pong(msg *busmsg.Pong) error {
	return self.user_repo.SetLastSeen(
		msg.SenderId,
		self.time.Now(),
		msg.SendToAddr,
	)
}

func (self *dispatcher) dispatch_invite(msg *busmsg.Invite) error {
	participants := []uuid.UUID{msg.SenderId}
	for uid, key := range msg.Users {
		add, err := self.validate_participant(uid, key)
		if err != nil {
			return err
		}
		if add {
			participants = append(participants, uid)
		}
	}
	return self.chat_repo.Create(local.Chat{
		Id:    local.ChatId{IsGroup: true, Id: msg.GroupId},
		Name:  msg.Name,
		Users: participants,
	})
}

func (self *dispatcher) validate_participant(
	uid uuid.UUID,
	msg_key *rsa.PublicKey,
) (bool, error) {
	key, err := self.user_repo.GetPubKey(uid)
	if err == nil {
		if key.Equal(msg_key) {
			log.Printf("Pub key missmatched for %s", uid.String())
			return true, nil
		}
		return false, nil
	}
	if !errors.As(err, &errs.NotFound{}) {
		return false, err
	}
	err = self.user_repo.Contact(uid.String(), uid, msg_key)
	return true, err
}

func (self *dispatcher) send_cofirmation(
	user_id uuid.UUID,
	msg_id uuid.UUID,
	err_added error,
) error {
	is_nil := err_added != nil
	is_duplicate := errors.Is(err_added, errs.UidAlreadyUsed{})
	if is_nil && !is_duplicate {
		return err_added
	}
	if is_duplicate {
		log.Printf("Received a duplicate for msg:%s", msg_id)
	}
	return self.msg_send_wf.AddToQueueMinial(
		user_id,
		&busmsg.SignalConfirmReceived{MsgId: msg_id},
	)
}

func (self *dispatcher) create_chat_if_required(
	chat local.ChatId,
	users ...uuid.UUID,
) error {
	return self.chat_repo.Create(local.Chat{Id: chat, Users: users})
}

func make_chat_id(
	user_id uuid.UUID,
	chat_id *uuid.UUID,
) local.ChatId {
	var res local.ChatId
	if chat_id == nil {
		res.Id = user_id
	} else {
		res.Id = *chat_id
		res.IsGroup = true
	}
	return res
}
