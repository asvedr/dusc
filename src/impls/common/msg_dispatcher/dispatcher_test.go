package msg_dispatcher_test

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/common/msg_dispatcher"
	"dusc/src/proto"
	"dusc/src/utils"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

type Setup struct {
	db          factories.Db
	time        *factories.Time
	downloader  *factories.Downloader
	uploader    *factories.Uploader
	msg_send_wf *factories.MsgSendWF
	dispatcher  proto.IMsgDispatcher
}

func new_setup() Setup {
	db := factories.NewDb()
	time := &factories.Time{}
	downloader := &factories.Downloader{}
	uploader := &factories.Uploader{}
	msg_send_wf := &factories.MsgSendWF{}

	dispatcher := msg_dispatcher.New(
		db.Msg,
		db.User,
		db.Chats,
		time,
		downloader,
		uploader,
		msg_send_wf,
	)
	return Setup{
		db:          db,
		time:        time,
		downloader:  downloader,
		uploader:    uploader,
		msg_send_wf: msg_send_wf,
		dispatcher:  dispatcher,
	}
}

func TestDispatchPrivateText(t *testing.T) {
	setup := new_setup()
	setup.time.NowData = 2
	sender_id, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()
	msg := &busmsg.Text{
		Base:   busmsg.Base{CreatedAt: 1, SenderId: sender_id},
		MsgId:  msg_id,
		ChatId: nil,
		Text:   "hello",
	}
	assert.Nil(t, setup.dispatcher.Dispatch(msg))
	db_msg, err := setup.db.Msg.GetByUid(msg_id)
	assert.Nil(t, err)
	exp_db_msg := local.NewMsg{
		Sender:     &sender_id,
		Chat:       local.ChatId{Id: sender_id},
		Uid:        msg_id,
		Text:       "hello",
		Visible:    true,
		Received:   true,
		Read:       false,
		CreatedAt:  1,
		ReceivedAt: 2,
	}
	assert.Equal(t, exp_db_msg, db_msg.NewMsg)
	chat, err := setup.db.Chats.GetById(local.ChatId{Id: sender_id})
	assert.Nil(t, err)
	assert.Equal(t, "", chat.Name)
	assert.Equal(t, []uuid.UUID{sender_id}, chat.Users)
	assert.Equal(t, 1, len(setup.msg_send_wf.Calls))
	confirm := factories.MsgSendWFMinial{
		Receiver: sender_id,
		Msg:      &busmsg.SignalConfirmReceived{MsgId: msg_id},
	}
	assert.Equal(t, confirm, setup.msg_send_wf.Calls[0])
}

func TestDispatchGroupText(t *testing.T) {
	setup := new_setup()
	setup.time.NowData = 2
	sender_id, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()
	chat_id, _ := uuid.NewRandom()
	msg := &busmsg.Text{
		Base:   busmsg.Base{CreatedAt: 1, SenderId: sender_id},
		MsgId:  msg_id,
		ChatId: &chat_id,
		Text:   "hello",
	}
	assert.Nil(t, setup.dispatcher.Dispatch(msg))
	db_msg, err := setup.db.Msg.GetByUid(msg_id)
	assert.Nil(t, err)
	exp_db_msg := local.NewMsg{
		Sender:     &sender_id,
		Chat:       local.ChatId{IsGroup: true, Id: chat_id},
		Uid:        msg_id,
		Text:       "hello",
		Visible:    true,
		Received:   true,
		Read:       false,
		CreatedAt:  1,
		ReceivedAt: 2,
	}
	assert.Equal(t, exp_db_msg, db_msg.NewMsg)
	chat, err := setup.db.Chats.GetById(
		local.ChatId{IsGroup: true, Id: chat_id},
	)
	assert.Nil(t, err)
	assert.Equal(t, "", chat.Name)
	assert.Equal(t, []uuid.UUID{sender_id}, chat.Users)
	assert.Equal(t, 1, len(setup.msg_send_wf.Calls))
	confirm := factories.MsgSendWFMinial{
		Receiver: sender_id,
		Msg:      &busmsg.SignalConfirmReceived{MsgId: msg_id},
	}
	assert.Equal(t, confirm, setup.msg_send_wf.Calls[0])
}

func TestDispatchFileMeta(t *testing.T) {
	setup := new_setup()
	setup.time.NowData = 2
	sender_id, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()
	msg := &busmsg.FileMeta{
		Base:     busmsg.Base{CreatedAt: 1, SenderId: sender_id},
		MsgId:    msg_id,
		FileId:   file_id,
		ChatId:   nil,
		FileName: "file.txt",
		FileSize: 100,
		Parts:    5,
	}
	assert.Nil(t, setup.dispatcher.Dispatch(msg))
	db_msg, err := setup.db.Msg.GetByUid(msg_id)
	assert.Nil(t, err)
	exp_db_msg := local.NewMsg{
		Sender: &sender_id,
		Chat:   local.ChatId{Id: sender_id},
		Uid:    msg_id,
		Text:   "file.txt",
		AttachFileMeta: &local.AttachFileMeta{
			FileId: file_id,
			Parts:  5,
			Size:   100,
		},
		Visible:    true,
		Received:   true,
		Read:       false,
		CreatedAt:  1,
		ReceivedAt: 2,
	}
	assert.Equal(t, exp_db_msg, db_msg.NewMsg)
	chat, err := setup.db.Chats.GetById(local.ChatId{Id: sender_id})
	assert.Nil(t, err)
	assert.Equal(t, "", chat.Name)
	assert.Equal(t, []uuid.UUID{sender_id}, chat.Users)
	assert.Equal(t, 1, len(setup.msg_send_wf.Calls))
	confirm := factories.MsgSendWFMinial{
		Receiver: sender_id,
		Msg:      &busmsg.SignalConfirmReceived{MsgId: msg_id},
	}
	assert.Equal(t, confirm, setup.msg_send_wf.Calls[0])
}

func TestDispatchFileSegment(t *testing.T) {
	setup := new_setup()
	setup.time.NowData = 2
	sender_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()
	msg := &busmsg.FileSegment{
		Base:   busmsg.Base{SenderId: sender_id},
		FileId: file_id,
		Part:   3,
		Data:   []byte{10, 20, 30},
	}
	assert.Nil(t, setup.dispatcher.Dispatch(msg))
	assert.Equal(t, 1, len(setup.downloader.AddBlockCalls))
	call := factories.AddBlockCall{
		UserId: sender_id,
		FileId: file_id,
		Part:   3,
		Data:   []byte{10, 20, 30},
	}
	assert.Equal(t, call, setup.downloader.AddBlockCalls[0])
}

func TestDispatchFileSegmentRequest(t *testing.T) {
	setup := new_setup()
	setup.time.NowData = 2
	sender_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()

	setup.db.User.Contact(
		"alex",
		sender_id,
		&factories.PrivKey2.Get().PublicKey,
	)
	setup.db.User.SetLastSeen(sender_id, 1, "local")
	setup.uploader.AddGetFilePart(
		file_id,
		sender_id,
		2,
		[]byte("data"),
	)

	msg := &busmsg.FileSegmentRequest{
		Base:   busmsg.Base{SenderId: sender_id},
		FileId: file_id,
		Part:   2,
	}
	assert.Nil(t, setup.dispatcher.Dispatch(msg))

	time.Sleep(time.Millisecond * 500)

	assert.Equal(t, 1, len(setup.msg_send_wf.Calls))
	call := factories.MsgSendWFFull{
		Receiver: sender_id,
		Addrs:    []string{"local"},
		Msg: &busmsg.FileSegment{
			FileId: file_id,
			Part:   2,
			Data:   []byte("data"),
		},
		EncKey: &factories.PrivKey2.Get().PublicKey,
	}
	assert.Equal(t, call, setup.msg_send_wf.Calls[0])
}

func TestDispatchSignalConfirmReceived(t *testing.T) {
	setup := new_setup()
	setup.time.NowData = 2
	sender_id, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()

	setup.db.Msg.Create(local.NewMsg{
		Chat:     local.ChatId{Id: sender_id},
		Uid:      msg_id,
		Text:     "x",
		Received: false,
	})

	msg := &busmsg.SignalConfirmReceived{
		Base:  busmsg.Base{SenderId: sender_id},
		MsgId: msg_id,
	}
	assert.Nil(t, setup.dispatcher.Dispatch(msg))

	db_msg, err := setup.db.Msg.GetByUid(msg_id)
	assert.Nil(t, err)
	assert.True(t, db_msg.Received)
}

func TestDispatchPing(t *testing.T) {
	setup := new_setup()
	setup.time.NowData = 2
	sender_id, _ := uuid.NewRandom()

	setup.db.User.Contact(
		"abc",
		sender_id,
		&factories.PrivKey1.Get().PublicKey,
	)

	msg := &busmsg.Ping{
		Base:         busmsg.Base{SenderId: sender_id},
		SentFromAddr: "#from",
		SendToAddr:   "#to",
		PublicKey:    &factories.PrivKey1.Get().PublicKey,
	}
	assert.Nil(t, setup.dispatcher.Dispatch(msg))

	addrs, err := setup.db.User.GetAddrs(sender_id)
	assert.Nil(t, err)
	assert.Equal(t, []string{"#from"}, addrs)

	assert.Equal(t, 1, len(setup.msg_send_wf.Calls))
	call := factories.MsgSendWFFull{
		Receiver: sender_id,
		Addrs:    []string{"#from"},
		Msg: &busmsg.Pong{
			SendToAddr: "#to",
		},
		EncKey: &factories.PrivKey1.Get().PublicKey,
	}
	assert.Equal(t, call, setup.msg_send_wf.Calls[0])
}

func TestDispatchPong(t *testing.T) {
	setup := new_setup()
	setup.time.NowData = 2
	sender_id, _ := uuid.NewRandom()

	setup.db.User.Contact(
		"abc",
		sender_id,
		&factories.PrivKey1.Get().PublicKey,
	)

	msg := &busmsg.Pong{
		Base:       busmsg.Base{SenderId: sender_id},
		SendToAddr: "addr",
	}
	assert.Nil(t, setup.dispatcher.Dispatch(msg))

	addrs, err := setup.db.User.GetAddrs(sender_id)
	assert.Nil(t, err)
	assert.Equal(t, []string{"addr"}, addrs)
}

func TestDispatchInvite(t *testing.T) {
	setup := new_setup()
	setup.time.NowData = 2
	sender_id, _ := uuid.NewRandom()
	participant, _ := uuid.NewRandom()
	group_id, _ := uuid.NewRandom()
	other_key := &factories.PrivKey2.Get().PublicKey
	msg := &busmsg.Invite{
		Base:    busmsg.Base{SenderId: sender_id},
		GroupId: group_id,
		Name:    "group",
		Users: map[uuid.UUID]*rsa.PublicKey{
			participant: other_key,
		},
	}

	assert.Nil(t, setup.dispatcher.Dispatch(msg))

	key, err := setup.db.User.GetPubKey(participant)
	assert.Nil(t, err)
	assert.Equal(t, other_key, key)

	chat, _ := setup.db.Chats.GetById(local.ChatId{
		IsGroup: true,
		Id:      group_id,
	})
	assert.Equal(t, "group", chat.Name)
	assert.Equal(
		t,
		utils.SortUids(chat.Users),
		utils.SortUids([]uuid.UUID{sender_id, participant}),
	)
}
