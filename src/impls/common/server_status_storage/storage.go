package server_status_storage

import (
	"dusc/src/entities/local"
	"dusc/src/proto"
	"maps"
	"sync"
)

type Storage struct {
	mtx          sync.Mutex
	time         proto.ITime
	statuses     map[string]local.ServerStatus
	updated      map[string]uint64
	max_lifetime uint64
}

func New(
	time proto.ITime,
	max_lifetime uint64,
) proto.IServerStatusStorage {
	return &Storage{
		time:         time,
		statuses:     map[string]local.ServerStatus{},
		updated:      map[string]uint64{},
		max_lifetime: max_lifetime,
	}
}

func (self *Storage) SetStatuses(updates map[string]local.ServerStatus) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	now := self.time.Now()
	for srv, status := range updates {
		self.statuses[srv] = status
		self.updated[srv] = now
	}
	for srv, update := range self.updated {
		if update < now && now-update > self.max_lifetime {
			delete(self.statuses, srv)
			delete(self.updated, srv)
		}
	}
}

func (self *Storage) GetStatuses() map[string]local.ServerStatus {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	return maps.Clone(self.statuses)
}
