package server_status_storage_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/common/server_status_storage"
	"dusc/src/proto"
	"testing"

	"github.com/stretchr/testify/assert"
)

type Setup struct {
	time    *factories.Time
	storage proto.IServerStatusStorage
}

func new_setup() Setup {
	time := &factories.Time{}
	return Setup{
		time:    time,
		storage: server_status_storage.New(time, 5),
	}
}

func TestGetStatusEmpty(t *testing.T) {
	setup := new_setup()
	statuses := setup.storage.GetStatuses()
	assert.Equal(t, 0, len(statuses))
}

func TestGetStatusUpdated(t *testing.T) {
	setup := new_setup()
	setup.storage.SetStatuses(map[string]local.ServerStatus{
		"a": local.ServerStatusOk,
		"b": local.ServerStatusOk,
	})
	statuses := setup.storage.GetStatuses()
	assert.Equal(
		t,
		map[string]local.ServerStatus{
			"a": local.ServerStatusOk,
			"b": local.ServerStatusOk,
		},
		statuses,
	)
	setup.storage.SetStatuses(map[string]local.ServerStatus{
		"c": local.ServerStatusOk,
		"b": local.ServerStatusError,
	})
	statuses = setup.storage.GetStatuses()
	assert.Equal(
		t,
		map[string]local.ServerStatus{
			"a": local.ServerStatusOk,
			"b": local.ServerStatusError,
			"c": local.ServerStatusOk,
		},
		statuses,
	)
}

func TestGetStatusExpired(t *testing.T) {
	setup := new_setup()
	setup.storage.SetStatuses(map[string]local.ServerStatus{
		"a": local.ServerStatusOk,
		"b": local.ServerStatusOk,
	})
	setup.time.NowData = 100
	setup.storage.SetStatuses(map[string]local.ServerStatus{
		"c": local.ServerStatusOk,
		"b": local.ServerStatusOk,
	})
	statuses := setup.storage.GetStatuses()
	assert.Equal(
		t,
		map[string]local.ServerStatus{
			"b": local.ServerStatusOk,
			"c": local.ServerStatusOk,
		},
		statuses,
	)
}
