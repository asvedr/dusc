package msg_maker

import (
	"bytes"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"dusc/src/utils"

	"github.com/google/uuid"
)

const chunk_size = 2

type maker struct {
	repo     proto.IMsgRepo
	time     proto.ITime
	executor proto.ITaskExecutor
}

func New(
	repo proto.IMsgRepo,
	time proto.ITime,
	executor proto.ITaskExecutor,
) proto.IMsgMaker {
	return maker{repo: repo, time: time, executor: executor}
}

func (self maker) MakeText(
	chat_id local.ChatId,
	text string,
	receivers ...uuid.UUID,
) error {
	maker := func(receiver uuid.UUID) []local.NewMsg {
		return []local.NewMsg{{
			Receiver: &receiver,
			Chat:     chat_id,
			Text:     text,
			Received: false,
			Read:     true,
		}}
	}
	return self.save(receivers, true, maker)
}

func (self maker) MakeFileMeta(
	chat_id local.ChatId,
	file_id uuid.UUID,
	file_name string,
	local_path string,
	parts int16,
	size uint64,
	receivers ...uuid.UUID,
) error {
	maker := func(receiver uuid.UUID) []local.NewMsg {
		return []local.NewMsg{{
			Receiver: &receiver,
			Chat:     chat_id,
			Text:     file_name,
			Read:     true,
			AttachFileMeta: &local.AttachFileMeta{
				FileId:    file_id,
				LocalPath: local_path,
				Parts:     parts,
				Size:      size,
			},
		}}
	}
	return self.save(receivers, true, maker)
}

func (self maker) MakeInvite(
	chat_id uuid.UUID,
	chat_name string,
	participants []uuid.UUID,
	receivers ...uuid.UUID,
) error {
	maker := func(receiver uuid.UUID) []local.NewMsg {
		filtered := exclude(participants, receiver)
		chunks := utils.Chunkify(filtered, chunk_size)
		if len(chunks) == 0 {
			return []local.NewMsg{{
				Receiver:     &receiver,
				Read:         true,
				Chat:         local.ChatId{IsGroup: true, Id: chat_id},
				AttachInvite: &local.AttachInvite{Name: chat_name},
			}}
		}
		res := []local.NewMsg{}
		for _, chunk := range chunks {
			msg := local.NewMsg{
				Receiver: &receiver,
				Read:     true,
				Chat:     local.ChatId{IsGroup: true, Id: chat_id},
				AttachInvite: &local.AttachInvite{
					Name:  chat_name,
					Users: chunk,
				},
			}
			res = append(res, msg)
		}
		return res
	}
	return self.save(receivers, false, maker)
}

func (self maker) save(
	receivers []uuid.UUID,
	visible bool,
	f func(uuid.UUID) []local.NewMsg,
) error {
	now := self.time.Now()
	var err error
	created := false
	for i, rec := range receivers {
		for _, msg := range f(rec) {
			msg.Visible = false
			if i == 0 {
				msg.Visible = visible
			}
			msg.CreatedAt = now
			msg.ReceivedAt = now
			msg.Uid, err = uuid.NewRandom()
			if err != nil {
				return err
			}
			err = self.repo.Create(msg)
			if err != nil {
				return err
			}
			created = true
		}
	}
	if !created {
		return nil
	}
	return self.executor.ExecuteNow(local.TaskNameSendAllMsgs)
}

func exclude(id_list []uuid.UUID, id uuid.UUID) []uuid.UUID {
	res := []uuid.UUID{}
	for _, item := range id_list {
		if !bytes.Equal(item[:], id[:]) {
			res = append(res, item)
		}
	}
	return res
}
