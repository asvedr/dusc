package msg_maker_test

import (
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/common/msg_maker"
	"dusc/src/proto"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

type Setup struct {
	db       factories.Db
	time     *factories.Time
	executor *factories.TaskExecutor

	maker proto.IMsgMaker
}

func new_setup() Setup {
	db := factories.NewDb()
	time := &factories.Time{}
	executor := &factories.TaskExecutor{}
	return Setup{
		db:       db,
		time:     time,
		executor: executor,
		maker: msg_maker.New(
			db.Msg,
			time,
			executor,
		),
	}
}

func TestMakeTextGroupMsg(t *testing.T) {
	setup := new_setup()
	setup.executor.ExecuteNowResp = []error{nil}
	chat_id, _ := uuid.NewRandom()
	user_a, _ := uuid.NewRandom()
	user_b, _ := uuid.NewRandom()

	err := setup.maker.MakeText(
		local.ChatId{IsGroup: true, Id: chat_id},
		"hello",
		user_a,
		user_b,
	)
	assert.Nil(t, err)

	msgs, err := setup.db.Msg.GetMessagesToSend()
	assert.Nil(t, err)
	assert.Equal(t, 2, len(msgs))

	msg1 := msgs[0]
	msg2 := msgs[1]

	assert.Nil(t, msg1.Sender)
	assert.Equal(t, &user_a, msg1.Receiver)
	assert.Equal(t, local.ChatId{IsGroup: true, Id: chat_id}, msg1.Chat)
	assert.True(t, msg1.Visible)
	assert.Equal(t, "hello", msg1.Text)
	assert.False(t, msg1.Received)
	assert.True(t, msg1.Read)
	assert.Nil(t, msg1.AttachInvite)
	assert.Nil(t, msg1.AttachFileMeta)

	assert.Nil(t, msg2.Sender)
	assert.Equal(t, &user_b, msg2.Receiver)
	assert.Equal(t, local.ChatId{IsGroup: true, Id: chat_id}, msg2.Chat)
	assert.False(t, msg2.Visible)
	assert.Equal(t, "hello", msg2.Text)
	assert.False(t, msg2.Received)
	assert.True(t, msg2.Read)
	assert.Nil(t, msg2.AttachInvite)
	assert.Nil(t, msg2.AttachFileMeta)
}

func TestMakeTextPrivateMsg(t *testing.T) {
	setup := new_setup()
	setup.executor.ExecuteNowResp = []error{nil}
	user, _ := uuid.NewRandom()

	err := setup.maker.MakeText(
		local.ChatId{Id: user},
		"hello",
		user,
	)
	assert.Nil(t, err)

	msgs, err := setup.db.Msg.GetMessagesToSend()
	assert.Nil(t, err)
	assert.Equal(t, 1, len(msgs))

	msg := msgs[0]

	assert.Nil(t, msg.Sender)
	assert.Equal(t, &user, msg.Receiver)
	assert.Equal(t, local.ChatId{Id: user}, msg.Chat)
	assert.True(t, msg.Visible)
	assert.Equal(t, "hello", msg.Text)
	assert.False(t, msg.Received)
	assert.True(t, msg.Read)
	assert.Nil(t, msg.AttachInvite)
	assert.Nil(t, msg.AttachFileMeta)
}

func TestMakeFileMetaGroupMsg(t *testing.T) {
	setup := new_setup()
	setup.executor.ExecuteNowResp = []error{nil}
	chat_id, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()
	user_a, _ := uuid.NewRandom()
	user_b, _ := uuid.NewRandom()

	err := setup.maker.MakeFileMeta(
		local.ChatId{IsGroup: true, Id: chat_id},
		file_id,
		"name",
		"/path",
		10,
		100,
		user_a,
		user_b,
	)
	assert.Nil(t, err)

	msgs, err := setup.db.Msg.GetMessagesToSend()
	assert.Nil(t, err)
	assert.Equal(t, 2, len(msgs))

	msg1 := msgs[0]
	msg2 := msgs[1]

	assert.Nil(t, msg1.Sender)
	assert.Equal(t, &user_a, msg1.Receiver)
	assert.Equal(t, local.ChatId{IsGroup: true, Id: chat_id}, msg1.Chat)
	assert.True(t, msg1.Visible)
	assert.Equal(t, "name", msg1.Text)
	assert.False(t, msg1.Received)
	assert.True(t, msg1.Read)
	assert.Nil(t, msg1.AttachInvite)
	assert.Equal(
		t,
		&local.AttachFileMeta{
			FileId:    file_id,
			LocalPath: "/path",
			Parts:     10,
			Size:      100,
		},
		msg1.AttachFileMeta,
	)

	assert.Nil(t, msg2.Sender)
	assert.Equal(t, &user_b, msg2.Receiver)
	assert.Equal(t, local.ChatId{IsGroup: true, Id: chat_id}, msg2.Chat)
	assert.False(t, msg2.Visible)
	assert.Equal(t, "name", msg2.Text)
	assert.False(t, msg2.Received)
	assert.True(t, msg2.Read)
	assert.Nil(t, msg2.AttachInvite)
	assert.Equal(
		t,
		&local.AttachFileMeta{
			FileId:    file_id,
			LocalPath: "/path",
			Parts:     10,
			Size:      100,
		},
		msg2.AttachFileMeta,
	)
}

func TestMakeFileMetaPrivateMsg(t *testing.T) {
	setup := new_setup()
	setup.executor.ExecuteNowResp = []error{nil}
	file_id, _ := uuid.NewRandom()
	user, _ := uuid.NewRandom()

	err := setup.maker.MakeFileMeta(
		local.ChatId{Id: user},
		file_id,
		"name",
		"/path",
		10,
		100,
		user,
	)
	assert.Nil(t, err)

	msgs, err := setup.db.Msg.GetMessagesToSend()
	assert.Nil(t, err)
	assert.Equal(t, 1, len(msgs))

	msg := msgs[0]

	assert.Nil(t, msg.Sender)
	assert.Equal(t, &user, msg.Receiver)
	assert.Equal(t, local.ChatId{Id: user}, msg.Chat)
	assert.True(t, msg.Visible)
	assert.Equal(t, "name", msg.Text)
	assert.False(t, msg.Received)
	assert.True(t, msg.Read)
	assert.Nil(t, msg.AttachInvite)
	assert.Equal(
		t,
		&local.AttachFileMeta{
			FileId:    file_id,
			LocalPath: "/path",
			Parts:     10,
			Size:      100,
		},
		msg.AttachFileMeta,
	)
}

func TestMakeInvite(t *testing.T) {
	setup := new_setup()
	setup.executor.ExecuteNowResp = []error{nil}
	user_a, _ := uuid.Parse("3691db1a-19c1-482e-9897-461e06ee11c6")
	user_b, _ := uuid.Parse("05f4b752-6008-40a1-9a58-e9ba7e326d1f")
	user_c, _ := uuid.Parse("66811301-45a6-4289-9676-6dbf1d5653e1")
	chat_id, _ := uuid.NewRandom()

	err := setup.maker.MakeInvite(
		chat_id,
		"chat name",
		[]uuid.UUID{
			user_a,
			user_b,
		},
		user_b,
		user_c,
	)
	assert.Nil(t, err)

	msgs, err := setup.db.Msg.GetMessagesToSend()
	assert.Nil(t, err)
	assert.Equal(t, 2, len(msgs))

	msg1 := msgs[0]
	msg2 := msgs[1]

	assert.Nil(t, msg1.Sender)
	assert.Equal(t, &user_b, msg1.Receiver)
	assert.Equal(t, local.ChatId{IsGroup: true, Id: chat_id}, msg1.Chat)
	assert.False(t, msg1.Visible)
	assert.Equal(t, "", msg1.Text)
	assert.False(t, msg1.Received)
	assert.True(t, msg1.Read)
	assert.Nil(t, msg1.AttachFileMeta)
	assert.Equal(
		t,
		&local.AttachInvite{
			Name:  "chat name",
			Users: []uuid.UUID{user_a},
		},
		msg1.AttachInvite,
	)

	assert.Nil(t, msg2.Sender)
	assert.Equal(t, &user_c, msg2.Receiver)
	assert.Equal(t, local.ChatId{IsGroup: true, Id: chat_id}, msg2.Chat)
	assert.False(t, msg2.Visible)
	assert.Equal(t, "", msg2.Text)
	assert.False(t, msg2.Received)
	assert.True(t, msg2.Read)
	assert.Nil(t, msg2.AttachFileMeta)
	assert.Equal(
		t,
		&local.AttachInvite{
			Name:  "chat name",
			Users: []uuid.UUID{user_a, user_b},
		},
		msg2.AttachInvite,
	)
}
