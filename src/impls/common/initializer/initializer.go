package initializer

import (
	"crypto/rsa"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"

	"github.com/google/uuid"
)

type Initializer struct {
	settings      proto.ISettingsRepo
	crypto        proto.ICrypto
	token_manager proto.IAuthTokenManager
	hasher        proto.IHasher
}

func New(
	settings proto.ISettingsRepo,
	crypto proto.ICrypto,
	token_manager proto.IAuthTokenManager,
	hasher proto.IHasher,
) proto.IInitializer {
	return &Initializer{
		settings:      settings,
		crypto:        crypto,
		token_manager: token_manager,
		hasher:        hasher,
	}
}

func (self *Initializer) CheckInit() error {
	inited, err := self.settings.CheckInited()
	if err != nil {
		return err
	}
	if !inited {
		err = errs.NotInitialized{}
	}
	return err
}

func (self *Initializer) Init(
	opt_id *uuid.UUID,
	opt_priv_key *rsa.PrivateKey,
	name string,
	description string,
	password *string,
) error {
	id, err := unwrap_id(opt_id)
	if err != nil {
		return err
	}
	priv_key, err := self.unwrap_priv_key(opt_priv_key)
	if err != nil {
		return err
	}
	me := local.Me{Uid: id, PrivKey: priv_key}
	err = self.settings.SetMe(me)
	if err != nil {
		return err
	}
	err = self.settings.SetName(name)
	if err != nil {
		return err
	}
	err = self.settings.SetDescription(description)
	if err != nil {
		return err
	}
	hash := ""
	if password != nil {
		hash = self.hasher.GetStrSum([]byte(*password))
	}
	err = self.settings.SetPwdHash(hash)
	if err != nil {
		return err
	}
	self.token_manager.SetActive(password != nil)
	return nil
}

func (self *Initializer) unwrap_priv_key(opt_key *rsa.PrivateKey) (*rsa.PrivateKey, error) {
	if opt_key != nil {
		return opt_key, nil
	}
	return self.crypto.GenPrivKey()
}

func unwrap_id(opt_id *uuid.UUID) (uuid.UUID, error) {
	if opt_id != nil {
		return *opt_id, nil
	}
	return uuid.NewRandom()
}
