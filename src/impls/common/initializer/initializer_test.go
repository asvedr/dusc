package initializer_test

import (
	"crypto/rsa"
	"dusc/src/entities/errs"
	"dusc/src/factories"
	"dusc/src/impls/common/hasher"
	"dusc/src/impls/common/initializer"
	"dusc/src/impls/common/token_manager"
	"dusc/src/proto"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

type Setup struct {
	db            factories.Db
	crypto        *factories.Crypto
	init          proto.IInitializer
	token_manager proto.IAuthTokenManager
}

func new_setup() Setup {
	db := factories.NewDb()
	crypto := &factories.Crypto{}
	tman := token_manager.New()
	init := initializer.New(
		db.Settings,
		crypto,
		tman,
		hasher.New(),
	)
	return Setup{db: db, crypto: crypto, init: init, token_manager: tman}
}

func TestCheckInitInit(t *testing.T) {
	setup := new_setup()
	setup.crypto.GenPrivKeyResp = []*rsa.PrivateKey{
		factories.PrivKey1.Get(),
	}
	assert.Equal(
		t,
		errs.NotInitialized{},
		setup.init.CheckInit(),
	)
	setup.init.Init(nil, nil, "name", "descr", nil)
	assert.Nil(t, setup.init.CheckInit())
	me, err := setup.db.Settings.GetMe()
	assert.Nil(t, err)
	assert.Equal(t, factories.PrivKey1.Get(), me.PrivKey)
	meta, err := setup.db.Settings.GetMeta()
	assert.Nil(t, err)
	assert.Equal(t, "name", meta.Name)
	assert.Equal(t, "descr", meta.Description)
	assert.False(t, setup.token_manager.IsAuthTurnedOn())
}

func TestCheckInitCursomData(t *testing.T) {
	setup := new_setup()
	uid, _ := uuid.NewRandom()
	setup.init.Init(
		&uid,
		factories.PrivKey1.Get(),
		"name",
		"descr",
		nil,
	)
	assert.Nil(t, setup.init.CheckInit())
	me, err := setup.db.Settings.GetMe()
	assert.Nil(t, err)
	assert.Equal(t, factories.PrivKey1.Get(), me.PrivKey)
	assert.Equal(t, uid, me.Uid)
	meta, err := setup.db.Settings.GetMeta()
	assert.Nil(t, err)
	assert.Equal(t, "name", meta.Name)
	assert.Equal(t, "descr", meta.Description)
	assert.False(t, setup.token_manager.IsAuthTurnedOn())
}

func TestInitWithPassword(t *testing.T) {
	setup := new_setup()
	pwd := "abc"
	setup.init.Init(
		nil,
		factories.PrivKey1.Get(),
		"name",
		"descr",
		&pwd,
	)
	assert.Nil(t, setup.init.CheckInit())
	assert.True(t, setup.token_manager.IsAuthTurnedOn())
}
