package serde

import (
	"bytes"

	"github.com/google/uuid"
)

type reader struct {
	buf_u64 [8]byte
	buf_u32 [4]byte
	buf_u16 [2]byte
	input   *bytes.Buffer
}

func (self *reader) U8() (uint8, error) {
	num, err := self.input.ReadByte()
	return uint8(num), err
}

func (self *reader) U16() (uint16, error) {
	_, err := self.input.Read(self.buf_u16[:])
	return endian.Uint16(self.buf_u16[:]), err
}

func (self *reader) U32() (uint32, error) {
	_, err := self.input.Read(self.buf_u32[:])
	return endian.Uint32(self.buf_u32[:]), err
}

func (self *reader) U64() (uint64, error) {
	_, err := self.input.Read(self.buf_u64[:])
	return endian.Uint64(self.buf_u64[:]), err
}

func (self *reader) BytesLenU8() ([]byte, error) {
	cnt, err := self.U8()
	if err != nil {
		return nil, err
	}
	return self.read_bytes(int(cnt))
}

func (self *reader) BytesLenU16() ([]byte, error) {
	cnt, err := self.U16()
	if err != nil {
		return nil, err
	}
	return self.read_bytes(int(cnt))
}

func (self *reader) BytesLenU32() ([]byte, error) {
	cnt, err := self.U32()
	if err != nil {
		return nil, err
	}
	return self.read_bytes(int(cnt))
}

func (self *reader) UUID() (uuid.UUID, error) {
	var res uuid.UUID
	_, err := self.input.Read(res[:])
	return res, err
}

func (self *reader) read_bytes(cnt int) ([]byte, error) {
	if cnt == 0 {
		return []byte{}, nil
	}
	buf := make([]byte, cnt)
	_, err := self.input.Read(buf)
	return buf, err
}
