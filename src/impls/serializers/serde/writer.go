package serde

import (
	"bytes"

	"github.com/google/uuid"
)

type writer struct {
	buf_u64 [8]byte
	buf_u32 [4]byte
	buf_u16 [2]byte
	result  *bytes.Buffer
}

func (self *writer) U8(num uint8) {
	self.result.WriteByte(byte(num))
}

func (self *writer) U16(num uint16) {
	endian.PutUint16(self.buf_u16[:], num)
	self.result.Write(self.buf_u16[:])
}

func (self *writer) U32(num uint32) {
	endian.PutUint32(self.buf_u32[:], num)
	self.result.Write(self.buf_u32[:])
}

func (self *writer) U64(num uint64) {
	endian.PutUint64(self.buf_u64[:], num)
	self.result.Write(self.buf_u64[:])
}

func (self *writer) BytesLenU8(src []byte) {
	l_src := len(src)
	self.U8(uint8(l_src))
	if l_src != 0 {
		self.result.Write(src)
	}
}

func (self *writer) BytesLenU16(src []byte) {
	l_src := len(src)
	self.U16(uint16(l_src))
	if l_src != 0 {
		self.result.Write(src)
	}
}

func (self *writer) BytesLenU32(src []byte) {
	l_src := len(src)
	self.U32(uint32(l_src))
	if l_src != 0 {
		self.result.Write(src)
	}
}

func (self *writer) UUID(src uuid.UUID) {
	self.result.Write(src[:])
}

func (self *writer) Finish() []byte {
	return self.result.Bytes()
}
