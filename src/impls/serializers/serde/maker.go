package serde

import (
	"bytes"
	"dusc/src/proto"
	"encoding/binary"
)

type maker struct{}

var endian = binary.BigEndian

func New() proto.ISerdeMaker {
	return maker{}
}

func (maker) NewWriter() proto.IWSerde {
	return &writer{
		result: bytes.NewBuffer([]byte{}),
	}
}

func (maker) NewReader(src []byte) proto.IRSerde {
	return &reader{input: bytes.NewBuffer(src)}
}
