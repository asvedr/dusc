package serde_test

import (
	"dusc/src/impls/serializers/serde"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

var uid_src = "a74bb0e5-19c0-415b-9dc8-53fa1678a63b"

func TestSerDeMsg(t *testing.T) {
	uid, err := uuid.Parse(uid_src)
	assert.Nil(t, err)
	maker := serde.New()

	writer := maker.NewWriter()
	writer.BytesLenU16([]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
	writer.BytesLenU32([]byte("hello"))
	writer.BytesLenU16([]byte{})
	writer.BytesLenU16([]byte{})
	writer.BytesLenU8([]byte("abc"))
	writer.UUID(uid)

	msg := writer.Finish()

	reader := maker.NewReader(msg)

	nums, err := reader.BytesLenU16()
	assert.Nil(t, err)
	assert.Equal(
		t,
		[]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
		nums,
	)
	txt, err := reader.BytesLenU32()
	assert.Nil(t, err)
	assert.Equal(t, "hello", string(txt))
	empty, err := reader.BytesLenU16()
	assert.Nil(t, err)
	assert.Equal(t, []byte{}, empty)
	empty, err = reader.BytesLenU16()
	assert.Nil(t, err)
	assert.Equal(t, []byte{}, empty)
	txt, err = reader.BytesLenU8()
	assert.Nil(t, err)
	assert.Equal(t, "abc", string(txt))
	got_uid, err := reader.UUID()
	assert.Nil(t, err)
	assert.Equal(t, uid, got_uid)
}
