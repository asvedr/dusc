package msg_serializer_test

import (
	"dusc/src/entities/busmsg"
	"dusc/src/impls/serializers/msg_serializer"
	"dusc/src/impls/serializers/serde"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPacked(t *testing.T) {
	exp := busmsg.Packed{
		Encrypted: []byte("encrypted long message"),
		HashSum:   []byte{1, 2, 3, 4},
	}

	wbuf := serde.New().NewWriter()
	msg_serializer.NewPacked().Write(wbuf, exp)
	bts := wbuf.Finish()

	rbuf := serde.New().NewReader(bts)
	got, err := msg_serializer.NewPacked().Read(rbuf)
	assert.Nil(t, err)
	assert.Equal(t, exp, got)
}
