package msg_serializer

func setter[T any](dst *T, f func() (T, error)) func() error {
	return func() error {
		var err error
		*dst, err = f()
		return err
	}
}

func exec_seq(setters ...func() error) error {
	for _, f := range setters {
		err := f()
		if err != nil {
			return err
		}
	}
	return nil
}
