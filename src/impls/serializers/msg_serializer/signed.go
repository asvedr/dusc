package msg_serializer

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/proto"
	"fmt"
	"reflect"

	"github.com/google/uuid"
)

type ser_sig struct {
	key_ser proto.ISerializer[*rsa.PublicKey]
}

const (
	msg_type_text                 uint8 = 1
	msg_type_file_meta            uint8 = 2
	msg_type_file_segment         uint8 = 3
	msg_type_file_segment_request uint8 = 4
	msg_type_sig_conf_rec         uint8 = 5
	msg_type_ping                 uint8 = 6
	msg_type_pong                 uint8 = 7
	msg_type_invite               uint8 = 8
)

func NewSigned(
	key_ser proto.ISerializer[*rsa.PublicKey],
) proto.ISerializer[busmsg.Signed] {
	return ser_sig{key_ser: key_ser}
}

func (self ser_sig) Write(out proto.IWSerde, msg busmsg.Signed) {
	switch body := msg.Msg.(type) {
	case *busmsg.Text:
		write_text(out, body)
	case *busmsg.FileMeta:
		write_file_meta(out, body)
	case *busmsg.FileSegment:
		write_file_segment(out, body)
	case *busmsg.FileSegmentRequest:
		write_file_segment_request(out, body)
	case *busmsg.SignalConfirmReceived:
		write_sig_confirm_received(out, body)
	case *busmsg.Ping:
		self.write_ping(out, body)
	case *busmsg.Pong:
		write_pong(out, body)
	case *busmsg.Invite:
		self.write_invite(out, body)
	default:
		panic("unsupported msg type: " + reflect.TypeOf(msg).String())
	}
	out.BytesLenU16(msg.Signature)
}

func (self ser_sig) Read(inp proto.IRSerde) (busmsg.Signed, error) {
	var res busmsg.Signed
	num, err := inp.U8()
	if err != nil {
		return res, err
	}
	switch num {
	case msg_type_text:
		res.Msg, err = read_text(inp)
	case msg_type_file_meta:
		res.Msg, err = read_file_meta(inp)
	case msg_type_file_segment:
		res.Msg, err = read_file_segment(inp)
	case msg_type_file_segment_request:
		res.Msg, err = read_file_segment_request(inp)
	case msg_type_sig_conf_rec:
		res.Msg, err = read_sig_confirm_received(inp)
	case msg_type_ping:
		res.Msg, err = self.read_ping(inp)
	case msg_type_pong:
		res.Msg, err = read_pong(inp)
	case msg_type_invite:
		res.Msg, err = self.read_invite(inp)
	default:
		err = fmt.Errorf("unknown msg type: %v", num)
	}
	if err != nil {
		return res, err
	}
	res.Signature, err = inp.BytesLenU16()
	return res, err
}

func write_base(out proto.IWSerde, base busmsg.Base) {
	out.U64(base.CreatedAt)
	out.UUID(base.SenderId)
}

func read_base(inp proto.IRSerde) func() (busmsg.Base, error) {
	return func() (busmsg.Base, error) {
		var res busmsg.Base
		err := exec_seq(
			setter(&res.CreatedAt, inp.U64),
			setter(&res.SenderId, inp.UUID),
		)
		return res, err
	}
}

func write_chat_id(out proto.IWSerde, id *uuid.UUID) {
	if id == nil {
		out.U8(0)
	} else {
		out.U8(1)
		out.UUID(*id)
	}
}

func read_chat_id(inp proto.IRSerde) func() (*uuid.UUID, error) {
	return func() (*uuid.UUID, error) {
		flag, err := inp.U8()
		if err != nil {
			return nil, err
		}
		if flag == 0 {
			return nil, nil
		}
		id, err := inp.UUID()
		return &id, err
	}
}

func write_text_field(out proto.IWSerde, txt string) {
	out.BytesLenU16([]byte(txt))
}

func read_text_field(inp proto.IRSerde) func() (string, error) {
	return func() (string, error) {
		bts, err := inp.BytesLenU16()
		return string(bts), err
	}
}

func write_text(out proto.IWSerde, body *busmsg.Text) {
	out.U8(msg_type_text)
	write_base(out, body.Base)
	out.UUID(body.MsgId)
	write_chat_id(out, body.ChatId)
	write_text_field(out, body.Text)
}

func read_text(inp proto.IRSerde) (busmsg.DynMsg, error) {
	var msg busmsg.Text
	err := exec_seq(
		setter(&msg.Base, read_base(inp)),
		setter(&msg.MsgId, inp.UUID),
		setter(&msg.ChatId, read_chat_id(inp)),
		setter(&msg.Text, read_text_field(inp)),
	)
	return &msg, err
}

func write_file_meta(out proto.IWSerde, body *busmsg.FileMeta) {
	out.U8(msg_type_file_meta)
	write_base(out, body.Base)
	out.UUID(body.MsgId)
	out.UUID(body.FileId)
	write_chat_id(out, body.ChatId)
	write_text_field(out, body.FileName)
	out.U64(body.FileSize)
	out.U16(body.Parts)
}

func read_file_meta(inp proto.IRSerde) (busmsg.DynMsg, error) {
	var msg busmsg.FileMeta
	err := exec_seq(
		setter(&msg.Base, read_base(inp)),
		setter(&msg.MsgId, inp.UUID),
		setter(&msg.FileId, inp.UUID),
		setter(&msg.ChatId, read_chat_id(inp)),
		setter(&msg.FileName, read_text_field(inp)),
		setter(&msg.FileSize, inp.U64),
		setter(&msg.Parts, inp.U16),
	)
	return &msg, err
}

func write_file_segment(out proto.IWSerde, body *busmsg.FileSegment) {
	out.U8(msg_type_file_segment)
	write_base(out, body.Base)
	out.UUID(body.FileId)
	out.U16(body.Part)
	out.BytesLenU16(body.Data)
}

func read_file_segment(inp proto.IRSerde) (busmsg.DynMsg, error) {
	var msg busmsg.FileSegment
	err := exec_seq(
		setter(&msg.Base, read_base(inp)),
		setter(&msg.FileId, inp.UUID),
		setter(&msg.Part, inp.U16),
		setter(&msg.Data, inp.BytesLenU16),
	)
	return &msg, err
}

func write_file_segment_request(out proto.IWSerde, body *busmsg.FileSegmentRequest) {
	out.U8(msg_type_file_segment_request)
	write_base(out, body.Base)
	out.U16(body.Part)
	out.UUID(body.FileId)
}

func read_file_segment_request(inp proto.IRSerde) (busmsg.DynMsg, error) {
	var msg busmsg.FileSegmentRequest
	err := exec_seq(
		setter(&msg.Base, read_base(inp)),
		setter(&msg.Part, inp.U16),
		setter(&msg.FileId, inp.UUID),
	)
	return &msg, err
}

func write_sig_confirm_received(out proto.IWSerde, body *busmsg.SignalConfirmReceived) {
	out.U8(msg_type_sig_conf_rec)
	write_base(out, body.Base)
	out.UUID(body.MsgId)
}

func read_sig_confirm_received(inp proto.IRSerde) (busmsg.DynMsg, error) {
	var msg busmsg.SignalConfirmReceived
	err := exec_seq(
		setter(&msg.Base, read_base(inp)),
		setter(&msg.MsgId, inp.UUID),
	)
	return &msg, err
}

func (self ser_sig) write_ping(out proto.IWSerde, body *busmsg.Ping) {
	out.U8(msg_type_ping)
	write_base(out, body.Base)
	write_text_field(out, body.SendToAddr)
	write_text_field(out, body.SentFromAddr)
	self.key_ser.Write(out, body.PublicKey)
}

func (self ser_sig) read_ping(inp proto.IRSerde) (busmsg.DynMsg, error) {
	var msg busmsg.Ping
	var err error
	msg.Base, err = read_base(inp)()
	if err != nil {
		return &msg, err
	}
	msg.SendToAddr, err = read_text_field(inp)()
	if err != nil {
		return &msg, err
	}
	msg.SentFromAddr, err = read_text_field(inp)()
	if err != nil {
		return &msg, err
	}
	msg.PublicKey, err = self.key_ser.Read(inp)
	return &msg, err
}

func write_pong(out proto.IWSerde, body *busmsg.Pong) {
	out.U8(msg_type_pong)
	write_base(out, body.Base)
	write_text_field(out, body.SendToAddr)
}

func read_pong(inp proto.IRSerde) (busmsg.DynMsg, error) {
	var msg busmsg.Pong
	err := exec_seq(
		setter(&msg.Base, read_base(inp)),
		setter(&msg.SendToAddr, read_text_field(inp)),
	)
	return &msg, err
}

func (self ser_sig) write_invite(out proto.IWSerde, body *busmsg.Invite) {
	out.U8(msg_type_invite)
	write_base(out, body.Base)
	out.UUID(body.GroupId)
	write_text_field(out, body.Name)
	out.U8(uint8(len(body.Users)))
	for id, key := range body.Users {
		out.UUID(id)
		self.key_ser.Write(out, key)
	}
}

func (self ser_sig) read_invite(inp proto.IRSerde) (busmsg.DynMsg, error) {
	msg := busmsg.Invite{Users: map[uuid.UUID]*rsa.PublicKey{}}
	var cnt uint8
	err := exec_seq(
		setter(&msg.Base, read_base(inp)),
		setter(&msg.GroupId, inp.UUID),
		setter(&msg.Name, read_text_field(inp)),
		setter(&cnt, inp.U8),
	)
	if err != nil {
		return nil, err
	}
	for range cnt {
		uid, err := inp.UUID()
		if err != nil {
			return nil, err
		}
		key, err := self.key_ser.Read(inp)
		if err != nil {
			return nil, err
		}
		msg.Users[uid] = key
	}
	return &msg, nil
}
