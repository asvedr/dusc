package msg_serializer_test

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/factories"
	"dusc/src/impls/serializers/key_serializer"
	"dusc/src/impls/serializers/msg_serializer"
	"dusc/src/impls/serializers/serde"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

var msg_id, _ = uuid.NewRandom()
var group_id, _ = uuid.NewRandom()
var file_id, _ = uuid.NewRandom()
var sender, _ = uuid.NewRandom()
var chat, _ = uuid.NewRandom()

func base() busmsg.Base {
	return busmsg.Base{
		CreatedAt: 123,
		SenderId:  sender,
	}
}

func msg_text() busmsg.Signed {
	return busmsg.Signed{
		Msg: &busmsg.Text{
			Base:   base(),
			MsgId:  msg_id,
			ChatId: &chat,
			Text:   "hello",
		},
		Signature: []byte("signature"),
	}
}

func msg_file_meta() busmsg.Signed {
	return busmsg.Signed{
		Msg: &busmsg.FileMeta{
			Base:     base(),
			MsgId:    msg_id,
			FileId:   file_id,
			ChatId:   &chat,
			FileName: "file.txt",
			FileSize: 1024,
			Parts:    10,
		},
		Signature: []byte("signature"),
	}
}

func msg_file_segment() busmsg.Signed {
	return busmsg.Signed{
		Msg: &busmsg.FileSegment{
			Base:   base(),
			FileId: file_id,
			Part:   32,
			Data:   []byte{1, 2, 3, 4, 5, 6},
		},
		Signature: []byte("sig"),
	}
}

func msg_file_seg_request() busmsg.Signed {
	return busmsg.Signed{
		Msg: &busmsg.FileSegmentRequest{
			Base:   base(),
			FileId: file_id,
			Part:   123,
		},
		Signature: []byte("sig"),
	}
}

func signal_confirm_received() busmsg.Signed {
	return busmsg.Signed{
		Msg: &busmsg.SignalConfirmReceived{
			Base:  base(),
			MsgId: msg_id,
		},
		Signature: []byte("sig"),
	}
}

func msg_ping() busmsg.Signed {
	priv_key := factories.PrivKey1.Get()
	return busmsg.Signed{
		Msg: &busmsg.Ping{
			Base:         base(),
			PublicKey:    &priv_key.PublicKey,
			SendToAddr:   "127.0.0.1",
			SentFromAddr: "abcdef",
		},
		Signature: []byte("sig"),
	}
}

func msg_pong() busmsg.Signed {
	return busmsg.Signed{
		Msg: &busmsg.Pong{
			Base:       base(),
			SendToAddr: "127.0.0.1",
		},
		Signature: []byte("sig"),
	}
}

func msg_invite() busmsg.Signed {
	priv1 := factories.PrivKey1.Get()
	priv2 := factories.PrivKey2.Get()
	return busmsg.Signed{
		Msg: &busmsg.Invite{
			Base:    base(),
			GroupId: group_id,
			Name:    "group",
			Users: map[uuid.UUID]*rsa.PublicKey{
				sender: &priv1.PublicKey,
				msg_id: &priv2.PublicKey,
			},
		},
		Signature: []byte("sig"),
	}
}

func TestSigned(t *testing.T) {
	key_ser := key_serializer.NewPub()
	msg_ser := msg_serializer.NewSigned(key_ser)
	for _, exp_msg := range []busmsg.Signed{
		msg_text(),
		msg_file_meta(),
		msg_file_segment(),
		msg_file_seg_request(),
		signal_confirm_received(),
		msg_ping(),
		msg_pong(),
		msg_invite(),
	} {
		wbuf := serde.New().NewWriter()
		msg_ser.Write(wbuf, exp_msg)
		bts := wbuf.Finish()
		rbuf := serde.New().NewReader(bts)
		msg, err := msg_ser.Read(rbuf)
		assert.Nil(t, err)
		assert.Equal(t, exp_msg, msg)
	}
}
