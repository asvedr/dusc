package msg_serializer

import (
	"dusc/src/entities/busmsg"
	"dusc/src/proto"
)

type ser_pack struct{}

func NewPacked() proto.ISerializer[busmsg.Packed] {
	return ser_pack{}
}

func (ser_pack) Write(out proto.IWSerde, obj busmsg.Packed) {
	// ser_sig{}.Write(out, obj.Signed)
	out.BytesLenU32(obj.Encrypted)
	out.BytesLenU8(obj.HashSum)
}

func (ser_pack) Read(inp proto.IRSerde) (busmsg.Packed, error) {
	var res busmsg.Packed
	var err error
	res.Encrypted, err = inp.BytesLenU32()
	if err != nil {
		return res, err
	}
	res.HashSum, err = inp.BytesLenU8()
	return res, err
}
