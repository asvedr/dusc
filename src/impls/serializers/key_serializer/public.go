package key_serializer

import (
	"crypto/rsa"
	"crypto/x509"
	"dusc/src/proto"
)

type pub_ser struct{}

func NewPub() proto.ISerializer[*rsa.PublicKey] {
	return pub_ser{}
}

func (pub_ser) Write(out proto.IWSerde, key *rsa.PublicKey) {
	out.BytesLenU16(x509.MarshalPKCS1PublicKey(key))
}

func (pub_ser) Read(inp proto.IRSerde) (*rsa.PublicKey, error) {
	bts, err := inp.BytesLenU16()
	if err != nil {
		return nil, err
	}
	return x509.ParsePKCS1PublicKey(bts)
}
