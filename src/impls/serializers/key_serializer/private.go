package key_serializer

import (
	"crypto/rsa"
	"crypto/x509"
	"dusc/src/proto"
)

type priv_ser struct{}

func NewPriv() proto.ISerializer[*rsa.PrivateKey] {
	return priv_ser{}
}

func (priv_ser) Write(out proto.IWSerde, key *rsa.PrivateKey) {
	out.BytesLenU16(x509.MarshalPKCS1PrivateKey(key))
}

func (priv_ser) Read(inp proto.IRSerde) (*rsa.PrivateKey, error) {
	bts, err := inp.BytesLenU16()
	if err != nil {
		return nil, err
	}
	return x509.ParsePKCS1PrivateKey(bts)
}
