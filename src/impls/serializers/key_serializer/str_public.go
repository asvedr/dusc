package key_serializer

import (
	"crypto/rsa"
	"crypto/x509"
	"dusc/src/proto"
	"encoding/base64"
)

type pub_ser_str struct{}

func NewPubStr() proto.IStrSerializer[*rsa.PublicKey] {
	return pub_ser_str{}
}

func (pub_ser_str) Write(key *rsa.PublicKey) string {
	bts := x509.MarshalPKCS1PublicKey(key)
	return base64.StdEncoding.EncodeToString(bts)
}

func (pub_ser_str) Read(inp string) (*rsa.PublicKey, error) {
	bts, err := base64.StdEncoding.DecodeString(inp)
	if err != nil {
		return nil, err
	}
	return x509.ParsePKCS1PublicKey(bts)
}
