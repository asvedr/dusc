package key_serializer

import (
	"crypto/rsa"
	"crypto/x509"
	"dusc/src/proto"
	"encoding/base64"
)

type priv_ser_str struct{}

func NewPrivStr() proto.IStrSerializer[*rsa.PrivateKey] {
	return priv_ser_str{}
}

func (priv_ser_str) Write(key *rsa.PrivateKey) string {
	bts := x509.MarshalPKCS1PrivateKey(key)
	return base64.StdEncoding.EncodeToString(bts)
}

func (priv_ser_str) Read(inp string) (*rsa.PrivateKey, error) {
	bts, err := base64.StdEncoding.DecodeString(inp)
	if err != nil {
		return nil, err
	}
	return x509.ParsePKCS1PrivateKey(bts)
}
