package contact_serializer

import (
	"crypto/rsa"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"encoding/base64"
)

type ser struct {
	serde          proto.ISerdeMaker
	pub_serializer proto.ISerializer[*rsa.PublicKey]
}

func New(
	serde proto.ISerdeMaker,
	pub_serializer proto.ISerializer[*rsa.PublicKey],
) proto.IStrSerializer[local.ClientContact] {
	return ser{
		serde:          serde,
		pub_serializer: pub_serializer,
	}
}

func (self ser) Write(cnt local.ClientContact) string {
	writer := self.serde.NewWriter()
	writer.UUID(cnt.Uid)
	self.pub_serializer.Write(writer, cnt.PubKey)
	bts := writer.Finish()
	return base64.StdEncoding.EncodeToString(bts)
}

func (self ser) Read(inp string) (cnt local.ClientContact, err error) {
	var bts []byte
	bts, err = base64.StdEncoding.DecodeString(inp)
	if err != nil {
		return
	}
	reader := self.serde.NewReader(bts)
	cnt.Uid, err = reader.UUID()
	if err != nil {
		return
	}
	cnt.PubKey, err = self.pub_serializer.Read(reader)
	return
}
