package task_executor

import "dusc/src/entities/local"

type sig_type int

const (
	sig_type_force_one sig_type = iota
	sig_type_heart_beat
)

type signal struct {
	sig_type  sig_type
	task_name local.TaskName
}

func (self signal) String() string {
	switch self.sig_type {
	case sig_type_force_one:
		return "force_one(" + self.task_name.String() + ")"
	case sig_type_heart_beat:
		return "heart_beat"
	default:
		panic("unknown signal")
	}
}
