package task_executor

import (
	"dusc/src/entities/local"
	"dusc/src/proto"
	"log"
	"sync"
	"time"
)

const heart_beat = time.Millisecond * 500

type executor struct {
	time         proto.ITime
	name_to_task map[local.TaskName]*task
	queue        chan signal
}

type task struct {
	name          local.TaskName
	impl          proto.ITask
	timeout       uint64
	last_finished uint64
	mtx           sync.Mutex
}

func New(
	time proto.ITime,
	tasks ...proto.ITask,
) proto.ITaskExecutor {
	name_to_task := map[local.TaskName]*task{}
	for _, impl := range tasks {
		schema := impl.Schema()
		name_to_task[schema.Name] = &task{
			impl:          impl,
			timeout:       schema.Timeout,
			last_finished: 0,
		}
	}
	return &executor{
		time:         time,
		name_to_task: name_to_task,
		queue:        make(chan signal, 1000),
	}
}

func (self *executor) Name() string {
	return "task_executor"
}

func (self *executor) Run() error {
	go func() {
		for {
			self.queue <- signal{sig_type: sig_type_heart_beat}
			time.Sleep(heart_beat)
		}
	}()
	log.Print("task executor linstener started...")
	for sig := range self.queue {
		switch sig.sig_type {
		case sig_type_heart_beat:
			self.heart_beat()
		case sig_type_force_one:
			self.force_one(sig)
		default:
			panic("unexpected signal: " + sig.String())
		}
	}
	return nil
}

func (self *executor) ExecuteNow(task local.TaskName) error {
	self.queue <- signal{
		sig_type:  sig_type_force_one,
		task_name: task,
	}
	return nil
}

func (self *executor) heart_beat() {
	now := self.time.Now()
	for _, task := range self.name_to_task {
		locked := task.mtx.TryLock()
		if !locked {
			continue
		}
		if now-task.last_finished >= task.timeout {
			go self.exec_task(task)
		} else {
			task.mtx.Unlock()
		}
	}
}

func (self *executor) force_one(sig signal) {
	task, found := self.name_to_task[sig.task_name]
	if !found {
		log.Printf("task not found: %v", sig.task_name)
		return
	}
	locked := task.mtx.TryLock()
	if !locked {
		log.Printf("task %v is locked", sig.task_name)
		return
	}
	go self.exec_task(task)
}

func (self *executor) exec_task(task *task) {
	defer task.mtx.Unlock()
	err := task.impl.Execute()
	if err != nil {
		log.Printf("task %v failed: %v", task.name, err)
		return
	}
	task.last_finished = self.time.Now()
}
