package msg_pusher

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"log"

	"github.com/google/uuid"
)

type Service struct {
	locker    proto.IDbLocker
	msg_repo  proto.IMsgRepo
	user_repo proto.IUserRepo
	sender    proto.IMsgSendWorkflow
}

func New(
	locker proto.IDbLocker,
	msg_repo proto.IMsgRepo,
	user_repo proto.IUserRepo,
	sender proto.IMsgSendWorkflow,
) proto.ITask {
	return &Service{
		msg_repo:  msg_repo,
		user_repo: user_repo,
		sender:    sender,
		locker:    locker,
	}
}

func (self *Service) Schema() local.TaskSchema {
	return local.TaskSchema{
		Timeout: 1,
		Name:    local.TaskNameSendAllMsgs,
	}
}

func (self *Service) Execute() error {
	var err error
	self.locker.Lock(func() { err = self.atomic_iteration() })
	return err
}

func (self *Service) atomic_iteration() error {
	msgs, err := self.msg_repo.GetMessagesToSend()
	log.Printf("msg_pusher: fetched=%d", len(msgs))
	if err != nil {
		return err
	}
	for _, msg := range msgs {
		err := self.send(msg)
		if err != nil {
			return err
		}
	}
	return nil
}

func (self *Service) send(raw_msg *local.Msg) error {
	var msg busmsg.DynMsg
	var err error
	if raw_msg.AttachFileMeta != nil {
		msg, err = prepare_msg_file_meta(raw_msg)
	} else if raw_msg.AttachInvite != nil {
		msg, err = self.prepare_msg_invite(raw_msg)
	} else {
		msg = prepare_msg_text(raw_msg)
	}
	if err != nil {
		return err
	}
	return self.sender.AddToQueueMinial(*raw_msg.Receiver, msg)
}

func (self *Service) prepare_msg_invite(raw_msg *local.Msg) (busmsg.DynMsg, error) {
	users := map[uuid.UUID]*rsa.PublicKey{}
	for _, uid := range raw_msg.AttachInvite.Users {
		key, err := self.user_repo.GetPubKey(uid)
		if err != nil {
			return nil, err
		}
		users[uid] = key
	}
	return &busmsg.Invite{
		GroupId: raw_msg.Chat.Id,
		Name:    raw_msg.AttachInvite.Name,
		Users:   users,
	}, nil
}

func prepare_msg_file_meta(raw_msg *local.Msg) (busmsg.DynMsg, error) {
	meta := raw_msg.AttachFileMeta
	return &busmsg.FileMeta{
		MsgId:    raw_msg.Uid,
		FileId:   meta.FileId,
		ChatId:   prepare_chat_id(raw_msg.Chat),
		FileName: raw_msg.Text,
		FileSize: meta.Size,
		Parts:    uint16(meta.Parts),
	}, nil
}

func prepare_msg_text(raw_msg *local.Msg) busmsg.DynMsg {
	return &busmsg.Text{
		MsgId:  raw_msg.Uid,
		ChatId: prepare_chat_id(raw_msg.Chat),
		Text:   raw_msg.Text,
	}
}

func prepare_chat_id(raw local.ChatId) *uuid.UUID {
	if raw.IsGroup {
		return &raw.Id
	}
	return nil
}
