package msg_pusher_test

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/tasks/msg_pusher"
	"sort"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestWorkflowEmpty(t *testing.T) {
	db := factories.NewDb()
	sender := &factories.MsgSendWF{}
	pusher := msg_pusher.New(db.Locker, db.Msg, db.User, sender)

	err := pusher.Execute()
	assert.Nil(t, err)
	assert.Equal(t, 0, len(sender.Calls))
}

func TestWorkflowMsgs(t *testing.T) {
	db := factories.NewDb()
	sender := &factories.MsgSendWF{}
	pusher := msg_pusher.New(db.Locker, db.Msg, db.User, sender)

	user_1, _ := uuid.NewRandom()
	user_2, _ := uuid.NewRandom()
	user_3, _ := uuid.NewRandom()
	user_4, _ := uuid.NewRandom()
	msg_1, _ := uuid.NewRandom()
	msg_2, _ := uuid.NewRandom()
	msg_3, _ := uuid.NewRandom()
	msg_4, _ := uuid.NewRandom()
	file_id, _ := uuid.NewRandom()
	chat_id, _ := uuid.NewRandom()
	key_2 := factories.PrivKey2.Get()
	db.User.Contact("alex", user_2, &key_2.PublicKey)
	// key_ser := serializers.PubKeyStrSerializer.Get()
	// user_2_key := key_ser.Write(&key_2.PublicKey)

	db.Msg.Create(local.NewMsg{
		Receiver:   &user_1,
		Chat:       local.ChatId{Id: user_1},
		Uid:        msg_1,
		Text:       "hi",
		Received:   false,
		CreatedAt:  1,
		ReceivedAt: 1,
	})
	db.Msg.Create(local.NewMsg{
		Receiver:   &user_2,
		Chat:       local.ChatId{Id: user_2},
		Uid:        msg_2,
		Text:       "ho",
		Received:   true,
		CreatedAt:  2,
		ReceivedAt: 2,
	})
	db.Msg.Create(local.NewMsg{
		Receiver: &user_3,
		Chat:     local.ChatId{Id: user_2},
		Uid:      msg_3,
		Text:     "file.txt",
		AttachFileMeta: &local.AttachFileMeta{
			FileId:    file_id,
			LocalPath: "/a/b",
			Parts:     10,
			Size:      100,
		},
		CreatedAt:  3,
		ReceivedAt: 3,
	})
	db.Msg.Create(local.NewMsg{
		Receiver: &user_4,
		Chat:     local.ChatId{IsGroup: true, Id: chat_id},
		Uid:      msg_4,
		Text:     "ho",
		AttachInvite: &local.AttachInvite{
			Name:  "chat",
			Users: []uuid.UUID{user_2},
		},
		CreatedAt:  4,
		ReceivedAt: 4,
	})

	err := pusher.Execute()
	assert.Nil(t, err)
	order := map[uuid.UUID]int{
		user_1: 1,
		user_2: 2,
		user_3: 3,
		user_4: 4,
	}
	sort.Slice(
		sender.Calls,
		func(i int, j int) bool {
			a := sender.Calls[i].(factories.MsgSendWFMinial)
			b := sender.Calls[j].(factories.MsgSendWFMinial)
			return order[a.Receiver] < order[b.Receiver]
		},
	)
	call1 := factories.MsgSendWFMinial{
		Receiver: user_1,
		Msg: &busmsg.Text{
			MsgId:  msg_1,
			ChatId: nil,
			Text:   "hi",
		},
	}
	call2 := factories.MsgSendWFMinial{
		Receiver: user_3,
		Msg: &busmsg.FileMeta{
			MsgId:    msg_3,
			FileId:   file_id,
			ChatId:   nil,
			FileName: "file.txt",
			FileSize: 100,
			Parts:    10,
		},
	}
	call3 := factories.MsgSendWFMinial{
		Receiver: user_4,
		Msg: &busmsg.Invite{
			GroupId: chat_id,
			Name:    "chat",
			Users: map[uuid.UUID]*rsa.PublicKey{
				user_2: &key_2.PublicKey,
			},
		},
	}
	assert.Equal(t, 3, len(sender.Calls))
	assert.Equal(t, call1, sender.Calls[0])
	assert.Equal(t, call2, sender.Calls[1])
	assert.Equal(t, call3, sender.Calls[2])
}
