package addr_checker

import (
	"dusc/src/entities/busmsg"
	"dusc/src/entities/config"
	"dusc/src/entities/errs"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"log"
	"sync"

	"github.com/google/uuid"
)

/*
	- get my addr via STUN.
		If changed - update local addr
	- publish my addr to ext servers
	- fetch addrs of friends from servers
	- send ping to all known friends
*/

type checker struct {
	locker    proto.IDbLocker
	sender    proto.IMsgSendWorkflow
	stun      proto.IFactory[proto.ISTUNClient]
	ext_stor  proto.IFactory[proto.IExtStorage]
	user_repo proto.IUserRepo
	db_repo   proto.IServerRepo
	me_getter proto.IMeGetter

	config      *config.Config
	my_addr     string
	my_addr_mtx sync.Mutex
}

type result[T any] struct {
	Val    T
	Sender string
	Err    error
}

func New(
	locker proto.IDbLocker,
	sender proto.IMsgSendWorkflow,
	stun proto.IFactory[proto.ISTUNClient],
	ext_stor proto.IFactory[proto.IExtStorage],
	user_repo proto.IUserRepo,
	db_repo proto.IServerRepo,
	me_getter proto.IMeGetter,
	config *config.Config,
) proto.IAddrManager {
	return &checker{
		locker:    locker,
		sender:    sender,
		stun:      stun,
		ext_stor:  ext_stor,
		user_repo: user_repo,
		db_repo:   db_repo,
		me_getter: me_getter,
		config:    config,
	}
}

func (self *checker) Schema() local.TaskSchema {
	return local.TaskSchema{
		Timeout: 5,
		Name:    local.TaskNameCheckAddr,
	}
}

func (self *checker) Execute() error {
	var addrs map[uuid.UUID]map[string]bool
	var from_db map[uuid.UUID][]string
	var err error
	var servers []local.Server

	self.upd_my_addr()
	if self.my_addr != "" {
		self.publush_me()
	}

	self.locker.Lock(func() {
		addrs = self.prepare_map()
		servers, err = self.db_repo.GetByType(local.ServerTypeExtStor)
	})
	if err != nil {
		return err
	}

	self.fetch_friends(addrs, servers)

	self.locker.Lock(func() {
		from_db, err = self.user_repo.GetAddrMap()
		if err != nil {
			log.Printf("addr_checker: %v", err)
			return
		}
		for uid, addr_list := range from_db {
			for _, addr := range addr_list {
				addrs[uid][addr] = true
			}
		}
		self.ping_friends(addrs)
	})
	return err
}

func (self *checker) GetMyAddr() string {
	self.my_addr_mtx.Lock()
	defer self.my_addr_mtx.Unlock()
	return self.my_addr
}

func (self *checker) PingUser(uid uuid.UUID, addr string) error {
	my_addr := self.GetMyAddr()
	if my_addr == "" {
		return errs.MyAddrIsUnknown{}
	}
	me, err := self.me_getter.GetMe()
	if err != nil {
		return err
	}
	msg := &busmsg.Ping{
		SentFromAddr: my_addr,
		SendToAddr:   addr,
		PublicKey:    &me.PrivKey.PublicKey,
	}
	key, err := self.user_repo.GetPubKey(uid)
	if err != nil {
		return err
	}
	self.sender.AddToQueue(uid, []string{addr}, msg, key)
	return nil
}

func (self *checker) ping_friends(full_set map[uuid.UUID]map[string]bool) {
	for uid, addr_set := range full_set {
		key, err := self.user_repo.GetPubKey(uid)
		if err != nil {
			log.Printf("addr_checker: %v", err)
			continue
		}
		for addr := range addr_set {
			addr_list := []string{addr}
			msg := &busmsg.Ping{
				SentFromAddr: self.my_addr,
				SendToAddr:   addr,
				PublicKey:    key,
			}
			self.sender.AddToQueue(uid, addr_list, msg, key)
		}
	}
}

func (self *checker) prepare_map() map[uuid.UUID]map[string]bool {
	res := map[uuid.UUID]map[string]bool{}
	uids, err := self.user_repo.GetUids()
	if err != nil {
		log.Printf("addr_checker: %v", err)
		return res
	}
	for _, id := range uids {
		res[id] = map[string]bool{}
	}
	return res
}

func (self *checker) fetch_friends(
	res map[uuid.UUID]map[string]bool,
	servers []local.Server,
) {
	var mtx sync.Mutex
	var wg sync.WaitGroup
	wg.Add(len(servers))
	for _, srv := range servers {
		go func(addr string) {
			err := self.fetch_from(addr, &mtx, res)
			if err != nil {
				log.Printf("addr_checker srv(%s): %v", addr, err)
			}
			wg.Done()
		}(srv.Addr)
	}
	wg.Wait()
}

func (self *checker) fetch_from(
	srv_addr string,
	mtx *sync.Mutex,
	res map[uuid.UUID]map[string]bool,
) error {
	uids := []uuid.UUID{}
	for uid := range res {
		uids = append(uids, uid)
	}
	storage, err := self.ext_stor.Make(srv_addr)
	if err != nil {
		return err
	}
	got, err := storage.FetchUsers(uids)
	if err != nil {
		return err
	}
	mtx.Lock()
	defer mtx.Unlock()
	for id, addrs := range got {
		for _, addr := range addrs {
			res[id][addr] = true
		}
	}
	return nil
}

func (self *checker) upd_my_addr() {
	var srvs []local.Server
	var err error

	if self.config.Force_My_Addr != "" {
		log.Printf("addr_checker: addr forced")
		self.set_my_addr(self.config.Force_My_Addr)
		return
	}

	self.locker.Lock(func() {
		srvs, err = self.db_repo.GetByType(local.ServerTypeSTUN)
	})
	if err != nil {
		log.Printf("addr_checker: %v", err)
		return
	}
	if len(srvs) == 0 {
		return
	}
	queue := make(chan result[string])
	use_stun := func(addr string) {
		res, err := self.use_stun(addr)
		queue <- result[string]{
			Sender: addr,
			Val:    res,
			Err:    err,
		}
	}
	for _, srv := range srvs {
		go use_stun(srv.Addr)
	}
	var res result[string]
	for range len(srvs) {
		res = <-queue
		if res.Err == nil {
			break
		}
		log.Printf("can not use stun(%s): %v", res.Sender, res.Err)
	}
	self.set_my_addr(res.Val)
	log.Printf("Addr updated(%s): %s", res.Sender, res.Val)
}

func (self *checker) set_my_addr(addr string) {
	self.my_addr_mtx.Lock()
	defer self.my_addr_mtx.Unlock()
	self.my_addr = addr
}

func (self *checker) use_stun(addr string) (string, error) {
	stun, err := self.stun.Make(addr)
	if err != nil {
		return "", err
	}
	return stun.GetMe()
}

func (self *checker) publush_me() {
	var me *local.Me
	var srvs []local.Server
	var err error
	self.locker.Lock(func() {
		me, err = self.me_getter.GetMe()
		if err != nil {
			return
		}
		srvs, err = self.db_repo.GetByType(local.ServerTypeExtStor)
	})
	if err != nil {
		log.Printf("addr_checker: %v", err)
		return
	}
	for _, srv := range srvs {
		go self.publish_on_server(me.Uid, srv.Addr)
	}
}

func (self *checker) publish_on_server(me uuid.UUID, addr string) {
	server, err := self.ext_stor.Make(addr)
	if err != nil {
		log.Printf(
			"addr_checker: can not make server(%s): %v",
			addr,
			err,
		)
		return
	}
	err = server.PublushMe(me, self.my_addr)
	if err != nil {
		log.Printf(
			"add_checker: can not publush me(%s): %v",
			addr,
			err,
		)
	}
}
