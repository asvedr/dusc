package addr_checker_test

import (
	"dusc/src/entities/busmsg"
	"dusc/src/entities/config"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/tasks/addr_checker"
	"dusc/src/proto"
	"errors"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

type Setup struct {
	db       factories.Db
	wf       *factories.MsgSendWF
	stun     *factories.StunFactory
	ext_stor *factories.ExtStorageFactory
	checker  proto.IAddrManager
}

func new_setup() Setup {
	db := factories.NewDb()
	wf := &factories.MsgSendWF{}
	stun := &factories.StunFactory{}
	ext_stor := &factories.ExtStorageFactory{}
	checker := addr_checker.New(
		db.Locker,
		wf,
		stun,
		ext_stor,
		db.User,
		db.Servers,
		db.Settings,
		&config.Config{},
	)
	return Setup{
		db:       db,
		wf:       wf,
		stun:     stun,
		ext_stor: ext_stor,
		checker:  checker,
	}
}

func TestEmptyBase(t *testing.T) {
	setup := new_setup()
	setup.checker.Execute()
}

func TestStunFailed(t *testing.T) {
	setup := new_setup()
	setup.db.Settings.SetMe(factories.Me.Get())
	new_stun := local.Server{
		Addr: "saddr",
		Type: local.ServerTypeSTUN,
	}
	new_ext := local.Server{
		Addr: "eaddr",
		Type: local.ServerTypeExtStor,
	}
	assert.Nil(t, setup.db.Servers.Add(new_stun, new_ext))

	setup.stun.MakeResp = []any{&factories.Stun{Err: errors.New("fail")}}
	ext_stor := &factories.ExtStorage{}
	setup.ext_stor.MakeResp = []any{ext_stor}

	setup.checker.Execute()

	assert.Equal(t, 0, len(ext_stor.Publushed))
}

func TestStunOneFailedOneOk(t *testing.T) {
	setup := new_setup()
	setup.db.Settings.SetMe(factories.Me.Get())
	for _, addr := range []string{"addr1", "addr2", "addr3"} {
		srv := local.Server{Addr: addr, Type: local.ServerTypeSTUN}
		assert.Nil(t, setup.db.Servers.Add(srv))
	}
	setup.db.Servers.Add(local.Server{
		Addr: "eaddr",
		Type: local.ServerTypeExtStor,
	})

	setup.stun.MakeResp = []any{
		&factories.Stun{Err: errors.New("fail")},
		&factories.Stun{Me: "me2"},
		&factories.Stun{Err: errors.New("fail")},
	}
	ext_stor := &factories.ExtStorage{}
	setup.ext_stor.MakeResp = []any{
		ext_stor, // fetch
		ext_stor, // publish
	}

	setup.checker.Execute()

	time.Sleep(time.Millisecond * 500)

	expected := "5534cb71-f34c-42ff-ab06-bb43c2d90ce3|me2"
	assert.Equal(t, []string{expected}, ext_stor.Publushed)
}

func TestPingFriends(t *testing.T) {
	setup := new_setup()
	setup.db.Settings.SetMe(factories.Me.Get())
	friend, _ := uuid.NewRandom()
	setup.db.User.Contact(
		"abc",
		friend,
		&factories.PrivKey2.Get().PublicKey,
	)
	setup.db.User.SetLastSeen(friend, 1, "friend")
	srv := local.Server{Addr: "saddr", Type: local.ServerTypeSTUN}
	assert.Nil(t, setup.db.Servers.Add(srv))

	setup.stun.MakeResp = []any{&factories.Stun{Me: "me"}}

	setup.checker.Execute()

	time.Sleep(time.Millisecond * 500)

	pub := &factories.PrivKey2.Get().PublicKey
	expected := factories.MsgSendWFFull{
		Receiver: friend,
		Addrs:    []string{"friend"},
		EncKey:   pub,
		Msg: &busmsg.Ping{
			SentFromAddr: "me",
			SendToAddr:   "friend",
			PublicKey:    pub,
		},
	}

	assert.Equal(t, []any{expected}, setup.wf.Calls)
}

func TestPingUser(t *testing.T) {
	setup := new_setup()
	setup.db.Settings.SetMe(factories.Me.Get())
	friend, _ := uuid.NewRandom()
	setup.db.User.Contact(
		"abc",
		friend,
		&factories.PrivKey2.Get().PublicKey,
	)
	// setup.db.User.SetLastSeen(friend, 1, "friend")
	srv := local.Server{Addr: "saddr", Type: local.ServerTypeSTUN}
	assert.Nil(t, setup.db.Servers.Add(srv))

	setup.stun.MakeResp = []any{&factories.Stun{Me: "me"}}

	setup.checker.Execute()

	time.Sleep(time.Millisecond * 500)

	assert.Equal(t, 0, len(setup.wf.Calls))

	err := setup.checker.PingUser(friend, "friend_addr")
	assert.Nil(t, err)

	pub := &factories.PrivKey2.Get().PublicKey
	expected := factories.MsgSendWFFull{
		Receiver: friend,
		Addrs:    []string{"friend_addr"},
		EncKey:   pub,
		Msg: &busmsg.Ping{
			SentFromAddr: "me",
			SendToAddr:   "friend_addr",
			PublicKey:    &factories.Me.Get().PrivKey.PublicKey,
		},
	}
	assert.Equal(t, []any{expected}, setup.wf.Calls)
}
