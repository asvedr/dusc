package msg_send_wf_test

import (
	"dusc/src/entities/busmsg"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/common/hasher"
	"dusc/src/impls/services/msg_send_wf"
	"sort"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

type Setup struct {
	db         factories.Db
	time       *factories.Time
	crypto     *factories.Crypto
	signer     *factories.Signer
	bus_sender *factories.BusSender
	wf         *msg_send_wf.WF
}

func new_setup() Setup {
	db := factories.NewDb()
	time := &factories.Time{}
	crypto := &factories.Crypto{}
	signer := &factories.Signer{}
	bus_sender := &factories.BusSender{}
	wf := msg_send_wf.New(
		db.Locker,
		time,
		signer,
		crypto,
		hasher.New(),
		bus_sender,
		db.Settings,
		db.User,
	)
	return Setup{
		db:         db,
		time:       time,
		crypto:     crypto,
		signer:     signer,
		bus_sender: bus_sender,
		wf:         wf,
	}
}

func TestAddToQueueMininal(t *testing.T) {
	setup := new_setup()
	receiver, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()
	enc_key := &factories.PrivKey2.Get().PublicKey
	setup.db.User.Contact(
		"user",
		receiver,
		enc_key,
	)
	setup.db.User.SetLastSeen(receiver, 1, "addr1")
	setup.db.User.SetLastSeen(receiver, 2, "addr2")
	msg := &busmsg.Text{
		MsgId:  msg_id,
		ChatId: nil,
		Text:   "hello",
	}

	err := setup.wf.AddToQueueMinial(receiver, msg)
	assert.Nil(t, err)

	expected := &msg_send_wf.QNode{
		Receiver: receiver,
		Addrs:    []string{"addr2", "addr1"},
		Msg:      msg,
		EncKey:   enc_key,
	}
	got := <-setup.wf.Queue
	assert.Equal(t, expected, got)
}

func TestAddToQueue(t *testing.T) {
	setup := new_setup()
	receiver, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()
	msg := &busmsg.Text{
		MsgId:  msg_id,
		ChatId: nil,
		Text:   "hello",
	}
	enc_key := &factories.PrivKey2.Get().PublicKey
	addrs := []string{"addr1", "addr2"}

	setup.wf.AddToQueue(
		receiver,
		addrs,
		msg,
		enc_key,
	)

	expected := &msg_send_wf.QNode{
		Receiver: receiver,
		Addrs:    addrs,
		Msg:      msg,
		EncKey:   enc_key,
	}
	got := <-setup.wf.Queue
	assert.Equal(t, expected, got)
}

func TestRunWorkflow(t *testing.T) {
	setup := new_setup()
	setup.time.NowData = 1
	my_key := factories.PrivKey1.Get()
	my_id, _ := uuid.NewRandom()
	setup.db.Settings.SetMe(local.Me{PrivKey: my_key, Uid: my_id})
	receiver, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()
	msg := &busmsg.Text{
		MsgId:  msg_id,
		ChatId: nil,
		Text:   "hello",
	}
	enc_key := &factories.PrivKey2.Get().PublicKey
	addrs := []string{"addr1", "addr2"}

	setup.wf.AddToQueue(
		receiver,
		addrs,
		msg,
		enc_key,
	)

	me, err := setup.db.Settings.GetMe()
	assert.Nil(t, err)
	node := <-setup.wf.Queue

	setup.signer.SignResp = []busmsg.Signed{
		{Signature: []byte{1, 2, 3}},
	}
	encrypted := []byte{3, 2, 1}
	setup.crypto.EncryptResp = [][]byte{encrypted}

	setup.wf.Process(me, node)
	time.Sleep(time.Millisecond * 500)

	sort.Slice(
		setup.bus_sender.Calls,
		func(i, j int) bool {
			a := setup.bus_sender.Calls[i]
			b := setup.bus_sender.Calls[j]
			return a.Addr < b.Addr
		},
	)

	exp_sign := []factories.SignCall{
		{
			Msg: &busmsg.Text{
				Base: busmsg.Base{
					CreatedAt: 1,
					SenderId:  my_id,
				},
				MsgId:  msg_id,
				ChatId: nil,
				Text:   "hello",
			},
			Key: my_key,
		},
	}
	assert.Equal(t, exp_sign, setup.signer.SignCalls)

	exp_enc := []factories.EncryptCall{
		{
			Msg: busmsg.Signed{Signature: []byte{1, 2, 3}},
			Key: enc_key,
		},
	}
	assert.Equal(t, exp_enc, setup.crypto.EncryptCalls)
	packed := busmsg.Packed{
		Encrypted: []byte{3, 2, 1},
		HashSum: []byte{
			0x13, 0x4a, 0xea, 0xd1, 0xd2, 0x2, 0xa, 0xdf,
			0xb1, 0xd2, 0x35, 0x2b, 0x1d, 0xff, 0xb2,
			0xaf, 0xd8, 0xfe, 0xd, 0xc5,
		},
	}
	exp_send := []factories.BusSenderCall{
		{Addr: "addr1", Msg: packed},
		{Addr: "addr2", Msg: packed},
	}
	assert.Equal(t, exp_send, setup.bus_sender.Calls)
}
