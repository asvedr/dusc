package msg_send_wf

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"log"

	"github.com/google/uuid"
)

const chan_capacity int = 1000

/*
workflow:
- set msg."base"
- sign
- encrypt
- pack
- send
*/
type QNode struct {
	Receiver uuid.UUID
	Addrs    []string
	Msg      busmsg.DynMsg
	EncKey   *rsa.PublicKey
}

type WF struct {
	locker     proto.IDbLocker
	time       proto.ITime
	signer     proto.IMsgSigner
	crypto     proto.ICrypto
	hasher     proto.IHasher
	bus_sender proto.IBusSender
	me_getter  proto.IMeGetter
	user_repo  proto.IUserRepo

	Queue chan *QNode
}

func New(
	locker proto.IDbLocker,
	time proto.ITime,
	signer proto.IMsgSigner,
	crypto proto.ICrypto,
	hasher proto.IHasher,
	bus_sender proto.IBusSender,
	me_getter proto.IMeGetter,
	user_repo proto.IUserRepo,
) *WF {
	return &WF{
		locker:     locker,
		time:       time,
		signer:     signer,
		crypto:     crypto,
		hasher:     hasher,
		bus_sender: bus_sender,
		me_getter:  me_getter,
		user_repo:  user_repo,

		Queue: make(chan *QNode, chan_capacity),
	}
}

//lint:ignore U1000 reason: compile time verification
func verify_interface() {
	var _ proto.IService = &WF{}
	var _ proto.IMsgSendWorkflow = &WF{}
}

// Must be called in lock
func (self *WF) AddToQueueMinial(receiver uuid.UUID, msg busmsg.DynMsg) error {
	key, addrs, err := self.user_repo.GetPubKeyAndAddrs(receiver)
	if err != nil {
		return err
	}
	self.AddToQueue(receiver, addrs, msg, key)
	return nil
}

func (self *WF) AddToQueue(
	receiver uuid.UUID,
	addrs []string,
	msg busmsg.DynMsg,
	enc_key *rsa.PublicKey,
) {
	self.Queue <- &QNode{
		Receiver: receiver,
		Addrs:    addrs,
		Msg:      msg,
		EncKey:   enc_key,
	}
}

func (self *WF) Name() string {
	return "msg_send_wf"
}

func (self *WF) Run() error {
	var me *local.Me
	var err error
	for node := range self.Queue {
		self.locker.Lock(func() {
			me, err = self.me_getter.GetMe()
		})
		if me == nil || err != nil {
			log.Printf(
				"can not send msg to %v because ME not set",
				node.Receiver,
			)
		}
		go self.Process(me, node)
	}
	return nil
}

func (self *WF) Process(me *local.Me, node *QNode) {
	msg := self.set_base(me, node.Msg)
	signed, err := self.signer.Sign(msg, me.PrivKey)
	if err != nil {
		log.Printf("Can not sign msg for %v: %v", node.Receiver, err)
		return
	}
	encrypted, err := self.crypto.Encrypt(signed, node.EncKey)
	if err != nil {
		log.Printf("Can not encrypt msg for %v: %v", node.Receiver, err)
		return
	}
	log.Printf("SIG_LEN: %d", len(signed.Signature))
	log.Printf("RES_MSG_LEN: %d", len(encrypted))
	packed := self.pack(encrypted)
	send := func(addr string) {
		_, err = self.bus_sender.Send(addr, packed)
		if err != nil {
			log.Printf(
				"Msg %v was not sent to %s: %v",
				node.Msg,
				addr,
				err,
			)
		}
	}
	for _, addr := range node.Addrs {
		go send(addr)
	}
}

func (self *WF) set_base(me *local.Me, msg busmsg.DynMsg) busmsg.DynMsg {
	msg.SetBase(busmsg.Base{
		CreatedAt: self.time.Now(),
		SenderId:  me.Uid,
	})
	return msg
}

func (self *WF) pack(enc []byte) busmsg.Packed {
	hash := self.hasher.GetSum(enc)
	return busmsg.Packed{Encrypted: enc, HashSum: hash}
}
