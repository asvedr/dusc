package api_test

import (
	"dusc/src/factories"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test404(t *testing.T) {
	server := factories.NewApiServer()
	req := factories.ApiRequest{Url: "/unused/path"}
	resp := server.Call(req)
	assert.Equal(t, resp.Status, 404)
	exp := `{"code":"URL_NOT_FOUND","details":""}`
	assert.Equal(t, exp, string(resp.Body))
}
