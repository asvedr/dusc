package api

import (
	"dusc/src/entities/api"
	"dusc/src/entities/api_err"
	"dusc/src/proto"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"
)

type Service struct {
	Handler http.Handler
	server  *http.Server
}

type srv_handler struct {
	handlers map[string]map[string]proto.IApiHandler
}

var default_response = []byte(`{"status": "ok"}`)

func New(
	api_port int,
	handlers []proto.IApiHandler,
) proto.IService {
	handler_schemas := map[string]map[string]proto.IApiHandler{}
	for _, h := range handlers {
		schema := h.Schema()
		path_map := handler_schemas[schema.Path]
		if path_map == nil {
			path_map = map[string]proto.IApiHandler{}
			handler_schemas[schema.Path] = path_map
		}
		method := schema.Method
		if method == "" {
			method = "GET"
		}
		path_map[method] = h
	}
	handler := &srv_handler{
		handlers: handler_schemas,
	}
	server := &http.Server{
		Addr:           fmt.Sprintf(":%d", api_port),
		Handler:        handler,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	return Service{server: server, Handler: handler}
}

func (self Service) Name() string {
	return "api"
}

func (self Service) Run() error {
	return self.server.ListenAndServe()
}

func (self *srv_handler) ServeHTTP(
	response http.ResponseWriter,
	request *http.Request,
) {
	bts, err := self.wrapped_handler(request)
	if err == nil {
		response.WriteHeader(200)
		if len(bts) == 0 {
			bts = default_response
		}
		response.Write(bts)
		return
	}

	apie, casted := err.(api_err.ApiError)
	if !casted {
		apie = api_err.Internal(err.Error())
	}
	response.WriteHeader(apie.Status)
	response.Write([]byte(apie.Error()))
}

func (self *srv_handler) wrapped_handler(request *http.Request) ([]byte, error) {
	handler, err := self.find_handler(request)
	if err != nil {
		return nil, err
	}
	schema := handler.Schema()
	query, err := parse_query(schema, request)
	if err != nil {
		return nil, err
	}
	body, err := parse_body(schema, request)
	if err != nil {
		return nil, err
	}
	resp, err := handler.Handle(query, body)
	if err != nil {
		return nil, err
	}
	return serialize_resp(resp)
}

func (self *srv_handler) find_handler(
	request *http.Request,
) (proto.IApiHandler, error) {
	path := request.URL.Path
	if !strings.HasSuffix(path, "/") {
		path += "/"
	}
	method_map := self.handlers[path]
	if method_map == nil {
		return nil, api_err.UrlNotFound
	}
	handler := method_map[request.Method]
	if handler == nil {
		return nil, api_err.MethodNotAllowed
	}
	return handler, nil
}

func parse_query(
	schema api.HandlerSchema,
	request *http.Request,
) (any, error) {
	if schema.QueryParser == nil {
		return nil, nil
	}
	val, err := schema.QueryParser.Parse(request.URL.Query())
	if err != nil {
		return nil, api_err.InvalidQuery(err)
	}
	return val, nil
}

func parse_body(
	schema api.HandlerSchema,
	request *http.Request,
) (any, error) {
	obj := schema.BodyParserFactory
	defer request.Body.Close()
	if obj == nil {
		return nil, nil
	}
	dec := json.NewDecoder(request.Body)
	err := dec.Decode(obj)
	if err != nil {
		return nil, api_err.InvalidBody(err)
	}
	return obj, nil
}

func serialize_resp(resp any) ([]byte, error) {
	if resp == nil {
		return nil, nil
	}
	return json.Marshal(resp)
}
