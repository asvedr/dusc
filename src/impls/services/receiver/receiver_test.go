package receiver_test

import (
	"dusc/src/entities/busmsg"
	"dusc/src/entities/local"
	"dusc/src/factories"
	"dusc/src/impls/common/hasher"
	"dusc/src/impls/services/receiver"
	"dusc/src/proto"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

type Setup struct {
	db           factories.Db
	bus_receiver *factories.BusReceiver
	signer       *factories.Signer
	crypto       *factories.Crypto
	dispatcher   *factories.MsgDispatcher
	receiver     proto.IService
}

func new_setup() Setup {
	db := factories.NewDb()
	bus_receiver := &factories.BusReceiver{}
	signer := &factories.Signer{}
	crypto := &factories.Crypto{}
	dispatcher := &factories.MsgDispatcher{}
	recv := receiver.New(
		db.Locker,
		bus_receiver,
		hasher.New(),
		signer,
		crypto,
		dispatcher,
		db.Settings,
		db.User,
	)
	return Setup{
		db:           db,
		bus_receiver: bus_receiver,
		signer:       signer,
		crypto:       crypto,
		dispatcher:   dispatcher,
		receiver:     recv,
	}
}

func TestReceiveWorkflow(t *testing.T) {
	setup := new_setup()
	setup.db.Settings.SetMe(local.Me{
		PrivKey: factories.PrivKey1.Get(),
		Uid:     factories.Uuid.Get(),
	})
	sender, _ := uuid.NewRandom()
	msg_id, _ := uuid.NewRandom()
	signed_msg := busmsg.Signed{
		Msg: &busmsg.Text{
			Base: busmsg.Base{
				CreatedAt: 1,
				SenderId:  sender,
			},
			MsgId: msg_id,
			Text:  "hi",
		},
		Signature: []byte("sig"),
	}
	setup.crypto.DecryptResp = []busmsg.Signed{signed_msg}
	setup.signer.ValidateResp = []bool{true}
	setup.db.User.Contact("name", sender, &factories.PrivKey2.Get().PublicKey)

	// Prepare
	assert.Nil(t, setup.receiver.Run())

	// Call
	setup.bus_receiver.Handler(busmsg.Packed{
		Encrypted: []byte("encrypted"),
		HashSum: []byte{
			217, 35, 66, 151, 109, 114, 15, 243, 140,
			245, 220, 179, 41, 190, 65, 149, 154, 177,
			186, 108,
		},
	})

	dec_calls := []factories.DecryptCall{
		{
			Bts: []byte("encrypted"),
			Key: factories.PrivKey1.Get(),
		},
	}
	assert.Equal(t, dec_calls, setup.crypto.DecryptCalls)

	val_calls := []factories.ValidateCall{
		{
			Msg: signed_msg,
			Key: &factories.PrivKey2.Get().PublicKey,
		},
	}
	assert.Equal(t, val_calls, setup.signer.ValidateCalls)

	dis_calls := []busmsg.DynMsg{signed_msg.Msg}
	assert.Equal(t, dis_calls, setup.dispatcher.Calls)
}
