package receiver

import (
	"bytes"
	"dusc/src/entities/busmsg"
	"dusc/src/entities/local"
	"dusc/src/proto"
	"log"
)

type receiver struct {
	locker       proto.IDbLocker
	bus_receiver proto.IBusReceiver
	hasher       proto.IHasher
	signer       proto.IMsgSigner
	crypto       proto.ICrypto
	dispatcher   proto.IMsgDispatcher
	me_getter    proto.IMeGetter
	user_repo    proto.IUserRepo
}

func New(
	locker proto.IDbLocker,
	bus_receiver proto.IBusReceiver,
	hasher proto.IHasher,
	signer proto.IMsgSigner,
	crypto proto.ICrypto,
	dispatcher proto.IMsgDispatcher,
	me_getter proto.IMeGetter,
	user_repo proto.IUserRepo,
) proto.IService {
	return &receiver{
		locker:       locker,
		bus_receiver: bus_receiver,
		hasher:       hasher,
		signer:       signer,
		crypto:       crypto,
		dispatcher:   dispatcher,
		me_getter:    me_getter,
		user_repo:    user_repo,
	}
}

func (self *receiver) Name() string {
	return "receiver"
}

func (self *receiver) Run() error {
	self.bus_receiver.SetHandler(self.handler)
	return self.bus_receiver.Listen()
}

func (self *receiver) handler(packed busmsg.Packed) error {
	var me *local.Me
	var err error
	self.locker.Lock(func() {
		me, err = self.me_getter.GetMe()
	})
	if err != nil {
		log.Printf("Can not get private key")
		return nil
	}
	if !self.validate_hash(packed) {
		log.Printf("message hash is invalid")
		return nil
	}
	signed, err := self.crypto.Decrypt(packed.Encrypted, me.PrivKey)
	if err != nil {
		log.Printf("Message can not be decrypted")
		return nil
	}
	if !self.validate_signature(signed) {
		log.Printf("Message %v signature is invalid", signed.Msg)
		return nil
	}
	self.locker.Lock(func() {
		err = self.dispatcher.Dispatch(signed.Msg)
	})
	return err
}

func (self *receiver) validate_hash(packed busmsg.Packed) bool {
	sum := self.hasher.GetSum(packed.Encrypted)
	return bytes.Equal(sum, packed.HashSum)
}

func (self *receiver) validate_signature(msg busmsg.Signed) bool {
	key, err := self.user_repo.GetPubKey(msg.Msg.Sender())
	if err != nil {
		log.Printf(
			"Can not get user(%v) pub key: %v",
			msg.Msg.Sender(),
			err,
		)
		return false
	}
	return self.signer.Validate(msg, key)
}
