package factories

import (
	"dusc/src/proto"
	"sync"
)

type StunFactory struct {
	mtx       sync.Mutex
	MakeCalls []string
	MakeResp  []any
}

type Stun struct {
	Me  string
	Err error
}

//lint:ignore U1000 reason: compile time verification
func (self *StunFactory) _verify_interface() {
	var _ proto.IFactory[proto.ISTUNClient] = self
}

//lint:ignore U1000 reason: compile time verification
func (self *Stun) _verify_interface() {
	var _ proto.ISTUNClient = self
}

func (self *StunFactory) Make(addr string) (proto.ISTUNClient, error) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	self.MakeCalls = append(self.MakeCalls, addr)
	resp := self.MakeResp[0]
	self.MakeResp = self.MakeResp[1:]
	stun, casted := resp.(*Stun)
	if casted {
		return stun, nil
	}
	err, casted := resp.(error)
	if casted {
		return nil, err
	}
	panic("invalid stun response")
}

func (self *Stun) GetMe() (string, error) {
	return self.Me, self.Err
}
