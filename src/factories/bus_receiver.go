package factories

import (
	"dusc/src/entities/busmsg"
)

type BusReceiver struct {
	Handler func(busmsg.Packed) error
}

func (self *BusReceiver) SetHandler(handler func(busmsg.Packed) error) {
	self.Handler = handler
}

func (*BusReceiver) Listen() error {
	return nil
}
