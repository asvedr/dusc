package factories

import "dusc/src/entities/busmsg"

type MsgDispatcher struct {
	Calls []busmsg.DynMsg
}

func (self *MsgDispatcher) Dispatch(msg busmsg.DynMsg) error {
	self.Calls = append(self.Calls, msg)
	return nil
}
