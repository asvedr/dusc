package factories

import (
	"dusc/src/impls/db/sqlite/chats"
	"dusc/src/impls/db/sqlite/db_locker"
	"dusc/src/impls/db/sqlite/msg"
	"dusc/src/impls/db/sqlite/server"
	"dusc/src/impls/db/sqlite/settings"
	"dusc/src/impls/db/sqlite/shared_files"
	"dusc/src/impls/db/sqlite/user"
	"dusc/src/impls/serializers/key_serializer"
	"dusc/src/proto"
)

type Db struct {
	Locker   proto.IDbLocker
	Msg      proto.IMsgRepo
	User     proto.IUserRepo
	Settings proto.ISettingsRepo
	Files    proto.ISharedFileRepo
	Servers  proto.IServerRepo
	Chats    proto.IChatRepo
}

func NewDb() Db {
	db, err := db_locker.New(":memory:", false)
	if err != nil {
		panic(err.Error())
	}
	msg_repo, err := msg.New(db)
	if err != nil {
		panic(err.Error())
	}
	user_repo, err := user.New(db, key_serializer.NewPubStr())
	if err != nil {
		panic(err.Error())
	}
	stt_repo, err := settings.New(db, key_serializer.NewPrivStr())
	if err != nil {
		panic(err.Error())
	}
	file_repo, err := shared_files.New(db)
	if err != nil {
		panic(err.Error())
	}
	srv_repo, err := server.New(db)
	if err != nil {
		panic(err.Error())
	}
	chat_repo, err := chats.New(db, user_repo, msg_repo)
	if err != nil {
		panic(err.Error())
	}
	return Db{
		Locker:   db,
		Msg:      msg_repo,
		User:     user_repo,
		Settings: stt_repo,
		Files:    file_repo,
		Servers:  srv_repo,
		Chats:    chat_repo,
	}
}
