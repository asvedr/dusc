package factories

import "dusc/src/proto"

type TokenManager struct {
	SetActiveCalls     []bool
	IsAuthTurnedOnData bool
	Token              string
}

//lint:ignore U1000 reason: compile time verification
func (self *TokenManager) _verify_interface() proto.IAuthTokenManager {
	return self
}

func (self *TokenManager) SetActive(value bool) {
	self.SetActiveCalls = append(self.SetActiveCalls, value)
}

func (self *TokenManager) IsAuthTurnedOn() bool {
	return self.IsAuthTurnedOnData
}

func (self *TokenManager) GenToken() (string, error) {
	return self.Token, nil
}

func (self *TokenManager) ValidateToken(token string) bool {
	return self.Token == token
}
