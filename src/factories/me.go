package factories

import (
	"dusc/src/entities/local"

	"gitlab.com/asvedr/cldi/di"
)

var Me = di.Singleton[local.Me]{
	PureFunc: func() local.Me {
		return local.Me{
			PrivKey: PrivKey1.Get(),
			Uid:     Uuid.Get(),
		}
	},
}
