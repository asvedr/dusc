package factories

import "io"

type Rand struct{}

//lint:ignore U1000 reason: compile time verification
func (self Rand) _verify_interface() io.Reader {
	return self
}

func (Rand) Read(p []byte) (n int, err error) {
	for i := range p {
		p[i] = byte(i % 10)
	}
	return len(p), nil
}
