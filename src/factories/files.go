package factories

import (
	"dusc/src/entities/local"
	"dusc/src/proto"
	"errors"
	"fmt"
	"strings"
)

type FS struct {
	ExecDirData       string
	CreatedDir        []string
	ReadFileData      map[string][]byte
	WriteFileData     map[string][]byte
	WriteFileResp     []error
	AppendFileData    map[string][]byte
	GetBlockData      map[string][][]byte
	GetMetaCalls      []string
	GetMetaResp       []any
	GetDirContentData map[string][]local.DirItem
}

//lint:ignore U1000 reason: compile time verification
func (self *FS) _verify_interface() proto.IFS {
	return self
}

func (self *FS) ExecDir() (string, error) {
	return self.ExecDirData, nil
}

func (self *FS) CreateDir(path string) error {
	self.CreatedDir = append(self.CreatedDir, path)
	return nil
}

func (self *FS) ReadFile(path string) ([]byte, error) {
	if self.ReadFileData == nil {
		return nil, errors.New("ReadFile: " + path)
	}
	data, found := self.ReadFileData[path]
	if !found {
		return nil, errors.New("ReadFile: " + path)
	}
	return data, nil
}

func (self *FS) WriteFile(path string, data []byte) error {
	if self.WriteFileData == nil {
		self.WriteFileData = map[string][]byte{}
	}
	self.WriteFileData[path] = data
	err := self.WriteFileResp[0]
	self.WriteFileResp = self.WriteFileResp[1:]
	return err
}

func (self *FS) AppendFile(path string, data []byte) error {
	if self.AppendFileData == nil {
		self.AppendFileData = map[string][]byte{}
	}
	self.AppendFileData[path] = data
	return nil
}

func (self *FS) GetBlock(path string, block_id int) ([]byte, error) {
	if self.GetBlockData == nil {
		return nil, errors.New("GetBlock: " + path)
	}
	blocks := self.GetBlockData[path]
	if block_id >= len(blocks) {
		return nil, fmt.Errorf("GetBlock: %s, %d", path, block_id)
	}
	return blocks[block_id], nil
}

func (self *FS) GetMeta(path string) (local.FSFileMeta, error) {
	self.GetMetaCalls = append(self.GetMetaCalls, path)
	item := self.GetMetaResp[0]
	self.GetMetaResp = self.GetMetaResp[1:]
	switch res := item.(type) {
	case local.FSFileMeta:
		return res, nil
	case error:
		return local.FSFileMeta{}, res
	default:
		panic("unexpected value: " + fmt.Sprint(item))
	}
}

func (self *FS) GetParentDir(path string) (string, error) {
	split := strings.Split(path, "/")
	if len(split) == 1 {
		return path, nil
	}
	split = split[:len(split)-1]
	if len(split) == 1 && split[0] == "" {
		return "/", nil
	}
	return strings.Join(split, "/"), nil
}

func (self *FS) GetAbsPath(path string) (string, error) {
	if strings.HasPrefix(path, "/") {
		return path, nil
	}
	return "/root/" + path, nil
}

func (self *FS) GetDirContent(path string) ([]local.DirItem, error) {
	if self.GetDirContentData == nil {
		return nil, errors.New("GetDirContent: " + path)
	}
	return self.GetDirContentData[path], nil
}
