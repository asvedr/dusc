package factories

import (
	"dusc/src/entities/local"
	"dusc/src/proto"

	"github.com/google/uuid"
)

type AddrManager struct {
	MyAddr        string
	PingUserCalls []PingUserCall
	PingUserResp  []error
}

type PingUserCall struct {
	Uid  uuid.UUID
	Addr string
}

//lint:ignore U1000 reason: compile time verification
func (self *AddrManager) _verify_interface() proto.IAddrManager {
	return self
}

func (self *AddrManager) Schema() local.TaskSchema {
	panic("AddrManager.Schema")
}

func (self *AddrManager) Execute() error {
	panic("AddrManager.Execute")
}

func (self *AddrManager) GetMyAddr() string {
	return self.MyAddr
}

func (self *AddrManager) PingUser(uid uuid.UUID, addr string) error {
	call := PingUserCall{Uid: uid, Addr: addr}
	self.PingUserCalls = append(self.PingUserCalls, call)
	resp := self.PingUserResp[0]
	self.PingUserResp = self.PingUserResp[1:]
	return resp
}
