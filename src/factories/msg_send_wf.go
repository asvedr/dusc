package factories

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/proto"
	"sync"

	"github.com/google/uuid"
)

type MsgSendWF struct {
	mtx   sync.Mutex
	Calls []any
}

type MsgSendWFMinial struct {
	Receiver uuid.UUID
	Msg      busmsg.DynMsg
}

type MsgSendWFFull struct {
	Receiver uuid.UUID
	Addrs    []string
	Msg      busmsg.DynMsg
	EncKey   *rsa.PublicKey
}

//lint:ignore U1000 reason: compile time verification
func (self *MsgSendWF) _verify_interface() {
	var _ proto.IMsgSendWorkflow = self
	// var _ proto.IService = self
}

func (self *MsgSendWF) AddToQueueMinial(receiver uuid.UUID, msg busmsg.DynMsg) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	call := MsgSendWFMinial{Receiver: receiver, Msg: msg}
	self.Calls = append(self.Calls, call)
	return nil
}

func (self *MsgSendWF) AddToQueue(
	receiver uuid.UUID,
	addrs []string,
	msg busmsg.DynMsg,
	enc_key *rsa.PublicKey,
) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	call := MsgSendWFFull{
		Receiver: receiver,
		Addrs:    addrs,
		Msg:      msg,
		EncKey:   enc_key,
	}
	self.Calls = append(self.Calls, call)
}
