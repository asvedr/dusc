package factories

import (
	"bytes"
	"dusc/src/entities/api_err"
	"dusc/src/impls/services/api"
	"dusc/src/proto"
	"encoding/json"
	"net/http/httptest"
)

type ApiServer struct {
	Db                  Db
	Crypto              *Crypto
	Time                *Time
	Initializer         *Initializer
	TokenManager        *TokenManager
	Downloader          *Downloader
	Fs                  *FS
	StunFactory         *StunFactory
	ExtStorageFactory   *ExtStorageFactory
	AddrManager         *AddrManager
	TaskExecutor        *TaskExecutor
	ServerStatusStorage proto.IServerStatusStorage

	service api.Service
}

type ApiRequest struct {
	Url     string
	Method  string
	Headers map[string]string
	Json    any
}

type ApiResponse struct {
	Status int
	Body   []byte
}

func (self ApiServer) Call(request ApiRequest) ApiResponse {
	method := request.Method
	if method == "" {
		method = "GET"
	}
	var err error
	bts := []byte{}
	if request.Json != nil {
		bts, err = json.Marshal(request.Json)
	}
	if err != nil {
		panic("CAN NOT ENCODE JSON: " + err.Error())
	}
	req := httptest.NewRequest(method, request.Url, bytes.NewBuffer(bts))
	if request.Headers != nil {
		for h_key, h_val := range request.Headers {
			req.Header.Add(h_key, h_val)
		}
	}
	resp := httptest.NewRecorder()
	self.service.Handler.ServeHTTP(resp, req)
	resp_body := resp.Body.Bytes()
	result := resp.Result()
	return ApiResponse{
		Status: result.StatusCode,
		Body:   resp_body,
	}
}

func (self *ApiResponse) ParseJson(dst any) {
	err := json.Unmarshal(self.Body, dst)
	if err != nil {
		panic("CAN NOT PARSE RESPONSE: " + err.Error())
	}
}

func (self *ApiResponse) ParseErr() api_err.ApiError {
	err := api_err.ApiError{}
	self.ParseJson(&err)
	err.Status = self.Status
	return err
}
