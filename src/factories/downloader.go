package factories

import (
	"dusc/src/entities/errs"
	"dusc/src/proto"
	"errors"
	"sync"

	"github.com/google/uuid"
)

type Downloader struct {
	mtx                  sync.Mutex
	StartLoadingCalls    []StartLoadingCall
	StartLoadingResp     error
	GetLoadingStatusData map[uuid.UUID]map[uuid.UUID]int
	AddBlockCalls        []AddBlockCall
}

type StartLoadingCall struct {
	UserId     uuid.UUID
	FileId     uuid.UUID
	MaxParts   int
	PathToSave string
}

type AddBlockCall struct {
	UserId uuid.UUID
	FileId uuid.UUID
	Part   int
	Data   []byte
}

//lint:ignore U1000 reason: compile time verification
func (self *Downloader) _verify() {
	var _ proto.IFileDownloader = self
}

func (self *Downloader) StartLoading(
	user_id uuid.UUID,
	file_id uuid.UUID,
	max_parts int,
	path_to_save string,
) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	call := StartLoadingCall{
		UserId:     user_id,
		FileId:     file_id,
		MaxParts:   max_parts,
		PathToSave: path_to_save,
	}
	self.StartLoadingCalls = append(self.StartLoadingCalls, call)
	return self.StartLoadingResp
}

func (self *Downloader) GetLoadingStatus(user_id uuid.UUID, file_id uuid.UUID) (int, error) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	if self.GetLoadingStatusData == nil {
		return 0, errors.New("GetLoadingStatus: empty data")
	}
	files := self.GetLoadingStatusData[user_id]
	if files == nil {
		return 0, errors.New("GetLoadingStatus: empty files")
	}
	data, found := files[file_id]
	if !found {
		return 0, errs.NotFound{}
	}
	return data, nil
}

func (self *Downloader) AddBlock(
	user_id uuid.UUID,
	file_id uuid.UUID,
	part int,
	data []byte,
) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	call := AddBlockCall{
		UserId: user_id,
		FileId: file_id,
		Part:   part,
		Data:   data,
	}
	self.AddBlockCalls = append(self.AddBlockCalls, call)
	return nil
}
