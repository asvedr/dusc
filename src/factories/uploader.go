package factories

import (
	"dusc/src/proto"
	"errors"
	"fmt"

	"github.com/google/uuid"
)

type Uploader struct {
	GetFilePartData map[string][]byte
}

//lint:ignore U1000 reason: compile time verification
func (self *Uploader) _verify_interface() {
	var _ proto.IFileUploader = self
}

func (self *Uploader) AddGetFilePart(
	file_id uuid.UUID,
	sender uuid.UUID,
	part int,
	bts []byte,
) {
	if self.GetFilePartData == nil {
		self.GetFilePartData = map[string][]byte{}
	}
	key := fmt.Sprintf(
		"[%s][%s]%d",
		file_id.String(),
		sender.String(),
		part,
	)
	self.GetFilePartData[key] = bts
}

func (self *Uploader) GetFilePart(
	file_id uuid.UUID,
	sender uuid.UUID,
	part int,
) ([]byte, error) {
	key := fmt.Sprintf(
		"[%s][%s]%d",
		file_id.String(),
		sender.String(),
		part,
	)
	data, found := self.GetFilePartData[key]
	if !found {
		return nil, errors.New("GetFilePart: not found")
	}
	return data, nil
}
