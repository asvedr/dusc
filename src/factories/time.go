package factories

import "dusc/src/proto"

type Time struct {
	NowData uint64
}

//lint:ignore U1000 reason: compile time verification
func (self *Time) _verify_interface() proto.ITime {
	return self
}

func (self *Time) Now() uint64 {
	return self.NowData
}
