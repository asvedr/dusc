package factories

import (
	"dusc/src/entities/local"
	"dusc/src/proto"
)

type TaskExecutor struct {
	ExecuteNowCalls []local.TaskName
	ExecuteNowResp  []error
}

//lint:ignore U1000 reason: compile time verification
func (self *TaskExecutor) _verify_interface() proto.ITaskExecutor {
	return self
}

func (self *TaskExecutor) Name() string {
	panic("")
}

func (self *TaskExecutor) Run() error {
	panic("")
}

func (self *TaskExecutor) ExecuteNow(task local.TaskName) error {
	self.ExecuteNowCalls = append(self.ExecuteNowCalls, task)
	resp := self.ExecuteNowResp[0]
	self.ExecuteNowResp = self.ExecuteNowResp[1:]
	return resp
}
