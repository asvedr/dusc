package factories

import (
	"dusc/src/app/serializers"
	"dusc/src/impls/api/api_init"
	"dusc/src/impls/api/chats"
	"dusc/src/impls/api/files"
	"dusc/src/impls/api/messages"
	"dusc/src/impls/api/servers"
	"dusc/src/impls/api/settings"
	"dusc/src/impls/api/tasks"
	"dusc/src/impls/api/users"
	"dusc/src/impls/common/hasher"
	"dusc/src/impls/common/msg_maker"
	"dusc/src/impls/common/server_status_storage"
	"dusc/src/impls/services/api"
	"slices"
)

func NewApiServer() ApiServer {
	db := NewDb()
	crypto := &Crypto{}
	time := &Time{}
	initializer := &Initializer{}
	token_manager := &TokenManager{}
	downloader := &Downloader{}
	fs := &FS{}
	exec := &TaskExecutor{}
	msg_maker := msg_maker.New(db.Msg, time, exec)
	stun_factory := &StunFactory{}
	ext_stor_factory := &ExtStorageFactory{}
	sss := server_status_storage.New(time, 10)
	addr_man := &AddrManager{}
	handlers := slices.Concat(
		settings.MakeHandlers(
			db.Locker,
			db.Settings,
			serializers.PrivKeyStrSerializer.Get(),
			serializers.PubKeyStrSerializer.Get(),
			serializers.ContactStrSerializer.Get(),
			crypto,
			addr_man,
		),
		users.MakeHandlers(
			db.Locker,
			db.User,
			db.Chats,
			crypto,
			serializers.ContactStrSerializer.Get(),
			serializers.PubKeyStrSerializer.Get(),
			addr_man,
		),
		chats.MakeHandlers(
			db.Locker,
			db.User,
			db.Chats,
			crypto,
			msg_maker,
		),
		api_init.MakeHandlers(
			db.Locker,
			db.Settings,
			initializer,
			token_manager,
			hasher.New(),
			serializers.PrivKeyStrSerializer.Get(),
		),
		files.MakeHandlers(
			db.Locker,
			db.Msg,
			downloader,
			fs,
		),
		messages.MakeHandlers(
			db.Locker,
			db.Msg,
			db.Chats,
			db.Files,
			msg_maker,
			fs,
			"/upload",
		),
		servers.MakeHandlers(
			db.Locker,
			db.Servers,
			stun_factory,
			ext_stor_factory,
			sss,
		),
		tasks.MakeHandlers(),
	)
	service := api.New(8080, handlers).(api.Service)
	return ApiServer{
		Db:                  db,
		Crypto:              crypto,
		Time:                time,
		Initializer:         initializer,
		TokenManager:        token_manager,
		Downloader:          downloader,
		Fs:                  fs,
		StunFactory:         stun_factory,
		ExtStorageFactory:   ext_stor_factory,
		ServerStatusStorage: sss,
		AddrManager:         addr_man,
		TaskExecutor:        exec,

		service: service,
	}
}
