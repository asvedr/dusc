package factories

import (
	"crypto/rsa"
	"dusc/src/entities/errs"
	"dusc/src/proto"

	"github.com/google/uuid"
)

type Initializer struct {
	Inited    bool
	InitCalls []InitCall
	InitResp  []error
}

type InitCall struct {
	Uuid        *uuid.UUID
	PrivKey     *rsa.PrivateKey
	Name        string
	Description string
	Password    *string
}

//lint:ignore U1000 reason: compile time verification
func (self *Initializer) _verify_interface() proto.IInitializer {
	return self
}

func (self *Initializer) CheckInit() error {
	if self.Inited {
		return nil
	}
	return errs.NotInitialized{}
}

func (self *Initializer) Init(
	uuid *uuid.UUID,
	priv_key *rsa.PrivateKey,
	name string,
	description string,
	password *string,
) error {
	call := InitCall{
		Uuid:        uuid,
		PrivKey:     priv_key,
		Name:        name,
		Description: description,
		Password:    password,
	}
	self.InitCalls = append(self.InitCalls, call)
	res := self.InitResp[0]
	self.InitResp = self.InitResp[1:]
	return res
}
