package factories

import "dusc/src/entities/busmsg"

type BusSender struct {
	Calls []BusSenderCall
}

type BusSenderCall struct {
	Addr string
	Msg  busmsg.Packed
}

func (self *BusSender) Send(
	addr string,
	msg busmsg.Packed,
) (int, error) {
	call := BusSenderCall{Addr: addr, Msg: msg}
	self.Calls = append(self.Calls, call)
	return 0, nil
}
