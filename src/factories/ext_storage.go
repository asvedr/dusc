package factories

import (
	"dusc/src/proto"
	"sync"

	"github.com/google/uuid"
)

type ExtStorageFactory struct {
	mtx       sync.Mutex
	MakeCalls []string
	MakeResp  []any
}

type ExtStorage struct {
	Publushed      []string
	FetchUsersData map[uuid.UUID][]string
	Err            error
}

//lint:ignore U1000 reason: compile time verification
func (self *ExtStorageFactory) _verify_interface() {
	var _ proto.IFactory[proto.IExtStorage] = self
}

//lint:ignore U1000 reason: compile time verification
func (self *ExtStorage) _verify_interface() {
	var _ proto.IExtStorage = self
}

func (self *ExtStorageFactory) Make(addr string) (proto.IExtStorage, error) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	self.MakeCalls = append(self.MakeCalls, addr)
	if len(self.MakeResp) == 0 {
		panic("unexpected call with addr: " + addr)
	}
	resp := self.MakeResp[0]
	self.MakeResp = self.MakeResp[1:]
	stor, casted := resp.(*ExtStorage)
	if casted {
		return stor, nil
	}
	err, casted := resp.(error)
	if casted {
		return nil, err
	}
	panic("invalid ext storage response")
}

func (self *ExtStorage) PublushMe(id uuid.UUID, addr string) error {
	val := id.String() + "|" + addr
	self.Publushed = append(self.Publushed, val)
	return nil
}

func (self *ExtStorage) FetchUsers(ids []uuid.UUID) (map[uuid.UUID][]string, error) {
	res := map[uuid.UUID][]string{}
	for _, id := range ids {
		res[id] = self.FetchUsersData[id]
	}
	return res, self.Err
}
