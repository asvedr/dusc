package factories

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/proto"
)

type Crypto struct {
	EncryptCalls    []EncryptCall
	EncryptResp     [][]byte
	DecryptCalls    []DecryptCall
	DecryptResp     []busmsg.Signed
	GetKeyCodeCalls []*rsa.PublicKey
	GetKeyCodeResp  []string
	GenPrivKeyResp  []*rsa.PrivateKey
}

type EncryptCall struct {
	Msg busmsg.Signed
	Key *rsa.PublicKey
}

type DecryptCall struct {
	Bts []byte
	Key *rsa.PrivateKey
}

//lint:ignore U1000 reason: compile time verification
func (self *Crypto) _validate_interface() proto.ICrypto {
	return self
}

func (self *Crypto) Encrypt(
	msg busmsg.Signed,
	key *rsa.PublicKey,
) ([]byte, error) {
	call := EncryptCall{Msg: msg, Key: key}
	self.EncryptCalls = append(self.EncryptCalls, call)
	resp := self.EncryptResp[0]
	self.EncryptResp = self.EncryptResp[1:]
	return resp, nil
}

func (self *Crypto) Decrypt(
	bts []byte,
	key *rsa.PrivateKey,
) (busmsg.Signed, error) {
	call := DecryptCall{Bts: bts, Key: key}
	self.DecryptCalls = append(self.DecryptCalls, call)
	resp := self.DecryptResp[0]
	self.DecryptResp = self.DecryptResp[1:]
	return resp, nil
}

func (self *Crypto) GetKeyCode(key *rsa.PublicKey) string {
	self.GetKeyCodeCalls = append(self.GetKeyCodeCalls, key)
	code := self.GetKeyCodeResp[0]
	self.GetKeyCodeResp = self.GetKeyCodeResp[1:]
	return code
}

func (self *Crypto) GenPrivKey() (*rsa.PrivateKey, error) {
	key := self.GenPrivKeyResp[0]
	self.GenPrivKeyResp = self.GenPrivKeyResp[1:]
	return key, nil
}
