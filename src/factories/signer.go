package factories

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
)

type Signer struct {
	SignCalls     []SignCall
	SignResp      []busmsg.Signed
	ValidateCalls []ValidateCall
	ValidateResp  []bool
}

type SignCall struct {
	Msg busmsg.DynMsg
	Key *rsa.PrivateKey
}

type ValidateCall struct {
	Msg busmsg.Signed
	Key *rsa.PublicKey
}

func (self *Signer) Sign(
	msg busmsg.DynMsg,
	key *rsa.PrivateKey,
) (busmsg.Signed, error) {
	call := SignCall{Msg: msg, Key: key}
	self.SignCalls = append(self.SignCalls, call)
	resp := self.SignResp[0]
	self.SignResp = self.SignResp[1:]
	return resp, nil
}

func (self *Signer) Validate(
	msg busmsg.Signed,
	key *rsa.PublicKey,
) bool {
	call := ValidateCall{Msg: msg, Key: key}
	self.ValidateCalls = append(self.ValidateCalls, call)
	resp := self.ValidateResp[0]
	self.ValidateResp = self.ValidateResp[1:]
	return resp
}
