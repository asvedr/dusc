package serializers

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/entities/local"
	"dusc/src/impls/serializers/contact_serializer"
	"dusc/src/impls/serializers/key_serializer"
	"dusc/src/impls/serializers/msg_serializer"
	"dusc/src/impls/serializers/serde"
	"dusc/src/proto"

	"gitlab.com/asvedr/cldi/di"
)

var Serde = di.Factory[proto.ISerdeMaker]{PureFunc: serde.New}

var PubKeySerializer = di.Factory[proto.ISerializer[*rsa.PublicKey]]{
	PureFunc: key_serializer.NewPub,
}
var PubKeyStrSerializer = di.Factory[proto.IStrSerializer[*rsa.PublicKey]]{
	PureFunc: key_serializer.NewPubStr,
}
var PrivKeySerializer = di.Factory[proto.ISerializer[*rsa.PrivateKey]]{
	PureFunc: key_serializer.NewPriv,
}
var PrivKeyStrSerializer = di.Factory[proto.IStrSerializer[*rsa.PrivateKey]]{
	PureFunc: key_serializer.NewPrivStr,
}
var MsgSignedSerializer = di.Factory[proto.ISerializer[busmsg.Signed]]{
	PureFunc: func() proto.ISerializer[busmsg.Signed] {
		return msg_serializer.NewSigned(PubKeySerializer.Get())
	},
}
var MsgPackedSerializer = di.Factory[proto.ISerializer[busmsg.Packed]]{PureFunc: msg_serializer.NewPacked}

var ContactStrSerializer = di.Singleton[proto.IStrSerializer[local.ClientContact]]{
	PureFunc: func() proto.IStrSerializer[local.ClientContact] {
		return contact_serializer.New(
			Serde.Get(),
			PubKeySerializer.Get(),
		)
	},
}
