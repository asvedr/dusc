package tasks

import (
	"dusc/src/app/common"
	"dusc/src/app/components"
	"dusc/src/app/db"
	"dusc/src/impls/common/msg_maker"
	"dusc/src/impls/files/file_downloader"
	"dusc/src/impls/task_executor"
	"dusc/src/impls/tasks/msg_pusher"
	"dusc/src/proto"

	"gitlab.com/asvedr/cldi/di"
)

var FileDownloader = di.Factory[proto.ITask]{
	PureFunc: func() proto.ITask {
		dw := components.FileDownloader.Get()
		return dw.(*file_downloader.Downloader)
	},
}

var MsgPusher = di.Singleton[proto.ITask]{
	PureFunc: func() proto.ITask {
		return msg_pusher.New(
			db.DbLocker.Get(),
			db.MsgRepo.Get(),
			db.UserRepo.Get(),
			components.MsgSendWf.Get(),
		)
	},
}

var AddrChecker = di.Factory[proto.ITask]{
	PureFunc: func() proto.ITask {
		return components.AddrManager.Get()
	},
}

var TaskExecutor = di.Singleton[proto.ITaskExecutor]{
	PureFunc: func() proto.ITaskExecutor {
		return task_executor.New(
			common.Time.Get(),
			FileDownloader.Get(),
			MsgPusher.Get(),
			AddrChecker.Get(),
		)
	},
}

var MsgMaker = di.Singleton[proto.IMsgMaker]{
	PureFunc: func() proto.IMsgMaker {
		return msg_maker.New(
			db.MsgRepo.Get(),
			common.Time.Get(),
			TaskExecutor.Get(),
		)
	},
}
