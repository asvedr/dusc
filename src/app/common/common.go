package common

import (
	"dusc/src/entities/config"
	"dusc/src/impls/common/time"
	"dusc/src/impls/files/fs"
	"dusc/src/proto"

	"gitlab.com/asvedr/cldi/cl"
	"gitlab.com/asvedr/cldi/di"
)

var Config = di.Singleton[*config.Config]{
	ErrFunc: cl.BuildConfig[config.Config],
}

var Time = di.Singleton[proto.ITime]{
	PureFunc: time.New,
}

var FS = di.Singleton[proto.IFS]{PureFunc: fs.New}
