package components

import (
	"crypto/rand"
	"dusc/src/app/common"
	"dusc/src/app/db"
	"dusc/src/app/serializers"
	"dusc/src/impls/common/crypto"
	"dusc/src/impls/common/hasher"
	"dusc/src/impls/common/initializer"
	"dusc/src/impls/common/msg_dispatcher"
	"dusc/src/impls/common/server_status_storage"
	"dusc/src/impls/common/signer"
	"dusc/src/impls/common/token_manager"
	"dusc/src/impls/ext_storages"
	"dusc/src/impls/files/file_downloader"
	"dusc/src/impls/files/file_uploader"
	"dusc/src/impls/net/datagram"
	"dusc/src/impls/net/stun"
	"dusc/src/impls/net/udp"
	"dusc/src/impls/services/msg_send_wf"
	"dusc/src/impls/tasks/addr_checker"
	"dusc/src/proto"

	"gitlab.com/asvedr/cldi/di"
)

var DatagramSeparator = di.Singleton[proto.IDatagramSeparator]{
	PureFunc: func() proto.IDatagramSeparator {
		return datagram.NewSeparator(common.Config.Get())
	},
}

var DatagramCompiler = di.Singleton[proto.IDatagramCompiler]{
	PureFunc: func() proto.IDatagramCompiler {
		return datagram.NewCompiler(
			common.Config.Get(),
			common.Time.Get(),
		)
	},
}

var BusSender = di.Singleton[proto.IBusSender]{
	PureFunc: func() proto.IBusSender {
		return udp.NewSender(
			common.Config.Get(),
			serializers.Serde.Get(),
			serializers.MsgPackedSerializer.Get(),
			DatagramSeparator.Get(),
		)
	},
}

var BusReceiver = di.Singleton[proto.IBusReceiver]{
	PureFunc: func() proto.IBusReceiver {
		return udp.NewReceiver(
			common.Config.Get(),
			serializers.Serde.Get(),
			serializers.MsgPackedSerializer.Get(),
			DatagramCompiler.Get(),
		)
	},
}

var StunFactory = di.Singleton[proto.IFactory[proto.ISTUNClient]]{
	PureFunc: stun.New,
}

var Hasher = di.Singleton[proto.IHasher]{
	PureFunc: hasher.New,
}

var Signer = di.Singleton[proto.IMsgSigner]{
	PureFunc: func() proto.IMsgSigner {
		return signer.New(
			rand.Reader,
			serializers.Serde.Get(),
			serializers.MsgSignedSerializer.Get(),
		)
	},
}

var Crypto = di.Singleton[proto.ICrypto]{
	PureFunc: func() proto.ICrypto {
		return crypto.New(
			rand.Reader,
			serializers.Serde.Get(),
			serializers.MsgSignedSerializer.Get(),
			Hasher.Get(),
		)
	},
}

var FileUploader = di.Singleton[proto.IFileUploader]{
	PureFunc: func() proto.IFileUploader {
		return file_uploader.New(
			common.FS.Get(),
			db.SharedFileRepo.Get(),
		)
	},
}

var MsgSendWf = di.Singleton[proto.IMsgSendWorkflow]{
	PureFunc: func() proto.IMsgSendWorkflow {
		return msg_send_wf.New(
			db.DbLocker.Get(),
			common.Time.Get(),
			Signer.Get(),
			Crypto.Get(),
			Hasher.Get(),
			BusSender.Get(),
			db.MeGetter.Get(),
			db.UserRepo.Get(),
		)
	},
}

var FileDownloader = di.Singleton[proto.IFileDownloader]{
	PureFunc: func() proto.IFileDownloader {
		return file_downloader.New(
			common.Time.Get(),
			MsgSendWf.Get(),
			common.FS.Get(),
		)
	},
}

var MsgDispatcher = di.Singleton[proto.IMsgDispatcher]{
	PureFunc: func() proto.IMsgDispatcher {
		return msg_dispatcher.New(
			db.MsgRepo.Get(),
			db.UserRepo.Get(),
			db.ChatRepo.Get(),
			common.Time.Get(),
			FileDownloader.Get(),
			FileUploader.Get(),
			MsgSendWf.Get(),
		)
	},
}

var ExtStorageFactory = di.Singleton[proto.IFactory[proto.IExtStorage]]{
	PureFunc: func() proto.IFactory[proto.IExtStorage] {
		return ext_storages.Factory(common.Config.Get())
	},
}

var TokenManager = di.Singleton[proto.IAuthTokenManager]{
	PureFunc: token_manager.New,
}

var Initializer = di.Singleton[proto.IInitializer]{
	PureFunc: func() proto.IInitializer {
		return initializer.New(
			db.SettingsRepo.Get(),
			Crypto.Get(),
			TokenManager.Get(),
			Hasher.Get(),
		)
	},
}

var ServerStatusStorage = di.Singleton[proto.IServerStatusStorage]{
	PureFunc: func() proto.IServerStatusStorage {
		return server_status_storage.New(
			common.Time.Get(),
			uint64(common.Config.Get().Server_Status_Lifetime),
		)
	},
}

var AddrManager = di.Singleton[proto.IAddrManager]{
	PureFunc: func() proto.IAddrManager {
		return addr_checker.New(
			db.DbLocker.Get(),
			MsgSendWf.Get(),
			StunFactory.Get(),
			ExtStorageFactory.Get(),
			db.UserRepo.Get(),
			db.ServerRepo.Get(),
			db.MeGetter.Get(),
			common.Config.Get(),
		)
	},
}
