package app

import (
	"dusc/src/app/api_handlers"
	"dusc/src/app/common"
	"dusc/src/app/components"
	"dusc/src/app/db"
	"dusc/src/app/tasks"
	"dusc/src/impls/services/api"
	"dusc/src/impls/services/msg_send_wf"
	"dusc/src/impls/services/receiver"
	"dusc/src/proto"

	"gitlab.com/asvedr/cldi/di"
)

var MsgSendWf = di.Factory[proto.IService]{
	PureFunc: func() proto.IService {
		wf := components.MsgSendWf.Get()
		return wf.(*msg_send_wf.WF)
	},
}

var Receiver = di.Singleton[proto.IService]{
	PureFunc: func() proto.IService {
		return receiver.New(
			db.DbLocker.Get(),
			components.BusReceiver.Get(),
			components.Hasher.Get(),
			components.Signer.Get(),
			components.Crypto.Get(),
			components.MsgDispatcher.Get(),
			db.MeGetter.Get(),
			db.UserRepo.Get(),
		)
	},
}

var Api = di.Singleton[proto.IService]{
	PureFunc: func() proto.IService {
		return api.New(
			common.Config.Get().Api_Port,
			api_handlers.AllHandlers(),
		)
	},
}

func AllServices() []proto.IService {
	return []proto.IService{
		MsgSendWf.Get(),
		Receiver.Get(),
		Api.Get(),
		tasks.TaskExecutor.Get(),
	}
}
