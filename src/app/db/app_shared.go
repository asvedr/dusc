package db

import (
	"dusc/src/app/common"
	"dusc/src/impls/db/me_getter"
	"dusc/src/proto"

	"gitlab.com/asvedr/cldi/di"
)

var MeGetter = di.Singleton[proto.IMeGetter]{
	PureFunc: func() proto.IMeGetter {
		return me_getter.New(
			SettingsRepo.Get(),
			common.Time.Get(),
			common.Config.Get(),
		)
	},
}
