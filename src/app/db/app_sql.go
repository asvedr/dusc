//go:build !json_db
// +build !json_db

package db

import (
	"dusc/src/app/common"
	"dusc/src/app/serializers"
	"dusc/src/impls/db/sqlite/chats"
	"dusc/src/impls/db/sqlite/db_locker"
	"dusc/src/impls/db/sqlite/msg"
	"dusc/src/impls/db/sqlite/server"
	"dusc/src/impls/db/sqlite/settings"
	"dusc/src/impls/db/sqlite/shared_files"
	"dusc/src/impls/db/sqlite/user"
	"dusc/src/proto"

	"gitlab.com/asvedr/cldi/di"
)

var DbLocker = di.Singleton[*db_locker.Locker]{
	ErrFunc: func() (*db_locker.Locker, error) {
		path := common.Config.Get().Storage
		return db_locker.New(path, true)
	},
}

var SettingsRepo = di.Singleton[proto.ISettingsRepo]{
	ErrFunc: func() (proto.ISettingsRepo, error) {
		return settings.New(
			DbLocker.Get(),
			serializers.PrivKeyStrSerializer.Get(),
		)
	},
}

var UserRepo = di.Singleton[proto.IUserRepo]{
	ErrFunc: func() (proto.IUserRepo, error) {
		return user.New(
			DbLocker.Get(),
			serializers.PubKeyStrSerializer.Get(),
		)
	},
}

var MsgRepo = di.Singleton[proto.IMsgRepo]{
	ErrFunc: func() (proto.IMsgRepo, error) {
		return msg.New(DbLocker.Get())
	},
}

var ChatRepo = di.Singleton[proto.IChatRepo]{
	ErrFunc: func() (proto.IChatRepo, error) {
		return chats.New(
			DbLocker.Get(),
			UserRepo.Get(),
			MsgRepo.Get(),
		)
	},
}

var SharedFileRepo = di.Singleton[proto.ISharedFileRepo]{
	ErrFunc: func() (proto.ISharedFileRepo, error) {
		return shared_files.New(DbLocker.Get())
	},
}

var ServerRepo = di.Singleton[proto.IServerRepo]{
	ErrFunc: func() (proto.IServerRepo, error) {
		return server.New(DbLocker.Get())
	},
}

func InitDb() {
	DbLocker.Get().Lock(func() {
		SettingsRepo.Get()
		UserRepo.Get()
		MsgRepo.Get()
		ChatRepo.Get()
		SharedFileRepo.Get()
		ServerRepo.Get()
	})
}
