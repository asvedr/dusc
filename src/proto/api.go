package proto

import (
	"dusc/src/entities/api"
)

type IApiHandler interface {
	Schema() api.HandlerSchema
	Handle(query any, body any) (any, error)
}
