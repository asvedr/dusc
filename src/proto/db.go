package proto

import (
	"crypto/rsa"
	"dusc/src/entities/local"

	"github.com/google/uuid"
)

type IDbLocker interface {
	Lock(func())
}

type IMsgRepo interface {
	Create(local.NewMsg) error
	GetLastUnreadMessages(limit int) ([]*local.Msg, error)
	GetChatMsgsByIntId(
		chat_id local.ChatId,
		last_id *int,
		count int,
		filter_visible bool,
	) ([]*local.Msg, error)
	GetChatMsgs(
		chat_id local.ChatId,
		last_id *uuid.UUID,
		count int,
		filter_visible bool,
	) ([]*local.Msg, error)
	GetByUid(id uuid.UUID) (*local.Msg, error)
	GetMessagesToSend() ([]*local.Msg, error)
	MarkRead(msg_id ...uuid.UUID) error
	MarkReceived(msg_id uuid.UUID) error
	Delete(msg_id ...uuid.UUID) error
}

type IUserRepo interface {
	Contact(name string, id uuid.UUID, key *rsa.PublicKey) error
	GetByUid(id ...uuid.UUID) ([]local.User, error)
	GetPubKey(uuid.UUID) (*rsa.PublicKey, error)
	SetLastSeen(id uuid.UUID, time uint64, addr string) error
	GetLastSeen(id uuid.UUID) (uint64, error)
	GetAddrs(uuid.UUID) ([]string, error)
	GetAddrMap() (map[uuid.UUID][]string, error)
	GetPubKeyAndAddrs(uuid.UUID) (*rsa.PublicKey, []string, error)
	GetUids() ([]uuid.UUID, error)
	DeleteExpiredAddrs(threshold_ts uint64) error
	SetStatus(id uuid.UUID, status local.UserStatus) error
	Delete(id uuid.UUID) error
}

type IMeGetter interface {
	GetMe() (*local.Me, error)
}

type ISettingsRepo interface {
	IMeGetter
	SetMe(local.Me) error
	SetName(string) error
	SetDescription(string) error
	// fn set_photo(&self, val: &str) -> DbResult<()>;
	GetMeta() (local.ApiUserMeta, error)
	CheckInited() (bool, error)
	// opt[string]
	SetPwdHash(string) error
	// opt[string]
	GetPwdHash() (string, error)
}

type ISharedFileRepo interface {
	RegisterFile(meta local.FileMeta, users []uuid.UUID) error
	GetRegisteredFiles() ([]*local.FileMetaUserAccess, error)
	GetFileForUser(file_id uuid.UUID, user_id uuid.UUID) (local.FileMeta, error)
	DeleteFileById(file_id uuid.UUID) error
}

type IServerRepo interface {
	Add(servers ...local.Server) error
	GetByType(tp local.ServerType) ([]local.Server, error)
	GetByAddr(addr string) (local.Server, error)
	Delete(addrs ...string) error
}

type IChatRepo interface {
	Create(local.Chat) error
	GetById(local.ChatId) (local.Chat, error)
	SetName(local.ChatId, string) error
	AddParticipants(local.ChatId, []uuid.UUID) error
	GetParticipants(local.ChatId) ([]uuid.UUID, error)
	GetShortInfoForAll() ([]local.ChatShort, error)
	GetFullList() ([]local.ChatListItem, error)
	BanChat(local.ChatId) error
	Delete(local.ChatId) error
}
