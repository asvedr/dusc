package proto

import "github.com/google/uuid"

type ISerdeMaker interface {
	NewWriter() IWSerde
	NewReader([]byte) IRSerde
}

type ISerializer[Obj any] interface {
	Write(out IWSerde, obj Obj)
	Read(inp IRSerde) (Obj, error)
}

type IStrSerializer[Obj any] interface {
	Write(obj Obj) string
	Read(inp string) (Obj, error)
}

type IWSerde interface {
	U8(uint8)
	U16(uint16)
	U32(uint32)
	U64(uint64)
	BytesLenU8([]byte)
	BytesLenU16([]byte)
	BytesLenU32([]byte)
	UUID(uuid.UUID)
	Finish() []byte
}

type IRSerde interface {
	U8() (uint8, error)
	U16() (uint16, error)
	U32() (uint32, error)
	U64() (uint64, error)
	BytesLenU8() ([]byte, error)
	BytesLenU16() ([]byte, error)
	BytesLenU32() ([]byte, error)
	UUID() (uuid.UUID, error)
}
