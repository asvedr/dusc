package proto

import (
	"dusc/src/entities/local"

	"github.com/google/uuid"
)

type IFS interface {
	ExecDir() (string, error)
	CreateDir(path string) error
	ReadFile(path string) ([]byte, error)
	WriteFile(path string, data []byte) error
	AppendFile(path string, data []byte) error
	GetBlock(path string, block_id int) ([]byte, error)
	GetMeta(path string) (local.FSFileMeta, error)
	GetParentDir(path string) (string, error)
	GetAbsPath(path string) (string, error)
	GetDirContent(path string) ([]local.DirItem, error)
}

type IFileDownloader interface {
	StartLoading(
		user_id uuid.UUID,
		file_id uuid.UUID,
		max_parts int,
		path_to_save string,
	) error
	// RetryBlockRequest(user_id uuid.UUID, file_id uuid.UUID) error
	GetLoadingStatus(user_id uuid.UUID, file_id uuid.UUID) (int, error)
	AddBlock(
		user_id uuid.UUID,
		file_id uuid.UUID,
		part int,
		data []byte,
	) error
}

type IFileUploader interface {
	GetFilePart(
		file_id uuid.UUID,
		sender uuid.UUID,
		part int,
	) ([]byte, error)
}
