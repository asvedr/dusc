package proto

import (
	"dusc/src/entities/busmsg"

	"github.com/google/uuid"
)

type IDatagramSeparator interface {
	Split(msg []byte) [][]byte
}

type IDatagramCompiler interface {
	Add(msg []byte) ([]byte, error)
}

type IBusSender interface {
	Send(addr string, msg busmsg.Packed) (int, error)
}

type IBusReceiver interface {
	SetHandler(func(busmsg.Packed) error)
	Listen() error
}

type ISTUNClient interface {
	GetMe() (string, error)
}

type IExtStorage interface {
	PublushMe(uuid.UUID, string) error
	FetchUsers([]uuid.UUID) (map[uuid.UUID][]string, error)
}

type IFactory[Obj any] interface {
	Make(addr string) (Obj, error)
}

type IAddrManager interface {
	ITask
	GetMyAddr() string
	PingUser(uid uuid.UUID, addr string) error
}
