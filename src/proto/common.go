package proto

import (
	"crypto/rsa"
	"dusc/src/entities/busmsg"
	"dusc/src/entities/local"

	"github.com/google/uuid"
)

type IHasher interface {
	GetSum([]byte) []byte
	GetStrSum([]byte) string
}

type IMsgSigner interface {
	Sign(busmsg.DynMsg, *rsa.PrivateKey) (busmsg.Signed, error)
	Validate(busmsg.Signed, *rsa.PublicKey) bool
}

type ICrypto interface {
	Encrypt(busmsg.Signed, *rsa.PublicKey) ([]byte, error)
	Decrypt([]byte, *rsa.PrivateKey) (busmsg.Signed, error)
	GetKeyCode(*rsa.PublicKey) string
	GenPrivKey() (*rsa.PrivateKey, error)
}

type IMsgSendWorkflow interface {
	AddToQueueMinial(receiver uuid.UUID, msg busmsg.DynMsg) error
	AddToQueue(
		receiver uuid.UUID,
		addrs []string,
		msg busmsg.DynMsg,
		enc_key *rsa.PublicKey,
	)
}

type IMsgMaker interface {
	MakeText(
		chat_id local.ChatId,
		text string,
		receivers ...uuid.UUID,
	) error
	MakeFileMeta(
		chat_id local.ChatId,
		file_id uuid.UUID,
		file_name string,
		local_path string,
		Parts int16,
		Size uint64,
		receivers ...uuid.UUID,
	) error
	MakeInvite(
		chat_id uuid.UUID,
		chat_name string,
		new_users []uuid.UUID,
		receivers ...uuid.UUID,
	) error
}

type IMsgDispatcher interface {
	Dispatch(busmsg.DynMsg) error
}

type IService interface {
	Name() string
	Run() error
}

type ITask interface {
	Schema() local.TaskSchema
	Execute() error
}

type ITaskExecutor interface {
	IService
	ExecuteNow(task local.TaskName) error
}

type ITime interface {
	Now() uint64
}

type IAuthTokenManager interface {
	SetActive(value bool)
	IsAuthTurnedOn() bool
	GenToken() (string, error)
	ValidateToken(token string) bool
}

type IInitializer interface {
	CheckInit() error
	Init(
		uuid *uuid.UUID,
		priv_key *rsa.PrivateKey,
		name string,
		description string,
		password *string,
	) error
}

type IServerStatusStorage interface {
	SetStatuses(updates map[string]local.ServerStatus)
	GetStatuses() map[string]local.ServerStatus
}
