package config

type Config struct {
	Api_Port               int    `default:"8080"`
	Udp_Port               int    `default:"8012"`
	Udp_Max_Msg_Lifetime   int    `default:"10"`
	Udp_Datagram_Len       int    `default:"1500"`
	Me_Lifetime            int    `default:"3"`
	Server_Status_Lifetime int    `default:"10"`
	Storage                string `default:"storage.db"`
	Redis_Lifetime_Sec     int    `default:"900"`
	Upload_Dir             string `default:"./upload"`
	Force_My_Addr          string `default:""`
	Log_Udp                bool   `default:"0"`
	Log_Prefix             string `default:""`
}
