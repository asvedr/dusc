package api

import "gitlab.com/asvedr/cldi/cl"

type HandlerSchema struct {
	Method            string
	Path              string
	QueryParser       cl.IUrlQueryParser
	BodyParserFactory any // link to struct or none
	AuthTokenRequired bool
}

func (self HandlerSchema) Key() string {
	return self.Path + ":" + self.Method
}
