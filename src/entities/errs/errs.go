package errs

import "fmt"

type NotFound struct{}

type LoadingAlreadyStarted struct{}

type Inconsistency struct {
	Msg string
}

type InvalidServer struct {
	Key  string
	Addr string
}

type UidAlreadyUsed struct{}

type NotInitialized struct{}

type MyAddrIsUnknown struct{}

func (MyAddrIsUnknown) Error() string {
	return "my addr is unknown"
}

func (NotFound) Error() string {
	return "not found"
}

func (LoadingAlreadyStarted) Error() string {
	return "loading already started"
}

func Inconsist(tmpl string, params ...any) error {
	return Inconsistency{Msg: fmt.Sprintf(tmpl, params...)}
}

func (self Inconsistency) Error() string {
	return "Inconsistency detected: " + self.Msg
}

func (self InvalidServer) Error() string {
	return fmt.Sprintf(
		"Invalid Server(%s): %s",
		self.Key,
		self.Addr,
	)
}

func (UidAlreadyUsed) Error() string {
	return "uuid is already used"
}

func (NotInitialized) Error() string {
	return "not initialized"
}
