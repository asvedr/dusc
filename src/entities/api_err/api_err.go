package api_err

import (
	"encoding/json"
)

type ApiError struct {
	Status  int
	Code    string
	Details string
}

func (self ApiError) Error() string {
	js := map[string]any{
		"code":    self.Code,
		"details": self.Details,
	}
	bts, _ := json.Marshal(js)
	return string(bts)
}

var UrlNotFound error = ApiError{Status: 404, Code: "URL_NOT_FOUND"}
var MethodNotAllowed error = ApiError{Status: 405, Code: "METHOD_NOT_ALLOWED"}
var InvalidPassword error = ApiError{Status: 403, Code: "INVALID_PASSWORD"}
var AlreadyExists error = ApiError{Status: 409, Code: "ALREADY_EXISTS"}
var MyAddrIsUnknown error = ApiError{Status: 409, Code: "MY_ADDR_IS_UNKNOWN"}

func Internal(detail string) ApiError {
	return ApiError{
		Status:  500,
		Code:    "INTERNAL_ERROR",
		Details: detail,
	}
}

func NotFound(detail string) ApiError {
	return ApiError{
		Status:  404,
		Code:    "NOT_FOUND",
		Details: detail,
	}
}

func InvalidQuery(err error) ApiError {
	return ApiError{
		Status:  400,
		Code:    "INVALID_QUERY",
		Details: err.Error(),
	}
}

func InvalidBody(err error) ApiError {
	return ApiError{
		Status:  400,
		Code:    "INVALID_BODY",
		Details: err.Error(),
	}
}

func NotInitialized() ApiError {
	return ApiError{
		Status: 400,
		Code:   "NOT_INITIALIZED",
	}
}

func CanNotLoadFile(err error) ApiError {
	return ApiError{
		Status:  400,
		Code:    "CAN_NOT_LOAD_FILE",
		Details: err.Error(),
	}
}

func CanNotSaveFile(err error) ApiError {
	return ApiError{
		Status:  400,
		Code:    "CAN_NOT_SAVE_FILE",
		Details: err.Error(),
	}
}
