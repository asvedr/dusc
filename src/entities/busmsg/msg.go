package busmsg

import (
	"crypto/rsa"
	"encoding/base64"
	"fmt"
	"strings"

	"github.com/google/uuid"
)

type Base struct {
	CreatedAt uint64
	SenderId  uuid.UUID
}

func (self *Base) SetBase(other Base) {
	*self = other
}

func (self *Base) Sender() uuid.UUID {
	return self.SenderId
}

type Text struct {
	Base
	MsgId  uuid.UUID
	ChatId *uuid.UUID
	Text   string
}

func (self Text) String() string {
	var chat string
	if self.ChatId != nil {
		chat = self.ChatId.String()
	}
	return fmt.Sprintf(
		`Text(id: %s, chat: "%s", text: "%s")`,
		self.MsgId.String(),
		chat,
		self.Text,
	)
}

type FileMeta struct {
	Base
	MsgId    uuid.UUID
	FileId   uuid.UUID
	ChatId   *uuid.UUID
	FileName string
	FileSize uint64
	Parts    uint16
}

func (self FileMeta) String() string {
	var chat string
	if self.ChatId != nil {
		chat = self.ChatId.String()
	}
	return fmt.Sprintf(
		`FileMeta(id: %s, chat: %s, name: %s, size: %d, parts: %d)`,
		self.FileId.String(),
		chat,
		self.FileName,
		self.FileSize,
		self.Parts,
	)
}

type FileSegment struct {
	Base
	FileId uuid.UUID
	Part   uint16
	Data   []byte
}

func (self FileSegment) String() string {
	return fmt.Sprintf(
		"FileSegment(id: %s, part: %d, datalen: %d)",
		self.FileId.String(),
		self.Part,
		len(self.Data),
	)
}

type FileSegmentRequest struct {
	Base
	FileId uuid.UUID
	Part   uint16
}

func (self FileSegmentRequest) String() string {
	return fmt.Sprintf("FileSegmentRequest(id: %s, part: %d)", self.FileId.String(), self.Part)
}

type SignalConfirmReceived struct {
	Base
	MsgId uuid.UUID
}

func (self SignalConfirmReceived) String() string {
	return fmt.Sprintf("SignalConfirmReceived(id: %s)", self.MsgId.String())
}

type Ping struct {
	Base
	SentFromAddr string
	SendToAddr   string
	PublicKey    *rsa.PublicKey
}

func (self Ping) String() string {
	return fmt.Sprintf(
		"Ping(from: %s, to: %s)",
		self.SentFromAddr,
		self.SendToAddr,
	)
}

type Pong struct {
	Base
	SendToAddr string
}

func (self Pong) String() string {
	return fmt.Sprintf("Pong(addr: %s)", self.SendToAddr)
}

type Invite struct {
	Base
	GroupId uuid.UUID
	Name    string
	Users   map[uuid.UUID]*rsa.PublicKey
}

func (self Invite) String() string {
	users := []string{}
	for uid := range self.Users {
		users = append(users, uid.String())
	}
	return fmt.Sprintf(
		"Invite(chat: %v, users: %s)",
		self.GroupId.String(),
		strings.Join(users, ","),
	)
}

type DynMsg interface {
	String() string
	SetBase(Base)
	Sender() uuid.UUID
}

type Signed struct {
	Msg       DynMsg
	Signature []byte
}

func (self Signed) String() string {
	sig := base64.StdEncoding.EncodeToString(self.Signature)
	return fmt.Sprintf(
		`Signed(msg: %s, sig: "%s")`,
		self.Msg.String(),
		sig,
	)
}

type Packed struct {
	Encrypted []byte // encrypted signed msg
	HashSum   []byte
}

func (self Packed) String() string {
	msg := base64.StdEncoding.EncodeToString(self.Encrypted)
	sum := base64.StdEncoding.EncodeToString(self.HashSum)
	return fmt.Sprintf(`Packed(enc: "%s", sum: "%s")`, msg, sum)
}
