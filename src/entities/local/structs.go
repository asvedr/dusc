package local

import (
	"crypto/rsa"
	"errors"

	"github.com/google/uuid"
)

type Me struct {
	PrivKey *rsa.PrivateKey
	Uid     uuid.UUID
}

type ChatId struct {
	IsGroup bool
	Id      uuid.UUID
}

func (self ChatId) String() string {
	var prefix string
	if self.IsGroup {
		prefix = "G"
	} else {
		prefix = "U"
	}
	return prefix + self.Id.String()
}

var err_invalid_chat_id = errors.New("invalid chat id")

func StringToChatId(src string) (ChatId, error) {
	var res ChatId
	if len(src) == 0 {
		return res, err_invalid_chat_id
	}
	switch src[0] {
	case 'G':
		res.IsGroup = true
	case 'U':
		res.IsGroup = false
	default:
		return res, err_invalid_chat_id
	}
	id, err := uuid.Parse(src[1:])
	if err != nil {
		return res, err_invalid_chat_id
	}
	res.Id = id
	return res, nil
}

type Chat struct {
	Id    ChatId
	Name  string
	Users []uuid.UUID
}

type ChatShort struct {
	Id      ChatId
	Name    string
	LastMsg string
	Read    bool
}

type ChatListItem struct {
	Id     ChatId
	Name   string
	Banned bool
}

type NewMsg struct {
	Sender         *uuid.UUID
	Receiver       *uuid.UUID
	Chat           ChatId
	Visible        bool
	Uid            uuid.UUID
	Text           string
	Received       bool
	Read           bool
	CreatedAt      uint64
	ReceivedAt     uint64
	AttachInvite   *AttachInvite
	AttachFileMeta *AttachFileMeta
}

type AttachInvite struct {
	Name  string
	Users []uuid.UUID
}

type AttachFileMeta struct {
	FileId    uuid.UUID
	LocalPath string
	Parts     int16
	Size      uint64
}

type Msg struct {
	NewMsg
	Id int
}

type ApiUserMeta struct {
	Name        string
	Description string
	Photo       string
	Hash        string
}

type User struct {
	Id     uuid.UUID
	PubKey *rsa.PublicKey
	// PubKeyCode string
	Info   *ApiUserMeta
	Status UserStatus
}

type FSFileMeta struct {
	Blocks int
	Size   int64
}

type FileMeta struct {
	FSFileMeta
	Id   uuid.UUID
	Path string
	Name string
}

type FileMetaUserAccess struct {
	FileMeta
	Users []uuid.UUID
}

type DirItem struct {
	Name  string
	Path  string
	IsDir bool
}

type Server struct {
	Addr string
	Type ServerType
}

type ClientContact struct {
	PubKey *rsa.PublicKey
	Uid    uuid.UUID
}

type TaskSchema struct {
	Timeout uint64
	Name    TaskName
}
