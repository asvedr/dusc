package local

import "errors"

type UserStatus int

const UserStatusNew UserStatus = 1
const UserStatusTrusted UserStatus = 2
const UserStatusBanned UserStatus = 3

var err_invalid_user_status = errors.New("invalid user status")

var string_to_user_status = map[string]UserStatus{
	"new":     UserStatusNew,
	"trusted": UserStatusTrusted,
	"banned":  UserStatusBanned,
}

func (self UserStatus) String() string {
	for str, stat := range string_to_user_status {
		if self == stat {
			return str
		}
	}
	panic("unknown user status")
}

func StringToUserStatus(src string) (UserStatus, error) {
	res, found := string_to_user_status[src]
	if !found {
		return 0, err_invalid_user_status
	}
	return res, nil
}
