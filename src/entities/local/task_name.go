package local

type TaskName int

const (
	TaskNameSendAllMsgs TaskName = iota
	TaskNameDownloadFile
	TaskNameCheckAddr
)

var task_name_to_string = map[TaskName]string{
	TaskNameSendAllMsgs:  "send_all_msgs",
	TaskNameDownloadFile: "download_file",
}

func (self TaskName) String() string {
	return task_name_to_string[self]
}
