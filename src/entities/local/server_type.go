package local

import "errors"

type ServerType int

const ServerTypeSTUN ServerType = 1
const ServerTypeExtStor ServerType = 2

var string_to_server_type = map[string]ServerType{
	"stun": ServerTypeSTUN,
	"stor": ServerTypeExtStor,
}

func (self ServerType) String() string {
	for str, tp := range string_to_server_type {
		if tp == self {
			return str
		}
	}
	panic("invalid server type")
}

var err_invalid_server_type = errors.New("invalid server type")

func StringToServerType(src string) (ServerType, error) {
	res, found := string_to_server_type[src]
	if !found {
		return 0, err_invalid_server_type
	}
	return res, nil
}
