package local

import "errors"

type ServerStatus int

const (
	ServerStatusUnknown = iota
	ServerStatusOk
	ServerStatusError
)

var string_to_server_status = map[string]ServerStatus{
	"unknown": ServerStatusUnknown,
	"ok":      ServerStatusOk,
	"error":   ServerStatusError,
}

func (self ServerStatus) String() string {
	for str, stat := range string_to_server_status {
		if self == stat {
			return str
		}
	}
	panic("unknown server status")
}

var err_invalid_server_status = errors.New("invalid server status")

func StringToServerStatus(src string) (ServerStatus, error) {
	res, found := string_to_server_status[src]
	if !found {
		return 0, err_invalid_server_status
	}
	return res, nil
}
