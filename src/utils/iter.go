package utils

func ConcatUniq[T comparable](lists ...[]T) []T {
	used := map[T]bool{}
	res := []T{}
	for _, list := range lists {
		for _, item := range list {
			used[item] = true
			res = append(res, item)
		}
	}
	return res
}

func Chunkify[T any](src []T, chunk_size int) [][]T {
	chunks := [][]T{}
	max_size := len(src)
	max_i := max_size / chunk_size
	for i := range max_i {
		begin := i * chunk_size
		end := min((i+1)*chunk_size, max_size)
		chunks = append(chunks, src[begin:end])
	}
	if max_i*chunk_size < max_size {
		chunks = append(chunks, src[max_i*chunk_size:])
	}
	return chunks
}
