package utils

import (
	"sort"

	"github.com/google/uuid"
)

func SortUids(uids []uuid.UUID) []uuid.UUID {
	sort.Slice(
		uids,
		func(i, j int) bool {
			return uids[i].String() < uids[j].String()
		},
	)
	return uids
}
