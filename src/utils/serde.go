package utils

import "dusc/src/proto"

func DumpBts[
	T any,
	M proto.ISerdeMaker,
	S proto.ISerializer[T],
](maker M, ser S, obj T) []byte {
	buf := maker.NewWriter()
	ser.Write(buf, obj)
	return buf.Finish()
}

func LoadBts[
	T,
	M proto.ISerdeMaker,
	S proto.ISerializer[T],
](maker M, ser S, bts []byte) (T, error) {
	buf := maker.NewReader(bts)
	return ser.Read(buf)
}
