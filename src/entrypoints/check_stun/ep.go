package check_stun

import (
	"dusc/src/app/components"

	"gitlab.com/asvedr/cldi/cl"
)

type EP struct {
}

type request struct {
	Addr string `short:"s" description:"server address"`
}

func (EP) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "check-stun",
		Description: "try stun server",
		ParamParser: cl.MakeParser[request](),
	}
}

func (EP) Execute(prog string, args any) error {
	req := args.(request)
	factory := components.StunFactory.Get()
	stun, err := factory.Make(req.Addr)
	if err != nil {
		return err
	}
	me, err := stun.GetMe()
	if err != nil {
		return err
	}
	println("me: " + me)
	return nil
}
