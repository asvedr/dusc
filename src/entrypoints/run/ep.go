package run

import (
	"dusc/src/app/common"
	app_db "dusc/src/app/db"
	app_srv "dusc/src/app/services"
	"dusc/src/proto"
	"log"

	"gitlab.com/asvedr/cldi/cl"
)

type EP struct{}

func (EP) Schema() cl.EPSchema {
	return cl.EPSchema{Command: "run", Description: "run service"}
}

type err_signal struct {
	service string
	err     error
}

func (EP) Execute(prog string, args any) error {
	conf := common.Config.Get()
	if conf.Log_Prefix != "" {
		log.SetPrefix(conf.Log_Prefix)
	}
	app_db.InitDb()
	out := make(chan err_signal, 1)
	for _, srv := range app_srv.AllServices() {
		go func(srv proto.IService) {
			err := srv.Run()
			out <- err_signal{service: srv.Name(), err: err}
		}(srv)
	}
	for sig := range out {
		log.Printf("service %s failed:\n  %v", sig.service, sig.err)
		return sig.err
	}
	return nil
}
