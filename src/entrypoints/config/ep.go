package config

import (
	"dusc/src/app/common"
	"dusc/src/app/db"
	"dusc/src/entities/config"

	"gitlab.com/asvedr/cldi/cl"
)

type EP struct{}

func (EP) Schema() cl.EPSchema {
	return cl.EPSchema{
		Command:     "config",
		Description: "show config",
	}
}

func (EP) Execute(prog string, args any) error {
	println(cl.GetConfigHelp[config.Config]())
	println("\n---\n")
	println(cl.DescribeConfig(*common.Config.Get()))
	println("\n---\n")
	println("starting db validation...")
	db.InitDb()
	println("db validation done")
	return nil
}
